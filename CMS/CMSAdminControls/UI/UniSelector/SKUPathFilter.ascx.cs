﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CMS.Base.Web.UI;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Web.UI;
using CMS.Helpers;


public partial class CMSAdminControls_UI_UniSelector_SKUPathFilter : CMSAbstractBaseFilterControl
{
    #region "Private variables"

    #endregion


    #region "Public properties"

    /// <summary>
    /// Where condition.
    /// </summary>
    public override string WhereCondition
    {
        get
        {
            base.WhereCondition = GetCondition();
            return base.WhereCondition;
        }
        set
        {
            base.WhereCondition = value;
        }
    }


    /// <summary>
    /// Gets/sets current text in filter.
    /// </summary>
    public string FilterText
    {
        get
        {
            return drpCondition.SelectedValue;
        }
        set
        {
            drpCondition.SelectedValue = value;
        }
    }


    /// <summary>
    /// Determines whether the filter is set.
    /// </summary>
    public override bool FilterIsSet
    {
        get
        {
            return !string.IsNullOrEmpty(drpCondition.SelectedValue);
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        InitFilterDropDown(drpCondition);
    }

    #endregion


    #region "Public methods"

    public string GetCondition()
    {
        if (FilterText.Length > 0)
        return $"SKUID IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE NodeAliasPath LIKE '{FilterText}%') OR SKUParentSKUID IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE NodeAliasPath LIKE '{FilterText}%')";
        else
        {
            return "1 = 1";
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes standard filter dropdown.
    /// </summary>
    /// <param name="drp">Dropdown to init</param>
    private void InitFilterDropDown(CMSDropDownList drp)
    {
        if ((drp != null) && (drp.Items.Count <= 0))
        {
            var nodeAliases = DocumentHelper.GetDocuments()
                .Column("NodeAliasPath")
                .WhereNotNull("NodeSKUID")
                .OnCurrentSite()
                .OrderBy("NodeAliasPath")
                .GetListResult<string>();

            var items = new List<string>();
            foreach (var nodeAlias in nodeAliases)
            {
                var builder = string.Empty;
                var nodeAliasComponents = nodeAlias.Split('/');
                int i = 0;
                foreach (var nodeAliasComponent in nodeAliasComponents)
                {
                    builder += $"/{nodeAliasComponent}";
                    if (!items.Contains(builder) && i < nodeAliasComponents.Length - 1)
                    {
                        items.Add(builder);
                    }
                    i++;
                }
            }

            foreach (var item in items)
            {
                if (item != "/")
                {
                    drp.Items.Add(new ListItem { Text = item.Substring(1), Value = item.Substring(1) + "/" });
                }
                else
                {
                    drp.Items.Add(new ListItem { Text = "(all)", Value = "" });
                }
            }
        }
    }

    #endregion


    #region "State management"

    /// <summary>
    /// Stores filter state to the specified object.
    /// </summary>
    /// <param name="state">The object that holds the filter state.</param>
    public override void StoreFilterState(FilterState state)
    {
        state.AddValue("Text", FilterText);
    }


    /// <summary>
    /// Restores filter state from the specified object.
    /// </summary>
    /// <param name="state">The object that holds the filter state.</param>
    public override void RestoreFilterState(FilterState state)
    {
        FilterText = state.GetString("Text");
    }


    /// <summary>
    /// Resets the filter settings.
    /// </summary>
    public override void ResetFilter()
    {
        FilterText = String.Empty;
    }

    #endregion
}