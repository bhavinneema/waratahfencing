<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="MultipleMediaList.ascx.cs" Inherits="CMSFormControls_WDM_MultipleMediaList" %>

<script id="<%=ClientID%>_template" type="text/x-handlebars-template">
    <table class="table">
	    <thead>
		    <tr>
			    <th>&nbsp;</th>
			    <th>Image</th>
                <th>Alt Text</th>
                <th>Link URL</th>
                <th>&nbsp;</th>
		    </tr>
	    </thead>
	    <tbody>
		    {{#each images}}
		    <tr>
			    <td style="white-space:nowrap; width:100px;">
                    <a href="javascript:;"><i data-elementid="{{../elementID}}" data-index="{{@index}}" class="text-{{#if @first}}muted{{else}}primary{{/if}} icon-chevron-up-circle cms-icon-150 wdm--multiple-media-list--button" data-action="moveup"></i></a>
                    <a href="javascript:;"><i data-elementid="{{../elementID}}" data-index="{{@index}}" class="text-{{#if @last}}muted{{else}}primary{{/if}} icon-chevron-down-circle cms-icon-150 wdm--multiple-media-list--button" data-action="movedown"></i></a>
			    </td>
			    <td><a href="javascript:;"><img style="max-height:100px;" data-elementid="{{../elementID}}" data-index="{{@index}}" class="wdm--multiple-media-list--image" src="{{#if src}}{{src}}{{else}}https://placeholdit.imgix.net/~text?txtsize=33&txt=Click%20to%20Choose%20Image&w=200&h=100{{/if}}" /></a></td>
                <td><input type="text" style="height:32px;" data-elementid="{{../elementID}}" data-index="{{@index}}" class="form-control wdm--multiple-media-list--textbox" data-field="alt" value="{{alt}}" /></td>
                <td><input type="text" style="height:32px;" data-elementid="{{../elementID}}" data-index="{{@index}}" class="form-control wdm--multiple-media-list--textbox" data-field="href" value="{{href}}" /></td>
                <td>
                    <span class="pull-right">
                        <a href="javascript:;"><i data-elementid="{{../elementID}}" data-index="{{@index}}" class="text-danger icon-times-circle cms-icon-150 wdm--multiple-media-list--button" data-action="delete"></i></a>
                    </span>
			    </td>
		    </tr>
		    {{/each}}
	    </tbody>
    </table>
    <input type="button" data-elementid="{{elementID}}" class="btn btn-primary wdm--multiple-media-list--button" data-action="add" value="Add an Image" />
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#<%=ClientID%>_pnlImages').multipleMediaList({
		    clientID:	'<%=ClientID%>',
		    templateID:	'template',
		    valueID:	'hdnValue'
	    });
    });
</script>

<asp:HiddenField id="hdnValue" runat="server" />
<div style="visibility:hidden;">
    <cms:MediaSelector ID="cmsMediaSelector" runat="server"/>
</div>
<asp:Panel id="pnlImages" runat="server">Loading...</asp:Panel>