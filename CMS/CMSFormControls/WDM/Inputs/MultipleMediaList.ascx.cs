﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base.Web.UI;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using WF.Promotions.DataClasses;

public partial class CMSFormControls_WDM_MultipleMediaList : FormEngineUserControl
{
    /// <summary>
    /// The data is a JSON object.
    /// An array of objects containing guid, url, and altText.
    /// </summary>
    public override object Value
    {
        get {
            return StripBlankRows(hdnValue.Value);
        }

        set {
            hdnValue.Value = ValidationHelper.GetString(value, string.Empty);
        }
    }


    /// <summary>
    /// The NCC.Event being edited.
    /// </summary>
    protected CMS.DocumentEngine.TreeNode EventNode
    {
        get { return Form.EditedObject as CMS.DocumentEngine.TreeNode; }
    }


    /// <summary>
    /// Set up the control.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Include javascript files.
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "jquery", "//code.jquery.com/jquery-1.12.4.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "jqueryui", "//code.jquery.com/ui/1.12.0/jquery-ui.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "handlebars", "/CMSScripts/Custom/js/libs/handlebars-v4.0.5.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "multiplemedialist", "/CMSFormControls/WDM/Inputs/MultipleMediaList_files/MultipleMediaList.js");
        
        ConfigureMediaSelector();
    }


    /// <summary>
    /// Removes any blank rows.
    /// </summary>
    private string StripBlankRows(string json)
    {
        // Try to Parse the JSON.
        List<GalleryImage> originalList;
        try
        {
            originalList = JsonConvert.DeserializeObject<List<GalleryImage>>(json);
        }
        catch (Exception ex)
        {
            originalList = new List<GalleryImage>();
        }

        // Only copy rows with an image specified.
        var updatedList = new List<GalleryImage>();
        foreach (var image in originalList)
            if (!string.IsNullOrEmpty(image.Src))
                updatedList.Add(image);

        // Re-stringify the updated list.
        return JsonConvert.SerializeObject(updatedList);
    }


    /// <summary>
    /// Configure the MediaSelector form control to allow selecting from libraries and attachments etc.
    /// </summary>
    private void ConfigureMediaSelector()
    {
        int documentID = 0;
        if (EventNode == null)
        {
            if (CurrentDocument != null)
                documentID = CurrentDocument.DocumentID;
        }
        else
        {
            documentID = EventNode.DocumentID;
        }

        var config = new DialogConfiguration();
        config.AttachmentDocumentID = documentID;
        config.HideAttachments = false;
        config.HideWeb = true;
        config.SelectableContent = SelectableContentEnum.OnlyImages;
        config.OutputFormat = OutputFormatEnum.URL;
        config.ContentSites = AvailableSitesEnum.OnlyCurrentSite;
        config.LibSites = AvailableSitesEnum.All;

        cmsMediaSelector.UseCustomDialogConfig = true;
        cmsMediaSelector.DialogConfig = config;
        cmsMediaSelector.ShowPreview = true;
        cmsMediaSelector.IsLiveSite = IsLiveSite;
    }    
}