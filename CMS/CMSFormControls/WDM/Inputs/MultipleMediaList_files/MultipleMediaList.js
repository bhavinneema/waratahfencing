﻿$.widget("wdm.multipleMediaList", {

    options: {
        templateID: '',
        clientID: '',
        valueID: ''
    },


    _data: {
        elementID: '',
        images: []
    },


    _create: function () {

        $(document).on('click', '.wdm--multiple-media-list--button', function (e) {
            var elementID = $(e.target).data('elementid');
            var index = $(e.target).data('index');
            var action = $(e.target).data('action');
            switch (action)
            {
                case 'add':
                    $('#' + elementID).multipleMediaList('add');
                    break;

                case 'moveup':
                    $('#' + elementID).multipleMediaList('move', index, -1);
                    break;

                case 'movedown':
                    $('#' + elementID).multipleMediaList('move', index, 1);
                    break;

                case 'delete':
                    $('#' + elementID).multipleMediaList('delete', index);
                    break;
            }
        });

        $(document).on('change', '.wdm--multiple-media-list--textbox', function (e) {
            var $textBox = $(e.target);
            var elementID = $textBox.data('elementid');
            var index = $textBox.data('index');
            var field = $textBox.data('field');
            var value = $textBox.val();
            $('#' + elementID).multipleMediaList('change', index, field, value);
        });

        $(document).on('click', '.wdm--multiple-media-list--image', function (e) {
            var elementID = $(e.target).data('elementid');
            var index = $(e.target).data('index');
            $('#' + elementID).multipleMediaList('chooseImagePopup', index);
        });
        
        // Override the default media selector.
        window['SetMediaValue_' + this.options.clientID + '_cmsMediaSelector'] = function (selectorId) {
            var newValue = document.getElementById(selectorId + '_txtPath').value;
            document.getElementById(selectorId + '_hidValue').value = newValue;
            ////////////////////////////////////////////////////////////////////////////////
            // We insert out own code here.
            var $hidValue   = $('#' + selectorId + '_hidValue');
            var clientID    = $hidValue.data('clientid');
            var elementID   = $hidValue.data('elementid');
            var index       = $hidValue.data('index');

            $('#' + elementID).multipleMediaList('changeImage', index, newValue);
            ////////////////////////////////////////////////////////////////////////////////
            WebForm_DoCallback(clientID.replace('_', '$') + '$cmsMediaSelector', newValue, ReceiveDisplayMediaValues, selectorId, null, false);
        }

        this._data.elementID = $(this.element).attr('id');
        this._parseValue();
        this.update();
    },


    ////////////////////////////////////////////////////////////////////////////////
    // Public methods.

    update: function () {
        var source = $('#' + this.options.clientID + '_' + this.options.templateID).html();
        var template = Handlebars.compile(source);
        $(this.element).html(template(this._data));
        this._stringifyValue();
    },


    add: function () {
        this._data.images.push({
            src: '',
            alt: '',
            href: ''
        });
        this.update();
    },


    change: function (index, field, value) {
        switch (field) {
            case 'src': this._data.images[index].src = value; break;
            case 'alt': this._data.images[index].alt = value; break;
            case 'href': this._data.images[index].href = value; break;
        }
        this._stringifyValue();
    },


    delete: function (index) {
        this._data.images.splice(index, 1);
        this.update();
    },


    move: function (index, direction) {
        if (((index + direction) < 0) || ((index + direction) >= this._data.images.length)) return;
        this._data.images.splice(index + direction, 0, this._data.images.splice(index, 1)[0]);
        this.update();
    },

    chooseImagePopup: function (index) {
        $('#' + this.options.clientID + '_cmsMediaSelector_hidValue').data('clientid', this.options.clientID);
        $('#' + this.options.clientID + '_cmsMediaSelector_hidValue').data('elementid', this._data.elementID);
        $('#' + this.options.clientID + '_cmsMediaSelector_hidValue').data('index', index);
        $('#' + this.options.clientID + '_cmsMediaSelector_btnSelect').click();
    },

    changeImage: function (index, url) {
        this._data.images[index].src = url;
        this.update();
    },


    ////////////////////////////////////////////////////////////////////////////////
    // Private methods

    _parseValue: function () {

        this._data.images = [];

        var value = $('#' + this.options.clientID + '_' + this.options.valueID).val();
        if (value.length > 0) {

            try {
                this._data.images = JSON.parse(value);
            } catch (e) {
                console.log('JSON parsing failed: ', e.message);
                console.log(value);
            }
        }
    },


    _stringifyValue: function () {
        var value = JSON.stringify(this._data.images);
        $('#' + this.options.clientID + '_' + this.options.valueID).val(value);
    }
});