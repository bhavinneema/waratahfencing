<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PurchaseInformation.ascx.cs" Inherits="CMSFormControls_WDM_PurchaseInformation" %>

<div class="purchaseinformation">

    <!-- Table Template -->
    <script id="<%=ClientID%>_templatetable" type="text/x-handlebars-template">
        <table class="table purchaseinformation__table">
            <thead>
                <tr>
                    <th>Product</th>
                    <th class="text-right">Unit price (ex {{value/taxName}})</th>
                    <th class="text-right">{{value/taxName}}</th>
                    <th class="text-right">Qty</th>
                    <% if (!Disabled) Response.Write("<th>&nbsp;</th>"); %>
                </tr>
            </thead>
            <tbody>
                {{#each value/groups}}
                    <tr>
                        <td colspan="5"><span class="cashback-form-group-heading">{{name}}</span></td>
                    </tr>
                {{#each products}}
                        <%--<tr>
                            <td colspan="5"><span class="cashback-form-product-heading">{{name}}</span></td>
                        </tr>--%>
                {{#each variants}}
		                    <tr>
                                <td>{{name}}</td>
                                <td class="text-right">
                                    <input type="text" style="height: 32px;" data-elementid="{{../../../elementID}}" data-skuid="{{skuid}}" data-type="float" class="form-control text-right js-purchase-information--textbox" data-field="unitPrice" value="{{currency unitPrice}}" <% if (Disabled) Response.Write("disabled"); %> /></td>
                                <td class="text-right">${{currency tax}}</td>
                                <td class="text-right">
                                    <input type="text" style="height: 32px;" data-elementid="{{../../../elementID}}" data-skuid="{{skuid}}" data-type="integer" class="form-control text-right js-purchase-information--textbox" data-field="qty" value="{{qty}}" <% if (Disabled) Response.Write("disabled"); %> /></td>
                                <% if (!Disabled)
                                { %>
                                <td class="text-right">
                                    <button data-elementid="{{../../../elementID}}" data-skuid="{{skuid}}" class="btn btn-primary js-purchase-information--button" data-action="delete">
                                        X
                                    </button>
                                </td>
                                <% } %>
                            </tr>
                {{/each}}
                    {{/each}}
		        {{/each}}
            </tbody>
        </table>

        <% if (!Disabled)
            { %>
        <button type="button" class="btn btn-primary" onclick="jQuery('#<%=ClientID%>_addproductpopup').show();">
            Add a product
        </button>
        <% } %>
    </script>

    <script id="<%=ClientID%>_templatecashback" type="text/x-handlebars-template">
        {{#if showCashback}}
            <table class="table purchaseinformation__cashback">
                <tbody>
                    {{#if calculating}}
                        <tr>
                            <th>Rewards:</th>
                            <td>calculating...</td>
                        </tr>
                    {{else}}
                        <!--
                        <tr>
                            <th>Total claimed amount:</th>
                            <td>${{currency value/totalClaimAmount}}</td>
		                </tr>
                        -->
                    {{#unless value/mustQualifyForAllGroups}}
		                    {{#each value/groups}}
                                {{#if (gt cashback 0)}}
                                    <tr>
                                        <th>{{name}} cashback:</th>
                                        <td>${{currency cashback}}</td>
                                    </tr>
                    {{/if}}
		                    {{/each}}
                        {{/unless}}
                        {{#if (gt value/extraDiscountAmount 0)}}
                            <tr>
                                <th>{{value/extraDiscountPercent}}% extra cashback:</th>
                                <td>${{currency value/extraDiscountAmount}}</td>
                            </tr>
                    {{/if}}
                        {{#if (gt value/totalCashback 0)}}
                            <tr>
                                <th>Total cashback:</th>
                                <td>${{currency value/totalCashback}}</td>
                            </tr>
                    {{/if}}
                        {{#if (gt value/totalItemRewards.length 0)}}
                            <tr>
                                <th>Item Rewards:</th>
                                <td>
                                    <ul class="list-unstyled">
                                        {{#each value/totalItemRewards}}
                                        <li>{{quantity}}x {{name}}</li>
                                        {{/each}}
                                    </ul>
                                </td>
                            </tr>
                    {{/if}}
                    {{/if}}
                </tbody>
            </table>
        {{/if}}
    </script>

    <!-- "Add a product" popup -->
    <div id="<%=ClientID%>_addproductpopup" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="modal-title">Add a product</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="jQuery('#<%=ClientID%>_addproductpopup').hide();">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="<%=ClientID%>_addproductdropdowngroup">Group</label>
                                <select id="<%=ClientID%>_addproductdropdowngroup" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="<%=ClientID%>_addproductdropdownproduct">Product</label>
                                <select id="<%=ClientID%>_addproductdropdownproduct" class="form-control"></select>
                            </div>
                            <div id="<%=ClientID%>_addproductdropdownvariantcontainer">
                                <div class="form-group">
                                    <label for="<%=ClientID%>_addproductdropdownvariant">Variant</label>
                                    <select id="<%=ClientID%>_addproductdropdownvariant" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="<%=ClientID%>_addproductbutton" class="btn btn-primary" onclick="jQuery('#<%=ClientID%>_addproductpopup').hide();">Add</button>
                            <button type="button" class="btn btn-secondary" onclick="jQuery('#<%=ClientID%>_addproductpopup').hide();">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Hidden field -->
    <asp:HiddenField ID="hdnValue" runat="server" />

    <!-- Table and totals are rendered here -->
    <asp:Panel ID="pnlPurchaseInformation" runat="server" CssClass="purchase-information">
        <div class="purchase-information-table">Loading...</div>
        <div class="purchase-information-cashback"></div>
    </asp:Panel>

    <!-- Initialise the jQuery widget -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=ClientID%>_pnlPurchaseInformation').purchaseInformation({

                // System settings
                disabled:   <%=Disabled.ToString().ToLower()%>,
                clientID: '<%=ClientID%>',
                templateTableID: 'templatetable',
                templateCashbackID: 'templatecashback',
                addProductPopupID: 'addproductpopup',
                addProductDropDownGroupID: 'addproductdropdowngroup',
                addProductDropDownProductID: 'addproductdropdownproduct',
                addProductDropDownVariantID: 'addproductdropdownvariant',
                addProductButtonID: 'addproductbutton',
                valueID: 'hdnValue',

                // properties - taken from the form control
                promotionID: <%=PromotionID%>,
                taxName: '<%=TaxName%>',
                taxPercentage: <%=TaxPercentage%>,
                showCashback: <%=ShowCashback.ToString().ToLower()%>,

                // promotion-specific settings
                mustQualifyForAllGroups:<%=MustQualifyForAllGroups.ToString().ToLower()%>,
                extraDiscountPercent:<%=ExtraDiscountPercent%>,

                // available groups/products/variants
                inventoryJson: '<%=InventoryJson%>'
            });
        });
    </script>
</div>
