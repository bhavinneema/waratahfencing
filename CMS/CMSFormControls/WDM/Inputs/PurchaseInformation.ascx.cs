﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

using CMS.Base.Web.UI;
using CMS.DataEngine;
using CMS.Ecommerce;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;

using WF;
using WF.Domain.Classes;
using WF.Promotions.Classes;
using WF.Promotions.Cashback;
using WF.Promotions.DataClasses;

public partial class CMSFormControls_WDM_PurchaseInformation : FormEngineUserControl
{
    private bool? _disabled;
    private PromotionInfo _promotion;
    private List<PurchaseInformationGroup> _inventoryGroups;
    private List<PurchaseInformationProduct> _inventoryProducts;
    private List<PurchaseInformationVariant> _inventoryVariants;
    private CashbackDetails _cashbackDetailsValue;


    /// <summary>
    /// Inputs are only enabled when the page is checked out.
    /// </summary>
    protected bool Disabled
    {
        get
        {
            if (_disabled != null) return (bool)_disabled;

            _disabled = (Form == null) || !Form.Enabled;

            return (bool)_disabled;
        }
    }


    /// <summary>
    /// What are we calling the tax?
    /// </summary>
    protected string TaxName
    {
        get { return ValidationHelper.GetString(GetValue("TaxName"), "GST"); }
    }


    /// <summary>
    /// What percentage is the tax?
    /// </summary>
    protected double TaxPercentage
    {
        get { return ValidationHelper.GetDouble(GetValue("TaxPercentage"), 10.0); }
    }


    /// <summary>
    /// Which promotion are we showing groups/products for?.
    /// </summary>
    protected int PromotionID
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("PromotionID"), 0);
        }
    }


    /// <summary>
    /// Name of double field to store the total cashback calculated.
    /// </summary>
    protected string TotalCashbackFieldName
    {
        get { return ValidationHelper.GetString(GetValue("TotalCashbackFieldName"), "TotalCashback"); }
    }



    /// <summary>
    /// Name of boolean field to store whether the 10% extra cashback was earned.
    /// </summary>
    protected string ExtraCashbackEarnedFieldName
    {
        get { return ValidationHelper.GetString(GetValue("ExtraCashbackEarnedFieldName"), "ExtraCashbackEarned"); }
    }


    private PromotionInfo Promotion
    {
        get
        {
            return _promotion ?? (_promotion = PromotionInfoProvider.GetPromotionInfo(PromotionID));
        }
    }


    /// <summary>
    /// Promotion-specific
    /// What percentage discount applies to the calim total if all reward groups qualify?
    /// </summary>
    protected bool MustQualifyForAllGroups
    {
        get { return Promotion.PromotionMustQualifyForAllGroups; }
    }


    /// <summary>
    /// Show the cashback totals at the bottom?
    /// </summary>
    protected bool ShowCashback
    {
        get { return ValidationHelper.GetBoolean(GetValue("ShowCashback"), false); }
    }


    /// <summary>
    /// An extra discount given if all groups qualify.
    /// </summary>
    protected double ExtraDiscountPercent
    {
        get { return Promotion.PromotionExtraDiscountPercent; }
    }


    /// <summary>
    /// Available folders.
    /// </summary>
    private List<PurchaseInformationGroup> InventoryGroups
    {
        get
        {
            return _inventoryGroups ?? (_inventoryGroups = GetInventoryGroups());
        }
    }


    /// <summary>
    /// Products belong to rewards, which belong to groups, which belong to promotions
    /// </summary>
    private List<PurchaseInformationProduct> InventoryProducts
    {
        get
        {
            return _inventoryProducts ?? (_inventoryProducts = new DataQuery("WF.Promotion", "GetAllProductsForPromotion")
            {
                Parameters = new QueryDataParameters
                {
                    { "PromotionID", PromotionID }
                }
            }
            .Result
            .Tables[0]
            .AsEnumerable()
            .Select(row => new PurchaseInformationProduct()
            {
                SKUID = ValidationHelper.GetInteger(row["SKUID"], 0),
                GroupID = ValidationHelper.GetInteger(row["GroupID"], 0),
                Name = ValidationHelper.GetString(row["Name"], string.Empty)
            }).ToList());
        }
    }


    /// <summary>
    /// Available variants.
    /// </summary>
    private List<PurchaseInformationVariant> InventoryVariants
    {
        get
        {
            return _inventoryVariants ?? (_inventoryVariants = SKUInfoProvider.GetSKUs()
                .Columns("SKUID, SKUParentSKUID, SKUName")
                .WhereIn("SKUParentSKUID", InventoryProducts.Select(prod => prod.SKUID).ToList<int>())
                .WhereNotNull("SalesforceID")
                .OrderBy("SKUName")
                .TypedResult
                .Select(sku => new PurchaseInformationVariant()
                {
                    SKUID = sku.SKUID,
                    ParentSKUID = sku.SKUParentSKUID,
                    Name = sku.SKUName,
                }).ToList());
        }
    }


    /// <summary>
    /// Available folders, products, and variants.
    /// </summary>
    private PurchaseInformationInventory Inventory
    {
        get
        {
            return new PurchaseInformationInventory
            {
                Groups = InventoryGroups,
                Products = InventoryProducts,
                Variants = InventoryVariants
            };
        }
    }


    /// <summary>
    /// stringified list of products.
    /// Returns a JSON object that we can include in the widget's options when initialising.
    /// </summary>
    protected string InventoryJson
    {
        get { return HttpUtility.JavaScriptStringEncode(JsonConvert.SerializeObject(Inventory)); }
    }


    /// <summary>
    /// The data is a JSON array of "PurchaseInformation" objects.
    /// </summary>
    public override object Value
    {
        get
        {
            return hdnValue.Value;
        }

        set
        {
            hdnValue.Value = ValidationHelper.GetString(value, string.Empty);
        }
    }


    /// <summary>
    /// Creates a PurchaseInfo object from the hidden value.
    /// </summary>
    private PurchaseInfo PurchaseInfoValue
    {
        get
        {
            string json = ValidationHelper.GetString(Value, string.Empty).Trim();
            PurchaseInfo purchaseInfo;
            try
            {
                purchaseInfo = JsonConvert.DeserializeObject<PurchaseInfo>(json);
                purchaseInfo.Deserialized = true;
            }
            catch (Exception ex)
            {
                purchaseInfo = new PurchaseInfo
                {
                    DeserializationError = ex.Message,
                    Deserialized = false
                };
            }
            return purchaseInfo;
        }
    }


    private CashbackDetails CashbackDetailsValue
    {
        get { return _cashbackDetailsValue ?? (_cashbackDetailsValue = new CashbackDetails(PurchaseInfoValue)); }
    }


    /// <summary>
    /// Returns an array of values of any other fields returned by the control.
    /// </summary>
    /// <returns>It returns an array where the first dimension is the attribute name and the second is its value.</returns>
    public override object[,] GetOtherValues()
    {
        object[,] array = new object[2, 2];

        array[0, 0] = TotalCashbackFieldName;
        array[0, 1] = CashbackDetailsValue.TotalCashback;

        array[1, 0] = ExtraCashbackEarnedFieldName;
        array[1, 1] = (CashbackDetailsValue.ExtraDiscountAmount != 0);

        return array;
    }


    /// <summary>
    /// Set up the control.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Include javascript files.
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "jquery", "//code.jquery.com/jquery-1.12.4.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "jqueryui", "//code.jquery.com/ui/1.12.0/jquery-ui.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "handlebars", "/CMSScripts/Custom/js/libs/handlebars-v4.0.5.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "purchaseinformation", "/CMSFormControls/WDM/Inputs/PurchaseInformation_files/PurchaseInformation.js", false);
    }



    /// <summary>
    /// Field is valid only if there's cashback being claimed.
    /// </summary>
    public override bool IsValid()
    {
        return ValidationHelper.GetInteger(CashbackDetailsValue?.TotalCashback, 0) > 0 || ValidationHelper.GetInteger(CashbackDetailsValue?.TotalItemRewards?.Count, 0) > 0;
    }


    /// <summary>
    /// Gets a list of groups belonging to the selected promotion.
    /// </summary>
    private List<PurchaseInformationGroup> GetInventoryGroups()
    {
        var inventoryFolders = PromotionRewardGroupInfoProvider.GetPromotionRewardGroups()
            .Columns("PromotionRewardGroupID, PromotionRewardGroupDisplayName")
            .WhereEquals("PromotionRewardGroupPromotionID", PromotionID)
            .OrderBy("PromotionRewardGroupPromotionID")
            .TypedResult
            .Select(infoObject => new PurchaseInformationGroup()
            {
                GroupID = infoObject.PromotionRewardGroupID,
                Name = infoObject.PromotionRewardGroupDisplayName
            }).ToList();

        return inventoryFolders;
    }


    /// <summary>
    /// Gets a list of groups products within all groups.
    /// </summary>
    private List<PurchaseInformationProduct> GetInventoryProducts()
    {
        var inventoryProducts = PromotionRewardProductInfoProvider.GetPromotionRewardProducts()
            .Columns("PromotionRewardGroupID, PromotionRewardGroupDisplayName")
            .WhereEquals("PromotionRewardGroupPromotionID", PromotionID)
            .OrderBy("PromotionRewardGroupPromotionID")
            .TypedResult
            .Select(infoObject => new PurchaseInformationProduct()
            {
                SKUID = 0,  // todo
                GroupID = 0,  // todo
                Name = ""  // todo
            }).ToList();

        return inventoryProducts;
    }
}


public class PurchaseInformationInventory
{
    [JsonProperty("groups")]
    public List<PurchaseInformationGroup> Groups { get; set; }

    [JsonProperty("products")]
    public List<PurchaseInformationProduct> Products { get; set; }

    [JsonProperty("variants")]
    public List<PurchaseInformationVariant> Variants { get; set; }
}


public class PurchaseInformationGroup
{
    [JsonProperty("groupID")]
    public int GroupID { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }
}


public class PurchaseInformationProduct
{
    [JsonProperty("skuid")]
    public int SKUID { get; set; }

    [JsonProperty("groupID")]
    public int GroupID { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }
}


public class PurchaseInformationVariant
{
    [JsonProperty("skuid")]
    public int SKUID { get; set; }

    [JsonProperty("parentSkuid")]
    public int ParentSKUID { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }
}