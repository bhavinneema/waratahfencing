﻿// Purchase Information form control
//
// IMPORTANT:   This widget uses ES6 features.
//              It may not be compatible with older browsers.
//
//
// This widget allows the user to build a table of product variants claimed,
// with unit prices and quantities for each.
//
// The table is organised into:
//  - Groups (eg: "Posts" or "Wire")
//      - Products (eg: "GalStar Extreme®")
//          - Variants (eg: "165cm GalStar Extreme")
//
// The user can add or remove variants from the table. When adding, a popup
// with a dropdown of all available variants is shown (the 'inventory').
// 
// The value is get and set by the form control is a JSON string.
// This JSON string can then be interpretted by a custom macro for use in email
// templates etc.

$.widget("wdm.purchaseInformation", {

    // Defaults. These can be set when constructing the widget on load.
    options: {

        // System settings
        disabled: false,
        clientID: '',
        templateTableID: 'templatetable',
        templateCashbackID: 'templatecashback',
        addProductPopupID: 'addproductpopup',
        addProductDropDownGroupID: 'addproductdropdowngroup',
        addProductDropDownProductID: 'addproductdropdownproduct',
        addProductDropDownVariantContainerID: 'addproductdropdownvariantcontainer',
        addProductDropDownVariantID: 'addproductdropdownvariant',
        addProductButtonID: 'addproductbutton',
        valueID: 'hdnValue',

        // properties - taken from the form control
        promotionID: 0,
        taxName: 'GST',
        taxPercentage: 10.0,
        showCashback: false,

        // promotion-specific settings
        mustQualifyForAllGroups: false,
        extraDiscountPercent: 10.0,

        // available groups/products/variants
        inventoryJson: []
    },

    // This is used to populate the Add a product dropdown.
    _inventory: {
        groups: [],
        products: [],
        variants: []
    },

    // The data passed to handlebars
    _templateData: {
        elementID: '',
        addProductPopupID: '',
        calculating: false, // set to true while waiting for the the call to WF.SiteAPI.
        showCashback: false, // taken from options on create

        // This is the value the form control gets and sets.
        // Need to be initialised by either parsing JSON or setting with this._emptyValue();
        value: {}
    },


    ////////////////////////////////////////////////////////////////////////////////
    // jQuery widget events

    _create: function () {
        this._registerHandlebarsHelpers();
        this._eventHandlers();
        this._prepareData();
        this._populatePopupDropDownGroup(this._inventory.groups);
        this.update();
    },


    ////////////////////////////////////////////////////////////////////////////////
    // Public methods

    add: function (skuid) {
        if (skuid == 0) {
            return;
        }

        var invDetails = this._getInventoryDetails(skuid);
        console.log(invDetails);
        var group = this._findOrAddGroup(this._templateData.value.groups, invDetails.group);
        var product = this._findOrAddProduct(group, invDetails.product);
        var variant = this._findOrAddVariant(product, invDetails.variant);

        this.update();
        this._setValue(this._templateData.value);
    },


    // Deletes a variant from the table, removing empty products and groups.
    delete: function (skuid) {

        var cols = this._getCollectionsAndIndexes(skuid);

        // Delete the variant.
        cols.variants.splice(cols.variantIndex, 1);

        // Delete the product if it's empty.
        if (cols.variants.length == 0) {
            cols.products.splice(cols.productIndex, 1);
        }

        // Delete the group if it's empty.
        if (cols.products.length == 0) {
            cols.groups.splice(cols.groupIndex, 1);
        }

        this.update();
        this._setValue(this._templateData.value);
    },


    // Moves a variant up or down within it's product.
    // direction:
    //      -1 = up.
    //       1 = down.
    move: function (skuid, direction) {
        var cols = this._getCollectionsAndIndexes(skuid);

        if (((cols.variantIndex + direction) < 0) || ((cols.variantIndex + direction) >= cols.variants.length)) return;
        cols.variants.splice(cols.variantIndex + direction, 0, cols.variants.splice(cols.variantIndex, 1)[0]);

        this.update();
        this._setValue(this._templateData.value);
    },


    // Changes the value of a field of the given variant.
    change: function ($textbox) {

        // Get the textbox attributes + value.
        var skuid = parseInt($textbox.data('skuid'));
        var field = $textbox.data('field');
        var type = $textbox.data('type');
        var textboxValue = $textbox.val();
        var parsedValue = 0;
        if (type === 'integer') parsedValue = parseInt(textboxValue);
        if (type === 'float') parsedValue = parseFloat(textboxValue);
        if (isNaN(parsedValue)) parsedValue = 0;

        // Find it in the data.
        var collections = this._getCollectionsAndIndexes(skuid);

        // Update tax.
        var variant = collections.variants[collections.variantIndex];
        variant[field] = parsedValue;
        variant.tax = this._calculateTax(variant.unitPrice),

            // Refresh the template (to show tax calculated)
            this.update();
        this._setValue(this._templateData.value);
    },


    ////////////////////////////////////////////////////////////////////////////////
    // Private methods

    update: function () {
        this._renderTable();
        this._requestCalculateCashback();
    },


    _renderTable: function () {
        var source = $('#' + this.options.clientID + '_' + this.options.templateTableID).html();
        var template = Handlebars.compile(source);
        var tableDiv = this.element.find('.purchase-information-table');
        tableDiv.html(template(this._templateData));
    },


    _requestCalculateCashback: function () {
        var widget = this;
        widget._templateData.calculating = true;
        widget._renderCashback();

        var request = JSON.stringify(widget._templateData.value);
        console.log('request:');
        console.log(request);

        jQuery.ajax({
            url: '/api/promotion/calculate-cashback',
            type: 'POST',
            contentType: 'application/json',
            dataType: "json",
            data: request,
            success: function (data) {
                console.log('success response:');
                console.log(JSON.stringify(data));
                for (var g = 0; g < widget._templateData.value.groups.length; g++) {
                    group = widget._templateData.value.groups[g];
                    group.cashback = 0;

                    //rewardGroup = data.rewardGroups.find(obj => obj.groupID === group.groupID);     

                    rewardGroup = data.rewardGroups === undefined ? undefined : jQuery.grep(data.rewardGroups, function (aGroup, i) {
                        return (aGroup.groupID === group.groupID);
                    })[0];

                    if (typeof (rewardGroup) == 'object') {
                        group.cashback = rewardGroup.cashback;
                    }
                }
                widget._templateData.value.totalClaimAmount = data.totalClaimAmount;
                widget._templateData.value.extraDiscountAmount = data.extraDiscountAmount;
                widget._templateData.value.totalCashback = data.totalCashback;
                widget._templateData.value.totalItemRewards = data.totalItemRewards;
                widget._templateData.calculating = false;
                widget._renderCashback();
            },
            error: function (data) {
                console.log('error response:');
                console.log(data);
            }
        });
    },


    _renderCashback: function () {
        var source = $('#' + this.options.clientID + '_' + this.options.templateCashbackID).html();
        var template = Handlebars.compile(source);
        var cashbackDiv = this.element.find('.purchase-information-cashback');
        cashbackDiv.html(template(this._templateData));
    },


    _registerHandlebarsHelpers: function () {
        Handlebars.registerHelper({
            eq: function (v1, v2) { return v1 === v2; },
            ne: function (v1, v2) { return v1 !== v2; },
            lt: function (v1, v2) { return v1 < v2; },
            gt: function (v1, v2) { return v1 > v2; },
            lte: function (v1, v2) { return v1 <= v2; },
            gte: function (v1, v2) { return v1 >= v2; },
            and: function (v1, v2) { return v1 && v2; },
            or: function (v1, v2) { return v1 || v2; },
            currency: function (value) { return Number(Math.round(parseFloat(value) + 'e2') + 'e-2').toFixed(2); }
        });
    },


    // Bind funtions to certain events.
    _eventHandlers: function () {

        if (this.options.disabled) return;

        var widget = this;

        // Note:    Events for items rendered by the handlebars template need to be
        //          bound to the document. Othewise they'd need re-binding every time the
        //          template was updated.
        //
        //          Eg: See the 'Button click' and 'Textbox change' events below.

        // Button click
        $(document).on('click', '.js-purchase-information--button', function (e) {
            var elementID = $(e.target).data('elementid');
            var skuid = parseInt($(e.target).data('skuid'));
            var action = $(e.target).data('action');

            switch (action) {

                case 'moveup':
                    $('#' + elementID).purchaseInformation('move', skuid, -1);
                    break;

                case 'movedown':
                    $('#' + elementID).purchaseInformation('move', skuid, 1);
                    break;

                case 'delete':
                    $('#' + elementID).purchaseInformation('delete', skuid);
                    break;
            }
        });

        // Textbox change + keyup
        var onChangeEventHandler = function (e) {
            var $textbox = $(e.target);
            var elementID = $textbox.data('elementid');
            $('#' + elementID).purchaseInformation('change', $textbox);
        };
        $(document).on('change', '.js-purchase-information--textbox', onChangeEventHandler);
        //$(document).on('keyup', '.js-purchase-information--textbox', onChangeEventHandler);

        // "Add a product" button
        // This button isn't part of the handlebars template, so we can bind the click event directly to it.
        var $addProductButton = $('#' + widget.options.clientID + '_' + widget.options.addProductButtonID);
        $addProductButton.click(function (e) {
            var skuid = parseInt($('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantID).val());
            if (isNaN(skuid)) {
                skuid = parseInt($('#' + widget.options.clientID + '_' + widget.options.addProductDropDownProductID).val());
            }
            widget.add(skuid);
        });


        // Re-load product if a group is chosen.
        var $popupDropdownGroup = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownGroupID);
        $popupDropdownGroup.change(function (e) {
            var groupID = parseInt($(e.target).val());
            //var productsInThisGroup = widget._inventory.products.filter(obj => obj.groupID === groupID);

            var productsInThisGroup = jQuery.grep(widget._inventory.products, function (aProduct) {
                return aProduct.groupID === groupID;
            });

            widget._populatePopupDropDownProduct(productsInThisGroup);
        });

        // Re-load variants if a product is chosen.
        var $popupDropdownProduct = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownProductID);
        $popupDropdownProduct.change(function (e) {
            var skuid = parseInt($(e.target).val());
            //var variantsInThisProduct = widget._inventory.variants.filter(obj => obj.parentSkuid === skuid);

            var variantsInThisProduct = jQuery.grep(widget._inventory.variants, function (aVariant) {
                return aVariant.parentSkuid === skuid;
            });

            widget._populatePopupDropDownVariant(variantsInThisProduct);
        });
    },


    // Some options need to be copied over to the template data.
    _prepareData: function () {
        // Available groups, products, and variants (to be shown in the drop-downs)
        this._inventory = JSON.parse(this.options.inventoryJson);

        // These are needed for the template.
        this._templateData.elementID = $(this.element).attr('id');
        this._templateData.addProductPopupID = this.options.clientID + '_' + this.options.addProductPopupID;
        this._templateData.showCashback = this.options.showCashback;

        // Get the value from the form control. Could return a blank value.
        this._templateData.value = this._getValue();

        // Always overwrite value settings with properties from form control.
        this._templateData.value.promotionID = this.options.promotionID;
        this._templateData.value.taxName = this.options.taxName;
        this._templateData.value.taxPercentage = this.options.taxPercentage;
        this._templateData.value.mustQualifyForAllGroups = this.options.mustQualifyForAllGroups;
        this._templateData.value.extraDiscountPercent = this.options.extraDiscountPercent;
    },


    // We need to populate the 'group' drop-down list with data from the given 'groups' collection.
    _populatePopupDropDownGroup: function (groups) {
        var widget = this;
        var $popupDropdownGroup = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownGroupID);
        var $popupDropdownProduct = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownProductID);
        var $popupDropdownVariant = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantID);
        var $addProductButton = $('#' + widget.options.clientID + '_' + widget.options.addProductButtonID);
        $popupDropdownGroup[0].options.length = 0;
        $popupDropdownProduct[0].options.length = 0;
        $popupDropdownVariant[0].options.length = 0;
        $addProductButton.prop('disabled', true);

        for (var g = 0; g < groups.length; g++) {
            var group = groups[g];
            widget._addPopupOption($popupDropdownGroup[0], group.groupID, group.name);
        }

        if (groups.length > 0) {
            var groupID = groups[0].groupID;
            //var productsInThisGroup = widget._inventory.products.filter(obj => obj.groupID === groupID);

            var productsInThisGroup = jQuery.grep(widget._inventory.products, function (aProduct) {
                return aProduct.groupID === groupID;
            });

            if (productsInThisGroup.length > 0) {
                this._populatePopupDropDownProduct(productsInThisGroup);
            }
        }
    },


    // We need to populate the 'product' drop-down list with data from the given 'products' collection.
    _populatePopupDropDownProduct: function (products) {
        var widget = this;
        var $popupDropdownProduct = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownProductID);
        var $popupDropdownVariantContainer = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantContainerID);
        var $popupDropdownVariant = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantID);
        var $addProductButton = $('#' + widget.options.clientID + '_' + widget.options.addProductButtonID);
        $popupDropdownProduct[0].options.length = 0;
        $popupDropdownVariant[0].options.length = 0;
        $addProductButton.prop('disabled', true);

        for (var p = 0; p < products.length; p++) {
            var product = products[p];
            widget._addPopupOption($popupDropdownProduct[0], product.skuid, product.name);
        }

        if (products.length > 0) {
            var skuid = products[0].skuid;
            //var variantsInThisProduct = widget._inventory.variants.filter(obj => obj.parentSkuid === skuid);

            var variantsInThisProduct = jQuery.grep(widget._inventory.variants, function (aVariant) {
                return aVariant.parentSkuid === skuid;
            });

            this._populatePopupDropDownVariant(variantsInThisProduct);
        }
    },


    // We need to populate the 'variant' drop-down list with data from the given 'variants' collection.
    _populatePopupDropDownVariant: function (variants) {
        var widget = this;
        var $popupDropdownVariantContainer = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantContainerID);
        var $popupDropdownVariant = $('#' + widget.options.clientID + '_' + widget.options.addProductDropDownVariantID);
        $popupDropdownVariant[0].options.length = 0;
        var $addProductButton = $('#' + widget.options.clientID + '_' + widget.options.addProductButtonID);

        for (var v = 0; v < variants.length; v++) {
            var variant = variants[v];
            widget._addPopupOption($popupDropdownVariant[0], variant.skuid, variant.name);
        }

        if (variants.length > 0) {
            $popupDropdownVariantContainer.show();
        } else {
            $popupDropdownVariantContainer.hide();
        }

        $addProductButton.prop('disabled', false);
    },


    // Helper function for adding a single option to the given drop-down (<select>).
    _addPopupOption: function (dropdown, value, text) {
        var option = document.createElement("option");
        option.value = value;
        option.text = text;
        dropdown.add(option);
    },


    // Calculates the tax for a given price, depending on if tax is inclusive or exclusive.
    _calculateTax: function (unitPrice) {
        var value = parseFloat(unitPrice);
        var taxPercentage = parseFloat(this.options.taxPercentage);
        //if (taxInclusive) {
        //    var withoutTax = value / ((100 + taxPercentage) / 100);
        //    return value - withoutTax;
        //}
        //else {
        return value * taxPercentage / 100;
        //}
    },


    // Finds the variant in the inventory, and returns an object containing
    // info about it and it's parent product and group.
    _getInventoryDetails: function (skuid) {
        //var inventoryVariant = this._inventory.variants.find(obj => obj.skuid === skuid);
        //var inventoryProduct = this._inventory.products.find(obj => obj.skuid === inventoryVariant.parentSkuid);
        //var inventoryGroup  = this._inventory.groups.find(obj => obj.groupID === inventoryProduct.groupID);
        console.log(this._inventory, skuid);
        var inventoryVariant = jQuery.grep(this._inventory.variants, function (aVariant) {
            return aVariant.skuid === skuid;
        })[0];

        var inventoryProduct = jQuery.grep(this._inventory.products, function (aProduct) {
            if (inventoryVariant === undefined) {
                return aProduct.skuid === skuid;
            } else {
                return aProduct.skuid === inventoryVariant.parentSkuid;
            }
        })[0];

        var inventoryGroup = jQuery.grep(this._inventory.groups, function (aGroup) {
            return aGroup.groupID === inventoryProduct.groupID;
        })[0];

        return {
            group: inventoryGroup,
            product: inventoryProduct,
            variant: inventoryVariant
        };
    },


    // Finds the variant in the table.
    // Returns an object that contains the collection and index for each: group, product, and variant.
    _getCollectionsAndIndexes: function (skuid) {
        var invDetails = this._getInventoryDetails(skuid);

        var groups = this._templateData.value.groups;
        //var groupIndex     = groups.findIndex(obj => obj.groupID === invDetails.group.groupID);
        //var group          = groups[groupIndex];

        var group = jQuery.grep(groups, function (aGroup) {
            return aGroup.groupID === invDetails.group.groupID;
        })[0];
        var groupIndex = jQuery.inArray(group, groups);

        var products = group.products;
        //var productIndex    = products.findIndex(obj => obj.skuid === invDetails.product.skuid);
        //var product         = products[productIndex];

        var product = jQuery.grep(products, function (aProduct) {
            return aProduct.skuid === invDetails.product.skuid;
        })[0];
        var productIndex = jQuery.inArray(product, products);

        var variants = product.variants;
        //var variantIndex    = variants.findIndex(obj => obj.skuid === skuid);
        var variant = jQuery.grep(variants, function (aVariant) {
            return aVariant.skuid === skuid;
        })[0];
        var variantIndex = jQuery.inArray(variant, variants);

        return {
            groups: groups,
            groupIndex: groupIndex,
            products: products,
            productIndex: productIndex,
            variants: variants,
            variantIndex: variantIndex
        };
    },


    // Tries to find a group in the table.
    // If it can't be found, it's added from the inventory.
    _findOrAddGroup: function (groups, inventoryGroup) {
        //var group = groups.find(function (obj) { return obj.groupID === inventoryGroup.groupID; });
        var group;
        if (groups !== null) {
            group = jQuery.grep(groups, function (aGroup) {
                return aGroup.groupID === inventoryGroup.groupID;
            })[0];
        }

        if (group === undefined) {
            group = {
                groupID: inventoryGroup.groupID,
                name: inventoryGroup.name,
                products: [],
                cashback: 0.0
            };
            groups.push(group);
        }
        return group;
    },


    // Tries to find a product in the table.
    // If it can't be found, it's added from the inventory.
    _findOrAddProduct: function (group, inventoryProduct) {
        //var product = products.find(function (obj) { return obj.skuid === inventoryProduct.skuid; });
        var product = group.products === undefined ? undefined : jQuery.grep(group.products, function (aProduct) {
            return aProduct.skuid === inventoryProduct.skuid;
        })[0];

        if (product === undefined) {
            product = {
                skuid: inventoryProduct.skuid,
                name: inventoryProduct.name,
                variants: []
            };
            group.products.push(product);
        }
        return product;
    },


    // Tries to find a variant in the table.
    // If it can't be found, it's added from the inventory.
    _findOrAddVariant: function (product, inventoryVariant) {
        //var variant = variants.find(function (obj) { return obj.skuid === inventoryVariant.skuid; });

        var variant = product.variants === undefined ? undefined : jQuery.grep(product.variants, function (aVariant) {
            return aVariant.skuid === inventoryVariant.skuid;
        })[0];

        if (variant === undefined) {
            variant = {
                skuid: inventoryVariant === undefined ? product.skuid : inventoryVariant.skuid,
                name: inventoryVariant === undefined ? product.name : inventoryVariant.name,
                unitPrice: 0, // filled in by customer, based on the physical store they bought at.
                tax: 0, // will be calculated on this.change()
                qty: 1.0
            };
            product.variants.push(variant);
        }
        return variant;
    },


    // The initial state when nothing is added to the table.
    _emptyValue: function () {
        return {
            promotionID: 0,                 // Taken from options on create
            taxName: '',                    // Taken from options on create
            taxPercentage: 0.0,             // Taken from options on create
            groups: [],                     // This is where the selected groups/products/variants are stored.
            totalClaimAmount: 0.0,          // Calculated via a WF.SiteAPI call on update
            extraDiscountAmount: 0.0,       // Calculated via a WF.SiteAPI call on update
            totalCashback: 0.0,             // Calculated via a WF.SiteAPI call on update
        };
    },


    // Takes the JSON string value from the Form Control's hidden field, and parses it.
    // Returns a javascript object.
    _getValue: function () {
        var $hiddenField = $('#' + this.options.clientID + '_' + this.options.valueID);
        var hiddenFieldValue = $hiddenField.val();

        if (hiddenFieldValue.length == 0) {
            return this._emptyValue();
        }
        else {
            return JSON.parse(hiddenFieldValue);
        }
    },


    // Takes the javascript object that represents the return value, and stringifies it into a JSON string.
    // The string is then stored in the hidden field, accessible by the Form Control.
    _setValue: function (obj) {
        var hiddenFieldValue = JSON.stringify(obj);
        var $hiddenField = $('#' + this.options.clientID + '_' + this.options.valueID);

        $hiddenField.val(hiddenFieldValue);
    }
});