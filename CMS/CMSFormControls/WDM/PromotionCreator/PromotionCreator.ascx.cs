﻿using CMS.Ecommerce;
using CMS.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Base.Web.UI;
using CMS.FormEngine.Web.UI;

public partial class CMSFormControls_WDM_PromotionCreator_PromotionCreator : FormEngineUserControl
{
    public override object Value
    {
        get
        {
            return hdnQuery.Value;
        }

        set
        {
            var stringValue = ValidationHelper.GetString(value, "");
            if ((string.IsNullOrWhiteSpace(stringValue)) || (stringValue == "{}"))
            {
                hdnQuery.Value = "{\"condition\": \"AND\",\"rules\": []}";
            }
            else
            {
                hdnQuery.Value = ValidationHelper.GetString(stringValue, "");
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CssRegistration.RegisterCssLink(Page, "//cdn.jsdelivr.net/jquery.query-builder/2.4.0/css/query-builder.default.min.css");
        CssRegistration.RegisterCssLink(Page, "/CMSFormControls/WDM/PromotionCreator/dependencies/bootstrap.min.css");

        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "jquery", "//code.jquery.com/jquery-1.12.4.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "bootstrap", "/CMSFormControls/WDM/PromotionCreator/dependencies/bootstrap.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "extendext", "/CMSFormControls/WDM/PromotionCreator/dependencies/jQuery.extendext.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "dotjs", "/CMSFormControls/WDM/PromotionCreator/dependencies/doT.min.js", false);
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "querybuilder", "//cdn.jsdelivr.net/jquery.query-builder/2.4.0/js/query-builder.min.js");
        ScriptHelper.RegisterClientScriptInclude(Page, typeof(string), "promotioncreator", "/CMSFormControls/WDM/PromotionCreator/PromotionCreator.js", false);

        var productsCollection = new Dictionary<int, string>();
        var products = SKUInfoProvider.GetSKUs();
        foreach (var product in products)
        {
            productsCollection.Add(product.SKUID, product.SKUName);
        }
        hdnProducts.Value = JsonConvert.SerializeObject(productsCollection);

    }
}