﻿var setValue = function () {
    console.log("setting value");
    $('input[id*="hdnQuery"]').val(JSON.stringify($('#querybuilder').queryBuilder('getRules')));
}

$(document).ready(function () {
    $('#querybuilder').queryBuilder({
        plugins: ['bt-tooltip-errors'],
        filters: [{
            id: 'name',
            label: 'Name',
            type: 'string'
        }, {
            id: 'product',
            label: 'Product',
            type: 'integer',
            input: 'select',
            values: JSON.parse($('input[id*="hdnProducts"]').val()),
            operators: ['equal', 'not_equal', 'is_null', 'is_not_null']
        },{
            id: 'quantity',
            label: 'Quantity',
            type: 'integer'
        }, {
            id: 'price',
            label: 'Price',
            type: 'double',
            validation: {
                min: 0,
                step: 0.01
            }
        }
        ],

        rules: JSON.parse($('input[id*="hdnQuery"]').val()),
        allow_empty: true
    });

    $('#querybuilder').on('afterAddGroup.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterDeleteGroup.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterAddRule.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterDeleteRule.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterCreateRuleFilters.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterCreateRuleOperators.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterUpdateRuleValue.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterUpdateRuleFilter.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterUpdateRuleOperator.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterApplyRuleFlags.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterUpdateGroupCondition.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterReset.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterClear.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterMove.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterSetFilters.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
    $('#querybuilder').on('afterInvert.queryBuilder', function (e, rule, error, value) {
        setValue();
    });
});

