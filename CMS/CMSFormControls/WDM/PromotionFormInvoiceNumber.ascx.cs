﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.OnlineForms;

public partial class CMSFormControls_WDM_PromotionFormInvoiceNumber : FormEngineUserControl
{
    public override object Value
    {
        get
        {
            return txtValue.Text;
        }
        set
        {
            txtValue.Text = ValidationHelper.GetString(value, string.Empty);
        }
    }

    public override bool IsValid()
    {
        var bfi = Form.EditedObject as BizFormItem;

        if (bfi == null)
            return false;

        var existing = BizFormItemProvider.GetItems(bfi.BizFormClassName).WhereEquals(Field, Value).WhereNotEquals(bfi.BizFormInfo.FormName + "ID", bfi.ItemID).Count;

        if (existing > 0)
        {
            ValidationError = "Invoice number has already been used.";
            return false;
        }
        else
            return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}