﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PromotionFormPurchaseInfo.ascx.cs" Inherits="CMSFormControls_WDM_PromotionFormPurchaseInfo" ViewStateMode="Enabled" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <asp:Repeater runat="server" ID="rptGroups">
            <ItemTemplate>
                <div>
                    <h5><%# Eval("PromotionRewardGroupDisplayName") %></h5>
                </div>
                <asp:Repeater runat="server" ID="rptConditions">
                    <ItemTemplate>
                        <asp:Repeater runat="server" ID="rptProducts">
                            <ItemTemplate>
                                <div class="promotion-form-group">
                                    <div class="promotion-form-label-cell">
                                        <asp:Label runat="server" ID="lblProduct" CssClass="EditingFormLabel" AssociatedControlID="txtAmount"><%# Eval("SKUName") %></asp:Label>
                                    </div>
                                    <div class="promotion-form-value-cell">
                                        <div class="input-group">
                                            <asp:Panel runat="server" CssClass="input-group-addon" Visible='<%# Eval<bool>("PromotionRewardIsValueBased") %>'>$</asp:Panel>
                                            <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" placeholder='<%# Eval<bool>("PromotionRewardIsValueBased") ? "value" : "quantity" %>' AutoPostBack="True"></asp:TextBox>
                                            <asp:Panel runat="server" CssClass="input-group-addon" Visible='<%# !Eval<bool>("PromotionRewardIsValueBased") %>'>qty</asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="promotion-form-group">
                            <div class="promotion-form-label-cell">
                                <asp:Label runat="server" ID="lblGrpTotal" CssClass="EditingFormLabel" AssociatedControlID="txtGrpTotal">Total</asp:Label>
                            </div>
                            <div class="promotion-form-value-cell">
                                <div class="input-group">
                                    <asp:Panel runat="server" CssClass="input-group-addon">$</asp:Panel>
                                    <asp:TextBox runat="server" ID="txtGrpTotal" CssClass="form-control" ReadOnly="True" Text="0"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <hr />
            </ItemTemplate>
        </asp:Repeater>

        <div class="promotion-form-group">
            <div class="promotion-form-label-cell">
                <asp:Label runat="server" ID="lblGrpTotal" CssClass="EditingFormLabel">Cashback/Giveaways</asp:Label>
            </div>
            <div class="promotion-form-value-cell">
                <ul>
                    <asp:Literal runat="server" ID="litCashbackText"></asp:Literal>
                    <asp:Literal runat="server" ID="litProductsText"></asp:Literal>
                </ul>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
