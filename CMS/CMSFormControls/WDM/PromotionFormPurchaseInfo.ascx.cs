﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using CMS;
using CMS.Base.Web.UI;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Web.UI.WS;
using CMS.Ecommerce;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.OnlineForms;
using RestSharp.Extensions;
using WF;
using WF.Domain.Classes;
using WF.Promotions.Classes;

public partial class CMSFormControls_WDM_PromotionFormPurchaseInfo : FormEngineUserControl
{
    protected Dictionary<int, Dictionary<int, TextBox>> ValueBoxes = new Dictionary<int, Dictionary<int, TextBox>>();
    protected Dictionary<int, TextBox> ResultBoxes = new Dictionary<int, TextBox>();
    protected Dictionary<int, int> Totals = new Dictionary<int, int>();
    protected Dictionary<int, int> ItemRewards = new Dictionary<int, int>();
    protected List<int> Groups;
    protected double CashbackTotal;
    protected string PurchaseInfo;
    protected string CashbackGiveawayResult;

    /// <summary>
    /// Gets or sets the value entered into the field, a hexadecimal color code in this case.
    /// </summary>
    public override object Value
    {
        get
        {
            return GetData();
        }
        set
        {
            Render();
            if (!Page.IsPostBack)
            {
                SetData(value);
            }
        }
    }

    private string GetData()
    {
        Calculate();
        var result = new Dictionary<string, Dictionary<string, int>>();

        var specifiedProducts = new Dictionary<int, Dictionary<int, int>>();

        foreach (var vbGroup in ValueBoxes)
        {
            var group = new Dictionary<string, int>();
            var specifiedProductsGroup = new Dictionary<int, int>();
            foreach (var vbValue in vbGroup.Value)
            {
                var value = ValidationHelper.GetInteger(vbValue.Value.Text, 0);
                if (value > 0)
                {
                    specifiedProductsGroup.Add(vbValue.Key, value);
                    group.Add(vbValue.Key.ToString(), value);
                }
            }
            result.Add(vbGroup.Key.ToString(), group);
            specifiedProducts.Add(vbGroup.Key, specifiedProductsGroup);
        }
        var jss = new JavaScriptSerializer();

        // Save other fields
        var specifiedProductsString = new List<string>();
        foreach (var specifiedProductsGroup in specifiedProducts)
        {
            var groupInfo = PromotionRewardInfoProvider.GetPromotionRewardInfo(specifiedProductsGroup.Key);
            if (groupInfo == null)
                continue;
            var valueBased = groupInfo.PromotionRewardIsValueBased;
            foreach (var product in specifiedProductsGroup.Value)
            {
                var sku = SKUInfoProvider.GetSKUInfo(product.Key);
                if (sku == null)
                    continue;
                specifiedProductsString.Add(string.Format("{0}{1} of {2}", (valueBased) ? "$" : "qty:", product.Value, sku.SKUName));
            }
        }
        Form.Data.SetValue("PurchaseInfo", string.Join("<br />", specifiedProductsString));

        var rewardsString = new List<string>();
        rewardsString.Add(string.Format("Cashback: ${0:0.00}", CashbackTotal));
        if (Groups.Count > 1)
            rewardsString.Add(string.Format("10% cashback bonus"));
        foreach (var itemReward in ItemRewards)
        {
            var product = SKUInfoProvider.GetSKUInfo(itemReward.Key);
            if (product == null)
                continue;
            rewardsString.Add(string.Format("{0} x {1}", itemReward.Value, product.SKUName));
        }
        Form.Data.SetValue("CashbackGiveawayResult", string.Join("<br />", rewardsString));

        return jss.Serialize(result);
    }

    private void SetData(object value)
    {
        var jss = new JavaScriptSerializer();
        try
        {
            var result = jss.Deserialize<Dictionary<string, Dictionary<string, int>>>(ValidationHelper.GetString(value, string.Empty));
            foreach (var resultGroup in result)
            {
                foreach (var resultValue in resultGroup.Value)
                {
                    if (resultValue.Value > 0)
                        ValueBoxes[ValidationHelper.GetInteger(resultGroup.Key, 0)][ValidationHelper.GetInteger(resultValue.Key, 0)].Text = resultValue.Value.ToString();
                }
            }
            Calculate();
        }
        catch
        {

        }
    }

    /// <summary>
    /// Returns true if a color is selected. Otherwise, it returns false and displays an error message.
    /// </summary>
    public override bool IsValid()
    {
        Calculate();
        if (Totals.Sum(x => x.Value) > 0 || ItemRewards.Count > 0)
        {
            return true;
        }
        else
        {
            ValidationError = "You do not qualify.";
            return false;
        }
    }

    /// <summary>
    /// Handler for the Load event of the control.
    /// </summary>
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handler for the Load event of the control.
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        var ctrl = Request.Params.Get("__EVENTTARGET");
        if (Page.IsPostBack && ctrl == "m$actionsElem$editMenuElem$menu")
        {
            Response.Redirect(Request.RawUrl);
        }
    }

    protected void Render()
    {
        var bfi = Form.EditedObject as BizFormItem;

        if (bfi == null)
            return;

        var bf = bfi.BizFormInfo;

        var promotion = PromotionInfoProvider.GetPromotions()
            .WhereEquals("PromotionBizFormID", bf.FormID)
            .FirstObject;

        if (promotion == null)
            return;

        var groups = PromotionRewardGroupInfoProvider.GetPromotionRewardGroups()
            .WhereEquals("PromotionRewardGroupPromotionID", promotion.PromotionID)
            .OrderBy("PromotionRewardGroupOrder");

        rptGroups.ItemDataBound += RptGroups_ItemDataBound;
        rptGroups.DataSource = groups;
        rptGroups.DataBind();
    }

    private void RptGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var item = e.Item;
        var rptConditions = (Repeater)item.FindControl("rptConditions");

        var promotionRewardGroup = (PromotionRewardGroupInfo)item.DataItem;

        var conditions = PromotionRewardInfoProvider.GetPromotionRewards()
            .WhereEquals("PromotionRewardPromotionID", promotionRewardGroup.PromotionRewardGroupPromotionID)
            .WhereEquals("PromotionRewardGroupID", promotionRewardGroup.PromotionRewardGroupID)
            .OrderBy("PromotionRewardOrder");

        rptConditions.ItemDataBound += rptGroups_ItemDataBound;
        rptConditions.DataSource = conditions;
        rptConditions.DataBind();
    }

    private void rptGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var item = e.Item;
        var rptProducts = (Repeater)item.FindControl("rptProducts");
        var txtGrpTotal = (TextBox)item.FindControl("txtGrpTotal");

        var promotionReward = (PromotionRewardInfo)item.DataItem;

        if (!ResultBoxes.ContainsKey(promotionReward.PromotionRewardID))
            ResultBoxes.Add(promotionReward.PromotionRewardID, txtGrpTotal);

        var products = PromotionRewardProductInfoProvider.GetPromotionRewardProducts()
            .Source(x => x.Join<PromotionRewardInfo>("WF_PromotionRewardProduct.PromotionRewardProductPromotionRewardID", "PromotionRewardID").Join<SKUInfo>("WF_PromotionRewardProduct.PromotionRewardProductSKUID", "SKUID"))
            .WhereEquals("PromotionRewardProductPromotionRewardID", promotionReward.PromotionRewardID)
            .OrderBy("PromotionRewardProductOrder");

        rptProducts.ItemDataBound += rptProducts_ItemDataBound;
        rptProducts.DataSource = products.Result;
        rptProducts.DataBind();
    }

    private void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var item = e.Item;
        var txtAmount = (TextBox)item.FindControl("txtAmount");
        txtAmount.TextChanged += TxtAmount_TextChanged;
        var dataRow = ((DataRowView)item.DataItem).Row;

        var promotionProduct = new PromotionRewardProductInfo(dataRow);

        var conditionGroupId = promotionProduct.PromotionRewardProductPromotionRewardID;
        var product = SKUInfoProvider.GetSKUInfo(promotionProduct.PromotionRewardProductSKUID);

        if (!ValueBoxes.ContainsKey(conditionGroupId))
            ValueBoxes.Add(conditionGroupId, new Dictionary<int, TextBox>());
        ValueBoxes[conditionGroupId].Add(product.SKUID, txtAmount);
    }

    private void TxtAmount_TextChanged(object sender, EventArgs e)
    {
        Calculate();
    }

    private bool Calculate()
    {
        CashbackTotal = 0;
        Groups = new List<int>();

        foreach (var conditionGroup in ValueBoxes)
        {
            var groupTotal = 0;
            foreach (var valueBox in conditionGroup.Value)
            {
                groupTotal += ValidationHelper.GetInteger(valueBox.Value.Text, 0);
            }
            if (!Totals.ContainsKey(conditionGroup.Key))
                Totals.Add(conditionGroup.Key, groupTotal);
        }

        foreach (var groupTotal in Totals)
        {
            var pr = PromotionRewardInfoProvider.GetPromotionRewardInfo(groupTotal.Key);
            var groupTotalValue = CalculateCashbackReward(pr, groupTotal.Value);
            ResultBoxes[groupTotal.Key].Text = string.Format("{0}", groupTotalValue);
            CashbackTotal += groupTotalValue;
            if (groupTotalValue > 0)
                if (!Groups.Contains(pr.PromotionRewardGroupID))
                    Groups.Add(pr.PromotionRewardGroupID);
        }

        litProductsText.Text = "";
        if (Groups.Count > 1)
        {
            litProductsText.Text += string.Format("<li>10% cashback bonus</li>");
            CashbackTotal += CashbackTotal * 0.1;
        }
        litCashbackText.Text = string.Format("<li>Cashback: ${0:0.00}</li>", CashbackTotal);
        foreach (var item in ItemRewards)
        {
            var product = SKUInfoProvider.GetSKUInfo(item.Key);
            if (product == null)
                continue;
            litProductsText.Text += string.Format("<li>{0} x {1}</li>", item.Value, product.SKUName);
        }

        return (CashbackTotal > 0 || ItemRewards.Count > 0);
    }

    private int CalculateCashbackReward(PromotionRewardInfo pr, int value)
    {
        var cashbackResult = 0;

        var total = value;
        var remaining = value;

        var minRequired = pr.PromotionRewardMinRequired;
        var cashback = pr.PromotionRewardCashback;

        if (total >= minRequired)
        {
            remaining -= minRequired;
            cashbackResult += cashback;
            if (pr.PromotionRewardItemQty > 0)
            {
                if (ItemRewards.ContainsKey(pr.PromotionRewardGiveawayItemID))
                    ItemRewards[pr.PromotionRewardGiveawayItemID] += pr.PromotionRewardItemQty;
                else
                    ItemRewards.Add(pr.PromotionRewardGiveawayItemID, pr.PromotionRewardItemQty);
            }
        }

        if (remaining > 0)
        {
            var additionalConditions = PromotionProductInfoProvider.GetPromotionProducts()
                .WhereEquals("PromotionProductPromotionRewardID", pr.PromotionRewardID)
                .ToList();
            int i = 0;
            foreach (var additionalCondition in additionalConditions)
            {
                var upperLimit = (additionalCondition.PromotionProductUpperLimit > 0)
                    ? additionalCondition.PromotionProductUpperLimit
                    : int.MaxValue;
                var range = (total >= upperLimit) ? upperLimit - additionalCondition.PromotionProductMinRequired : total - additionalCondition.PromotionProductMinRequired;
                var every = (additionalCondition.PromotionProductValue == 0) ? range : additionalCondition.PromotionProductValue;
                minRequired = additionalCondition.PromotionProductMinRequired;
                cashback = additionalCondition.PromotionProductCashback;

                if (total >= minRequired)
                {
                    cashbackResult += (range / every) * cashback;
                    if (additionalCondition.PromotionProductItemQty > 0)
                    {
                        var itemQty = (range / every) * additionalCondition.PromotionProductItemQty;
                        if (ItemRewards.ContainsKey(additionalCondition.PromotionProductItem))
                            ItemRewards[additionalCondition.PromotionProductItem] += itemQty;
                        else
                            ItemRewards.Add(additionalCondition.PromotionProductItem, itemQty);
                    }
                }
                i++;
            }
        }

        return cashbackResult;
    }

    protected string GetAddon(int promotionRewardID)
    {
        var promoRewardGroup = PromotionRewardInfoProvider.GetPromotionRewardInfo(promotionRewardID);
        return (promoRewardGroup.PromotionRewardIsValueBased) ? "$" : "qty";
    }
}