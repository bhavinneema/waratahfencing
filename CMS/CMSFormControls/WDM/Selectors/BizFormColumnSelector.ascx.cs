﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.OnlineForms;
using CMS.UIControls;
using WDM.SyncedObjects.Classes;
using WF;

public partial class CMSFormControls_WDM_Selectors_BizFormColumnSelector : FormEngineUserControl
{
    public override object Value
    {
        get { return dropDownList.SelectedValue; }
        set
        {
            var val = ValidationHelper.GetString(value, string.Empty);
            Bind(val);
            if (dropDownList.Items.FindByValue(val) != null)
                dropDownList.SelectedValue = val;
        }
    }

    private void Bind(string currentField)
    {
        var formId = UIContext.ParentObjectID;
        var form = BizFormInfoProvider.GetBizFormInfo(formId);
        var columns = from field in form.Form.GetFields(true, true, true, false, false).Where(x => x.Name == currentField)
                      select new
                      {
                          Text = string.Format("{0} ({1})", field.GetDisplayName(MacroContext.CurrentResolver), field.Name),
                          Value = field.Name
                      };
        dropDownList.DataSource = columns;
        dropDownList.DataTextField = "Text";
        dropDownList.DataValueField = "Value";
        dropDownList.DataBind();
    }

    public override bool IsValid()
    {
        return base.IsValid();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack )
        {
            //Bind();
        }
    }
}