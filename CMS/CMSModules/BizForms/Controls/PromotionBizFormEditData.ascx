<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PromotionBizFormEditData.ascx.cs" Inherits="CMSModules_BizForms_Controls_PromotionBizFormEditData" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Namespace="CMS.UIControls.UniGridConfig" TagPrefix="ug" Assembly="CMS.UIControls" %>

<cms:UniGrid runat="server" ID="gridData" IsLiveSite="false">
    <GridActions>
        <ug:action name="edit" externalsourcename="edit" caption="$General.Edit$" FontIconClass="icon-edit" FontIconStyle="Allow" />
        <ug:action name="delete" caption="$General.Delete$" FontIconClass="icon-bin" FontIconStyle="Critical" confirmation="$general.confirmdelete$"/>
        <ug:action name="syncpromotion" externalsourcename="syncpromotion" caption="Resync Promotion" FontIconClass="icon-rotate-double-right" confirmation="Resync promotion?" />
        <ug:action name="syncpromotionproducts" externalsourcename="syncpromotionproducts" caption="Resync Promotion Products" FontIconClass="icon-rotate-double-right" confirmation="Resync promotion products?" />
    </GridActions>
    <GridColumns>
    </GridColumns>
    <GridOptions DisplayFilter="true" />
</cms:UniGrid>
<!-- TODO Custom Filter? -->

<script>
    function ExportAllAttachments(id) {
        $cmsj.ajax({
            url: '/CMSPages/Custom/ExportEntryAttachments.ashx',
            data: { formId: id }
        });
    }
</script>
