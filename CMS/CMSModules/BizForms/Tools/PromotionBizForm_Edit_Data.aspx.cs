﻿using System;
using System.Linq;
using CMS.Base.Web.UI;
using CMS.Helpers;
using CMS.OnlineForms;
using CMS.OnlineForms.Web.UI;
using CMS.UIControls;
using WF.Domain.Classes;
using WF.Promotions.Classes;

// Edited object
[EditedObject(BizFormInfo.OBJECT_TYPE, "formId")]
[Security(Resource = "CMS.Form", Permission= "ReadData")]
[UIElement("CMS.Form", "Forms.Data")]
public partial class CMSModules_BizForms_Tools_PromotionBizForm_Edit_Data : CMSBizFormPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (QueryHelper.GetInteger("formId", 0) <= 0 && QueryHelper.GetInteger("objectId", 0) > 0)
        {
            var promo = PromotionInfoProvider.GetPromotionInfo(QueryHelper.GetInteger("objectId", 0));
            if (promo != null)
            {
                var url = URLHelper.GetFullApplicationUrl() + RequestContext.CurrentURL;
                url = URLHelper.RemoveParameterFromUrl(url, "objectId");
                url = URLHelper.AddParameterToUrl(url, "formId", promo.PromotionBizFormID.ToString());
                URLHelper.Redirect(url);
            }
        }

        ScriptHelper.RefreshTabHeader(Page);
    }
}
