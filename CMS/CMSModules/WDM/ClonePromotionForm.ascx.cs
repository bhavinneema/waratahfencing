﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Base.Web.UI;
using CMS.FormEngine;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.OnlineForms;
using CMS.PortalEngine.Web.UI;
using CMS.UIControls;
using WF.Domain.Classes;
using WF.Promotions.Classes;
using WF.Promotions.CloneServices;
using Page = System.Web.UI.Page;

public partial class CMSModules_WDM_ClonePromotionForm : CMSAbstractUIWebpart
{
    private BizFormInfo PromotionBizForm => (BizFormInfo)UIContext.EditedObject;
    private PromotionInfo Promotion => PromotionInfoProvider.GetPromotions().WhereEquals("PromotionBizFormID", PromotionBizForm.FormID).FirstObject;

    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateForm();

        ScriptHelper.RegisterWOpenerScript(Page);
        ScriptHelper.RegisterDialogScript(Page);
        ScriptHelper.RegisterCloseDialogScript(Page);
    }

    private FormInfo GenerateForm()
    {
        var fi = new FormInfo();

        fi.AddFormItem(GenerateField("OldPromotionName", true, 200, "text", FormFieldControlTypeEnum.LabelControl, "LabelControl", "Promotion to clone"));
        fi.AddFormItem(GenerateField("NewPromotionName", true, 200, "text", FormFieldControlTypeEnum.TextBoxControl, "TextBoxControl", "New promotion name"));

        form.FormInformation = fi;

        var data = new ContainerCustomData();
        data.SetValue("OldPromotionName", Promotion.PromotionName);
        form.LoadData(data);

        return fi;
    }

    private FormFieldInfo GenerateField(string fieldName, bool required, int fieldSize, string dataType, FormFieldControlTypeEnum fieldType, string controlName, string fieldCaption = "", string explanationText = "")
    {
        // Construct form field properties
        var formFieldProperties = new Hashtable
        {
            {"fieldcaption", fieldCaption },
            {"explanationtext", explanationText }
        };

        // Construct form field settings
        var formFieldSettings = new Hashtable
        {
            {"controlname", controlName }
        };

        var formField = new FormFieldInfo
        {
            Name = fieldName,
            AllowEmpty = !required,
            Size = fieldSize,
            Visible = true,
            DataType = dataType,
            FieldType = fieldType,
            Guid = new Guid(),
            Properties = formFieldProperties,
            Settings = formFieldSettings
        };
        return formField;
    }

    protected void form_OnOnAfterSave(object sender, EventArgs e)
    {
        var form = (BasicForm)sender;

        var promoCloneService = new PromotionCloneService();
        promoCloneService.Clone(Promotion, $"{form.GetFieldValue("NewPromotionName")}");

        var script = @"var $refreshWindow = wopener.GetTop();
while ($refreshWindow.frames.length > 0) {
    $refreshWindow = $refreshWindow.frames[0];
}
$refreshWindow.location.reload(true);
CloseDialog(true);";
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "close", script, true);
    }
}