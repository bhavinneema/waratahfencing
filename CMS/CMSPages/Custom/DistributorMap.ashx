﻿<%@ WebHandler Language="C#" Class="DistributorMap" %>

using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.Membership;
using CMS.Localization;
using WF.Domain.Classes;
using WF.Distributors.Classes;

public class DistributorMap : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        var result = new Dictionary<string, object>();

        var country = GetCountry();
        result.Add("country", country);
        result.Add("mapbounds", MapBounds());
        result.Add("mapcenter", MapCenter());

        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

        var dt = DistributorInfoProvider.GetDistributors().WhereEquals("DistributorCountry", country).WhereNotNull("DistributorLat").WhereNotNull("DistributorLng").WhereEquals("DistributorType", "Outlet").WhereEquals("DistributorStatus", "Active").Tables[0];

        Dictionary<string, object> row;
        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        result.Add("locations", rows);

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        context.Response.ContentType = "application/json";
        context.Response.Write(serializer.Serialize(result));
    }

    private string GetCountry()
    {
        var cultureCode = LocalizationContext.CurrentCulture.CultureCode;
        switch (cultureCode)
        {
            case "en-US":
                return "United States of America";
            case "en-NZ":
                return "New Zealand";
            default:
                return "Australia";
        }
    }

    private Dictionary<string, Dictionary<string, double>> MapBounds()
    {
        var x = new List<string> { "", "" };
        var cultureCode = LocalizationContext.CurrentCulture.CultureCode;
        switch (cultureCode)
        {
            case "en-US":
                return new Dictionary<string, Dictionary<string, double>> {
                    { "ne", new Dictionary<string, double> { { "lat", 71.5388001 }, { "lng", -66.885417 } } },
                    { "sw", new Dictionary<string, double> { { "lat", 18.7763 }, { "lng", 170.5957 } } }
                };
            case "en-NZ":
                return new Dictionary<string, Dictionary<string, double>> {
                    { "ne", new Dictionary<string, double> { { "lat", -28.8773225 }, { "lng", -175.1235077 } } },
                    { "sw", new Dictionary<string, double> { { "lat", -52.7224663 }, { "lng", 165.7437641 } } }
                };
            default:
                return new Dictionary<string, Dictionary<string, double>> {
                    { "ne", new Dictionary<string, double> { { "lat", -0.6911343999999999 }, { "lng", 166.7429167 } } },
                    { "sw", new Dictionary<string, double> { { "lat", -51.66332320000001 }, { "lng", 100.0911072 } } }
                };
        }
    }private Dictionary<string, double> MapCenter()
    {
        var x = new List<string> { "", "" };
        var cultureCode = LocalizationContext.CurrentCulture.CultureCode;
        switch (cultureCode)
        {
            case "en-US":
                return new Dictionary<string, double> { { "lat", 37.09024 }, { "lng", -95.712891 } };
            case "en-NZ":
                return new Dictionary<string, double> { { "lat", -40.900557 }, { "lng", 174.885971 } };
            default:
                return new Dictionary<string, double> { { "lat", -25.274398 }, { "lng", 133.775136 } };
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}