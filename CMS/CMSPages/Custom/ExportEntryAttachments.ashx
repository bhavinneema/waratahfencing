﻿<%@ WebHandler Language="C#" Class="ExportEntryAttachments" %>

using System;
using System.IO;
using System.Net;
using System.Web;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.OnlineForms;
using CMS.SiteProvider;
using Ionic.Zip;
using CMS.EventLog;
using WF;
using WF.Domain.Classes;
using WF.Promotions.Classes;

public class ExportEntryAttachments : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        var bizFormId = ValidationHelper.GetInteger(context.Request["formId"], 0);
        var bfi = BizFormInfoProvider.GetBizFormInfo(bizFormId);
        if (bfi == null)
            throw new InvalidOperationException();
        var dci = DataClassInfoProvider.GetDataClassInfo(bfi.FormClassID);
        if (dci == null)
            throw new InvalidOperationException();
        var entries = BizFormItemProvider.GetItems(dci.ClassName)
                .WhereEquals("FormComplete", true);
        if (entries.Count <= 0)
            throw new InvalidOperationException();

        var zip = new ZipFile();

        foreach (var entry in entries)
        {
            var promotionEntry = PromotionEntryInfoProvider.GetPromotionEntries()
                .WhereEquals("PromotionEntryFormID", bfi.FormID)
                .WhereEquals("PromotionEntryFormItemID", entry.ItemID)
                .FirstObject;
            var attachmentValue = entry.GetStringValue("PromotionEntryReceipt", string.Empty);
            if (attachmentValue.Length == 0 || promotionEntry == null)
                continue;
            var fileNameGuid = attachmentValue.Split('/')[0];
            using (WebClient wc = new WebClient())
            {
                try
                {
                    var urlFormat = URLHelper.GetFullApplicationUrl() +
                                    "/CMSPages/GetBizFormFile.aspx?filename={0}&sitename={1}";
                    var url = string.Format(urlFormat, fileNameGuid, SiteContext.CurrentSiteName);
                    var data = wc.DownloadData(url);
                    zip.AddEntry(promotionEntry.PromotionEntryClaimID+Path.GetExtension(fileNameGuid), data);
                }
                catch(Exception ex)
                {
                    EventLogProvider.LogException("ExportEntryAttachments", "ProcessRequest", ex);
                }
            }
        }
        var ms = new MemoryStream();
        zip.Save(ms);
        context.Response.AddHeader("Content-Type", "application/zip");
        context.Response.AddHeader("Content-Disposition", "attachment; filename='zipFile.zip'");
        context.Response.ContentType = "application/force-download";
        context.Response.BinaryWrite(ms.ToArray());
        context.Response.Flush();
        context.Response.Close();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}