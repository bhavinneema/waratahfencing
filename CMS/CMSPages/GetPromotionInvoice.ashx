﻿<%@ WebHandler Language="C#" Class="GetPromotionInvoiceHandler" %>

using System;
using System.Web;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.IO;
using CMS.Routing.Web;

public class GetPromotionInvoiceHandler : AdvancedGetFileHandler
{
    /// <summary>
    /// AdvancedGetFileHandler forces to implement AllowCache property.
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            return false;
        }
        set
        {
        }
    }

    /// <summary>Provides biz form file.</summary>
    /// <param name="context">Handler context</param>
    protected override void ProcessRequestInternal(HttpContextBase context)
    {
        string fileName = QueryHelper.GetString("filename", string.Empty);
        string siteName = QueryHelper.GetString("sitename", this.CurrentSiteName);
        if (!ValidationHelper.IsFileName((object)fileName) || siteName == null)
            RequestHelper.Respond404();
        string filePhysicalPath = FormHelper.GetFilePhysicalPath(siteName, fileName, (string)null);
        if (!File.Exists(filePhysicalPath))
            RequestHelper.Respond404();
        CookieHelper.ClearResponseCookies();
        this.Response.Clear();
        string extension = Path.GetExtension(filePhysicalPath);
        this.Response.ContentType = MimeTypeHelper.GetMimetype(extension, "application/octet-stream");
        this.SetDisposition(fileName, extension);
        this.WriteFile(filePhysicalPath, false);
        this.CompleteRequest();
    }
}