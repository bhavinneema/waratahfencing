﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreMe.aspx.cs" Inherits="CMSPages_StoreMe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" AssociatedControlID="drpObjectType">Object Type:</asp:Label> <asp:DropDownList runat="server" ID="drpObjectType"></asp:DropDownList><br />
            <asp:Button runat="server" ID="btnSerialise" Text="Serialise" OnClick="btnSerialise_OnClick" />
            <br/><br/>
            <asp:Literal runat="server" ID="ltrResult"></asp:Literal>
        </div>
    </form>
</body>
</html>
