using System;
using System.Text;
using CMS.Base;
using CMS.ContinuousIntegration;
using CMS.DataEngine;

public partial class CMSPages_StoreMe : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        var objectTypes = DataClassInfoProvider.GetClasses().OrderBy("ClassName");
        drpObjectType.DataSource = objectTypes;
        drpObjectType.DataTextField = "ClassName";
        drpObjectType.DataValueField = "ClassName";
        drpObjectType.DataBind();
    }

    protected void btnSerialise_OnClick(object sender, EventArgs e)
    {
        var log = new StringBuilder();

        // Disables automatic processing of new smart search indexing tasks during the restore operation to prevent potential conflicts
        // Indexing tasks are still created, but processed after the restore is finished
        using (new CMSActionContext { EnableSmartSearchIndexer = false })
        {
            var moduleObj = ModuleManager.GetReadOnlyObject(drpObjectType.SelectedValue);

            if (moduleObj != null)
            {
                var objects = new ObjectQuery(moduleObj.TypeInfo.ObjectType).TypedResult;

                foreach (var obj in objects)
                {
                    var result = FileSystemRepositoryManager.Store(obj);

                    log.Append($"Storing <strong>{obj.TypeInfo.ObjectType}</strong> with ID <strong>{obj.Generalized.ObjectID}</strong> ");

                    log.Append(result
                        ? "<span style='color: green'>OK</span>"
                        : "<span style='color: red'>FAILED</span>");

                    log.Append("<br />");
                }
            }
        }

        ltrResult.Text = log.ToString();
    }
}