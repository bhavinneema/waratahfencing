<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_BizForms_bizform" CodeFile="~/CMSWebParts/BizForms/bizform.ascx.cs" %>

<%@ Register TagPrefix="KenticoTricks" Namespace="WTF.SystemModule.CustomFormLayout" Assembly="WTF.SystemModule" %>
<asp:PlaceHolder runat="server">
    <KenticoTricks:CustomBizForm ID="viewBiz" runat="server" IsLiveSite="true" />

    <script>
        Sys.Application.add_load(function () {
            $('#<%=viewBiz.ClientID%> .contact-form-validation > span.EditingFormErrorLabel').each(function () {
                if ($(this).is(':visible')) {
                    $(this).parent().parent().addClass('has-error');
                } else {
                    $(this).parent().parent().removeClass('has-error');
                }
            });
        });
    </script>
</asp:PlaceHolder>
