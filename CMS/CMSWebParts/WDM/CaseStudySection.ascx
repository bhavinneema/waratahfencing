﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseStudySection.ascx.cs" Inherits="CMSWebParts_WDM_CaseStudySection" %>
<%@ Register Src="~/CMSWebParts/WDM/UserControls/CaseStudyControl.ascx" TagPrefix="uc1" TagName="CaseStudyControl" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <uc1:CaseStudyControl runat="server" ID="CaseStudyControl" />
      
    </ContentTemplate>

</asp:UpdatePanel>


<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
        <div id="preloader"></div>
    </ProgressTemplate>


</asp:UpdateProgress>
