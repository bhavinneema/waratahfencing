﻿using CMS.Helpers;
using CMS.PortalEngine.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMSWebParts_WDM_CaseStudySection : CMSAbstractWebPart
{ 
    #region "Properties"
/// <summary>
/// Gets or sets the path of the documents.
/// </summary>
public string Path
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("Path"), null);
        }
        set
        {
            SetValue("Path", value);
        }
    }

    /// <summary>
    /// Gets or sets the columns to get.
    /// </summary>
    public string Columns
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("Columns"), string.Empty);
        }
        set
        {
            SetValue("Columns", value);
        }
    }

    /// <summary>
    /// Gets or sets the name of the transforamtion which is used for displaying the results.
    /// </summary>
    public string CaseStudyTransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CaseStudyTransformationName"), string.Empty);
        }
        set
        {
            SetValue("CaseStudyTransformationName", value);
        }
    }


    public bool PaggingEnabled
    {
        get { return ValidationHelper.GetBoolean(GetValue("PaggingEnabled"), false); }
        set
        {
            SetValue("PaggingEnabled", value);
        }
    }

    public string NoResultText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("NoResultText"), string.Empty);
        }
        set
        {
            SetValue("NoResultText", value);
        }
    }
    public int PageSize
    {
        get { return ValidationHelper.GetInteger(GetValue("PageSize"), 10); }
        set
        {
            SetValue("PageSize", value);
        }
    }

    public int PageGroupSize
    {
        get { return ValidationHelper.GetInteger(GetValue("PageGroupSize"), 10); }
        set
        {
            SetValue("PageGroupSize", value);
        }
    }
    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {

            CaseStudyControl.Path = Path;
            CaseStudyControl.CaseStudyTransformationName = CaseStudyTransformationName;
            CaseStudyControl.PaggingEnabled = PaggingEnabled;
            CaseStudyControl.PageSize = PageSize;
            CaseStudyControl.noOfPagesDisplay = PageGroupSize;
            CaseStudyControl.NoResultText = NoResultText;


        }
    }
    

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}

