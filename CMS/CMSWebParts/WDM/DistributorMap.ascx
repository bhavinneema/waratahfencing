<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_WDM_DistributorMap" CodeFile="~/CMSWebParts/WDM/DistributorMap.ascx.cs" %>

<asp:Panel runat="server" CssClass="row" DefaultButton="btnGetLocation">
    <div class="col-xs-12">
        <div class="input-group">
            <input type="text" class="form-control" id="txtLocation" placeholder="enter postcode, city or state...">
            <span class="input-group-btn">
                <asp:Button CssClass="btn btn-default" ID="btnGetLocation" Text="Go!" runat="server"></asp:Button>
            </span>
        </div>
    </div>
</asp:Panel>
<!-- /input-group -->
<div class="row">
    <div class="col-sm-12">
        <div id="map" style="padding-top: 75%;"></div>
    </div>
    <div class="col-sm-4">
        <div id="list" class="list-group"></div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            url: '/CMSPages/Custom/DistributorMap.ashx',
            success: function (result) {
                var map = initMap(result);
                $('#<%=btnGetLocation.ClientID%>').click(function (e) {
                    e.preventDefault();
                    $('#txtLocation').blur();
                    $.ajax({
                        url: 'https://maps.googleapis.com/maps/api/geocode/json',
                        data: { address: $('#txtLocation').val() + ', ' + result.country, key: 'AIzaSyBZYMEkj1zuzWGn3PcJaAZapDN6IYbOuHY' },
                        success: function (result) {
                            if (result.results.length > 0) {
                                $('#txtLocation').val(result.results[0].formatted_address);
                                map.setCenter(result.results[0].geometry.location);
                                map.setZoom(12);
                            } else {
                                console.log(result);
                                // todo no results found from geocode service
                            }
                        }
                    });
                });
            }
        });
    });

    function initMap(data) {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            center: data.mapcenter
        });

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = data.locations.map(function (location, i) {
            return new google.maps.Marker({
                position: { lat: location.DistributorLat, lng: location.DistributorLng },
                starpartner: location.DistributorStarPartner,
                //label: location.title
                title: location.DistributorName,
                address1: location.DistributorAddress1 || '' + ' ' + location.DistributorAddress2 || '',
                address2: location.DistributorCity || '' + ' ' + location.DistributorPostcode || '',
                tel: location.DistributorPhone || ''
            });
        });

        // Add event
        map.addListener('bounds_changed', function () {
            if (map.getZoom() > 9) {
                $('#list').empty();
                var items = [];
                // 3 seconds after the center of the map has changed, pan back to the
                // marker.
                $.each(markers, function (i, e) {
                    if (map.getBounds().contains(e.getPosition())) {
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(map.getCenter(), e.getPosition());
                        var container = $('<a />', { 'class': 'list-group-item', 'href': '#', 'data-starpartner': e.starpartner, 'data-distance': distance, 'data-lat': e.position.lat(), 'data-lng': e.position.lng() }).click(focusMarker);
                        if (e.starpartner)
                            container.addClass('star-partner');
                        var heading = $('<h4 />', { 'class': 'list-group-item-heading', text: e.title }).appendTo(container);
                        var content = $('<ul />', { 'class': 'list-group-item-text' }).appendTo(container);
                        if (e.address1 != null)
                            $('<li />', { 'class': 'list-item', text: e.address1 }).appendTo(content);
                        if (e.address2 != null)
                            $('<li />', { 'class': 'list-item', text: e.address2 }).appendTo(content);
                        if (e.tel != null)
                            $('<li />', { 'class': 'list-item', text: e.tel }).appendTo(content);

                        items.push(container);
                    }
                });
                items.sort(function (a, b) {
                    if (a.attr('data-starpartner') != b.attr('data-starpartner')) {
                        if (b.attr('data-starpartner') == 'true')
                            return 1;
                        else
                            return -1;
                    } else {
                        var aDist = parseFloat(a.attr('data-distance'));
                        var bDist = parseFloat(b.attr('data-distance'));
                        if (aDist > bDist)
                            return 1;
                        else if (aDist < bDist)
                            return -1;
                        else
                            return 0;
                    }
                    return a.attr('data-distance') - b.attr('data-distance');
                });
                if (items.length > 0)
                    $('#list').html(items);
                else
                    $('#list').html($('<div />', { text: 'No distributors in this area.' }));
            } else {
                $('#list').html($('<div />', { text: 'Zoom in to view detailed results.' }));
            }
        });

        function focusMarker(e) {
            e.preventDefault();
            var lat = parseFloat($(this).attr('data-lat'));
            var lng = parseFloat($(this).attr('data-lng'));
            console.log(lat + ':' + lng);
            if (!isNaN(lat) && !isNaN(lng)) {
                var position = { lat: lat, lng: lng };
                map.setCenter(position);
                map.setZoom(12);
            }
        }

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });

        return map;
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZYMEkj1zuzWGn3PcJaAZapDN6IYbOuHY&libraries=geometry">
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
