<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_WDM_ProductSearch"  CodeFile="~/CMSWebParts/WDM/ProductSearch.ascx.cs" %>

<div class="tile-grey product-search-wrapper">
    <h3>Product Quick Search</h3>

    <div class="product-search-ddl">
        <asp:DropDownList ID="ddlCategory" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    </div>
    <div class="product-search-ddl">
        <asp:DropDownList ID="ddlProduct" runat="server"></asp:DropDownList>
    </div>
    <div class="product-search-btn">
        <asp:Button ID="btnSubmit" runat="server" Text="FIND" OnClick="btnSubmit_Click" />
    </div>
</div>