﻿using System;
using System.Web.UI.WebControls;

using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_WDM_ProductSearch : CMSAbstractWebPart
{

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        if (IsPostBack) return;

        //Populate categories.
        var provider = new TreeProvider();
        var categories = provider.SelectNodes()
            .Columns("NodeID, DocumentName")
            .Path("/Products/%")
            .NestingLevel(1)
            .OrderBy("NodeLevel, NodeOrder, NodeName")
            .Published()
            .Result;

        ddlCategory.DataValueField = "NodeID";
        ddlCategory.DataTextField = "DocumentName";
        ddlCategory.DataSource = categories;
        ddlCategory.DataBind();

        ddlCategory.Items.Insert(0, new ListItem("[Category]", "0"));
        ddlProduct.Items.Insert(0, new ListItem("[Products]", "0"));


        //Populate products with ALL products on first load.
        

        var provider2 = new TreeProvider();
        var products = provider2.SelectNodes("CMS.Product")
            .Columns("NodeAliasPath, DocumentName")
            .Path("/products/%")
            .OrderBy("NodeLevel, NodeOrder, NodeName")
            .Published()
            .Result;

        ddlProduct.DataValueField = "NodeAliasPath";
        ddlProduct.DataTextField = "DocumentName";
        ddlProduct.DataSource = products;
        ddlProduct.DataBind();

        ddlProduct.Items.Insert(0, new ListItem("[Products]", "0"));
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int categoryNodeID = ValidationHelper.GetInteger(ddlCategory.SelectedValue, 0);

        var provider = new TreeProvider();
        var products = provider.SelectNodes()
            .Columns("NodeAliasPath, DocumentName")
            .WhereEquals("NodeParentID", categoryNodeID)
            .OrderBy("NodeLevel, NodeOrder, NodeName")
            .Published()
            .Result;

        ddlProduct.DataValueField = "NodeAliasPath";
        ddlProduct.DataTextField = "DocumentName";
        ddlProduct.DataSource = products;
        ddlProduct.DataBind();

        ddlProduct.Items.Insert(0, new ListItem("[Products]", "0"));
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(ddlProduct.SelectedValue == "0" && ddlCategory.SelectedValue != "0")
        {
            int categoryID = ValidationHelper.GetInteger(ddlCategory.SelectedValue, 0);

            var provider = new TreeProvider();
            var category = provider.SelectNodes("WDM.ProductFolder")
                .WhereEquals("NodeID", categoryID)
                .Path("/Products/%")
                .Published()
                .FirstObject;

            if (category == null) return;

            Response.Redirect(category.NodeAliasPath, true);
        }

        string nodealiaspath = ValidationHelper.GetString(ddlProduct.SelectedValue, string.Empty);
        if (string.IsNullOrEmpty(nodealiaspath)) return;

        if (ddlProduct.SelectedItem.Text == "[Products]")
        {
            Response.Write("<script>alert('Please select a product before clicking find');</script>");
        }
        else
        {
            Response.Redirect(nodealiaspath, true);
        }
    }
}