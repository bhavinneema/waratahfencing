﻿using System;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalEngine.Web.UI;
using CMS.SiteProvider;
using CMS.WebAnalytics;
using WF.Domain.Classes;
using CMS.OnlineForms;
using CMS.PortalEngine;
using CMS.Membership;
using WF;
using WF.Promotions.Classes;

public partial class CMSWebParts_WDM_PromotionForm : CMSAbstractWebPart
{
    #region "Properties"

    public string PromotionLandingPageUrlAlias
    {
        get { return QueryHelper.GetString("promotionurlalias", string.Empty); }
    }

    public PromotionInfo Promotion => PromotionInfoProvider.GetPromotions()
        .WhereEquals("PromotionLandingPageURLAlias", PromotionLandingPageUrlAlias)
        .FirstObject;

    /// <summary>
    /// Gets or sets the form name of BizForm.
    /// </summary>
    public string BizFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BizFormName"), "");
        }
        set
        {
            SetValue("BizFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the alternative form full name (ClassName.AlternativeFormName).
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AlternativeFormName"), "");
        }
        set
        {
            SetValue("AlternativeFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the site name.
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SiteName"), "");
        }
        set
        {
            SetValue("SiteName", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the WebPart use colon behind label.
    /// </summary>
    public bool UseColonBehindLabel
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("UseColonBehindLabel"), true);
        }
        set
        {
            SetValue("UseColonBehindLabel", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after validation failed.
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ValidationErrorMessage"), "");
        }
        set
        {
            SetValue("ValidationErrorMessage", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TrackConversionName"), "");
        }
        set
        {
            if (value.Length > 400)
            {
                value = value.Substring(0, 400);
            }
            SetValue("TrackConversionName", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion value used after successful registration.
    /// </summary>
    public double ConversionValue
    {
        get
        {
            return ValidationHelper.GetDoubleSystem(GetValue("ConversionValue"), 0);
        }
        set
        {
            SetValue("ConversionValue", value);
        }
    }

    #endregion


    #region "Methods"

    protected override void OnLoad(EventArgs e)
    {
        viewBiz.OnAfterSave += viewBiz_OnAfterSave;
        base.OnLoad(e);
    }


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data for partial caching.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
            viewBiz.StopProcessing = true;
        }
        else
        {
            var currentDateTime = DateTime.Now;

            if ((Promotion == null || Promotion.PromotionSiteID != SiteContext.CurrentSiteID) && PortalContext.ViewMode == ViewModeEnum.LiveSite)
            {
                URLHelper.Redirect(string.Format("~/Promotions/{0}/Form/Expired", Promotion.PromotionLandingPageURLAlias));
                return;
            }
            else if (Promotion == null)
                return;

            if (Promotion != null && ((Promotion.PromotionDateTo != DateTimeHelper.ZERO_TIME && currentDateTime < Promotion.PromotionDateFrom) || (Promotion.PromotionDateTo != DateTimeHelper.ZERO_TIME && currentDateTime > Promotion.PromotionDateTo) || !Promotion.PromotionActive) && PortalContext.ViewMode == ViewModeEnum.LiveSite)
                URLHelper.Redirect(string.Format("~/Promotions/{0}/Form/Expired", Promotion.PromotionLandingPageURLAlias));

            var promotionForm = BizFormInfoProvider.GetBizFormInfo(Promotion.PromotionBizFormID);

            if (promotionForm != null)
            {

                var dci = DataClassInfoProvider.GetDataClassInfo(promotionForm.FormClassID);
                // Set BizForm properties
                viewBiz.FormName = promotionForm.FormName;
                viewBiz.SiteName = SiteContext.CurrentSite.SiteName;
                viewBiz.UseColonBehindLabel = UseColonBehindLabel;
                viewBiz.AlternativeFormFullName = $"{dci.ClassName}.PublicFacing";
                viewBiz.ValidationErrorMessage = ValidationErrorMessage;

                viewBiz.Visible = true;
                lblError.Visible = false;

                // Set the live site context
                if (viewBiz != null)
                {
                    viewBiz.ControlContext.ContextName = CMS.Base.Web.UI.ControlContext.LIVE_SITE;
                }
            }
        }
    }


    private void viewBiz_OnAfterSave(object sender, EventArgs e)
    {
        var bizFormEntry = viewBiz.EditedObject as BizFormItem;

        var formId = bizFormEntry.BizFormInfo.FormID;
        var formItemId = bizFormEntry.ItemID;
        var claimId = string.Format("{0:D4}{1:D4}", formId, formItemId);

        if (TrackConversionName != String.Empty)
        {
            string siteName = SiteContext.CurrentSiteName;

            if (AnalyticsHelper.AnalyticsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, RequestContext.UserHostAddress))
            {
                HitLogProvider.LogConversions(SiteContext.CurrentSiteName, LocalizationContext.PreferredCultureCode, TrackConversionName, 0, ConversionValue);
            }
        }

        URLHelper.Redirect(string.Format("~/Promotions/{0}/Form/Thankyou/{1}", Promotion.PromotionLandingPageURLAlias, claimId));

    }

    #endregion
}