using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using CMS.PortalEngine;
using CMS.OnlineForms;
using WF.Domain.Classes;
using CMS.SiteProvider;
using WF.Promotions.Classes;

public partial class CMSWebParts_WDM_PromotionStatus : CMSAbstractWebPart
{
    #region "Properties"

    public string PromotionLandingPageUrlAlias
    {
        get { return QueryHelper.GetString("promotionurlalias", string.Empty); }
    }

    public PromotionInfo Promotion => PromotionInfoProvider.GetPromotions()
        .WhereEquals("PromotionLandingPageURLAlias", PromotionLandingPageUrlAlias.ToString())
        .FirstObject;


    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            var currentDateTime = DateTime.Now;

            if (Promotion == null || Promotion.PromotionSiteID != SiteContext.CurrentSiteID)
            {
                ltrResult.Text = "Promotion could not be found"; // TODO format output
                return;
            }

            if (Promotion != null 
                && (currentDateTime < Promotion.PromotionDateFrom || currentDateTime > Promotion.PromotionDateTo || !Promotion.PromotionActive) 
                && PortalContext.ViewMode == ViewModeEnum.LiveSite)
                ltrResult.Text = Promotion.PromotionExpiredContent; // TODO format output
            else if (Promotion != null 
                && (currentDateTime > Promotion.PromotionDateFrom && currentDateTime < Promotion.PromotionDateTo && Promotion.PromotionActive) 
                && PortalContext.ViewMode == ViewModeEnum.LiveSite)
                URLHelper.Redirect(string.Format("~/Promotions/{0}/Form", Promotion.PromotionLandingPageURLAlias));

            var promotionForm = BizFormInfoProvider.GetBizFormInfo(Promotion.PromotionBizFormID);

            if (promotionForm == null)
            {
                ltrResult.Text = Promotion.PromotionExpiredContent; // TODO format output
                return;
            }
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



