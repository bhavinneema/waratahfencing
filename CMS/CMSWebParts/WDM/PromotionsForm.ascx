﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PromotionsForm.ascx.cs" Inherits="CMSWebParts_WDM_PromotionsForm" %>

<asp:Panel ID="pnlNoPromotion" Visible="false" runat="server">
</asp:Panel>
<asp:Panel ID="pnlPromotion" Visible="true" runat="server">
    <div>
        <div class="row">
            <div class="col-md-3">State</div>
            <div class="col-md-9">
                <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Suburb</div>
            <div class="col-md-9">
                <asp:DropDownList ID="ddlSuburb" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Store</div>
            <div class="col-md-9">
                <asp:DropDownList ID="ddlStore" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Invoice Date*</div>
            <div class="col-md-9">
                <asp:TextBox ID="calInvoiceDate" runat="server" CssClass="js-calendar"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Invoice Number*</div>
            <div class="col-md-9">
                <asp:TextBox ID="txtInvoiceNumber" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">Where did you hear abouut the Cashback promotion?</div>
            <div class="col-md-9">
                <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
            </div>
        </div>
        <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rptProducts_ItemDataBound" OnItemCommand="rptProducts_ItemCommand" OnItemCreated="rptProducts_ItemCreated">
            <ItemTemplate>
                <div>
                    <asp:Label ID="lblProductName" runat="server"></asp:Label>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
