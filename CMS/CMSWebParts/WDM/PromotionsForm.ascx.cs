﻿using CMS.Base.Web.UI;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.PortalEngine.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMSWebParts_WDM_PromotionsForm : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterScriptFile(Page, "~/CMSWebParts/WDM/promotionsForm.js");
        if (!IsPostBack)
        {
            //get promotion items from current promotion
        }
    }
    

    protected void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        
    }

    protected void rptProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       
    }

    protected void rptProducts_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
    }
}