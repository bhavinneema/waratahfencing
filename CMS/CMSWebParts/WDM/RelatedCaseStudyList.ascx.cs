﻿using CMS.Helpers;
using CMS.Localization;
using CMS.PortalEngine.Web.UI;
using CMS.SiteProvider;
using System;
using CMS.DocumentEngine;
using CMS.DataEngine;
using CMS.Taxonomy;
using WF.Domain.Classes;

public partial class CMSWebParts_WDM_RelatedCaseStudyList : CMSAbstractWebPart
{ 
    #region "Properties"

public string Path
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("Path"), null);
        }
        set
        {
            SetValue("Path", value);
        }
    }

    /// <summary>
    /// Gets or sets the name of the transforamtion which is used for displaying the results.
    /// </summary>
    public string TransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("TransformationName"), string.Empty);
        }
        set
        {
            SetValue("TransformationName", value);
        }
    }

    public int PageSize
    {
        get
        {
            return int.Parse(DataHelper.GetNotEmpty(GetValue("PageSize"), null));
        }
        set
        {
            SetValue("PageSize", value);
        }
    }

    public int RelatedDocumentID { get; set; }




    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            InitData();
        }
    }

    private void InitData()
    {
        // create query for getting case study pages
        var totalPages = 1;
        RelatedDocumentID = CurrentDocument.DocumentID;

        var caseStudyListQuery = CaseStudyProvider.GetCaseStudies()
            .Path(string.IsNullOrEmpty(Path) ? "/" : Path.Replace("%", string.Empty), PathTypeEnum.Children)
            .OrderByDescending(WellknownPageFields.DocumentPublishFrom, WellknownPageFields.DocumentLastPublished, WellknownPageFields.DocumentModifiedWhen)
            .Published();

        // exclude current document 
        caseStudyListQuery = caseStudyListQuery.Where(WellknownPageFields.DocumentID, QueryOperator.NotEquals, RelatedDocumentID);

        //
        var where = string.Format("{0} in (Select distinct {0} from CMS_DocumentCategory where {1} = {2})",
            WellknownPageFields.CategoryID,
            WellknownPageFields.DocumentID,
            RelatedDocumentID);

        var categoriesQuery = CategoryInfoProvider.GetCategories(where, WellknownPageFields.CategoryID);

        if (categoriesQuery.Count > 0)
        {
            var documentCategoriesQuery = DocumentCategoryInfoProvider.GetDocumentCategories()
            .WhereIn(WellknownPageFields.CategoryID, categoriesQuery)
            .Column(WellknownPageFields.DocumentID);

            caseStudyListQuery = caseStudyListQuery.WhereIn(WellknownPageFields.DocumentID, documentCategoriesQuery);
        }



        //// if pagging turns on

        totalPages = (int)Math.Ceiling(caseStudyListQuery.Count / (double)PageSize);
        caseStudyListQuery = caseStudyListQuery.Page(0, PageSize);


        // setup repeater control
        rptCaseStudy.DataSource = caseStudyListQuery;
        rptCaseStudy.TransformationName = TransformationName;
        rptCaseStudy.DataBind();


    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }

    #endregion
}