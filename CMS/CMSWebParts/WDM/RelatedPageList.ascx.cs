﻿using CMS.Helpers;
using CMS.Localization;
using CMS.PortalEngine.Web.UI;
using CMS.SiteProvider;
using System;
using CMS.DocumentEngine;


public partial class CMSWebParts_WDM_RelatedPageList : CMSAbstractWebPart
{
    #region "Properties"
    public string TransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("TransformationName"), string.Empty);
        }
        set
        {
            SetValue("TransformationName", value);
        }
    }

    public int PageSize
    {
        get
        {
            return int.Parse(DataHelper.GetNotEmpty(GetValue("PageSize"), null));
        }
        set
        {
            SetValue("PageSize", value);
        }
    }
    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            // check if stakeholder manager is selected or not

            CMSRepeater1.TransformationName = TransformationName;




            var releatedPages = DocumentHelper.GetDocuments()
                                 //  .Types("StandardAU.MenuPage", "custom.CaseStudy")
                                 //   .Path(string.IsNullOrEmpty(Path) ? "/" : Path.Replace("%", string.Empty), PathTypeEnum.Children)
                                 .InRelationWith(CurrentDocument.NodeGUID, "IsRelatedTo", RelationshipSideEnum.Both).TopN(PageSize);

            
            CMSRepeater1.DataSource = releatedPages;
            CMSRepeater1.DataBind();



        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }

    #endregion
}