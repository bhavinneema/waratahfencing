﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseStudyControl.ascx.cs" Inherits="CMSWebParts_WDM_UserControls_CaseStudyControl" %>
<%@ Register Src="~/CMSWebParts/WDM/UserControls/GoogleMap.ascx" TagPrefix="uc1" TagName="GoogleMap" %>

<div class="tab">

    <div class="tableft">
        <div class="tab-header-title">
            FILTER BY STATE
        </div>
        <asp:LinkButton class="tablinks all-web-tag" ID="btncountry" runat="server" Text='Australia'
            CommandName="Filter_Country" OnCommand="Filter_CaseStudyList"></asp:LinkButton>
        <asp:LinkButton class="tablinks all-mobile-tag" ID="btncountry2" runat="server" Text='All'
            CommandName="Filter_Country" OnCommand="Filter_CaseStudyList" CssClass="tablinks all-mobile-tag active"></asp:LinkButton>
        <asp:Repeater ID="rptStates" runat="server">
            <ItemTemplate>
                <asp:LinkButton class="tablinks" ID="btnStates" runat="server" Text='<%#Eval("StateCode") %>' CommandArgument='<%# Eval("StateCode") +","+ Eval("Lattitude")+","+ Eval("Longitude")  %>'
                    CommandName="Filter_States" OnCommand="Filter_CaseStudyList"></asp:LinkButton>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="tabright">
        <div class="tab-header-title">
            OR FILTER BY PRODUCT TYPE
        </div>
        <asp:LinkButton ID="imgPostsTag" class="tablinks" Text="Posts" OnCommand="Filter_CaseStudyList" CommandName="Filter_Tags" CommandArgument="Posts" runat="server" />
        <asp:LinkButton ID="imgWireTsg" class="tablinks" Text="Wire" OnCommand="Filter_CaseStudyList" CommandName="Filter_Tags" CommandArgument="Wire" runat="server" />
        <asp:LinkButton ID="imgAccessoriesTag" class="tablinks" Text="Accessories" OnCommand="Filter_CaseStudyList" CommandName="Filter_Tags" CommandArgument="Accessories" runat="server" />
    </div>
</div>

<div class="container-fluid">

    <div class="row">

        <div class="col-sm-6">
            <asp:TextBox ID="txtSearch" runat="server" ValidationGroup="search" class="search-box" placeholder="Search Case Studies" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtSearch" Display="None" ValidationGroup="search" class="required-text"></asp:RequiredFieldValidator>
            <asp:ImageButton ImageUrl="/Waratah/media/Generic-Site-Images/searchButton.png" ID="btnFilter" OnCommand="Filter_CaseStudyList" CommandName="Filter_SearchText" runat="server" AlternateText="Search Text" ValidationGroup="search" class="close-icon" />
            
            <asp:ImageButton ID="btnEmptyFilter" ImageUrl="/Waratah/media/Generic-Site-Images/xbutton.png" OnCommand="Filter_CaseStudyList" CommandName="Empty_SearchText" runat="server" AlternateText="Empty Text" Visible="false" class="close-icon" />
            <div style="display: none">
                <asp:ImageButton ImageUrl="/Waratah/media/Generic-Site-Images/searchButton.png" ID="ImageButton1" OnCommand="Filter_CaseStudyList" CommandName="Filter_SearchText" runat="server" AlternateText="Search Text" ValidationGroup="search" class="search-enter" />
            </div>
            <p>
                <br />
                <cms:CMSEditableRegion ID="CMSEditableRegion2" runat="server" RegionTitle="List Description" DialogHeight="50" RegionType="TextArea" />

            </p>
            <div class="row row-list">

                <cms:CMSRepeater ID="caseStudyRpt" runat="server">
                </cms:CMSRepeater>

            </div>
        </div>

        <div class="col-sm-6">

            <uc1:GoogleMap runat="server" ID="GoogleMap" />


            <div class="row"></div>
            <div class="row contactus-background">
                <cms:CMSEditableRegion ID="CMSEditableRegion1" runat="server" RegionTitle="Portfolio Section" RegionType="HTMLEditor" HtmlAreaToolbarLocation="In" />

            </div>
        </div>
    </div>

    <div class="row pagination-row">
        <div class="col-md-6 col-md-offset-4">
            <div class="pagination">
                <asp:LinkButton ID="lnkPrevious" runat="server" Text="<" OnCommand="Page_Move" CssClass="active" CommandArgument="Previous"></asp:LinkButton>
                <asp:LinkButton ID="lnkFirst" runat="server" Text="1" OnCommand="Page_Move" CommandArgument="First"></asp:LinkButton>


                <asp:LinkButton ID="lnkPreviousGroup" runat="server" Text="..." OnCommand="Page_Move" CommandArgument="PreviousGroup"></asp:LinkButton>


                <asp:Repeater ID="rptPager" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                            CssClass='<%# Convert.ToBoolean(Eval("Enabled")) ? "active" : "page_disabled" %>'
                            OnClick="Page_Changed" OnClientClick='<%# Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:LinkButton ID="lnkNextGroup" runat="server" Text="..." OnCommand="Page_Move" CommandArgument="NextGroup"></asp:LinkButton>
                <asp:LinkButton ID="lnkLast" runat="server" Text=">|" OnCommand="Page_Move" CommandArgument="Last"></asp:LinkButton>
                <asp:LinkButton ID="lnkNext" runat="server" Text=">" OnCommand="Page_Move" CssClass="active" CommandArgument="Next"></asp:LinkButton>

            </div>
        </div>
    </div>

    <div class="row pagination-title" align="center">
        <asp:Literal ID="litPagingText" runat="server"></asp:Literal>

    </div>

</div>


<script>
  
    $(document).ready(function () {

        $('.search-box').live("keypress", function (e) {

            if (e.keyCode == 13) {
                e.preventDefault();
                $('.search-enter').click();
                
            }            
        });
        $('.search-box').focus();

    });


    if (window.innerWidth <= 800 && window.innerHeight <= 600) {
        document.getElementsByClassName("nav")[0].style.display = "none";
    }

    function setActive(name) {

        $('.tableft a').each(function () {

            if (name == this.text) {
                $(this).addClass('active');

            }
            else
                $(this).removeClass('active');

            if (name == 'Australia') {
                if (this.text == "All")
                    $(this).addClass('active');;
            }

        });

        $('.tabright a').each(function () {
            if (name == this.text) {
                $(this).addClass('active');

            }
            else
                $(this).removeClass('active');

        });

    }

   
</script>

