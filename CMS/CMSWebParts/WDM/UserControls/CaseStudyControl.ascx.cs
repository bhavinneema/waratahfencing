﻿using CMS.CustomTables;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Taxonomy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WF.Domain.Classes;


public partial class CMSWebParts_WDM_UserControls_CaseStudyControl : System.Web.UI.UserControl
{
    #region Properties
    public int PageSize { get; set; }

    public int noOfPagesDisplay { get; set; }
    public string Mode { get; set; }

    public string Path { get; set; }

    public string NoResultText { get; set; }

    public bool PaggingEnabled { get; set; }

    public int CurrentPageNo
    {
        get
        {
            if (ViewState["CurrentPageNo"] == null)
            {
                ViewState["CurrentPageNo"] = 0;
            }
            return ValidationHelper.GetInteger(ViewState["CurrentPageNo"].ToString(), 0);
        }
        set
        {
            ViewState["CurrentPageNo"] = value;
        }
    }

    public object TagCategoryCode
    {
        get
        {
            return ViewState["TagCategoryCode"];
        }
        set
        {
            ViewState["TagCategoryCode"] = value;
        }
    }

    public object StateCode
    {
        get
        {
            return ViewState["StateCode"];
        }
        set
        {
            ViewState["StateCode"] = value;
        }
    }

    public object SearchText
    {
        get
        {
            return ViewState["SearchText"];
        }
        set
        {
            ViewState["SearchText"] = value;
        }
    }

    public object ActiveTag
    {
        get
        {
            return ViewState["activeTag"];
        }
        set
        {
            ViewState["activeTag"] = value;
        }
    }

    public object MapPoints
    {
        get
        {
            return ViewState["MapPoints"];
        }
        set
        {
            ViewState["MapPoints"] = value;
        }
    }

    public string CaseStudyTransformationName { get; set; }

    private int RecordCount
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["recordCount"].ToString(), 0);

        }
        set
        {
            ViewState["recordCount"] = value;
        }
    }

    public object MapDefaultLattitude
    {
        get
        {
            if (ViewState["MapDefaultLattitude"] == null)
            {
                // set austlia defaul lat
                ViewState["MapDefaultLattitude"] = "-25.939578";
            }
            return ViewState["MapDefaultLattitude"];
        }
        set
        {
            ViewState["MapDefaultLattitude"] = value;
        }
    }
    public object MapDefaultLongitude
    {
        get
        {
            if (ViewState["MapDefaultLongitude"] == null)
            {
                // set austlia defaul long
                ViewState["MapDefaultLongitude"] = "134.610694";
            }
            return ViewState["MapDefaultLongitude"];
        }
        set
        {
            ViewState["MapDefaultLongitude"] = value;
        }
    }

    public object MapZoomLevel
    {
        get
        {
            if (ViewState["MapZoomLevel"] == null)
            {
                ViewState["MapZoomLevel"] =4;
            }
            return ViewState["MapZoomLevel"];
        }
        set
        {
            ViewState["MapZoomLevel"] = value;
        }
    }

    #endregion

    #region PageEvents
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadStateFilters();
            CurrentPageNo = 1;
            caseStudyRpt.TransformationName = CaseStudyTransformationName;
            caseStudyRpt.ZeroRowsText = NoResultText;
            LoadCaseStudyList(CurrentPageNo - 1);

        }
       
    }

  
    #endregion

    #region Queries
    private ObjectQuery<DocumentCategoryInfo> MakeCategoriesQuery()
    {
        // if sector codes have values

        var where = string.Format("{0} in ('{1}')", WellknownPageFields.CategoryName, TagCategoryCode);
        var categoriesQuery = CategoryInfoProvider.GetCategories(where, WellknownPageFields.CategoryID);
        var documentCategoriesQuery = DocumentCategoryInfoProvider.GetDocumentCategories()
            .WhereIn(WellknownPageFields.CategoryID, categoriesQuery)
            .Column(WellknownPageFields.DocumentID);

        return documentCategoriesQuery;


        // return null;
    }

    protected void LoadCaseStudyList(int PageIndex)
    {

        var caseStudyListQuery = GetCaseStudyList();

        //tag filter query
        if (TagCategoryCode != null)
        {
            var categoriesQuery = MakeCategoriesQuery();
            caseStudyListQuery = caseStudyListQuery.WhereIn(WellknownPageFields.DocumentID, categoriesQuery);
        }

        //state filter query
        if (StateCode != null)
        {
            caseStudyListQuery = caseStudyListQuery.WhereLike("State", StateCode.ToString());
        }

        // text search filter query 
        if (SearchText != null)
        {
            string searchQuery = string.Format("DocumentContent LIKE {0} OR CaseStudyTitle LIKE {0} OR CaseStudyIntro LIKE {0} ", "'%" + SearchText.ToString() + "%'");
            caseStudyListQuery = caseStudyListQuery.Where(searchQuery);
           
        }

        //google map data set 
        if (PageIndex == 0)
        {
            GoogleMap.CaseStudyList = GetCaseStudyList();
            LoadMapPoints(caseStudyListQuery);
        }

        LoadMapDeafultData();


        RecordCount = caseStudyListQuery.Count;

        //enable paging
        if (PaggingEnabled)
        {
            caseStudyListQuery = caseStudyListQuery.Page(PageIndex, PageSize);
        }

        caseStudyRpt.DataSource = caseStudyListQuery;
        caseStudyRpt.DataBind();

        PopulatePager(RecordCount, PageIndex + 1);

    }

    protected DocumentQuery<CaseStudy> GetCaseStudyList()
    {
        return CaseStudyProvider.GetCaseStudies()
      .Path(string.IsNullOrEmpty(Path) ? "/" : Path.Replace("%", string.Empty), PathTypeEnum.Children)
      .Published()
      .OrderBy( WellknownPageFields.NodeOrder, WellknownPageFields.NodeLevel);

    }


    #endregion

    #region PagerControlEvents
    private void PopulatePager(int recordCount, int currentPage)
    {

        lnkNextGroup.Visible = false;
        lnkPreviousGroup.Visible = false;

        lnkFirst.Visible = false;
        lnkLast.Visible = false;
        lnkNext.Visible = false;

        var currentPageGroup = (int)Math.Ceiling((decimal)currentPage / (decimal)noOfPagesDisplay);

        var totalPageGroup = (int)Math.Ceiling((decimal)recordCount / (decimal)(noOfPagesDisplay * PageSize));

        int currentStartRecordCount = 1;

        double dblPageCount = (double)((decimal)recordCount / Convert.ToDecimal(PageSize));
        int noOfPages = (int)Math.Ceiling(dblPageCount);

        int start = (currentPageGroup - 1) * noOfPagesDisplay + 1;
        int? end = currentPageGroup * noOfPagesDisplay;
        end = end > noOfPages ? noOfPages : end;

        List<ListItem> pages = new List<ListItem>();
        if (noOfPages > 0)
        {
            for (int i = start; i <= end; i++)
            {
                pages.Add(new ListItem(i.ToString(), i.ToString(), i == currentPage));
            }
            if (totalPageGroup > currentPageGroup)
            {
                lnkNextGroup.Visible = true;
                lnkLast.Text = noOfPages.ToString();
                lnkLast.Visible = true;

            }
            if (1 < currentPageGroup)
            {
                lnkPreviousGroup.Visible = true;
                lnkPreviousGroup.Visible = true;
                lnkFirst.Visible = true;
            }
        }
        rptPager.DataSource = pages;
        rptPager.DataBind();


        //disable,enable -->previous,next buttons 
        lnkPrevious.Visible = true;
        lnkNext.Visible = true;
        end = PageSize;
        if (currentPage == 1)
        {
            lnkPrevious.Visible = false;
            currentStartRecordCount = 1;
            //end = end ;
        }
        else
        {
            currentStartRecordCount = ((currentPage - 1) * PageSize) + 1;
            end =( PageSize + currentStartRecordCount)-1;
        }

        if (currentPage == noOfPages)
        {
            end = recordCount;
            lnkNext.Visible = false;
        }


        if (recordCount == 0)
        {
            currentStartRecordCount = 0;
            end = 0;
            lnkNext.Visible = false;

        }


        litPagingText.Text = String.Format("{0} – {1} of {2} Solutions", currentStartRecordCount,  end,recordCount);

        //call java script functions
     
        if (TagCategoryCode == null && StateCode == null)
        {
            //filter by country
            ActiveTag = "Australia";
        }
        else {
            ActiveTag = TagCategoryCode != null ? TagCategoryCode.ToString() : StateCode.ToString();
        }
       
        ScriptManager.RegisterStartupScript(this, typeof(Page), "initMap", "initMap();", true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "setActive2", string.Format("setActive('{0}');", ActiveTag), true);

    }
    
    protected void Page_Changed(object sender, EventArgs e)
    {
        CurrentPageNo = int.Parse((sender as LinkButton).CommandArgument);
        //send page index
        this.LoadCaseStudyList(CurrentPageNo - 1);
    }

    protected void Page_Move(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString())
        {

            case "First":
                CurrentPageNo = 1;
                LoadCaseStudyList(CurrentPageNo - 1);

                break;

            case "Previous":
                LoadCaseStudyList(CurrentPageNo - 2);
                CurrentPageNo = CurrentPageNo - 1;
                break;

            case "Next":
                LoadCaseStudyList(CurrentPageNo);
                CurrentPageNo = CurrentPageNo + 1;
                break;

            case "Last":
                double dblPageCount = (double)((decimal)RecordCount / Convert.ToDecimal(PageSize));
                CurrentPageNo = (int)Math.Ceiling(dblPageCount);
                LoadCaseStudyList(CurrentPageNo - 1);
                break;

            case "NextGroup":

                var currentPageGroup = (int)Math.Ceiling((decimal)CurrentPageNo / (decimal)noOfPagesDisplay);
                CurrentPageNo = (currentPageGroup * noOfPagesDisplay) + 1;
                LoadCaseStudyList(CurrentPageNo - 1);
                break;

            case "PreviousGroup":
                currentPageGroup = (int)Math.Ceiling((decimal)CurrentPageNo / (decimal)noOfPagesDisplay);
                CurrentPageNo = ((currentPageGroup - 1) * noOfPagesDisplay);
                LoadCaseStudyList(CurrentPageNo - 1);
                CurrentPageNo = CurrentPageNo - 1;
                break;
        }
    }

    #endregion

    #region LoadControlData

    private void LoadStateFilters()
    {
        var StateList = CustomTableItemProvider.GetItems<StatesItem>().OrderBy(WellknownPageFields.ItemOrder);
        rptStates.DataSource = StateList.ToList();
        rptStates.DataBind();
    }

    protected void LoadMapDeafultData()
    {
        GoogleMap.MapPoints = MapPoints == null ? string.Empty : MapPoints.ToString();
        GoogleMap.MapDefaultLattitude = MapDefaultLattitude.ToString();
        GoogleMap.MapDefaultLongitude = MapDefaultLongitude.ToString();
        GoogleMap.MapZoomLevel = MapZoomLevel.ToString();
    }

    protected void LoadMapPoints(DocumentQuery<CaseStudy> CaseStudyList)
    {
        string CaseStudyPoints = null;
        foreach (CaseStudy caseStudy in CaseStudyList)
        {
            if (caseStudy.Lattitude != string.Empty)
                CaseStudyPoints += "||" + caseStudy.Lattitude + "~" + caseStudy.Longitude + "~" + caseStudy.DocumentID;
        }
        if (!string.IsNullOrEmpty(CaseStudyPoints))
        {
            CaseStudyPoints = CaseStudyPoints.Substring(2);
        }

        GoogleMap.MapPoints = CaseStudyPoints;
        MapPoints = CaseStudyPoints;
     

    }

    #endregion

    #region ControlEvents
    protected void Filter_CaseStudyList(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToString())
        {
            case "Filter_Tags":
                TagCategoryCode = e.CommandArgument.ToString();
                StateCode = null;
                MapDefaultLattitude = null;
                MapDefaultLongitude = null;
                MapZoomLevel = null;              
                break;

            case "Filter_States":
                //argument format state,Lattitude,Longitude
                var arguments = e.CommandArgument.ToString().Split(new char[] { ',' });

                TagCategoryCode = null;
                StateCode = arguments[0];
                MapDefaultLattitude = arguments[1];
                MapDefaultLongitude = arguments[2];
                MapZoomLevel = "5";              

                break;

            case "Filter_Country":

                TagCategoryCode = null;
                StateCode = null;
                MapDefaultLattitude = null;
                MapDefaultLongitude = null;
                MapZoomLevel = null;
              //  SearchText = null;

                break;

            case "Filter_SearchText":
                
                SearchText = txtSearch.Text.Trim();

                btnFilter.Visible = false;
                btnEmptyFilter.Visible = true;
                break;

            case "Empty_SearchText":
                
                SearchText = null;

                btnFilter.Visible = true;
                btnEmptyFilter.Visible = false;
                txtSearch.Text = string.Empty;
                break;
        }

        CurrentPageNo = 1;
        LoadCaseStudyList(CurrentPageNo - 1);
     

    }

    #endregion

   
}