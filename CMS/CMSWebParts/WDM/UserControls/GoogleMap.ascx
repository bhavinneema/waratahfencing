﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleMap.ascx.cs" Inherits="CMSWebParts_WDM_UserControls_GoogleMap" %>

<input type="hidden" name="mapPoints" id="mapPoints" value=<% =MapPoints %> />
<input type="hidden" name="mapDefaultLattitude" id="mapDefaultLattitude" value=<% =MapDefaultLattitude %> />
<input type="hidden" name="mapDefaultLongitude" id="mapDefaultLongitude" value=<% =MapDefaultLongitude %> />
<input type="hidden" name="mapZoomLevel" id="mapZoomLevel" value=<% =MapZoomLevel %> />
<%--<% =MapPoints %> <br />
<% =MapDefaultLattitude %>,<% =MapDefaultLongitude %> ,<% =MapZoomLevel %>--%>


<div class="googleMapHolder">
    <div class="g-map-section">
       <div id="map" ></div>

    </div>
    
    <cms:CMSRepeater ID="rptMapPopup" TransformationName="wdm.CaseStudy.MapPopup" runat="server">
        <HeaderTemplate>
            <div class="row" style="display:none">
                <ul class="place-holder">
        </HeaderTemplate>
        <FooterTemplate>
            </ul>
    </div>
        </FooterTemplate>
    </cms:CMSRepeater>
</div>

<script>

    $(document).ready(function () {

     
            initMap();
      
    }); 
 
    function initMap() {       

        var markers = [];

        var s = $("#mapPoints").val();
        locations = new Array();
        var spoints = s.split("||");
        for (var i = 0; i < spoints.length; i++) {
            var p = spoints[i].split("~");
            var point = {
                lat: p[0],
                lon: p[1],
                pid: p[2]
            }
            locations[locations.length] = point;
        }
        

            var point = {
                lat: $("#mapDefaultLattitude").val(),
                lon: $("#mapDefaultLongitude").val()
            }
            var mapProp = {
                center: new google.maps.LatLng(point.lat, point.lon),
                zoom: parseInt($("#mapZoomLevel").val())//,
             //   mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map"), mapProp);

            if (locations.length > 0) {

                var infoWin = new google.maps.InfoWindow();

                markers = locations.map(function (location, i) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i].lat, locations[i].lon),
                        animation: google.maps.Animation.DROP
                    });
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWin.setContent(jQuery("#mapinfo" + location.pid).html());
                        infoWin.open(map, marker);
                    })
                    return marker;
                });

                google.maps.event.addListener(map, 'click', function (event) {
                    this.setOptions({ scrollwheel: true });
                });
                google.maps.event.addListener(map, 'mouseout', function (event) {
                    this.setOptions({ scrollwheel: false });
                });

                // markerCluster.setMarkers(markers);
                // Add a marker clusterer to manage the markers.
                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });
            }
       
    }
</script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZYMEkj1zuzWGn3PcJaAZapDN6IYbOuHY&libraries=geometry">
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZYMEkj1zuzWGn3PcJaAZapDN6IYbOuHY" type="text/javascript"></script>--%>

