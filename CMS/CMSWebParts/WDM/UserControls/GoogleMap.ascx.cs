﻿using CMS.DocumentEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WF.Domain.Classes;

public partial class CMSWebParts_WDM_UserControls_GoogleMap : System.Web.UI.UserControl
{
    public DocumentQuery<CaseStudy> CaseStudyList { get; set; }
    public string MapPoints { get; set; }

    public string MapDefaultLattitude { get; set; }
    public string MapDefaultLongitude { get; set; }

    public string MapZoomLevel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            populateMapPopups();

        }



    }

    protected void populateMapPopups()
    {
        rptMapPopup.DataSource = CaseStudyList;
        rptMapPopup.DataBind();
    }
}