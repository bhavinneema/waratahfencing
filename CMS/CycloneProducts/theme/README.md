Gulp and Project Theme Folders
==================================

This project theme folder is to contain Gulp setup files and all source files for your Kentico site theme.

Gulp will automatically compile, copy and output files/folders to a `/dist/` folder. All assets inside the `/dist/` folder are available for use on the website.

__TODO:__ Name this folder the same name set as you sites codename (Settings > [edit] > General). This will ensure that Kentico's `/media/` folder (media library folder) and your sites `/theme/` folder reside in the same directory.


## Gulp's Configuration File

__TODO:__ Open the `config.js` file and edit the `paths` values.


### config.js

The `config.js` file defines all paths to files and folders for use in various gulp tasks.


#### `paths.styles`

Array of objects. Each object is passed through the Gulp task. Each object requires 4 properties:

- `name:` "Filename of the compiled file."  
- `src:` "Source file(s) to pass into the Gulp task."  
- `watch:` "Files to watch changes on, to trigger Gulp task."  
- `dest:` "Destination folder of the output file."  

__Example:__

```javascript
const themeDistDir = '../dist';

paths.styles = [

    {
        name:  'global.min.css',
        src:   '../styles/global/imports.less',
        watch: '../styles/global/**/*.less',
        dest:  themeDistDir + '/css'
    },

    {
        ...
    }

]
```


#### `paths.scripts`

Array of objects. Each object is passed through the Gulp task. Each object requires 4 properties:

- `name:` "Filename of the compiled file."  
- `src:` "Source file(s) to pass into the Gulp task."  
- `watch:` "Files to watch changes on, to trigger Gulp task."  
- `dest:` "Destination folder of the output file."  

__Example:__

```javascript
const themeDistDir = '../dist';

paths.scripts = [

    {
        name:  'global.min.js',
        src: [
            '../scripts/global/plugins/**/*.js',
            '../scripts/global/_*.js',
            '../scripts/global/init.js'
        ],
        watch: '../scripts/global/**/*.js',
        dest:  themeDistDir + '/js'
    },

    {
        ...
    }

]
```


#### `paths.copy`

Object. The object properties are passed through the Gulp task. Object has 2 properties:

- `src:` "Array. Glob of source files to pass into the Gulp task."  
- `dest:` "Destination folder of the output file/folders."

__Example:__

```javascript
const themeDistDir = '../dist';

paths.copy = {

    src: [
        '../images/**/*.+(svg|png|jpg|jpeg|gif)',
        '../fonts/**/*',
        '../libraries/**/*'
    ],
    dest: themeDistDir
};
```


#### `paths.server`

Object. The object properties are passed through the Gulp task. Object has 2 properties:

- `root:` "Array. Directories to be served. Typically contains html and `dist` assets."  
- `watch:` "Files to watch changes on, to trigger live reload."  

__Example:__

```javascript

paths.server = {

    root: [
        '../static_html/',
        '../' // allow access to /dist/ files
    ],
    watch: themeDistDir
};
```


## Gulp

### `/_gulp/`

The `/_gulp/` folder contains the `gulpfile.js`, `packages.json` and other files required to run various gulp tasks.

__NOTE__ No editing is required.


### Default Gulp Tasks

#### `$ gulp`

Compiles, copies and concatenates source files and outputs to dest paths.

#### `$ gulp w`

Start watching source files, and on change, auto run respective tasks to build or copy to dest path.

#### `$ gulp l`

Run less task to build and output the sites `.css` file(s) to dest path.

#### `$ gulp s`

Run script task to build and output the sites `.js` file(s) to dest path.

#### `$ gulp c`

Run copy task to copy fonts, libs and image file/folders to dest path.

#### `$ gulp clean --yoda`

Runs destructive task to deletes the complete dist folder and its contents.

#### `$ gulp production`

Runs all tasks required to build complete site dist file/folders, ready for site deployment.


### Default Gulp Flags

#### `--dev`

Run gulp task in 'development' environment. Will __not__ minify css, uglify js, and __will__ write source maps.

Example use: `$ gulp --dev` or `$ gulp l --dev` or `$ gulp w --dev` etc.

#### `--es6`

Run gulp task in 'es6' environment. Will transpile any es6 to es5 JavaScript.

Example use: `$ gulp --es6` or `$ gulp j --es6` or `$ gulp w --es6` etc.

#### `--yoda`

Required when running destructive tasks, cause the force is strong with this one.

I.e: `$ gulp clean --yoda`


## Source Folders

### `/fonts/`

The `/fonts/` folder contains any icon or type font files referenced in the site stylesheets.

The contents of this folder is copied to `/theme/dist/fonts/`.

__NOTE__ Fonts can be grouped into sub-folders.


### `/images/`

The `/images/` folder contains any images referenced in the site stylesheets.

Media files, such as slider and banner images, ads, page backgrounds photos, logos and other non-UI page graphics should be uploaded into your site media library.

Images like, UI graphics and elements, patterns, icons, loaders and spinners are valid to be saved here.

You can save original `.psd`, `.psb` and `.ai` files here too, for ease of updating site images. Note that `.psd`, `.psb` and `.ai` files are automatically excluded from including in Gulp's copy task, so only web friendly file types will be copied to the destination folder (E.g. `.jpg`, `.png`).

The contents of this folder is copied to `/theme/dist/images/`.

__NOTE__ Images can be grouped into sub-folders.


### `/libs/`

The `/libs/` folder contains any libraries to be referenced to on the site, such as jQuery.

The contents of this folder is copied to `/theme/dist/libs/`.

__NOTE__ library files can be grouped into sub-folders.


### `/js/`

The `/js/` folder contains the sites source JavaScript files.

The contents of these folders is processed by Gulp before outputting to the `/theme/dist/js/`.

__NOTE__ script files __should__ be grouped a sub-folder. Default folder is `/js/global/` to group all `global.min.js` scripts.


### `/less/`

The `/less/` folder contains the sites source Less files.

The contents of these folders is processed by Gulp before outputting to the `/theme/dist/css/`.

__NOTE__ styles files __should__ be grouped into a sub-folder for clearer segmentation. Default folder is `/less/global/` to group all `global.min.css` source styles.
