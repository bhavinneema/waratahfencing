/// Extra tasks
'use strict';

const gulp = require( 'gulp' );


/// Default task
// - Setup to run copy, less and scripts tasks. Useful to build a fresh set of /dist/ folder assets.
// - Run command: `$ gulp`

gulp.task( 'default', [ 'c', 'l', 'j' ] );


/// Productions task
// - Used with other production related tasks, such as jenkins and git hooks.
// - Run command: `$ gulp production`

gulp.task( 'production', [ 'default' ] );
