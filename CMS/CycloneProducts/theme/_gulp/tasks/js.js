/// Script tasks
'use strict';

// core modules
const es = require( 'event-stream' );
const gulp = require( 'gulp' );
const gutil = require( 'gulp-util' );
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const livereload = require( 'gulp-livereload' );

// tasks modules
const header = require( 'gulp-header' );
const concat = require( 'gulp-concat' );
const uglify = require( 'gulp-uglify' );
const babel = require( "gulp-babel" );
const babelPresetES2015 = require( 'babel-preset-es2015' );

// helpers
const env = require( './utils/env.js' );
const banner = require( './utils/banners.js' ).banner;
const report = require( './utils/reporters.js' ).report;

// paths
const paths = require( '../../config.js' ).paths;


/// JavaScript task
// - Concatinates and uglifies script files.
// - Run command: `$ gulp j`

gulp.task( 'j', () => {

    let babelOptions = {
        "presets": [ babelPresetES2015 ]
    };

    // define task
    let tasks = paths.scripts.map( ( script ) => {

        // the file
        gutil.log( 'Working on ' + script.name );

        // the process
        return gulp
            .src( script.src )
            .pipe( plumber( {
                errorHandler: report.taskError
            } ) ) // handle errors
            .pipe( babel( babelOptions ) ) // transpile ES6 (to ES5)
            .pipe( concat( script.name ) ) // concat
            // .pipe( env.dev ? gutil.noop() : uglify() ) // uglify
            .pipe( header( banner.doNotEdit ) ) // add banner
            .pipe( gulp.dest( script.dest ) ); // write file

    } );

    // init task
    return es
        .merge( tasks )
        .pipe( livereload() ); // trigger live repload

} );
