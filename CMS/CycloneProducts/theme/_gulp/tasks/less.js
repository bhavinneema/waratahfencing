/// Style tasks
'use strict';

// core modules
const es = require( 'event-stream' );
const gulp = require( 'gulp' );
const gutil = require( 'gulp-util' );
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const livereload = require( 'gulp-livereload' );

// tasks modules
const header = require( 'gulp-header' );
const rename = require( 'gulp-rename' );
const less = require( 'gulp-less' );
const cssmin = require( 'gulp-cssmin' );
const postcss = require( 'gulp-postcss' );
const autoprefixer = require( 'autoprefixer' );

// helpers
const env = require( './utils/env.js' );
const banner = require( './utils/banners.js' ).banner;
const report = require( './utils/reporters.js' ).report;

// paths
const paths = require( '../../config.js' ).paths;


/// Less/CSS task
// - Compiles Less and handles vendor prefixes with 'autoprefixer'
// - Run command: `$ gulp l`

gulp.task( 'l', () => {

    let postcssProcessors = [
        autoprefixer( { // https://github.com/postcss/autoprefixer
            browsers: [ 'last 2 versions' ], // https://github.com/ai/browserslist
            cascade: false // pretty vertical alignment of vendor properties
        } )
    ];

    // define task
    let tasks = paths.styles.map( ( stylesheet ) => {

        // the file
        gutil.log( 'Working on ' + stylesheet.name );

        // the process
        return gulp
            .src( stylesheet.src )
            .pipe( plumber( {
                errorHandler: report.taskError
            } ) ) // handle errors
            .pipe( less() ) // compile
            .pipe( env.dev ? gutil.noop() : postcss( postcssProcessors ) ) // auto css stuff
            .pipe( env.dev ? gutil.noop() : cssmin() ) // minify
            .pipe( env.dev ? gutil.noop() : header( banner.doNotEdit ) ) // add banner
            .pipe( rename( stylesheet.name ) ) // name file
            .pipe( gulp.dest( stylesheet.dest ) ); // write file

    } );

    // init task
    return es
        .merge( tasks )
        .pipe( livereload() ); // trigger live repload

} );
