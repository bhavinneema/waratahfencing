/// Server static files
'use strict';

const gulp = require( 'gulp' );
const connect = require( 'gulp-connect' );

const paths = require( '../../config.js' ).paths;


/// Watch and Server task
// - Start both watch task, and server task
gulp.task( 'ws', [ 'w', 's' ] );


/// Server task
// - Server static files
// - Run command: `$ gulp s`

gulp.task( 's', () => {

    connect.server( {
        base: 'http://localhost',
        root: paths.server.root,
        livereload: true
    } );

} );
