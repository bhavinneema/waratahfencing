/// Reporting utilities
'use strict';

const gutil = require( 'gulp-util' );
const notify = require( 'gulp-notify' );
const env = require( './env.js' );

let dist = require( '../../../config.js' ).dist;


/// reports
// - Reusable functions to report of errors and events resulting form running various gulp tasks.

let report = {

    // do not use fat arrow function for errorHandler
    // arrow function binds the context lexically with the window object
    taskError: function( error ) {
        notify.onError( {
            title: "Gulp",
            subtitle: "Failure!",
            message: "Error: <%= error.message %>",
            sound: "Beep"
        } )( error );

        // Make sure we call this.emit('end') so gulp knows to
        // correctly end this task even if it has errored.
        // This will allow the task to successfully run again,
        // such as if triggered by a persistent watch task.
        // If you do not end the task on error, the task will not
        // run again, but the watch task still reports on file
        // changes (even though this task is dead/does nothing).
        this.emit( 'end' ); // correctly end task
    },

    // Report file changes
    filesChanged: ( event ) => {

        var filePath = gutil.colors.yellow( event.path );
        var fileEvent = gutil.colors.magenta( event.type );

        gutil.log( 'File', filePath, 'was', fileEvent );

    },

    // Report error merging gulp src
    mergeSourceError: () => {

        // Log error
        gutil.log( 'Error prior to merge ;(' );

        // Emit end of task
        this.emit( 'end' ); //TODO check 'this' work when called from this function context!

    },

    // Report success of deleted folders
    deleteSuccessful: ( paths ) => {

        console.log( '\n' );
        console.log( gutil.colors.red( 'Folder(s) deleted:' ) );
        // if paths deleted
        if ( paths.length !== 0 ) {
            // report paths deleted
            for ( let path of paths ) {
                console.log( gutil.colors.yellow( path ) );
            }
        } else {
            // report nothing was deleted
            console.log( gutil.colors.yellow( 'Nothing deleted, folder(s) missing or empty.' ) );
        }
        console.log( '\n' );

    },

    // Report error deleted folders
    deleteFailed: ( err ) => {
        console.log( err );
    },

    // Report failure to delete folders
    requiresDistructiveFlag: () => {

        // report failure to complete deletion task
        console.log( '\n' );
        console.log( gutil.colors.yellow( 'Distructive task cancelled:' ) );
        console.log( 'Note: You must include the ' + gutil.colors.yellow( '--distructive' ) + ' flag to successfully run this task.' );
        console.log( '\n' );

    }

};


module.exports = {
    report: report
};
