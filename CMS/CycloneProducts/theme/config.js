/// Path to files and folders

var paths = {};


// Distribution folders
paths = {
    dist: '../dist',
    distStyles: '../dist/css',
    distScripts: '../dist/js',
    distTemplates: '../dist/js',
    distImages: '../dist/images',
    distFonts: '../dist/fonts',
    distLibs: '../dist/libs'
}


/// CSS styles paths
paths.styles = [

    {
        name: 'global.min.css',
        src: '../less/global/imports.less',
        watch: '../less/global/**/*.less',
        dest: paths.distStyles
    }

];


// Script paths
paths.scripts = [

    {
        name: 'global.min.js',
        src: [
            '../js/global/plugins/**/*.js',
            '../js/global/_*.js',
            '../js/global/*/_*.js',
            '../js/global/init.js'
        ],
        watch: '../js/global/**/*.js',
        dest: paths.distScripts
    }

];


// Template paths
paths.jsxTemplates = {
    name: 'views.js',
    src: '../js/_templates/*.jsx',
    watch: '../js/_templates/**/*.jsx',
    dest: paths.distScripts
};


// Files to copy
paths.copy = {

    src: [
        '../images/**/*.+(svg|png|jpg|jpeg|gif)',
        '../fonts/**/*.+(eot|woff|woff2|ttf|svg)',
        '../libs/**/*'
    ],
    dest: paths.dist

};


// Files to server
paths.server = {

    root: [
        '../static_html/',
        '../' // allows access to /dist/ files
    ],
    watch: '../static_html/**/*.html',

};


module.exports = {
    paths: paths
};
