JS Folder
=================

Gulp file paths are configured in the `/theme/config.js` file.

Changes to the scripts sub-folders and files may require updates to the paths set in the `config.js` file.

NOTE: If you are writing ES6, you will need to include the flag `--es6` when running Gulp to transpile ES6 to ES5. This also avoids errors by uglify when trying to parse ES6.


## /global/ folder

The `/global/` folder is to contain all your sites global scripts. This script file is intended to be linked to all pages on your website.

The `/global/plugins` folder is to contain any javascript plugin ".js" that should be bundled with the concatenation of the global scripts file.

All `.js` files, with the exception of init.js, should be prefixed with an underscore (e.g. "_maps.js") to represent the file as a "partial" and allows Gulp to glob all partial js files in an array for concatenation.

The `init.js` file can be used to initate your functions and methods, and should always be concatenated at the end of the array.

*Example setup*

/global/  
-- /plugins/  
----- bootstrap.tooltip.js  
-- _maps.js  
-- _admin.js  
-- init.js  
