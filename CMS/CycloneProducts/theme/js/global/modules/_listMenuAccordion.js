var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.listMenuAccordion = WDM.WTF.listMenuAccordion || (function() {

    var init = function() {

        $('.js-toggle').on('click', function(event) {
            // find out if clicked item has opened menu
            var menuIsOpened = $(this).parent().hasClass('open');

            // close all opened menues
            $('.left-navigation li').removeClass('open');

            // if menu is was opened
            if (menuIsOpened) {
                // close menu
                $(this).parent().removeClass('open');
            } else {
                // open menu
                $(this).parent().addClass('open');
            }
        })
    };

    return {
        init: init
    };

}());
