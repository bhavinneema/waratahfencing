using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Domain.Classes
{    
    /// <summary>
    /// Class providing DistributorInfo management.
    /// </summary>
    public partial class DistributorInfoProvider : AbstractInfoProvider<DistributorInfo, DistributorInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public DistributorInfoProvider()
            : base(DistributorInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the DistributorInfo objects.
        /// </summary>
        public static ObjectQuery<DistributorInfo> GetDistributors()
        {
            return ProviderObject.GetDistributorsInternal();
        }


        /// <summary>
        /// Returns DistributorInfo with specified ID.
        /// </summary>
        /// <param name="id">DistributorInfo ID</param>
        public static DistributorInfo GetDistributorInfo(int id)
        {
            return ProviderObject.GetDistributorInfoInternal(id);
        }


        /// <summary>
        /// Returns DistributorInfo with specified GUID.
        /// </summary>
        /// <param name="guid">DistributorInfo GUID</param>                
        public static DistributorInfo GetDistributorInfo(Guid guid)
        {
            return ProviderObject.GetDistributorInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified DistributorInfo.
        /// </summary>
        /// <param name="infoObj">DistributorInfo to be set</param>
        public static void SetDistributorInfo(DistributorInfo infoObj)
        {
            ProviderObject.SetDistributorInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified DistributorInfo.
        /// </summary>
        /// <param name="infoObj">DistributorInfo to be deleted</param>
        public static void DeleteDistributorInfo(DistributorInfo infoObj)
        {
            ProviderObject.DeleteDistributorInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes DistributorInfo with specified ID.
        /// </summary>
        /// <param name="id">DistributorInfo ID</param>
        public static void DeleteDistributorInfo(int id)
        {
            DistributorInfo infoObj = GetDistributorInfo(id);
            DeleteDistributorInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the DistributorInfo objects.
        /// </summary>
        protected virtual ObjectQuery<DistributorInfo> GetDistributorsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns DistributorInfo with specified ID.
        /// </summary>
        /// <param name="id">DistributorInfo ID</param>        
        protected virtual DistributorInfo GetDistributorInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns DistributorInfo with specified GUID.
        /// </summary>
        /// <param name="guid">DistributorInfo GUID</param>
        protected virtual DistributorInfo GetDistributorInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified DistributorInfo.
        /// </summary>
        /// <param name="infoObj">DistributorInfo to be set</param>        
        protected virtual void SetDistributorInfoInternal(DistributorInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified DistributorInfo.
        /// </summary>
        /// <param name="infoObj">DistributorInfo to be deleted</param>        
        protected virtual void DeleteDistributorInfoInternal(DistributorInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}