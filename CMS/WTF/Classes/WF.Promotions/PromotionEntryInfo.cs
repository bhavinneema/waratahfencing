using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;

[assembly: RegisterObjectType(typeof(PromotionEntryInfo), PromotionEntryInfo.OBJECT_TYPE)]
    
namespace WF
{
    /// <summary>
    /// PromotionEntryInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionEntryInfo : AbstractInfo<PromotionEntryInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionentry";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionEntryInfoProvider), OBJECT_TYPE, "WF.PromotionEntry", "PromotionEntryID", "PromotionEntryLastModified", "PromotionEntryGuid", null, null, null, "PromotionEntrySiteID", null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionEntrySiteID", "cms.site", ObjectDependencyEnum.Required), 
            },
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion entry ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntryID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntryID"), 0);
            }
            set
            {
                SetValue("PromotionEntryID", value);
            }
        }


        /// <summary>
        /// Promotion entry form ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntryFormID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntryFormID"), 0);
            }
            set
            {
                SetValue("PromotionEntryFormID", value);
            }
        }


        /// <summary>
        /// Promotion entry form item ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntryFormItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntryFormItemID"), 0);
            }
            set
            {
                SetValue("PromotionEntryFormItemID", value);
            }
        }


        /// <summary>
        /// Promotion entry claim ID
        /// </summary>
        [DatabaseField]
        public virtual string PromotionEntryClaimID
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionEntryClaimID"), String.Empty);
            }
            set
            {
                SetValue("PromotionEntryClaimID", value);
            }
        }


        /// <summary>
        /// Promotion entry site ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntrySiteID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntrySiteID"), 0);
            }
            set
            {
                SetValue("PromotionEntrySiteID", value);
            }
        }


        /// <summary>
        /// Promotion entry guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionEntryGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionEntryGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionEntryGuid", value);
            }
        }


        /// <summary>
        /// Promotion entry last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionEntryLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionEntryLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionEntryLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionEntryInfoProvider.DeletePromotionEntryInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionEntryInfoProvider.SetPromotionEntryInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionEntryInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionEntryInfo object.
        /// </summary>
        public PromotionEntryInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionEntryInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionEntryInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}