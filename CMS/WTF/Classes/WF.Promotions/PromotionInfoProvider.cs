using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Domain.Classes
{    
    /// <summary>
    /// Class providing PromotionInfo management.
    /// </summary>
    public partial class PromotionInfoProvider : AbstractInfoProvider<PromotionInfo, PromotionInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionInfoProvider()
            : base(PromotionInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionInfo> GetPromotions()
        {
            return ProviderObject.GetPromotionsInternal();
        }


        /// <summary>
        /// Returns PromotionInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionInfo ID</param>
        public static PromotionInfo GetPromotionInfo(int id)
        {
            return ProviderObject.GetPromotionInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionInfo GUID</param>                
        public static PromotionInfo GetPromotionInfo(Guid guid)
        {
            return ProviderObject.GetPromotionInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionInfo.
        /// </summary>
        /// <param name="infoObj">PromotionInfo to be set</param>
        public static void SetPromotionInfo(PromotionInfo infoObj)
        {
            ProviderObject.SetPromotionInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionInfo.
        /// </summary>
        /// <param name="infoObj">PromotionInfo to be deleted</param>
        public static void DeletePromotionInfo(PromotionInfo infoObj)
        {
            ProviderObject.DeletePromotionInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionInfo ID</param>
        public static void DeletePromotionInfo(int id)
        {
            PromotionInfo infoObj = GetPromotionInfo(id);
            DeletePromotionInfo(infoObj);
        }

        #endregion


        #region "Public methods - Advanced"


        /// <summary>
        /// Returns a query for all the PromotionInfo objects of a specified site.
        /// </summary>
        /// <param name="siteId">Site ID</param>
        public static ObjectQuery<PromotionInfo> GetPromotions(int siteId)
        {
            return ProviderObject.GetPromotionsInternal(siteId);
        }
        
        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionInfo> GetPromotionsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionInfo ID</param>        
        protected virtual PromotionInfo GetPromotionInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionInfo GUID</param>
        protected virtual PromotionInfo GetPromotionInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionInfo.
        /// </summary>
        /// <param name="infoObj">PromotionInfo to be set</param>        
        protected virtual void SetPromotionInfoInternal(PromotionInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionInfo.
        /// </summary>
        /// <param name="infoObj">PromotionInfo to be deleted</param>        
        protected virtual void DeletePromotionInfoInternal(PromotionInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion

        #region "Internal methods - Advanced"


        /// <summary>
        /// Returns a query for all the PromotionInfo objects of a specified site.
        /// </summary>
        /// <param name="siteId">Site ID</param>
        protected virtual ObjectQuery<PromotionInfo> GetPromotionsInternal(int siteId)
        {
            return GetObjectQuery().OnSite(siteId);
        }    
        
        #endregion		
    }
}