using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF
{    
    /// <summary>
    /// Class providing PromotionProductInfo management.
    /// </summary>
    public partial class PromotionProductInfoProvider : AbstractInfoProvider<PromotionProductInfo, PromotionProductInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionProductInfoProvider()
            : base(PromotionProductInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionProductInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionProductInfo> GetPromotionProducts()
        {
            return ProviderObject.GetPromotionProductsInternal();
        }


        /// <summary>
        /// Returns PromotionProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductInfo ID</param>
        public static PromotionProductInfo GetPromotionProductInfo(int id)
        {
            return ProviderObject.GetPromotionProductInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionProductInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionProductInfo GUID</param>                
        public static PromotionProductInfo GetPromotionProductInfo(Guid guid)
        {
            return ProviderObject.GetPromotionProductInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductInfo to be set</param>
        public static void SetPromotionProductInfo(PromotionProductInfo infoObj)
        {
            ProviderObject.SetPromotionProductInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductInfo to be deleted</param>
        public static void DeletePromotionProductInfo(PromotionProductInfo infoObj)
        {
            ProviderObject.DeletePromotionProductInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductInfo ID</param>
        public static void DeletePromotionProductInfo(int id)
        {
            PromotionProductInfo infoObj = GetPromotionProductInfo(id);
            DeletePromotionProductInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionProductInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionProductInfo> GetPromotionProductsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductInfo ID</param>        
        protected virtual PromotionProductInfo GetPromotionProductInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionProductInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionProductInfo GUID</param>
        protected virtual PromotionProductInfo GetPromotionProductInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductInfo to be set</param>        
        protected virtual void SetPromotionProductInfoInternal(PromotionProductInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductInfo to be deleted</param>        
        protected virtual void DeletePromotionProductInfoInternal(PromotionProductInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}