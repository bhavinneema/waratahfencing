using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;

[assembly: RegisterObjectType(typeof(PromotionRewardGroupInfo), PromotionRewardGroupInfo.OBJECT_TYPE)]
    
namespace WF
{
    /// <summary>
    /// PromotionRewardGroupInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionRewardGroupInfo : AbstractInfo<PromotionRewardGroupInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionrewardgroup";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionRewardGroupInfoProvider), OBJECT_TYPE, "WF.PromotionRewardGroup", "PromotionRewardGroupID", "PromotionRewardGroupLastModified", "PromotionRewardGroupGuid", null, "PromotionRewardGroupDisplayName", null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionRewardGroupPromotionID", "wf.promotion", ObjectDependencyEnum.Required), 
            },
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion reward group ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardGroupID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardGroupID"), 0);
            }
            set
            {
                SetValue("PromotionRewardGroupID", value);
            }
        }


        /// <summary>
        /// Promotion reward group promotion ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardGroupPromotionID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardGroupPromotionID"), 0);
            }
            set
            {
                SetValue("PromotionRewardGroupPromotionID", value);
            }
        }


        /// <summary>
        /// Promotion reward group display name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionRewardGroupDisplayName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionRewardGroupDisplayName"), String.Empty);
            }
            set
            {
                SetValue("PromotionRewardGroupDisplayName", value);
            }
        }


        /// <summary>
        /// Promotion reward group order
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardGroupOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardGroupOrder"), 0);
            }
            set
            {
                SetValue("PromotionRewardGroupOrder", value, 0);
            }
        }


        /// <summary>
        /// Promotion reward group guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionRewardGroupGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionRewardGroupGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionRewardGroupGuid", value);
            }
        }


        /// <summary>
        /// Promotion reward group last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionRewardGroupLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionRewardGroupLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionRewardGroupLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionRewardGroupInfoProvider.DeletePromotionRewardGroupInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionRewardGroupInfoProvider.SetPromotionRewardGroupInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionRewardGroupInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionRewardGroupInfo object.
        /// </summary>
        public PromotionRewardGroupInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionRewardGroupInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionRewardGroupInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}