using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF
{    
    /// <summary>
    /// Class providing PromotionRewardGroupInfo management.
    /// </summary>
    public partial class PromotionRewardGroupInfoProvider : AbstractInfoProvider<PromotionRewardGroupInfo, PromotionRewardGroupInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionRewardGroupInfoProvider()
            : base(PromotionRewardGroupInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionRewardGroupInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionRewardGroupInfo> GetPromotionRewardGroups()
        {
            return ProviderObject.GetPromotionRewardGroupsInternal();
        }


        /// <summary>
        /// Returns PromotionRewardGroupInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardGroupInfo ID</param>
        public static PromotionRewardGroupInfo GetPromotionRewardGroupInfo(int id)
        {
            return ProviderObject.GetPromotionRewardGroupInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionRewardGroupInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardGroupInfo GUID</param>                
        public static PromotionRewardGroupInfo GetPromotionRewardGroupInfo(Guid guid)
        {
            return ProviderObject.GetPromotionRewardGroupInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardGroupInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardGroupInfo to be set</param>
        public static void SetPromotionRewardGroupInfo(PromotionRewardGroupInfo infoObj)
        {
            ProviderObject.SetPromotionRewardGroupInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardGroupInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardGroupInfo to be deleted</param>
        public static void DeletePromotionRewardGroupInfo(PromotionRewardGroupInfo infoObj)
        {
            ProviderObject.DeletePromotionRewardGroupInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionRewardGroupInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardGroupInfo ID</param>
        public static void DeletePromotionRewardGroupInfo(int id)
        {
            PromotionRewardGroupInfo infoObj = GetPromotionRewardGroupInfo(id);
            DeletePromotionRewardGroupInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionRewardGroupInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionRewardGroupInfo> GetPromotionRewardGroupsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionRewardGroupInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardGroupInfo ID</param>        
        protected virtual PromotionRewardGroupInfo GetPromotionRewardGroupInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionRewardGroupInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardGroupInfo GUID</param>
        protected virtual PromotionRewardGroupInfo GetPromotionRewardGroupInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardGroupInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardGroupInfo to be set</param>        
        protected virtual void SetPromotionRewardGroupInfoInternal(PromotionRewardGroupInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardGroupInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardGroupInfo to be deleted</param>        
        protected virtual void DeletePromotionRewardGroupInfoInternal(PromotionRewardGroupInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}