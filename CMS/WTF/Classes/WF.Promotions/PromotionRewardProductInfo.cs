using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;

[assembly: RegisterObjectType(typeof(PromotionRewardProductInfo), PromotionRewardProductInfo.OBJECT_TYPE)]
    
namespace WF
{
    /// <summary>
    /// PromotionRewardProductInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionRewardProductInfo : AbstractInfo<PromotionRewardProductInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionrewardproduct";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionRewardProductInfoProvider), OBJECT_TYPE, "WF.PromotionRewardProduct", "PromotionRewardProductID", "PromotionRewardProductLastModified", "PromotionRewardProductGuid", null, null, null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionRewardProductSKUID", "ecommerce.sku", ObjectDependencyEnum.Required), 
            },
            OrderColumn = "PromotionRewardProductOrder"
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion reward product ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardProductID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardProductID"), 0);
            }
            set
            {
                SetValue("PromotionRewardProductID", value);
            }
        }


        /// <summary>
        /// Promotion reward product promotion reward ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardProductPromotionRewardID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardProductPromotionRewardID"), 0);
            }
            set
            {
                SetValue("PromotionRewardProductPromotionRewardID", value);
            }
        }


        /// <summary>
        /// Promotion reward product SKUID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardProductSKUID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardProductSKUID"), 0);
            }
            set
            {
                SetValue("PromotionRewardProductSKUID", value);
            }
        }


        /// <summary>
        /// Promotion reward product order
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardProductOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardProductOrder"), 0);
            }
            set
            {
                SetValue("PromotionRewardProductOrder", value, 0);
            }
        }


        /// <summary>
        /// Promotion reward product guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionRewardProductGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionRewardProductGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionRewardProductGuid", value);
            }
        }


        /// <summary>
        /// Promotion reward product last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionRewardProductLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionRewardProductLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionRewardProductLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionRewardProductInfoProvider.DeletePromotionRewardProductInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionRewardProductInfoProvider.SetPromotionRewardProductInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionRewardProductInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionRewardProductInfo object.
        /// </summary>
        public PromotionRewardProductInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionRewardProductInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionRewardProductInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}