/// Gulpfile
'use strict';

const requireDir = require( 'require-dir' );

// Require all in /tasks, including subfolders
requireDir( './tasks', {
    recurse: true
} );
