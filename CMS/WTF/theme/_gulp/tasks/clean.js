/// Cleaning tasks
'use strict';

// core modules
const gulp = require( 'gulp' );

// tasks modules
const del = require( 'del' );

// helpers
const env = require( './utils/env.js' );
const report = require( './utils/reporters.js' ).report;

// paths
const paths = require( '../../config.js' ).paths;


/// Delete dist task
// - Distructive task used to delete the /dist/ folder.
// - Run command: `$ gulp clean --distructive`

gulp.task( 'clean', () => {
    if ( env.distructive ) {
        return del( [
                paths.distStyles,
                paths.distScripts,
                paths.distTemplates,
                paths.distImages,
                paths.distFonts,
                paths.distLibs
            ], {
                force: true // Allow deleting the current working directory and outside.
            } )
            .then( report.deleteSuccessful );
    } else {
        report.requiresDistructiveFlag();
    }

} );


/// Delete dist folders of copy task
// - Distructive task used to delete the /dist/[images, fonts, libs] folders.
// - Command runs when `$ gulp c` is called, to remove old dist files.

gulp.task( 'clean-copied-assets', () => {

    return del( [
            paths.distImages,
            paths.distFonts,
            paths.distLibs
        ], {
            force: true // Allow deleting the current working directory and outside.
        } )
        .then( report.deleteSuccessful ) // success, promise resolved
        .catch( report.deleteFailed ); // failure, promise rejected

} );
