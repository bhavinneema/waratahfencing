/// Copying tasks
'use strict';

const gulp = require( 'gulp' );
const newer = require( 'gulp-newer' );

const paths = require( '../../config.js' ).paths;


/// copy task
// - copies all `paths.copy.src` files and folders to `paths.copy.dest` (if newer)
// - run command: `$ gulp c`

gulp.task( 'c', [ 'clean-copied-assets' ], () => {

    // init task
    return gulp
        .src( paths.copy.src, {
            'base': '../'
        } )
        // newer is not required as we are cleaning copied /dist/ folders.
        //.pipe( newer( paths.copy.dest ) ) // find newer files
        .pipe( gulp.dest( paths.copy.dest ) ); // copy files

} );
