/// Script tasks
'use strict';

// core modules
const es = require( 'event-stream' );
const gulp = require( 'gulp' );
const gutil = require( 'gulp-util' );
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const livereload = require( 'gulp-livereload' );

// tasks modules
const header = require( 'gulp-header' );
const concat = require( 'gulp-concat' );
const msx = require( "gulp-msx" ); // mithril template compiler

// helpers
const env = require( './utils/env.js' );
const banner = require( './utils/banners.js' ).banner;
const report = require( './utils/reporters.js' ).report;

// paths
const paths = require( '../../config.js' ).paths;


/// Library/Templating task
// - Precompile .jsx into JavaScript and concatinate files.
// - Run command: `$ gulp t`

gulp.task( 't', () => {

    // init task
    return gulp
        .src( paths.jsxTemplates.src )
        .pipe( plumber( {
            errorHandler: report.taskError
        } ) ) // handle errors
        .pipe( msx( {
            harmony: true // enable ES6 transforms
        } ) ) // compile mithril templates
        .pipe( concat( paths.jsxTemplates.name ) ) // concat
        .pipe( header( banner.doNotEdit ) ) // add banner
        .pipe( gulp.dest( paths.jsxTemplates.dest ) ) // write file
        .pipe( livereload() ); // trigger live repload

} );
