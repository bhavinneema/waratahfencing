/// Environment utilities
'use strict';

const gutil = require( 'gulp-util' );


/// Environments
// - Custom defined. Used in conjuection with gulp task commands and can vary the tasks processes.
// - For example, `$ gulp l --dev` will write sourcemaps and not minify css output file.


// Setup environment defaults
var isDevelopmentMode = false;
var isProductionMode = false;
var isDistructiveMode = false;
var isUsingES6 = false;


// Setup environment flags

// Development environment
// Example use: $ gulp --dev
if ( gutil.env.dev === true ) isDevelopmentMode = true;

// Production environment
// Example use: $ gulp --prod
if ( gutil.env.prod === true ) isProductionMode = true;

// Distructive environment
// Example use: $ gulp clean --distructive
if ( gutil.env.distructive === true ) isDistructiveMode = true;


// Export for use
exports.dev = isDevelopmentMode;
exports.pro = isProductionMode;
exports.distructive = isDistructiveMode;
