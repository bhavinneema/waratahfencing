/// Watcher tasks
'use strict';

// core modules
const gulp = require( 'gulp' );
const livereload = require( 'gulp-livereload' );

// helpers
const report = require( './utils/reporters.js' ).report;

// paths
const paths = require( '../../config.js' ).paths;


/// Watch task
// - Watches for file changes made and runs gulp tasks required to handle changed files.
// - Run command: `$ gulp w`

gulp.task( 'w', () => {

    livereload.listen( {
        'quiet': true
    } );


    // Watch styles
    paths.styles.map( ( entry ) => {

        gulp
            .watch( entry.watch, [ 'l' ] )
            .on( 'change', ( event ) => report.filesChanged( event ) );

    } );


    // Watch scripts
    paths.scripts.map( ( entry ) => {

        gulp
            .watch( entry.watch, [ 'j' ] )
            .on( 'change', ( event ) => report.filesChanged( event ) );

    } );


    // Watch jsx tempaltes to compile
    gulp
        .watch( paths.jsxTemplates.watch, [ 't' ] )
        .on( 'change', ( event ) => report.filesChanged( event ) );


    // Watch stuff to copy
    gulp
        .watch( paths.copy.src, [ 'c' ] )
        .on( 'change', ( event ) => report.filesChanged( event ) );


    // Watch served files and reload when changes made
    gulp
        .watch( paths.server.watch, () => {
            livereload.reload();
        } )
        .on( 'change', ( event ) => report.filesChanged( event ) );

} );
