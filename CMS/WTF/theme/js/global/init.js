$(document).ready(function() {

    WDM.WTF.DatePicker.init();
    WDM.WTF.FollowCurser.init();
    WDM.WTF.ListMenuAccordion.init();
    WDM.WTF.NavigationToggleMenu.init();
    WDM.WTF.ProductAccordion.init();
    WDM.WTF.ProductsImagePopup.init();
    WDM.WTF.ProductsSlider.init();
    WDM.WTF.ProductsTabs.init();
    WDM.WTF.StickyDialog.init();
    WDM.WTF.JSSocial.init();
});
