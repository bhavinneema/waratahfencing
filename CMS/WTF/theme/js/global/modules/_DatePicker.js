var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.DatePicker = WDM.WTF.DatePicker || (function() {

    var init = function() {

        $(".js-date-picker").datepicker({
            dateFormat: "dd/mm/yy"
        });

        $(".cashback-form-checkbox-label").before($(".cashback-form-checkbox-control"));
        $(".cashback-form-checkbox-label2").before($(".cashback-form-checkbox-control2"));
    };

    return {
        init: init
    };

}());
