var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.FollowCurser = WDM.WTF.FollowCurser || (function() {

    var init = function() {

        $('.the-shining').hide();

        $('.navigation').mouseenter(function(e) {
            $('.the-shining').show();
        });

        $('.navigation').mouseleave(function(e) {
            $('.the-shining').hide();
        });

        $('.navigation').mousemove(function(e) {
            $('.the-shining').offset({
                left: e.pageX - 100,
            });
        });
    };

    return {
        init: init
    };

}());