var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.JSSocial = WDM.WTF.JSSocial || (function() {

    var init = function() {

        $("#shareRoundIcons").jsSocials({
            showLabel: false,
            showCount: false,
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest"]
        });

    };

    return {
        init: init
    };

}());