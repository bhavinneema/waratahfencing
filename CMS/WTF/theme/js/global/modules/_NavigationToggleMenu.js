var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.NavigationToggleMenu = WDM.WTF.NavigationToggleMenu || (function() {

    var init = function() {
        $(".nav li a").each(function() {
            if ($(this).next().length > 0) {
                $(this).addClass("parent");
            };
        })

        $(".toggleMenu").click(function(e) {
            e.preventDefault();
            $(this).toggleClass("active");
            $(".nav").toggle();
        });

        $(window).bind('resize orientationchange', function() {
            adjustMenu();
        });

        adjustMenu();
    };


    var adjustMenu = function() {

        if (document.body.clientWidth < 1209) {

            $(".toggleMenu").css("display", "inline-block");

            $('.nav').toggle($(".toggleMenu").hasClass("active"));

            $(".nav li").unbind('mouseenter mouseleave');

            $(".nav li a.parent").unbind('click').bind('click', function(e) {
                // must be attached to anchor element to prevent bubbling
                e.preventDefault();
                $(this).parent("li").toggleClass("hover");
            });

        } else {

            $(".toggleMenu").css("display", "none");

            $(".nav").show();

            $(".nav li").removeClass("hover");

            $(".nav li a").unbind('click');

            $(".nav li").unbind('mouseenter').bind('mouseenter', function() {

                // must be attached to li so that mouseleave is not triggered when hover over submenu
                $(this).addClass('hover');
            });

            $(".nav li").unbind('mouseleave').bind('mouseleave', function() {

                // must be attached to li so that mouseleave is not triggered when hover over submenu
                $(this).removeClass('hover');
            });
        }
    }


    return {
        init: init
    };
}());