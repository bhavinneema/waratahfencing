var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.ProductAccordion = WDM.WTF.ProductAccordion || (function() {

    var init = function() {

        $("#product-details-mobile").accordion({
            animate: 200,
            collapsible: true,
            heightStyle: "content"
        });
    };

    return { 
        init: init
    };

}());
