var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.ProductsImagePopup = WDM.WTF.ProductsImagePopup || (function() {

    var init = function() {

        $('.product-fancybox').fancybox();
    };

    return {
        init: init
    };

}());