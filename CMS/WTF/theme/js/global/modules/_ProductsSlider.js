var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.ProductsSlider = WDM.WTF.ProductsSlider || (function() {

    var config = {
        sliderSettings: {
            centerMode: true,
            arrows: true,
            prevArrow: '<i class="fa fa-chevron-circle-left slick-prev" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-chevron-circle-right slick-next" aria-hidden="true"></i>',
            dots: false,
            slidesToShow: 2,
            autoplay: true,
            autoplaySpeed: 3000,
            variableWidth: true
        }
    }

    var init = function() {
        $('.wdm-slider').slick(config.sliderSettings);
    };

    return {
        init: init
    };

}());