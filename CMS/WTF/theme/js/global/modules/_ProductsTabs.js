var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

// Products Tabs
WDM.WTF.ProductsTabs = WDM.WTF.ProductsTabs || (function() {

    var init = function() {

        $('#product-tabs').tabs({
            load: function(event, ui) {}
        });
    };

    return {
        init: init
    };

}());