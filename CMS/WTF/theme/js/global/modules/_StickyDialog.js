var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

WDM.WTF.StickyDialog = WDM.WTF.StickyDialog || (function() {

    var init = function() {

        if ($('.floating-box').length > 0) {
            var stickySidebar = $('.floating-box').offset().top;
            var floatingboxwidth = $('.floating-box').width();

            if ($(window).width() > 992) {
                $(window).scroll(function() {
                    if ($(window).scrollTop() > stickySidebar) {
                        $('.floating-box').addClass('fixed');
                        $('.floating-box').width(floatingboxwidth);
                    } else {
                        $('.floating-box').removeClass('fixed');
                    }
                });
            }
        }

    };

    return {
        init: init
    };

}());
