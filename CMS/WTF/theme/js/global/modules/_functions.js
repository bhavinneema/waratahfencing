// Revealing modules method examples

var WDM = WDM || {};
WDM.WTF = WDM.WTF || {};

// Product Slider
WDM.WTF.ProductsSlider = WDM.WTF.ProductsSlider || (function() {

    var config = {
        sliderSettings: {
            centerMode: true,
            arrows: true,
            prevArrow: '<i class="fa fa-chevron-circle-left slick-prev" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-chevron-circle-right slick-next" aria-hidden="true"></i>',
            dots: false,
            slidesToShow: 2,
            autoplay: true,
            autoplaySpeed: 3000,
            variableWidth: true
        }
    }

    var init = function() {
        $('.wdm-slider').slick(config.sliderSettings);
    };

    return {
        init: init
    };

}());


// Products Image Popup
WDM.WTF.ProductsImagePopup = WDM.WTF.ProductsImagePopup || (function() {

    var init = function() {

        $('.product-fancybox').fancybox();
    };

    return {
        init: init
    };

}());


// Products Tabs
WDM.WTF.ProductsTabs = WDM.WTF.ProductsTabs || (function() {

    var init = function() {

        $('#product-tabs').tabs({
            load: function(event, ui) {}
        });
    };

    return {
        init: init
    };

}());

// JSSocial
WDM.WTF.JSSocial = WDM.WTF.JSSocial || (function() {

    var init = function() {

        $("#shareRoundIcons").jsSocials({
            showLabel: false,
            showCount: false,
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest"]
        });

    };

    return {
        init: init
    };

}());

WDM.WTF.ProductAccordion = WDM.WTF.ProductAccordion || (function() {

    var init = function() {

        $("#product-details-mobile").accordion({
            animate: 200,
            collapsible: true,
            heightStyle: "content"
        });
    };

    return {
        init: init
    };

}());

WDM.WTF.FollowCurser = WDM.WTF.FollowCurser || (function() {

    var init = function() {

        $('.the-shining').hide();

        $('.navigation').mouseenter(function(e) {
            $('.the-shining').show();
        });

        $('.navigation').mouseleave(function(e) {
            $('.the-shining').hide();
        });

        $('.navigation').mousemove(function(e) {
            $('.the-shining').offset({
                left: e.pageX - 100,
            });
        });
    };

    return {
        init: init
    };

}());
