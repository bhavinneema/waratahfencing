Less Folder
=================

Gulp file paths are configured in the `/theme/config.js` file.

Changes to the less sub-folders and files may require updates to the paths set in the `config.js` file.


## /global/ folder

The `/global/` folder is to contain all your sites global styles. This global stylesheet is intended to be linked to all pages on your website.

The `/global/components` folder is to contain style files to represent components of your website. Such as, `_typography.less`, `_site-header.less`, `_site-footer.less`, `_products.less`, `_carousels.less`, `_search.less` etc. Breaking your styles into components allows for separation of concerns and ease of maintenance and organization.

All `.less` files, with the exception of `imports.less`, should be prefixed with an underscore to represent the file as a "partial". This is a standard and helps indicate that the file should not be compiled directly.

The `imports.less` file is used to import all "partials" and to compose your final output stylesheet.

*Example setup*

/global/  
-- /components/  
----- _typography.less  
----- _site-header.less  
----- _site-footer.less  
-- /mixins/  
----- _clearfix.less  
-- imports.less  
