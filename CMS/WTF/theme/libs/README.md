Libraries Folder
===================

The contents of this this folder are copied the `/public/libraries` folder.


## /libraries folder

The `/libraries` folder are not handled in any way by Gulp, but simple copied to the sites distribution folder for linking on your website.

*Example use*

/libraries
-- jquery-latest.js
