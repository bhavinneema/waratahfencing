﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    public class Account : SfObject
    {
        public Account() {}

        public Account(sObject obj = null) : base(obj)
        {
            Obj.type = "Account";
        }
    }
}
