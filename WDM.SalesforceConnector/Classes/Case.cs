﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    public class Case : SfObject
    {
        private Case() {}

        public Case(sObject obj = null) : base(obj)
        {
            Obj.type = "Case";
        }
    }
}
