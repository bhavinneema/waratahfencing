﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    public class Lead : SfObject
    {
        public Lead() {}

        public Lead(sObject obj = null) : base(obj)
        {
            Obj.type = "Lead";
        }
    }
}
