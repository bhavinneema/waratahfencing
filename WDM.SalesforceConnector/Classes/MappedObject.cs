﻿using CMS.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDM.SalesforceConnector.Classes
{
    public class MappedObject
    {
        public int KenticoObjectID { get; set; }

        public string DotNetClassName { get; set; }

        public string KenticoClassName { get; set; }
        
        public SfObject SalesforceObject { get; set; }

        public bool IsParentObject { get; set; }
    }
}
