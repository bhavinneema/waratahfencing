﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    public class Product : SfObject
    {
        public Product() { }

        public Product(sObject obj = null) : base(obj)
        {
            Obj.type = "Product2";
        }

        public string GetProductLevelPath(string separator = "/")
        {
            var result = new List<string>();

            var level1 = GetString("Product_Level_1__c");
            if (level1.Length > 0)
            {
                result.Add(level1);
            }
            else
            {
                return string.Join(separator, result);
            }

            var level2 = GetString("Product_Level_2__c");
            if (level2.Length > 0)
            {
                result.Add(level2);
            }
            else
            {
                return string.Join(separator, result);
            }

            var level3 = GetString("Product_Level_3__c");
            if (level3.Length > 0)
            {
                result.Add(level3);
            }
            else
            {
                return string.Join(separator, result);
            }

            var level4 = GetString("Product_Level_4__c");
            if (level4.Length > 0)
            {
                result.Add(level4);
            }
            else
            {
                return string.Join(separator, result);
            }

            var level5 = GetString("Product_Level_5__c");
            if (level5.Length > 0)
            {
                result.Add(level5);
            }
            else
            {
                return string.Join(separator, result);
            }

            var level6 = GetString("Product_Level_6__c");
            if (level6.Length > 0)
            {
                result.Add(level6);
            }
            else
            {
                return string.Join(separator, result);
            }

            return string.Join(separator, result);
        }
    }
}
