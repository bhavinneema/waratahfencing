﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    public class Promotion : SfObject
    {
        public Promotion() { }

        public Promotion(sObject obj = null) : base(obj)
        {
            Obj.type = "Promotion__c";
        }
    }
}
