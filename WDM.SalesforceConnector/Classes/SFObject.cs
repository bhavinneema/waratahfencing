﻿using System;
using System.Collections.Generic;
using System.Xml;

using Newtonsoft.Json;

using CMS.Helpers;

using WDM.SalesforceConnector.JsonConverters;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Classes
{
    [JsonConverter(typeof(SfObjectConverter))]
    public class SfObject
    {
        public sObject Obj { get; set; }

        public SfObject(sObject obj = null)
        {
            Obj = obj ?? new sObject();
        }

        public string ParentIDFieldName
        {
            get;set;
        }

        private XmlDocument _doc;

        private XmlDocument Doc
        {
            get
            {
                return _doc ?? (_doc = new XmlDocument());
            }
            set
            {
                _doc = value;
            }
        }

        private Dictionary<string, XmlElement> _fields;
        private Dictionary<string, XmlElement> Fields
        {
            get
            {
                return _fields ?? (_fields = new Dictionary<string, XmlElement>());
            }
            set
            {
                _fields = value;
            }
        }

        public string GetProperty(string fieldName)
        {
            if (Obj.Any == null)
                return null;
            var internalPropertyName = string.Format("sf:{0}", fieldName);
            XmlElement property = null;
            foreach (var p in Obj.Any)
            {
                if (p.Name == internalPropertyName)
                {
                    property = p;
                    break;
                }
            }
            return property?.InnerText;
        }

        public string GetString(string fieldName, string defaultValue = "")
        {
            return ValidationHelper.GetString(GetProperty(fieldName), defaultValue);
        }

        public int GetInteger(string fieldName, int defaultValue = 0)
        {
            return ValidationHelper.GetInteger(GetProperty(fieldName), defaultValue);
        }

        public double GetDouble(string fieldName, double defaultValue = 0)
        {
            return ValidationHelper.GetDouble(GetProperty(fieldName), defaultValue);
        }

        public bool GetBoolean(string fieldName, bool defaultValue)
        {
            return ValidationHelper.GetBoolean(GetProperty(fieldName), defaultValue);
        }

        public DateTime GetDateTime(string fieldName, DateTime defaultValue)
        {
            return ValidationHelper.GetDateTime(GetProperty(fieldName), defaultValue);
        }

        public void SetValue(string fieldName, object value)
        {
            if (!Fields.ContainsKey(fieldName))
                Fields.Add(fieldName, Doc.CreateElement(fieldName));
            var field = Fields[fieldName];
            field.InnerText = ValidationHelper.GetString(value, string.Empty);

            var xmlElements = new List<XmlElement>();
            foreach (KeyValuePair<string, XmlElement> element in Fields)
            {
                if (element.Value.NodeType == XmlNodeType.Element)
                {
                    xmlElements.Add(element.Value);
                }
            }

            Obj.Any = xmlElements.ToArray();
        }

        public void SetValue(string fieldName, DateTime value)
        {
            SetValue(fieldName, DateTime.SpecifyKind(value,DateTimeKind.Local).ToString("o"));
        }
    }
}
