﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Connectors
{
    public partial class SalesforceConnector
    {
        public UpsertResult Upsert(string x, sObject @object)
        {
            return _service.upsert(x, new []{ @object })[0];
        }
    }
}
