﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.PartnerServices;

namespace WDM.SalesforceConnector.Connectors
{
    public partial class SalesforceConnector
    {
        public List<T> Query<T>(string soqlQuery) where T : SfObject, new()
        {
            var result = new List<T>();
            if (!_loggedIn)
            {
                Login();
            }
            if (_loggedIn)
            {
                var qr = _service.query(soqlQuery);

                bool done = false;

                if (qr.size > 0)
                {
                    while (!done)
                    {
                        foreach (var record in qr.records)
                        {
                            result.Add(new T() { Obj = record });
                        }
                        if (qr.done)
                        {
                            done = true;
                        }
                        else
                        {
                            qr = _service.queryMore(qr.queryLocator);
                        }
                    }

                }
            }
            return result;

        }


        public SfObject GetObjectByID(string salesforceId, string salesforceClass)
        {
            var result = new List<SfObject>();
            if (!_loggedIn)
            {
                Login();
            }
            if (_loggedIn)
            {
                var qr = _service.query(string.Format("SELECT {0} FROM {1} WHERE Id='{2}'",
                    string.Join(", ", GetFieldsInternal(salesforceClass)),
                    salesforceClass,
                    salesforceId));

                bool done = false;

                if (qr.size > 0)
                {
                    while (!done)
                    {
                        foreach (var record in qr.records)
                        {
                            result.Add(new SfObject { Obj = record });
                        }
                        if (qr.done)
                        {
                            done = true;
                        }
                        else
                        {
                            qr = _service.queryMore(qr.queryLocator);
                        }
                    }
                }
            }
            return result.First();
        }


        public List<string> DefaultAccountFieldsList
        {
            get
            {
                return GetFieldsInternal("Account");
            }
        }


        public List<string> DefaultContactFieldsList
        {
            get
            {
                return GetFieldsInternal("Contact");
            }
        }

        public List<Promotion> GetPromotions(string soqlQuery="")
        {
            return Query<Promotion>(soqlQuery);
        }

        public List<string> DefaultLeadFieldsList
        {
            get
            {
                return GetFieldsInternal("Lead");
            }
        }


        public List<string> DefaultProductFieldsList
        {
            get
            {
                return GetFieldsInternal("Product2");
            }
        }


        public string DescribeObject(string sObjectType)
        {
            var result = DescribeObjectInternal(sObjectType);
            if (result == null)
            {
                return string.Empty;
            }
            return JsonConvert.SerializeObject(result);
        }


        public List<Account> GetAccounts(string soqlQuery = "")
        {
            var result = new List<Account>();

            if (soqlQuery.Length <= 0)
            {
                soqlQuery = string.Format("SELECT {0} FROM Account",
                    string.Join(", ", DefaultAccountFieldsList));
            }
            else
            {
                soqlQuery = string.Format(soqlQuery,
                    string.Join(", ", DefaultAccountFieldsList));
            }

            return Query<Account>(soqlQuery);
        }


        public List<Contact> GetContacts(string soqlQuery = "")
        {
            if (soqlQuery.Length <= 0)
            {
                soqlQuery = string.Format("SELECT {0} FROM Contact",
                    string.Join(", ", DefaultContactFieldsList));
            }
            else
            {
                soqlQuery = string.Format(soqlQuery,
                    string.Join(", ", DefaultContactFieldsList));
            }

            return Query<Contact>(soqlQuery);
        }


        public List<Contact> GetContactsByContactID(string salesForceId)
        {
            return GetContacts(string.Format("SELECT {0} FROM Contact WHERE Id = '{1}'",
                string.Join(", ", DefaultContactFieldsList),
                salesForceId));
        }


        public List<Contact> GetContactsByUserID(int userId)
        {
            return GetContacts(string.Format("SELECT {0} FROM Contact WHERE Site_Core_Username__c = '{1}'",
                string.Join(", ", DefaultContactFieldsList),
                userId));
        }


        public List<Contact> GetContactsByEmail(string email)
        {
            return GetContacts(string.Format("SELECT {0} FROM Contact WHERE Email = '{1}'",
                string.Join(", ", DefaultContactFieldsList),
                email));
        }


        public List<Lead> GetLeads(string soqlQuery = "")
        {
            if (soqlQuery.Length <= 0)
            {
                soqlQuery = string.Format("SELECT {0} FROM Lead", string.Join(", ", DefaultLeadFieldsList));
            }
            else
            {
                soqlQuery = string.Format(soqlQuery, string.Join(", ", DefaultLeadFieldsList));
            }

            return Query<Lead>(soqlQuery);
        }


        public List<Lead> GetLeadsByLeadID(string salesForceId)
        {
            return GetLeads(string.Format("SELECT {0} FROM Lead WHERE Id = '{1}'",
                string.Join(", ", DefaultLeadFieldsList),
                salesForceId));
        }


        public List<Lead> GetLeadsByUserID(int userId)
        {
            return GetLeads(string.Format("SELECT {0} FROM Lead WHERE Site_Core_Username__c = '{1}'",
                string.Join(", ", DefaultLeadFieldsList),
                userId));
        }


        public List<Lead> GetLeadsByEmail(string email)
        {
            return GetLeads(string.Format("SELECT {0} FROM Lead WHERE Email = '{1}'",
                string.Join(", ", DefaultLeadFieldsList),
                email));
        }


        public List<Product> GetProducts(string soqlQuery = "")
        {
            var result = new List<Product>();

            if (soqlQuery.Length <= 0)
            {
                soqlQuery = string.Format("SELECT {0} FROM Product2", string.Join(", ", DefaultProductFieldsList));
            }
            else
            {
                soqlQuery = string.Format(soqlQuery, string.Join(", ", DefaultProductFieldsList));
            }

            return Query<Product>(soqlQuery);
        }


        private DescribeSObjectResult DescribeObjectInternal(string sObjectType)
        {
            return _service.describeSObject(sObjectType);
        }


        private List<string> GetFieldsInternal(string sObjectType)
        {
            var result = DescribeObjectInternal(sObjectType);
            if (result == null)
            {
                // Todo: Log or return errors?
                return null;
            }

            return result.fields.Select(x => x.name).ToList();
        }
    }
}