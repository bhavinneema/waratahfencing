﻿using System;
using System.Collections.Generic;
using System.Net;
using CMS.Helpers;
using Newtonsoft.Json;

using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.PartnerServices;
using WDM.SalesforceConnector.Settings;

namespace WDM.SalesforceConnector.Connectors
{
    public partial class SalesforceConnector : IDisposable
    {
        private static SalesforceConnector _instance;

        private SforceService _service;
        private bool _loggedIn;
        private ISalesForceConnectorSettings _settings;
        private string _sfServiceEndpoint;
        private string _sfUsername;
        private string _sfPassword;


        public SalesforceConnector(ISalesForceConnectorSettings settings)
        {
            _settings = settings;
            _service = new SforceService();
            _loggedIn = false;
            Login();
        }


        ~SalesforceConnector()
        {
            Dispose(false);
        }


        public void Login()
        {
            if (_loggedIn)
            {
                _service.logout();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _service.Url = _settings.ServiceEndpointUrl;
            LoginResult loginResult = _service.login(_settings.Username, _settings.Password);

            _service.Url = loginResult.serverUrl;
            _service.SessionHeaderValue = new SessionHeader();
            _service.SessionHeaderValue.sessionId = loginResult.sessionId;

            _loggedIn = true;
        }


        public void Logout()
        {
            if (!_loggedIn)
            {
                return;
            }

            _service.logout();
            _loggedIn = false;
        }


        private void ReleaseUnmanagedResources()
        {
            // TODO release unmanaged resources here
            Logout();
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _service?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}