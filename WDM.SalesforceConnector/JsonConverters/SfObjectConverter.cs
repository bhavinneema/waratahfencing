﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SalesforceConnector.Classes;

namespace WDM.SalesforceConnector.JsonConverters
{
    public class SfObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(SfObject))
            {
                return true;
            }
            return false;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            SfObject sfObject = new SfObject();
            JObject item = JObject.Load(reader);
            JObject sfObj = item.First.First as JObject;
            var sfType = sfObj.SelectToken("type").Value<string>();
            var parentIdField = item.First.Next as JProperty;
            sfObject.ParentIDFieldName = parentIdField.Value.Value<string>();
            sfObject.Obj.type = sfType;
            var propertiesArray = sfObj.SelectToken("Any").ToArray();
            foreach (var jToken in propertiesArray)
            {
                JProperty prop = jToken.First as JProperty;
                if (prop.Value.Type == JTokenType.Date)
                {
                    sfObject.SetValue(prop.Name, prop.Value.ToObject<DateTime>());
                }
                else sfObject.SetValue(prop.Name, prop.Value.ToObject<object>());
            }
            return sfObject;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
