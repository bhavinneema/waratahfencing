﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDM.SalesforceConnector.Settings
{
    public interface ISalesForceConnectorSettings
    {
        string ServiceEndpointUrl { get; }
        string Username { get; }
        string Password { get; }
    }
}
