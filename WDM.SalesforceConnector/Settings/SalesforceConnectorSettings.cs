﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDM.SalesforceConnector.Settings
{
    public class SalesforceConnectorSettings : ISalesForceConnectorSettings
    {
        public string ServiceEndpointUrl { get; }
        public string Username { get; }
        public string Password { get; }

        public SalesforceConnectorSettings(string serviceEndpointUrl, string username, string password)
        {
            ServiceEndpointUrl = serviceEndpointUrl;
            Username = username;
            Password = password;
        }
    }
}
