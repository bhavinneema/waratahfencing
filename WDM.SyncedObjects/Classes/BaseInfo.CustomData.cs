﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.DataEngine
{
    public class BaseInfod
    {
        public List<int> TriggerSyncedObjectIds { get; set; } = new List<int>();
    }
}