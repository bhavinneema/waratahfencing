﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Helpers;

namespace WDM.SyncedObjects.Classes
{
    public partial class SyncLogInfo
    {
        private ContainerCustomData _consumerData;

        public ContainerCustomData CustomData => _consumerData ?? (_consumerData = new ContainerCustomData(this, nameof(SyncLogCustomData)));
    }
}
