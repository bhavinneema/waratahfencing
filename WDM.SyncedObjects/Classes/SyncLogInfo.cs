﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WDM.SyncedObjects.Classes;

[assembly: RegisterObjectType(typeof(SyncLogInfo), SyncLogInfo.OBJECT_TYPE)]

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// Data container class for <see cref="SyncLogInfo"/>.
    /// </summary>
	[Serializable]
    public partial class SyncLogInfo : AbstractInfo<SyncLogInfo>
    {
        /// <summary>
        /// Object type.
        /// </summary>
        public const string OBJECT_TYPE = "wdm.synclog";


        /// <summary>
        /// Type information.
        /// </summary>
        public static readonly ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SyncLogInfoProvider), OBJECT_TYPE, "WDM.SyncLog", "SyncLogID", "SyncLogLastModified", "SyncLogGuid", null, null, null, null, "SyncLogSyncedObjectID", SyncedObjectInfo.OBJECT_TYPE)
        {
            ModuleName = "WDM.SyncedObjects",
            TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>()
            {
                new ObjectDependency("SyncLogSyncedObjectID", "wdm.syncedobject", ObjectDependencyEnum.Required),
            },
        };


        /// <summary>
        /// Sync log ID.
        /// </summary>
        [DatabaseField]
        public virtual int SyncLogID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncLogID"), 0);
            }
            set
            {
                SetValue("SyncLogID", value);
            }
        }


        /// <summary>
        /// Sync log synced object ID.
        /// </summary>
        [DatabaseField]
        public virtual int SyncLogSyncedObjectID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncLogSyncedObjectID"), 0);
            }
            set
            {
                SetValue("SyncLogSyncedObjectID", value, 0);
            }
        }


        /// <summary>
        /// Sync log kentico item ID.
        /// </summary>
        [DatabaseField]
        public virtual int SyncLogKenticoItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncLogKenticoItemID"), 0);
            }
            set
            {
                SetValue("SyncLogKenticoItemID", value, 0);
            }
        }


        /// <summary>
        /// Sync log crm item ID.
        /// </summary>
        [DatabaseField]
        public virtual string SyncLogCrmItemID
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncLogCrmItemID"), String.Empty);
            }
            set
            {
                SetValue("SyncLogCrmItemID", value, String.Empty);
            }
        }


        /// <summary>
        /// Sync log status.
        /// </summary>
        [DatabaseField]
        public virtual int SyncLogStatus
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncLogStatus"), 0);
            }
            set
            {
                SetValue("SyncLogStatus", value, 0);
            }
        }


        /// <summary>
        /// Sync log attempt timestamp.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SyncLogAttemptTimestamp
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SyncLogAttemptTimestamp"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SyncLogAttemptTimestamp", value, DateTimeHelper.ZERO_TIME);
            }
        }


        /// <summary>
        /// Sync log request.
        /// </summary>
        [DatabaseField]
        public virtual string SyncLogRequest
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncLogRequest"), String.Empty);
            }
            set
            {
                SetValue("SyncLogRequest", value, String.Empty);
            }
        }


        /// <summary>
        /// Sync log response.
        /// </summary>
        [DatabaseField]
        public virtual string SyncLogResponse
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncLogResponse"), String.Empty);
            }
            set
            {
                SetValue("SyncLogResponse", value, String.Empty);
            }
        }


        /// <summary>
        /// Sync log custom data
        /// </summary>
        [DatabaseField]
        public virtual string SyncLogCustomData
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncLogCustomData"), String.Empty);
            }
            set
            {
                SetValue("SyncLogCustomData", value, String.Empty);
            }
        }


        /// <summary>
        /// Sync log guid.
        /// </summary>
        [DatabaseField]
        public virtual Guid SyncLogGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SyncLogGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SyncLogGuid", value);
            }
        }


        /// <summary>
        /// Sync log last modified.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SyncLogLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SyncLogLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SyncLogLastModified", value);
            }
        }


        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SyncLogInfoProvider.DeleteSyncLogInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SyncLogInfoProvider.SetSyncLogInfo(this);
        }


        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        protected SyncLogInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Creates an empty instance of the <see cref="SyncLogInfo"/> class.
        /// </summary>
        public SyncLogInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Creates a new instances of the <see cref="SyncLogInfo"/> class from the given <see cref="DataRow"/>.
        /// </summary>
        /// <param name="dr">DataRow with the object data.</param>
        public SyncLogInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }
    }
}