﻿using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// Class providing <see cref="SyncLogInfo"/> management.
    /// </summary>
    public partial class SyncLogInfoProvider : AbstractInfoProvider<SyncLogInfo, SyncLogInfoProvider>
    {
        /// <summary>
        /// Creates an instance of <see cref="SyncLogInfoProvider"/>.
        /// </summary>
        public SyncLogInfoProvider()
            : base(SyncLogInfo.TYPEINFO)
        {
        }


        /// <summary>
        /// Returns a query for all the <see cref="SyncLogInfo"/> objects.
        /// </summary>
        public static ObjectQuery<SyncLogInfo> GetSyncLogs()
        {
            return ProviderObject.GetObjectQuery();
        }


        /// <summary>
        /// Returns <see cref="SyncLogInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncLogInfo"/> ID.</param>
        public static SyncLogInfo GetSyncLogInfo(int id)
        {
            return ProviderObject.GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified <see cref="SyncLogInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncLogInfo"/> to be set.</param>
        public static void SetSyncLogInfo(SyncLogInfo infoObj)
        {
            ProviderObject.SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified <see cref="SyncLogInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncLogInfo"/> to be deleted.</param>
        public static void DeleteSyncLogInfo(SyncLogInfo infoObj)
        {
            ProviderObject.DeleteInfo(infoObj);
        }


        /// <summary>
        /// Deletes <see cref="SyncLogInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncLogInfo"/> ID.</param>
        public static void DeleteSyncLogInfo(int id)
        {
            SyncLogInfo infoObj = GetSyncLogInfo(id);
            DeleteSyncLogInfo(infoObj);
        }
    }
}