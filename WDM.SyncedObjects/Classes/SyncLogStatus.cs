﻿namespace WDM.SyncedObjects.Classes
{
    public enum SyncLogStatus
    {
        New,
        AwaitingResponse,
        Synced,
        Failed
    }
}
