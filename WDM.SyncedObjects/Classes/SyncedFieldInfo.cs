﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WDM.SyncedObjects.Classes;

[assembly: RegisterObjectType(typeof(SyncedFieldInfo), SyncedFieldInfo.OBJECT_TYPE)]

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// SyncedFieldInfo data container class.
    /// </summary>
	[Serializable]
    public partial class SyncedFieldInfo : AbstractInfo<SyncedFieldInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wdm.syncedfield";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SyncedFieldInfoProvider), OBJECT_TYPE, "WDM.SyncedField", "SyncedFieldID", "SyncedFieldLastModified", "SyncedFieldGuid", null, "SyncedFieldKenticoFieldName", null, null, "SyncedFieldSyncedObjectID", SyncedObjectInfo.OBJECT_TYPE)
        {
            ModuleName = "WDM.SyncedObjects",
            TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>()
            {
                new ObjectDependency("SyncedFieldSyncedObjectID", "wdm.syncedobject", ObjectDependencyEnum.Required),
            },
            ContinuousIntegrationSettings =
            {
                Enabled=true
            },
            OrderColumn = "SyncedFieldOrder"
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Synced field ID
        /// </summary>
        [DatabaseField]
        public virtual int SyncedFieldID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedFieldID"), 0);
            }
            set
            {
                SetValue("SyncedFieldID", value);
            }
        }


        /// <summary>
        /// Synced field synced object ID
        /// </summary>
        [DatabaseField]
        public virtual int SyncedFieldSyncedObjectID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedFieldSyncedObjectID"), 0);
            }
            set
            {
                SetValue("SyncedFieldSyncedObjectID", value);
            }
        }


        /// <summary>
        /// Synced field kentico field name
        /// </summary>
        [DatabaseField]
        public virtual string SyncedFieldKenticoFieldName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedFieldKenticoFieldName"), String.Empty);
            }
            set
            {
                SetValue("SyncedFieldKenticoFieldName", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced field crm field name
        /// </summary>
        [DatabaseField]
        public virtual string SyncedFieldCrmFieldName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedFieldCrmFieldName"), String.Empty);
            }
            set
            {
                SetValue("SyncedFieldCrmFieldName", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced field data type
        /// </summary>
        [DatabaseField]
        public virtual string SyncedFieldDataType
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedFieldDataType"), "string");
            }
            set
            {
                SetValue("SyncedFieldDataType", value);
            }
        }


        /// <summary>
        /// Synced field macro
        /// </summary>
        [DatabaseField]
        public virtual string SyncedFieldMacro
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedFieldMacro"), String.Empty);
            }
            set
            {
                SetValue("SyncedFieldMacro", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced field custom data property
        /// </summary>
        [DatabaseField]
        public virtual string SyncedFieldCustomDataProperty
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedFieldCustomDataProperty"), String.Empty);
            }
            set
            {
                SetValue("SyncedFieldCustomDataProperty", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced field order
        /// </summary>
        [DatabaseField]
        public virtual int SyncedFieldOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedFieldOrder"), 0);
            }
            set
            {
                SetValue("SyncedFieldOrder", value, 0);
            }
        }


        /// <summary>
        /// Synced field guid
        /// </summary>
        [DatabaseField]
        public virtual Guid SyncedFieldGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SyncedFieldGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SyncedFieldGuid", value);
            }
        }


        /// <summary>
        /// Synced field last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime SyncedFieldLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SyncedFieldLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SyncedFieldLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SyncedFieldInfoProvider.DeleteSyncedFieldInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SyncedFieldInfoProvider.SetSyncedFieldInfo(this);
        }

        #endregion


        #region "Constructors"

        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected SyncedFieldInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty SyncedFieldInfo object.
        /// </summary>
        public SyncedFieldInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new SyncedFieldInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public SyncedFieldInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}