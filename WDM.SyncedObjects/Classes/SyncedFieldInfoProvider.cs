﻿using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// Class providing <see cref="SyncedFieldInfo"/> management.
    /// </summary>
    public partial class SyncedFieldInfoProvider : AbstractInfoProvider<SyncedFieldInfo, SyncedFieldInfoProvider>
    {
        /// <summary>
        /// Creates an instance of <see cref="SyncedFieldInfoProvider"/>.
        /// </summary>
        public SyncedFieldInfoProvider()
            : base(SyncedFieldInfo.TYPEINFO)
        {
        }


        /// <summary>
        /// Returns a query for all the <see cref="SyncedFieldInfo"/> objects.
        /// </summary>
        public static ObjectQuery<SyncedFieldInfo> GetSyncedFields()
        {
            return ProviderObject.GetObjectQuery();
        }


        /// <summary>
        /// Returns <see cref="SyncedFieldInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncedFieldInfo"/> ID.</param>
        public static SyncedFieldInfo GetSyncedFieldInfo(int id)
        {
            return ProviderObject.GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified <see cref="SyncedFieldInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncedFieldInfo"/> to be set.</param>
        public static void SetSyncedFieldInfo(SyncedFieldInfo infoObj)
        {
            ProviderObject.SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified <see cref="SyncedFieldInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncedFieldInfo"/> to be deleted.</param>
        public static void DeleteSyncedFieldInfo(SyncedFieldInfo infoObj)
        {
            ProviderObject.DeleteInfo(infoObj);
        }


        /// <summary>
        /// Deletes <see cref="SyncedFieldInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncedFieldInfo"/> ID.</param>
        public static void DeleteSyncedFieldInfo(int id)
        {
            SyncedFieldInfo infoObj = GetSyncedFieldInfo(id);
            DeleteSyncedFieldInfo(infoObj);
        }
    }
}