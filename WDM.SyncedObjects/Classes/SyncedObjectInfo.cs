﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WDM.SyncedObjects.Classes;

[assembly: RegisterObjectType(typeof(SyncedObjectInfo), SyncedObjectInfo.OBJECT_TYPE)]

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// SyncedObjectInfo data container class.
    /// </summary>
	[Serializable]
    public partial class SyncedObjectInfo : AbstractInfo<SyncedObjectInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wdm.syncedobject";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SyncedObjectInfoProvider), OBJECT_TYPE, "WDM.SyncedObject", "SyncedObjectID", "SyncedObjectLastModified", "SyncedObjectGuid", "SyncedObjectCodename", "SyncedObjectDisplayName", null, null, "SyncedObjectKenticoClass", DataClassInfo.OBJECT_TYPE)
        {
            ModuleName = "WDM.SyncedObjects",
            TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>()
            {
                new ObjectDependency("SyncedObjectKenticoClass", "cms.class", ObjectDependencyEnum.Required),
            },
            ContinuousIntegrationSettings =
            {
                Enabled = true
            }
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Synced object ID
        /// </summary>
        [DatabaseField]
        public virtual int SyncedObjectID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedObjectID"), 0);
            }
            set
            {
                SetValue("SyncedObjectID", value);
            }
        }


        /// <summary>
        /// Synced object display name
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectDisplayName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectDisplayName"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectDisplayName", value);
            }
        }


        /// <summary>
        /// Synced object codename
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectCodename
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectCodename"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectCodename", value);
            }
        }


        /// <summary>
        /// Synced object kentico class
        /// </summary>
        [DatabaseField]
        public virtual int SyncedObjectKenticoClass
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedObjectKenticoClass"), 0);
            }
            set
            {
                SetValue("SyncedObjectKenticoClass", value, 0);
            }
        }


        /// <summary>
        /// Synced object CRM class
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectCRMClass
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectCRMClass"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectCRMClass", value, String.Empty);
            }
        }


        /// <summary>
        /// Which field in the Kentico object to store the returned CRM GUID
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectKenticoReturnFieldName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectKenticoReturnFieldName"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectKenticoReturnFieldName", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced object connector assembly
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectConnectorAssembly
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectConnectorAssembly"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectConnectorAssembly", value, String.Empty);
            }
        }


        /// <summary>
        /// Synced object connector class
        /// </summary>
        [DatabaseField]
        public virtual string SyncedObjectConnectorClass
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SyncedObjectConnectorClass"), String.Empty);
            }
            set
            {
                SetValue("SyncedObjectConnectorClass", value, String.Empty);
            }
        }


        /// <summary>
        /// When you want to sync a single Kentico object to multiple CRM objects, you can specify the order in which objects are added to the queue.
        /// 
        /// This can be handy when the CRMID returned by one sync is used in another.
        /// 
        /// Lower numbers are synced first.
        /// 
        /// Note: The Kentico object will need a separate CRMID return field for each synced object.
        /// </summary>
        [DatabaseField]
        public virtual int SyncedObjectSyncOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SyncedObjectSyncOrder"), 0);
            }
            set
            {
                SetValue("SyncedObjectSyncOrder", value, 0);
            }
        }


        /// <summary>
        /// Synced object check item before sync
        /// </summary>
        [DatabaseField]
        public virtual bool SyncedObjectCheckItemBeforeSync
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("SyncedObjectCheckItemBeforeSync"), false);
            }
            set
            {
                SetValue("SyncedObjectCheckItemBeforeSync", value);
            }
        }


        /// <summary>
        /// This is necessary when you're relying on the result of a previous sync in the same  batch.
        /// </summary>
        [DatabaseField]
        public virtual bool SyncedObjectRebuildBeforeSync
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("SyncedObjectRebuildBeforeSync"), false);
            }
            set
            {
                SetValue("SyncedObjectRebuildBeforeSync", value);
            }
        }


        /// <summary>
        /// Synced object guid
        /// </summary>
        [DatabaseField]
        public virtual Guid SyncedObjectGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SyncedObjectGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SyncedObjectGuid", value);
            }
        }


        /// <summary>
        /// Synced object last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime SyncedObjectLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SyncedObjectLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SyncedObjectLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SyncedObjectInfoProvider.DeleteSyncedObjectInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SyncedObjectInfoProvider.SetSyncedObjectInfo(this);
        }

        #endregion


        #region "Constructors"

        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected SyncedObjectInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty SyncedObjectInfo object.
        /// </summary>
        public SyncedObjectInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new SyncedObjectInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public SyncedObjectInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}