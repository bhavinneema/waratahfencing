﻿using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WDM.SyncedObjects.Classes
{
    /// <summary>
    /// Class providing <see cref="SyncedObjectInfo"/> management.
    /// </summary>
    public partial class SyncedObjectInfoProvider : AbstractInfoProvider<SyncedObjectInfo, SyncedObjectInfoProvider>
    {
        /// <summary>
        /// Creates an instance of <see cref="SyncedObjectInfoProvider"/>.
        /// </summary>
        public SyncedObjectInfoProvider()
            : base(SyncedObjectInfo.TYPEINFO)
        {
        }


        /// <summary>
        /// Returns a query for all the <see cref="SyncedObjectInfo"/> objects.
        /// </summary>
        public static ObjectQuery<SyncedObjectInfo> GetSyncedObjects()
        {
            return ProviderObject.GetObjectQuery();
        }


        /// <summary>
        /// Returns <see cref="SyncedObjectInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncedObjectInfo"/> ID.</param>
        public static SyncedObjectInfo GetSyncedObjectInfo(int id)
        {
            return ProviderObject.GetInfoById(id);
        }


        /// <summary>
        /// Returns <see cref="SyncedObjectInfo"/> with specified name.
        /// </summary>
        /// <param name="name"><see cref="SyncedObjectInfo"/> name.</param>
        public static SyncedObjectInfo GetSyncedObjectInfo(string name)
        {
            return ProviderObject.GetInfoByCodeName(name);
        }


        /// <summary>
        /// Sets (updates or inserts) specified <see cref="SyncedObjectInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncedObjectInfo"/> to be set.</param>
        public static void SetSyncedObjectInfo(SyncedObjectInfo infoObj)
        {
            ProviderObject.SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified <see cref="SyncedObjectInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SyncedObjectInfo"/> to be deleted.</param>
        public static void DeleteSyncedObjectInfo(SyncedObjectInfo infoObj)
        {
            ProviderObject.DeleteInfo(infoObj);
        }


        /// <summary>
        /// Deletes <see cref="SyncedObjectInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SyncedObjectInfo"/> ID.</param>
        public static void DeleteSyncedObjectInfo(int id)
        {
            SyncedObjectInfo infoObj = GetSyncedObjectInfo(id);
            DeleteSyncedObjectInfo(infoObj);
        }
    }
}