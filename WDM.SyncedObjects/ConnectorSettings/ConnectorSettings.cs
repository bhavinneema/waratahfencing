﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDM.SyncedObjects.ConnectorSettings
{
    public class ConnectorSettings
    {
        private Dictionary<string, string> _val { get; }
        public string this[string key]
        {
            get
            {
                return GetValue(key);
            }
        }

        private string GetValue(string key)
        {
            return _val[key];
        }

        public ConnectorSettings()
        {
            _val = new Dictionary<string, string>();
        }

        public void Add(string key, string value)
        {
            _val.Add(key, value);
        }
    }
}
