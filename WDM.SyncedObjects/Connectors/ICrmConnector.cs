﻿using System.Collections.Generic;

using WDM.SyncedObjects.Classes;

namespace WDM.SyncedObjects.Connectors
{
    public interface ICrmConnector
    {
        List<string> GetRequiredSettingKeys();

        void SetSettings(ConnectorSettings.ConnectorSettings settings);

        string BuildRequestBody(List<RequestField> requestFields, int itemId = 0);

        SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo);
        
        string GetFields(string crmModule);

        string GetEnum(string crmModule, string crmId);
    }
}