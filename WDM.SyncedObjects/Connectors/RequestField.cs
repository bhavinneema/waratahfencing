﻿namespace WDM.SyncedObjects.Connectors
{
    public class RequestField
    {
        public string CrmFieldName { get; set; }

        public string DataType { get; set; }

        public object Value { get; set; }

        public string CustomDataProperty { get; set; }
        public string SourceFieldName { get; set; }
    }
}