﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Base;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Membership;
using CMS.OnlineForms;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Helpers;

namespace WDM.SyncedObjects.EventHandlers
{
    public class SyncedObjectsEvents
    {
        private static List<int?> SyncedClasses => SyncHelper.GetSyncedClasses();

        public static void RegisterEvents()
        {
            // Classes
            ObjectEvents.Insert.Before += CheckChangedColumnsBeforeInsert;
            ObjectEvents.Update.Before += CheckChangedColumnsBeforeUpdate;

            ObjectEvents.Insert.After += SyncAfterInsert;
            ObjectEvents.Update.After += SyncAfterUpdate;
        }

        private static void CheckChangedColumnsBeforeInsert(object sender, ObjectEventArgs e)
        {
            var dci = DataClassInfoProvider.GetDataClassInfo(e.Object.TypeInfo.ObjectClassName);
            if (!SyncedClasses.Contains(dci.ClassID))
            {
                return;
            }

            var bi = e.Object;

            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            if (syncedObjects.Count == 0)
            {
                return;
            }

            var ids = SyncHelper.GetSyncedObjectsToTrigger(bi, bi.ChangedColumns(), syncedObjects).Select(x => x.SyncedObjectID).ToList();

            bi.RelatedData = ids;

            bi["SyncedObjectsToSync"] = ids;
        }

        private static void SyncAfterInsert(object sender, ObjectEventArgs e)
        {
            var dci = DataClassInfoProvider.GetDataClassInfo(e.Object.TypeInfo.ObjectClassName);
            if (!SyncedClasses.Contains(dci.ClassID))
            {
                return;
            }

            var bi = e.Object;

            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            if (syncedObjects.Count == 0)
            {
                return;
            }

            var ids = bi.RelatedData as List<int> ?? new List<int>();

            var syncedObjectsToSync = SyncHelper.GetTriggeredSyncedObjects(ids);

            foreach (var syncedObject in syncedObjectsToSync)
            {
                SyncHelper.QueueItem(syncedObject, bi);
            }
        }

        private static void CheckChangedColumnsBeforeUpdate(object sender, ObjectEventArgs e)
        {
            var dci = DataClassInfoProvider.GetDataClassInfo(e.Object.TypeInfo.ObjectClassName);
            if (!SyncedClasses.Contains(dci.ClassID))
            {
                return;
            }

            var bi = e.Object;

            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            if (syncedObjects.Count == 0)
            {
                return;
            }

            var ids = SyncHelper.GetSyncedObjectsToTrigger(bi, bi.ChangedColumns(), syncedObjects).Select(x => x.SyncedObjectID).ToList();

            bi.RelatedData = ids;

            bi["SyncedObjectsToSync"] = ids;
        }

        private static void SyncAfterUpdate(object sender, ObjectEventArgs e)
        {
            var dci = DataClassInfoProvider.GetDataClassInfo(e.Object.TypeInfo.ObjectClassName);
            if (!SyncedClasses.Contains(dci.ClassID))
            {
                return;
            }

            var bi = e.Object;

            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            if (syncedObjects.Count == 0)
            {
                return;
            }

            var ids = bi.RelatedData as List<int> ?? new List<int>();

            var syncedObjectsToSync = SyncHelper.GetTriggeredSyncedObjects(ids);

            foreach (var syncedObject in syncedObjectsToSync)
            {
                SyncHelper.QueueItem(syncedObject, bi);
            }
        }
    }
}
