﻿using System;
using System.Collections.Generic;

namespace WDM.SyncedObjects.Events
{
    public class ObjectSyncEventArgs : EventArgs
    {
        public ObjectSyncResult ParentObjectSyncResult { get; set; }
        public List<ObjectSyncResult> ChildObjectSyncResults { get; set; }

        public ObjectSyncEventArgs() : base()
        {
            ChildObjectSyncResults = new List<ObjectSyncResult>();
        }
    }
}