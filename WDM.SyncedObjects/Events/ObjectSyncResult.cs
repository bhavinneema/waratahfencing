﻿using WDM.SyncedObjects.Classes;

namespace WDM.SyncedObjects.Events
{
    public class ObjectSyncResult
    {
        public int KenticoObjectID { get; set; }

        public string KenticoObjectType { get; set; }

        public string KenticoClassName { get; set; }

        public SyncLogInfo SyncResult { get; set; }
    }
}