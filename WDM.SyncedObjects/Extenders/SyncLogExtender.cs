﻿using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;

using CMS;
using CMS.Base;
using CMS.Base.Web.UI;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.UIControls;

using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Helpers;

[assembly: RegisterCustomClass("SyncLogExtender", typeof(WDM.SyncedObjects.Extenders.SyncLogExtender))]
namespace WDM.SyncedObjects.Extenders
{

    public class SyncLogExtender : ControlExtender<UniGrid>
    {
        /// <summary>
        /// Adds custom code that occurs during the initialization of the extended control.
        /// </summary>
        public override void OnInit()
        {
            // Assigns handlers to the UniGrid control's OnAction and OnExternalDatabound events
            Control.OnAction += Control_OnAction;
            Control.OnExternalDataBound += Control_OnExternalDataBound;
            Control.OnAfterRetrieveData += Control_OnAfterRetrieveData;
        }

        private DataSet Control_OnAfterRetrieveData(DataSet ds)
        {
            if(ds.Tables[0].Rows.Count>0)
            {
                var firstRow = ds.Tables[0].Rows[0];
                int kenticoObjectId = ValidationHelper.GetInteger(firstRow["SyncLogSyncedObjectID"], 0);
            }
            return ds;
        }

        /// <summary>
        /// Handles the UniGrid control's OnAction event.
        /// </summary>
        /// <param name="actionName">The name of the UniGrid action that was used.</param>
        /// <param name="actionArgument">The value of the data source column in the UniGrid row for which the action was used.</param>
        private void Control_OnAction(string actionName, object actionArgument)
        {
            if (actionName == "retry")
            {
                var id = ValidationHelper.GetInteger(actionArgument, 0);
                var syncLog = SyncLogInfoProvider.GetSyncLogInfo(id);
                if (syncLog != null)
                {
                    syncLog.SyncLogStatus = (int) SyncLogStatus.New;
                }

                SyncLogInfoProvider.SetSyncLogInfo(syncLog);
            }
        }

        private object Control_OnExternalDataBound(object sender, string sourceName, object parameter)
        {
            if (sourceName.ToLowerCSafe() == "retry")
            {

                CMSGridActionButton button = sender as CMSGridActionButton;
                GridViewRow grv = parameter as GridViewRow;

                if (grv != null)
                {
                    DataRowView drv = (DataRowView)grv.DataItem;
                    var status = ValidationHelper.GetInteger(drv["SyncLogStatus"], 0);

                    if (status == (int)SyncLogStatus.Synced) // todo: use enum
                        grv.BackColor = ColorTranslator.FromHtml("#D7EBBF");
                    else
                        grv.BackColor = ColorTranslator.FromHtml("#FFE1E1");
                }
            }
            else if (sourceName.ToLowerCSafe() == "statuscode")
            {
                int param = ValidationHelper.GetInteger(parameter, 0);
                return ((SyncLogStatus)param).ToString();
            }

            return parameter;
        }
    }
}
