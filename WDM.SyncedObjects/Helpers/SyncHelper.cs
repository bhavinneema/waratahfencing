﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.Base;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.Membership;
using CMS.OnlineForms;

using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;

namespace WDM.SyncedObjects.Helpers
{
    public class SyncHelper
    {
        public static void GenerateGenericQueueTask(SyncedObjectInfo syncedObjectInfo)
        {
            // Build the request body
            string connectorClass = syncedObjectInfo.SyncedObjectConnectorClass;
            ICrmConnector connector = GetConnector(connectorClass);
            var requestFields = GetRequestFields(syncedObjectInfo);
            string request = connector.BuildRequestBody(requestFields);

            // Create a new sync log record.
            var syncLogInfo = new SyncLogInfo
            {
                SyncLogCrmItemID = null,
                SyncLogKenticoItemID = 0,
                SyncLogSyncedObjectID = syncedObjectInfo.SyncedObjectID,
                SyncLogAttemptTimestamp = DateTime.Now,
                SyncLogStatus = (int)SyncLogStatus.New,
                SyncLogRequest = request
            };

            // Set custom data
            if (requestFields != null)
            {
                foreach (var prop in requestFields.Where(x => x.CustomDataProperty.Length > 0).ToList())
                {
                    syncLogInfo.CustomData.SetValue(prop.CustomDataProperty, prop.Value);
                }
            }

            // Add it with a status of "New".
            // This will be processed later in a scheduled task.
            SyncLogInfoProvider.SetSyncLogInfo(syncLogInfo);
        }

        public static List<SyncedObjectInfo> GetSyncedObjectsToTrigger(BaseInfo bi, List<string> changedColumns, List<SyncedObjectInfo> syncedObjects)
        {
            var result = new List<SyncedObjectInfo>();

            foreach (var syncedObject in syncedObjects)
            {
                var mappings = SyncedFieldInfoProvider.GetSyncedFields()
                    .WhereEquals("SyncedFieldSyncedObjectID", syncedObject.SyncedObjectID);

                var fields = mappings.Select(x => x.SyncedFieldKenticoFieldName);

                if (changedColumns.Any(x => fields.Contains(x)) || (syncedObject.SyncedObjectKenticoReturnFieldName.Length > 0 &&string.IsNullOrEmpty(bi.GetStringValue(syncedObject.SyncedObjectKenticoReturnFieldName, string.Empty))))
                {
                    result.Add(syncedObject);
                }
            }

            return result;
        }

        public static List<SyncedObjectInfo> GetSyncedObjects(BaseInfo bi)
        {
            var dci = DataClassInfoProvider.GetDataClassInfo(bi.TypeInfo.ObjectClassName);
            return SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereEquals("SyncedObjectKenticoClass", dci.ClassID)
                .ToList();
        }

        public static void QueueObject(BizFormItem bizFromItem)
        {
            if (bizFromItem == null)
            {
                return;
            }

            QueueObject(bizFromItem.ClassName, bizFromItem, bizFromItem.ItemID);
        }

        public static void QueueObject(UserInfo userInfo)
        {
            QueueObject("cms.user", userInfo, userInfo.UserID);
        }


        public static void QueueObject(string className, IDataContainer item, int itemID)
        {
            if (item == null)
            {
                return;
            }

            var dci = DataClassInfoProvider.GetDataClassInfo(className);

            // Get all synced objects for this class.
            var syncedObjects = SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereEquals("SyncedObjectKenticoClass", dci.ClassID)
                .OrderBy("SyncedObjectSyncOrder")
                .TypedResult;

            // Create a Sync Log for each synced object, using the data from this item.
            // This allows us to send multiple sync requests for a given insert/update event.
            foreach (SyncedObjectInfo syncedObjectInfo in syncedObjects)
            {
                // Get the CRMID
                string fieldName = syncedObjectInfo.SyncedObjectKenticoReturnFieldName;
                string crmItemID = ValidationHelper.GetString(item.GetValue(fieldName), string.Empty);

                // Build the request body
                string connectorClass = syncedObjectInfo.SyncedObjectConnectorClass;
                ICrmConnector connector = GetConnector(connectorClass);
                var requestFields = GetRequestFields(syncedObjectInfo, item);
                string request = connector.BuildRequestBody(requestFields, itemID);

                // Create a new sync log record.
                var syncLogInfo = new SyncLogInfo
                {
                    SyncLogCrmItemID = crmItemID,
                    SyncLogKenticoItemID = itemID,
                    SyncLogSyncedObjectID = syncedObjectInfo.SyncedObjectID,
                    SyncLogAttemptTimestamp = DateTime.Now,
                    SyncLogStatus = (int)SyncLogStatus.New,
                    SyncLogRequest = request
                };

                if (requestFields != null)
                {
                    // Set custom data
                    foreach (var prop in requestFields.Where(x => x.CustomDataProperty.Length > 0).ToList())
                    {
                        syncLogInfo.CustomData.SetValue(prop.CustomDataProperty, prop.Value);
                    }
                }

                // Add it with a status of "New".
                // This will be processed later in a scheduled task.
                SyncLogInfoProvider.SetSyncLogInfo(syncLogInfo);
            }
        }

        public static ICrmConnector GetConnector(string connectorClass)
        {
            var connector = Activator.CreateInstance(GetType(connectorClass)) as ICrmConnector;
            return GetSettings(connector);
        }


        public static ICrmConnector GetSettings(ICrmConnector connector)
        {
            if (connector == null)
            {
                return null;
            }

            var requiredSettingKeys = connector.GetRequiredSettingKeys();
            var settings = GetSettings(requiredSettingKeys);
            connector.SetSettings(settings);

            return connector;
        }


        /// <summary>
        /// Returns a dictonary of settings and their values.
        /// </summary>
        private static ConnectorSettings.ConnectorSettings GetSettings(List<string> requiredSettings)
        {
            var settings = new ConnectorSettings.ConnectorSettings();
            foreach (string key in requiredSettings)
            {
                settings.Add(key, SettingsKeyInfoProvider.GetValue(key));
            }
            return settings;
        }


        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }


        public static IDataContainer GetItem(SyncedObjectInfo syncedObjectInfo, int itemID)
        {
            DataClassInfo dataClassInfo = DataClassInfoProvider.GetDataClassInfo(syncedObjectInfo.SyncedObjectKenticoClass);
            if (dataClassInfo == null)
            {
                return null;
            }

            var dci = DataClassInfoProvider.GetDataClassInfo(syncedObjectInfo.SyncedObjectKenticoClass);

            if (dataClassInfo.ClassIsForm)
            {
                // BizForm
                var bizFormItem = BizFormItemProvider.GetItem(itemID, dci.ClassName);
                return bizFormItem;
            }
            else if (dataClassInfo.ClassIsDocumentType)
            {
                // TreeNode
                var provider = new TreeProvider();
                string cultureCode = string.Empty; // todo
                bool combineWithDefaultCulture = true;
                bool coupledData = true;
                var node = provider.SelectSingleNode(itemID, cultureCode, combineWithDefaultCulture, coupledData);
                return node;
            }
            else
            {
                // Class
                var objReadonly = ModuleManager.GetObject(dci.ClassName);
                var infoObject = new ObjectQuery(dci.ClassName).WhereEquals(objReadonly.TypeInfo.IDColumn, itemID)
                    .FirstObject;
                return infoObject;
            }
        }


        public static void UpdateItemCRMID(SyncedObjectInfo syncedObjectInfo, int itemID, string crmID)
        {
            DataClassInfo dataClassInfo = DataClassInfoProvider.GetDataClassInfo(syncedObjectInfo.SyncedObjectKenticoClass);
            if (dataClassInfo == null)
            {
                return;
            }

            var dci = DataClassInfoProvider.GetDataClassInfo(syncedObjectInfo.SyncedObjectKenticoClass);

            if (dataClassInfo.ClassIsForm)
            {
                // BizForm
                var bizFormItem = BizFormItemProvider.GetItem(itemID, dci.ClassName);
                bizFormItem.SetValue(syncedObjectInfo.SyncedObjectKenticoReturnFieldName, crmID);
                bizFormItem.Update();
            }
            else if (dataClassInfo.ClassIsDocumentType)
            {
                // TreeNode
                var provider = new TreeProvider();
                string cultureCode = string.Empty; // todo
                bool combineWithDefaultCulture = true;
                bool coupledData = true;
                var node = provider.SelectSingleNode(itemID, cultureCode, combineWithDefaultCulture, coupledData);
                if (node == null)
                {
                    return;
                }

                node.SetValue(syncedObjectInfo.SyncedObjectKenticoReturnFieldName, crmID);
                node.Update();
            }
            else
            {
                // Class
                var objReadonly = ModuleManager.GetObject(dci.ClassName);
                var infoObject = new ObjectQuery(dci.ClassName).WhereEquals(objReadonly.TypeInfo.IDColumn, itemID)
                    .FirstObject;

                infoObject.SetValue(syncedObjectInfo.SyncedObjectKenticoReturnFieldName, crmID);
                infoObject.Update();
            }
        }


        /// <summary>
        /// Builds a list of request fields for the synced object, populated with values from the given item.
        /// </summary>
        public static List<RequestField> GetRequestFields(SyncedObjectInfo syncedObjectInfo, IDataContainer item = null)
        {
            if (syncedObjectInfo == null || item == null)
            {
                return null;
            }

            var syncedFields = SyncedFieldInfoProvider.GetSyncedFields()
                .WhereEquals("SyncedFieldSyncedObjectID", syncedObjectInfo.SyncedObjectID)
                .OrderBy("SyncedFieldOrder")
                .TypedResult;
            
            var requestFields = syncedFields.Select(x => new RequestField() {

                SourceFieldName = x.SyncedFieldKenticoFieldName,

                CrmFieldName = x.SyncedFieldCrmFieldName,

                // Todo:
                // I don't like 'macro' being a datatype.
                // We have no way of determining what datatype the resolved macro should be.
                // The WDM.SyncedField class should have a separate boolean field called SyncedFieldIsMacro.
                DataType = (x.SyncedFieldDataType == "macro")
                    ? "object"
                    : x.SyncedFieldDataType,

                Value = (x.SyncedFieldDataType == "macro")
                    ? ResolveMacros(x.SyncedFieldMacro, item)
                    : item[x.SyncedFieldKenticoFieldName],

                CustomDataProperty = x.SyncedFieldCustomDataProperty

            }).ToList();

            return requestFields;
        }


        /// <summary>
        /// Resolves a macro expression.
        /// </summary>
        private static object ResolveMacros(string macroExpression, IDataContainer context)
        {
            var dict = CreateDictionary(context);

            MacroContext.CurrentResolver.SetNamedSourceData(dict);
            string macroExpressionWithoutBrackets = MacroProcessor.RemoveDataMacroBrackets(macroExpression);
            var resolved = MacroContext.CurrentResolver.ResolveMacroExpression(macroExpressionWithoutBrackets, true);

            if (resolved == null)
            {
                return null;
            }

            return resolved.Result;
        }


        /// <summary>
        /// Converts an IDataContainer into a dictionary, for use in the macro resolver.
        /// </summary>
        private static Dictionary<string, object> CreateDictionary(IDataContainer bizFormItem)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (var fieldName in bizFormItem.ColumnNames)
            {
                dict.Add(fieldName, bizFormItem.GetValue(fieldName));
            }
            return dict;
        }

        public static List<SyncedObjectInfo> GetTriggeredSyncedObjects(List<int> ids)
        {
            return SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereIn("SyncedObjectID", ids)
                .ToList();
        }

        public static void QueueItem(SyncedObjectInfo syncedObject, BaseInfo baseInfo)
        {
            // Get the CRMID
            var crmIdFieldName = syncedObject.SyncedObjectKenticoReturnFieldName;
            var itemId = baseInfo.GetIntegerValue(baseInfo.TypeInfo.IDColumn, 0);
            var crmItemId = ValidationHelper.GetString(baseInfo.GetValue(crmIdFieldName), string.Empty);

            // Build the request body
            var connectorClass = syncedObject.SyncedObjectConnectorClass;
            var connector = GetConnector(connectorClass);
            var requestFields = GetRequestFields(syncedObject, baseInfo);
            var request = connector.BuildRequestBody(requestFields, itemId);

            var syncLogInfo = SyncLogInfoProvider.GetSyncLogs()
                .WhereEquals("SyncLogKenticoItemID", itemId)
                .WhereEquals("SyncLogSyncedObjectID", syncedObject.SyncedObjectID)
                .WhereEqualsOrNull("SyncLogStatus", (int) SyncLogStatus.New)
                .FirstObject;

            // Create a new sync log record.
            if (syncLogInfo == null)
            {
                syncLogInfo = new SyncLogInfo();
            }

            syncLogInfo.SyncLogCrmItemID = crmItemId;
            syncLogInfo.SyncLogKenticoItemID = itemId;
            syncLogInfo.SyncLogSyncedObjectID = syncedObject.SyncedObjectID;
            syncLogInfo.SyncLogAttemptTimestamp = DateTime.Now;
            syncLogInfo.SyncLogStatus = (int) SyncLogStatus.New;
            syncLogInfo.SyncLogRequest = request;
            
            if (requestFields != null)
            {
                // Set custom data
                foreach (var prop in requestFields.Where(x => x.CustomDataProperty.Length > 0).ToList())
                {
                    syncLogInfo.CustomData.SetValue(prop.CustomDataProperty, prop.Value);
                }
            }

            // Add it with a status of "New".
            // This will be processed later in a scheduled task.
            SyncLogInfoProvider.SetSyncLogInfo(syncLogInfo);
        }

        public static bool QueueItemExists(BaseInfo bi)
        {
            var exists = false;
            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            foreach (var syncedObject in syncedObjects)
            {
                if (SyncLogInfoProvider.GetSyncLogs()
                    .WhereEquals("SyncLogSyncedObjectID", syncedObject.SyncedObjectID)
                    .WhereEquals("SyncLogKenticoItemID", bi.GetIntegerValue(bi.TypeInfo.IDColumn, 0))
                    .WhereEqualsOrNull("SyncLogStatus", (int)SyncLogStatus.New)
                    .Any())
                {
                    exists = true;
                }
            }

            return exists;
        }

        public static bool ItemHasCrmId(BaseInfo bi)
        {
            var syncedObjects = SyncHelper.GetSyncedObjects(bi);

            foreach (var syncedObject in syncedObjects)
            {
                if (string.IsNullOrEmpty(bi.GetStringValue(syncedObject.SyncedObjectKenticoReturnFieldName,
                    string.Empty)))
                {
                    return false;
                }
            }

            return true;
        }

        public static List<int?> GetSyncedClasses()
        {
            var result = CacheHelper.Cache(cs => GetSyncedClasses(cs), new CacheSettings(10080, "WDM.SyncedObjects.SyncedClasses"));

            return result;
        }

        private static List<int?> GetSyncedClasses(CacheSettings cs)
        {
            var result = SyncedObjectInfoProvider.GetSyncedObjects()
                .ToList()
                .Select(syncedObjectInfo => DataClassInfoProvider.GetDataClassInfo(syncedObjectInfo.SyncedObjectKenticoClass)?.ClassID)
                .ToList();

            if (cs.Cached)
            {
                cs.CacheDependency = CacheHelper.GetCacheDependency("wdm.syncedobject|all");
            }

            return result;
        }
    }
}
