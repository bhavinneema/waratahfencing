﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WDM.SyncedObjects.Classes;

namespace WDM.SyncedObjects.Helpers
{
    public static class SyncLogHelper
    {
        public static SyncLogInfo SyncLogSuccess(SyncLogInfo syncLogInfo, bool success, string request, string response, string returnId)
        {
            syncLogInfo.SyncLogLastModified = DateTime.Now;
            syncLogInfo.SyncLogStatus = success ? (int) SyncLogStatus.Synced : (int) SyncLogStatus.Failed;
            syncLogInfo.SyncLogRequest = request;
            syncLogInfo.SyncLogResponse = response;
            syncLogInfo.SyncLogCrmItemID = returnId;

            return syncLogInfo;
        }


        public static SyncLogInfo SyncLogFailed(SyncLogInfo syncLogInfo, string errorMessage)
        {
            syncLogInfo.SyncLogLastModified = DateTime.Now;
            syncLogInfo.SyncLogStatus = (int)SyncLogStatus.Failed;
            syncLogInfo.SyncLogResponse = errorMessage;

            return syncLogInfo;
        }
    }
}
