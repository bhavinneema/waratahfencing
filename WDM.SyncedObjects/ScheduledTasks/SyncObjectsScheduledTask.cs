﻿using System;
using System.Collections.Generic;
using CMS.Base;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Scheduler;
using CMS.SiteProvider;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.Helpers;

namespace WDM.SyncedObjects.ScheduledTasks
{
    public class SyncObjectsScheduledTask : ITask
    {
        public string Execute(TaskInfo ti)
        {
            // Get a list of new (unsynced) sync logs.
            var newSyncLogs = SyncLogInfoProvider.GetSyncLogs()
                .WhereEqualsOrNull("SyncLogStatus", (int)SyncLogStatus.New) // todo: should we retry failed sync logs too?
                .OrderBy("SyncLogSyncedObjectID, SyncLogID")
                .TypedResult;

            // Step through them, and try to sync each one.
            int attempts = 0;
            int success = 0;
            foreach (var syncLog in newSyncLogs)
            {
                attempts++;
                if (TryToSync(syncLog) == SyncLogStatus.Synced)
                {
                    success++;
                }
            }

            // Delete old ones
            var retentionDays = SettingsKeyInfoProvider.GetIntValue("WDM_SyncLogRetentionPeriod", SiteContext.CurrentSiteID);

            if (retentionDays > 0)
            {
                var syncLogsToDelete = GetSyncLogsToDelete(retentionDays);

                foreach (var syncLogToDelete in syncLogsToDelete)
                {
                    SyncLogInfoProvider.DeleteSyncLogInfo(syncLogToDelete);
                }
            }

            // How did we go?
            string scheduledTaskMessage = string.Format("{0}/{1} successfully synced.", success, attempts);
            return scheduledTaskMessage;
        }

        private IEnumerable<SyncLogInfo> GetSyncLogsToDelete(int retentionDays)
        {
            return SyncLogInfoProvider.GetSyncLogs()
                .WhereEquals("SyncLogStatus", (int)SyncLogStatus.Synced)
                .WhereLessThan("SyncLogAttemptTimestamp", DateTime.Now.AddDays(-retentionDays));
        }


        private SyncLogStatus TryToSync(SyncLogInfo syncLogInfo)
        {
            var syncedObjectInfo = SyncedObjectInfoProvider.GetSyncedObjectInfo(syncLogInfo.SyncLogSyncedObjectID);
            if (syncedObjectInfo == null)
            {
                string message = string.Format("Couldn't sync log #{0}. SyncedObject #{1} not found.", syncLogInfo.SyncLogID, syncLogInfo.SyncLogSyncedObjectID);
                EventLogProvider.LogException("SyncObjectsScheduledTask.TryToSync", "SYNCEDOBJECTNOTFOUND", new Exception(message));
                return SyncLogStatus.Failed;
            }

            var connectorClass = syncedObjectInfo.SyncedObjectConnectorClass;
            var type = SyncHelper.GetType(connectorClass);
            if (type == null)
            {
                string message = string.Format("Couldn't sync log #{0}. Connector class {1} not fould.", syncLogInfo.SyncLogID, connectorClass);
                EventLogProvider.LogException("SyncObjectsScheduledTask.TryToSync", "ICRMCONNECTORNOTFOUND", new Exception(message));
                return SyncLogStatus.Failed;
            }

            ICrmConnector connector = SyncHelper.GetConnector(connectorClass);

            // Try to find the item.
            var item = SyncHelper.GetItem(syncedObjectInfo, syncLogInfo.SyncLogKenticoItemID);

            if (syncedObjectInfo.SyncedObjectCheckItemBeforeSync && item == null)
            {
                string message = string.Format("\"{0}\" #{1} was not pushed to the CRM because the Kentico object can't be found.",
                    syncedObjectInfo.SyncedObjectKenticoClass,
                    syncLogInfo.SyncLogKenticoItemID);
                EventLogProvider.LogException("SugarCrmConnector.Sync", "SYNCITEMNOTFOUND", new Exception(message));
                syncLogInfo.SyncLogStatus = (int)SyncLogStatus.Failed;
                SyncLogInfoProvider.SetSyncLogInfo(syncLogInfo);
                return (SyncLogStatus)syncLogInfo.SyncLogStatus;
            }

            if (item != null)
            {
                // Rebuild before sync?
                if (syncedObjectInfo.SyncedObjectRebuildBeforeSync)
                {
                    var requestFields = SyncHelper.GetRequestFields(syncedObjectInfo, item);
                    string request = connector.BuildRequestBody(requestFields, syncLogInfo.SyncLogKenticoItemID);
                    syncLogInfo.SyncLogRequest = request;
                }
            }

            // Perform the actual sync.
            syncLogInfo = connector.Sync(syncedObjectInfo, syncLogInfo);

            if (item != null)
            {
                using (var rc = new RecursionControl("WDM.CRMSync"))
                {
                    // Update the CRM ID.
                    string returnFieldValue =
                        ValidationHelper.GetString(item.GetValue(syncedObjectInfo.SyncedObjectKenticoReturnFieldName),
                            string.Empty);
                    if ((syncLogInfo.SyncLogStatus == (int) SyncLogStatus.Synced) &&
                        (returnFieldValue != syncLogInfo.SyncLogCrmItemID))
                    {
                        SyncHelper.UpdateItemCRMID(syncedObjectInfo, syncLogInfo.SyncLogKenticoItemID, syncLogInfo.SyncLogCrmItemID);
                    }
                }
            }

            // Save the response / status to the database.
            SyncLogInfoProvider.SetSyncLogInfo(syncLogInfo);

            return (SyncLogStatus)syncLogInfo.SyncLogStatus;
        }
    }
}
