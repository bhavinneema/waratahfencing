﻿using CMS;
using CMS.DataEngine;

using WDM.SyncedObjects;
using WDM.SyncedObjects.EventHandlers;
using WDM.SyncedObjects.UniGridTransformations;

[assembly: RegisterModule(typeof(SyncedObjectsModule))]
namespace WDM.SyncedObjects
{
    public class SyncedObjectsModule : Module
    {
        public SyncedObjectsModule() : base("WDM.SyncedObjectsModule") { }

        protected override void OnInit()
        {
            base.OnInit();

            // Prevent loading of the module if the application is loaded from another assembly or program, instead of being loaded as part of the web application.
            // This ensures no 'pre-init' exceptions are thrown by programs such as ContinuousIntegration.exe
            //
            // In this case, it's being done to prevent CI from syncing changes to the CRM.
            var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
            if (entryAssembly == null)
            {
                SyncedObjectsEvents.RegisterEvents();
                CustomUniGridTransformations.RegisterCustomUniGridTransformations();
            }
        }
    }
}