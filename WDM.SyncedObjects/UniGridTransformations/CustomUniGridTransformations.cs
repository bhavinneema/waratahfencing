﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WDM.SyncedObjects.UniGridTransformations
{
    public class CustomUniGridTransformations
    {
        public static void RegisterCustomUniGridTransformations()
        {
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#syncedobjectresync", SyncedObjectUniGridTransformations.GetResyncObjectButton);
        }
    }
}
