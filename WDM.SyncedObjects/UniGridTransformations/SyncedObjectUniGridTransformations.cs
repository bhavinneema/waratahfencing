﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CMS.Base;
using CMS.Base.Web.UI;
using CMS.DataEngine;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.Membership;
using CMS.OnlineForms;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Helpers;

namespace WDM.SyncedObjects.UniGridTransformations
{
    public class SyncedObjectUniGridTransformations
    {
        internal static object GetResyncObjectButton(object arg)
        {
            var grv = (GridViewRow)arg;
            var cells = grv.Cells.Cast<TableCell>().Select(x => x.Controls.OfType<CMSGridActionButton>());
            foreach (var cellActions in cells)
            {
                foreach (var action in cellActions)
                {
                    if (action.CommandName == "syncedobjectresync")
                    {
                        var classId = 0;
                        if (UIContext.Current.EditedObject is BizFormInfo)
                        {
                            classId = ((BizFormInfo)UIContext.Current.EditedObject).FormClassID;
                        }
                        else
                        {
                            var className = UIContext.Current.Data.GetKeyValue("objecttype", true, true).ToString();
                            var userDci = DataClassInfoProvider.GetDataClassInfo(className);
                            classId = userDci.ClassID;
                        }

                        var syncedObjectIds = SyncedObjectInfoProvider.GetSyncedObjects()
                            .Column("SyncedObjectID")
                            .WhereEquals("SyncedObjectKenticoClass", classId)
                            .GetListResult<int>();
                        action.Enabled = !SyncLogInfoProvider.GetSyncLogs()
                            .WhereIn("SyncLogSyncedObjectID", syncedObjectIds)
                            .WhereEquals("SyncLogKenticoItemID", action.CommandArgument.ToInteger(0))
                            .WhereEqualsOrNull("SyncLogStatus", (int)SyncLogStatus.New)
                            .HasResults();

                        var latest = SyncLogInfoProvider.GetSyncLogs()
                            .TopN(1)
                            .WhereIn("SyncLogSyncedObjectID", syncedObjectIds)
                            .WhereEquals("SyncLogKenticoItemID", action.CommandArgument.ToInteger(0))
                            .OrderByDescending("SyncLogAttemptTimestamp")
                            .FirstObject;

                        if (latest != null)
                        {
                            switch ((SyncLogStatus)latest.SyncLogStatus)
                            {
                                case SyncLogStatus.Failed:
                                    action.IconStyle = GridIconStyle.Critical;
                                    action.ToolTip = "Last attempt failed. Click to resync.";
                                    break;
                                case SyncLogStatus.Synced:
                                    action.IconStyle = GridIconStyle.Allow;
                                    action.ToolTip = "Last attempt was successful. Click to resync.";
                                    break;
                                default:
                                    action.ToolTip = "Click to resync.";
                                    break;
                            }
                        }


                        action.OnClientClick = string.Empty;
                        action.Click += Button_Click;
                    }
                }
            }
            return arg;
        }

        private static void Button_Click(object sender, EventArgs e)
        {
            var button = ((CMSGridActionButton)sender);

            var classId = 0;
            if (UIContext.Current.EditedObject is BizFormInfo)
            {
                classId = ((BizFormInfo)UIContext.Current.EditedObject).FormClassID;
            }
            else
            {
                var className = UIContext.Current.Data.GetKeyValue("objecttype", true, true).ToString();
                var userDci = DataClassInfoProvider.GetDataClassInfo(className);
                classId = userDci.ClassID;
            }

            var itemId = button.CommandArgument.ToInteger(0);

            var dci = DataClassInfoProvider.GetDataClassInfo(classId);

            BaseInfo baseInfo = null;
            if (dci.ClassIsForm)
            {
                baseInfo = BizFormItemProvider.GetItem(itemId, dci.ClassName);
            }
            else
            {
                baseInfo = BaseAbstractInfoProvider.GetInfoById(dci.ClassName, itemId);
            }

            var syncedObjects = SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereEquals("SyncedObjectKenticoClass", classId)
                .ToList();
            var existing = SyncLogInfoProvider.GetSyncLogs()
                .WhereIn("SyncLogSyncedObjectID", syncedObjects.Select(x => x.SyncedObjectID).ToList())
                .WhereEquals("SyncLogKenticoItemID", itemId)
                .WhereEqualsOrNull("SyncLogStatus", (int)SyncLogStatus.New)
                .HasResults();

            if (!existing)
            {
                foreach (var syncedObject in syncedObjects)
                {
                    SyncHelper.QueueItem(syncedObject, baseInfo);
                }
            }
            button.Enabled = false;
        }
    }
}
