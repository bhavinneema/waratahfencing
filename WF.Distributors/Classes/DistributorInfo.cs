using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF.Distributors.Classes;

[assembly: RegisterObjectType(typeof(DistributorInfo), DistributorInfo.OBJECT_TYPE)]
    
namespace WF.Distributors.Classes
{
    /// <summary>
    /// DistributorInfo data container class.
    /// </summary>
	[Serializable]
    public partial class DistributorInfo : AbstractInfo<DistributorInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.distributor";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(DistributorInfoProvider), OBJECT_TYPE, "WF.Distributor", "DistributorID", "DistributorLastModified", "DistributorGuid", null, null, null, null, null, null)
        {
			ModuleName = "WF.Distributors",
			TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Distributor ID
        /// </summary>
        [DatabaseField]
        public virtual int DistributorID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("DistributorID"), 0);
            }
            set
            {
                SetValue("DistributorID", value);
            }
        }


        /// <summary>
        /// Distributor name
        /// </summary>
        [DatabaseField]
        public virtual string DistributorName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorName"), String.Empty);
            }
            set
            {
                SetValue("DistributorName", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor address 1
        /// </summary>
        [DatabaseField]
        public virtual string DistributorAddress1
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorAddress1"), String.Empty);
            }
            set
            {
                SetValue("DistributorAddress1", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor address 2
        /// </summary>
        [DatabaseField]
        public virtual string DistributorAddress2
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorAddress2"), String.Empty);
            }
            set
            {
                SetValue("DistributorAddress2", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor city
        /// </summary>
        [DatabaseField]
        public virtual string DistributorCity
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorCity"), String.Empty);
            }
            set
            {
                SetValue("DistributorCity", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor postcode
        /// </summary>
        [DatabaseField]
        public virtual string DistributorPostcode
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorPostcode"), String.Empty);
            }
            set
            {
                SetValue("DistributorPostcode", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor country
        /// </summary>
        [DatabaseField]
        public virtual string DistributorCountry
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorCountry"), String.Empty);
            }
            set
            {
                SetValue("DistributorCountry", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor phone
        /// </summary>
        [DatabaseField]
        public virtual string DistributorPhone
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorPhone"), String.Empty);
            }
            set
            {
                SetValue("DistributorPhone", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor lat
        /// </summary>
        [DatabaseField]
        public virtual double DistributorLat
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("DistributorLat"), 0);
            }
            set
            {
                SetValue("DistributorLat", value, 0);
            }
        }


        /// <summary>
        /// Distributor lng
        /// </summary>
        [DatabaseField]
        public virtual double DistributorLng
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("DistributorLng"), 0);
            }
            set
            {
                SetValue("DistributorLng", value, 0);
            }
        }


        /// <summary>
        /// Distributor star partner
        /// </summary>
        [DatabaseField]
        public virtual bool DistributorStarPartner
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("DistributorStarPartner"), false);
            }
            set
            {
                SetValue("DistributorStarPartner", value);
            }
        }


        /// <summary>
        /// Distributor star partner rating
        /// </summary>
        [DatabaseField]
        public virtual int DistributorStarPartnerRating
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("DistributorStarPartnerRating"), 0);
            }
            set
            {
                SetValue("DistributorStarPartnerRating", value, 0);
            }
        }


        /// <summary>
        /// Distributor type
        /// </summary>
        [DatabaseField]
        public virtual string DistributorType
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorType"), String.Empty);
            }
            set
            {
                SetValue("DistributorType", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor status
        /// </summary>
        [DatabaseField]
        public virtual string DistributorStatus
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorStatus"), String.Empty);
            }
            set
            {
                SetValue("DistributorStatus", value, String.Empty);
            }
        }


        /// <summary>
        /// Distributor salesforce ID
        /// </summary>
        [DatabaseField]
        public virtual string DistributorSalesforceID
        {
            get
            {
                return ValidationHelper.GetString(GetValue("DistributorSalesforceID"), String.Empty);
            }
            set
            {
                SetValue("DistributorSalesforceID", value);
            }
        }


        /// <summary>
        /// Distributor guid
        /// </summary>
        [DatabaseField]
        public virtual Guid DistributorGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("DistributorGuid"), Guid.Empty);
            }
            set
            {
                SetValue("DistributorGuid", value);
            }
        }


        /// <summary>
        /// Distributor last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime DistributorLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("DistributorLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("DistributorLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            DistributorInfoProvider.DeleteDistributorInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            DistributorInfoProvider.SetDistributorInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected DistributorInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty DistributorInfo object.
        /// </summary>
        public DistributorInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new DistributorInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public DistributorInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}