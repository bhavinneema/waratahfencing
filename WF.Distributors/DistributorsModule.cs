﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS;
using CMS.DataEngine;
using WF.Distributors;
using WF.Distributors.UniGridTransformations;

[assembly: RegisterModule(typeof(DistributorsModule))]
namespace WF.Distributors
{
    public class DistributorsModule : Module
    {
        public DistributorsModule() : base("WF.Distributors") { }

        protected override void OnInit()
        {
            base.OnInit();

            CustomUniGridTransformations.RegisterCustomUniGridTransformations();
        }
    }
}
