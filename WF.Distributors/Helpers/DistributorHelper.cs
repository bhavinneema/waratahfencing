﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using CMS.DataEngine;
using CMS.Helpers;
using RestSharp;
using WF.Distributors.Classes;

namespace WF.Distributors.Helpers
{
    public static class DistributorHelper
    {
        private static string GoogleMapsApiKey => "AIzaSyBZYMEkj1zuzWGn3PcJaAZapDN6IYbOuHY";

        public static object UpdateDistributors()
        {
            var result = new Dictionary<string, object>();
            var successful = 0;
            var failed = 0;

            var distributorsRequiringUpdate = DistributorInfoProvider.GetDistributors().WhereNull("DistributorLat").WhereNull("DistributorLng");

            foreach (var distributor in distributorsRequiringUpdate)
            {
                if (UpdateDistributor(distributor))
                {
                    successful++;
                }
                else
                {
                    failed++;
                }
            }

            result.Add("successful", successful);
            result.Add("failed", failed);

            return result;
        }

        public static bool UpdateDistributor(DistributorInfo distributor)
        {
            double? lat;
            double? lng;

            var address = string.Format("{0} {1} {2} {3} {4}", distributor.DistributorAddress1, distributor.DistributorAddress2,
                distributor.DistributorCity, distributor.DistributorPostcode, distributor.DistributorCountry);

            var successful = CalculatLatLng(address, distributor.DistributorCountry, out lat, out lng);

            if (ValidationHelper.GetDouble(lat, 0) != 0)
                distributor.DistributorLat = ValidationHelper.GetDouble(lat, 0);

            if (ValidationHelper.GetDouble(lng, 0) != 0)
                distributor.DistributorLng = ValidationHelper.GetDouble(lng, 0);

            DistributorInfoProvider.SetDistributorInfo(distributor);
            return successful;
        }

        public static bool CalculatLatLng(string address, string country, out double? lat, out double? lng)
        {
            lat = null;
            lng = null;
            address = address.Replace("&", " AND ");
            try
            {
                var url = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}", address, GoogleMapsApiKey);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                var response = client.Execute(request);

                var jss = new JavaScriptSerializer();
                var d = jss.Deserialize<dynamic>(response.Content);
                var countryMatch = false;
                foreach (var component in d["results"][0]["address_components"])
                {
                    foreach (var type in component["types"])
                    {
                        if (type == "country" && component["long_name"] == country)
                        {
                            countryMatch = true;
                            break;
                        }
                    }
                    if (countryMatch)
                        break;
                }
                if (countryMatch)
                {
                    lat = ValidationHelper.GetDouble(d["results"][0]["geometry"]["location"]["lat"], 0);
                    lng = ValidationHelper.GetDouble(d["results"][0]["geometry"]["location"]["lng"], 0);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
