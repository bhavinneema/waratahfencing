﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF.Distributors.UniGridTransformations
{
    public static class CustomUniGridTransformations
    {
        public static void RegisterCustomUniGridTransformations()
        {
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#updatelatlng", DistributorUniGridTransformations.GetUpdateLatLngAction);
        }
    }
}
