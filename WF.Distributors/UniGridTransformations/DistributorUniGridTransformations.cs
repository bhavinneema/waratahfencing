﻿using CMS.Base.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CMS.Helpers;
using WF.Distributors.Classes;
using WF.Distributors.Helpers;

namespace WF.Distributors.UniGridTransformations
{
    public class DistributorUniGridTransformations
    {
        internal static object GetUpdateLatLngAction(object arg)
        {
            var grv = (GridViewRow)arg;
            var cells = grv.Cells.Cast<TableCell>().Select(x => x.Controls.OfType<CMSGridActionButton>());
            foreach (var cellActions in cells)
            {
                foreach (var action in cellActions)
                {
                    if (action.CommandName == "updatelatlng")
                    {
                        action.OnClientClick = string.Empty;
                        action.Click += Button_Click;
                    }
                }
            }
            return arg;
        }

        private static void Button_Click(object sender, EventArgs e)
        {
            var button = ((CMSGridActionButton)sender);

            var distributorId = ValidationHelper.GetInteger(button.CommandArgument, 0);

            var distributor = DistributorInfoProvider.GetDistributorInfo(distributorId);

            DistributorHelper.UpdateDistributor(distributor);

            URLHelper.Redirect(RequestContext.CurrentURL);
        }
    }
}
