﻿using System;
using System.Collections.Generic;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;
using WF.Integration.Helpers;

namespace WF.Integration.Connectors
{
    public class PromotionStatusUpdateSync : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }

        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }

        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            return "SELECT Id, Claim_Status__c FROM Promotion__c";
        }

        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                using (var connector = new SalesforceConnector(_settings))
                {
                    var promotions = connector.GetPromotions(syncLogInfo.SyncLogRequest);
                    if (promotions != null)
                    {
                        var result = PromotionStatusUpdateRequestHelper.UpdatePromotionStatuses(promotions);

                        SyncLogHelper.SyncLogSuccess(syncLogInfo, true, syncLogInfo.SyncLogRequest, result, string.Empty);
                    }
                    else
                    {
                        SyncLogHelper.SyncLogFailed(syncLogInfo, "Promotions not found in CRM");
                    }
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }

        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}
