﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CMS.DataEngine;
using CMS.Helpers;
using Newtonsoft.Json;

using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.PartnerServices;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;

namespace WF.Integration.Connectors
{
    public class SalesforceBizFormConnector : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public SalesforceBizFormConnector(){}

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }

        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }

        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            var request = new SfObject(new sObject());

            foreach (var field in requestFields)
            {
                if (field.Value == null)
                {
                    continue;
                }
                switch (field.DataType.ToLower())
                {
                    case "datetime":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetDateTime(field.Value, DateTime.MinValue, "en-AU"));
                        break;

                    case "boolean":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetBoolean(field.Value, false));
                        break;

                    case "string":
                        request.SetValue(field.CrmFieldName, field.Value as string);
                        break;

                    default: // object
                        request.SetValue(field.CrmFieldName, field.Value);
                        break;
                }
            }

            string json = JsonConvert.SerializeObject(request);
            return json;
        }


        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                using (var connector = new SalesforceConnector(_settings))
                {
                    var request = JsonConvert.DeserializeObject<SfObject>(syncLogInfo.SyncLogRequest);
                    request.Obj.type = syncedObjectInfo.SyncedObjectCRMClass;
                    request.Obj.Id = syncLogInfo.SyncLogCrmItemID;

                    var firstname = syncLogInfo.CustomData.GetValue("ContactFirstName");
                    var lastname = syncLogInfo.CustomData.GetValue("ContactLastName");
                    var email = syncLogInfo.CustomData.GetValue("ContactEmail");

                    var qry = $"SELECT Id, AccountId FROM Contact WHERE AccountId != '' AND FirstName = '{firstname}' AND LastName = '{lastname}' AND Email = '{email}'";
                    var contact = connector.GetContacts(qry).FirstOrDefault();
                    if (contact != null)
                    {
                        request.SetValue("ContactId", contact.GetString("Id"));
                        request.SetValue("AccountId", contact.GetString("AccountId"));
                    }

                    var result = connector.Upsert("ID", request.Obj);

                    syncLogInfo = SyncLogHelper.SyncLogSuccess(syncLogInfo, result.success, JsonConvert.SerializeObject(request), XmlSerialization.Serialize(result), result.id);
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return  SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }

        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}