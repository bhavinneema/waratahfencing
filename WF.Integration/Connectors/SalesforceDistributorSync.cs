﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;
using WF.Integration.Helpers;

namespace WF.Integration.Connectors
{
    public class SalesforceDistributorSync : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }

        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }

        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            return "SELECT * FROM Account WHERE RecordTypeId IN ('0127F000000UenzQAC', '0127F000000UenyQAC')";
        }

        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                using (var connector = new SalesforceConnector(_settings))
                {
                    var accountQry = syncLogInfo.SyncLogRequest.Replace("*", "{0}");

                    var accounts = connector.GetAccounts(accountQry);

                    var result = DistributorSyncHelper.ImportDistributors(accounts);

                    syncLogInfo = SyncLogHelper.SyncLogSuccess(syncLogInfo, true, syncLogInfo.SyncLogRequest, JsonConvert.SerializeObject(result), null);
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }

        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}
