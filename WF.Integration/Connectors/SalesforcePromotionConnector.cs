﻿using CMS.FormEngine;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.SiteProvider;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CMS.DataEngine;
using CMS.OnlineForms;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.PartnerServices;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;
using WF.Integration.Helpers;
using CMS.EventLog;
using WF.Promotions.Helpers;

namespace WF.Integration.Connectors
{
    public class SalesforcePromotionConnector : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public SalesforcePromotionConnector(){}

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }


        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }


        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            var request = new SfObject(new sObject());

            var resolver = MacroContext.CurrentResolver;

            foreach (var field in requestFields)
            {
                if (field.Value == null)
                {
                    continue;
                }
                switch (field.DataType.ToLower())
                {
                    case "datetime":
                        request.SetValue(field.CrmFieldName, DateTimeConverter.FromString(field.Value));
                        break;

                    case "boolean":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetBoolean(field.Value, false));
                        break;

                    case "string":
                        request.SetValue(field.CrmFieldName, field.Value as string);
                        break;

                    case "fileurl":
                        string strValue = ValidationHelper.GetString(field.Value, "");
                        request.SetValue(field.CrmFieldName, URLHelper.GetAbsoluteUrl("~/CMSPages/GetPromotionInvoice.ashx?filename=" + FormHelper.GetGuidFileName(strValue) + "&sitename=" + SiteContext.CurrentSiteName));
                        break;

                    case "macro":
                        request.SetValue(field.CrmFieldName, resolver.ResolveMacros(ValidationHelper.GetString(field.Value, string.Empty)));
                        break;

                    case "none":
                        if (field.SourceFieldName == "PurchaseInfoJson")
                        {
                            //BuildRequestForProducts(ValidationHelper.GetString(field.Value, string.Empty), itemId);
                        }
                        break;

                    default: // object
                        request.SetValue(field.CrmFieldName, field.Value);
                        break;
                }
            }

            var json = JsonConvert.SerializeObject(request);
            return json;
        }

        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                using (var connector = new SalesforceConnector(_settings))
                {
                    var request = JsonConvert.DeserializeObject<SfObject>(syncLogInfo.SyncLogRequest);
                    request.Obj.type = syncedObjectInfo.SyncedObjectCRMClass;
                    request.Obj.Id = syncLogInfo.SyncLogCrmItemID;

                    if (string.IsNullOrEmpty(request.Obj.Id))
                    {
                        request.SetValue("Claim_Status__c", "Submitted");
                    }

                    var result = connector.Upsert("ID", request.Obj);

                    syncLogInfo = SyncLogHelper.SyncLogSuccess(syncLogInfo, result.success, JsonConvert.SerializeObject(request), XmlSerialization.Serialize(result), result.id);

                    try
                    {
                        if (syncLogInfo.SyncLogStatus == (int) SyncLogStatus.Synced)
                        {
                            var syncedObject = SyncedObjectInfoProvider.GetSyncedObjectInfo(syncLogInfo.SyncLogSyncedObjectID);
                            var dci = DataClassInfoProvider.GetDataClassInfo(syncedObject.SyncedObjectKenticoClass);
                            var promotionEntry = BizFormItemProvider.GetItem(syncLogInfo.SyncLogKenticoItemID, dci.ClassName);
                            PromotionHelper.BuildRequestForProducts(promotionEntry.GetStringValue("PurchaseInfoJson", string.Empty), promotionEntry.ItemID);
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("SalesforcePromotionConnector", "BuildRequestForProducts", ex);
                    }
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }




        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}