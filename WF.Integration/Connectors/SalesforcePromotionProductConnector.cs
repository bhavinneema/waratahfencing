﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.OnlineForms;
using CMS.SiteProvider;
using Newtonsoft.Json;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.PartnerServices;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;
using WF.Domain.Classes;
using WF.Promotions.Classes;

namespace WF.Integration.Connectors
{
    public class SalesforcePromotionProductConnector : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public SalesforcePromotionProductConnector() { }

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }


        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }


        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            var request = new SfObject(new sObject());

            var resolver = MacroContext.CurrentResolver;

            foreach (var field in requestFields)
            {
                if (field.Value == null)
                {
                    continue;
                }
                switch (field.DataType.ToLower())
                {
                    case "datetime":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetDateTime(field.Value, DateTime.MinValue, "en-AU"));
                        break;

                    case "boolean":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetBoolean(field.Value, false));
                        break;

                    case "string":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetString(field.Value, string.Empty));
                        break;

                    case "fileurl":
                        string strValue = ValidationHelper.GetString(field.Value, "");
                        request.SetValue(field.CrmFieldName, URLHelper.GetAbsoluteUrl("~/CMSPages/GetBizFormFile.aspx?filename=" + FormHelper.GetGuidFileName(strValue) + "&sitename=" + SiteContext.CurrentSiteName));
                        break;

                    case "macro":
                        request.SetValue(field.CrmFieldName, resolver.ResolveMacros(ValidationHelper.GetString(field.Value, string.Empty)));
                        break;

                    default: // object
                        request.SetValue(field.CrmFieldName, field.Value);
                        break;
                }
            }

            var json = JsonConvert.SerializeObject(request);
            return json;
        }


        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                var promotionProduct = PromotionProductVariantInfoProvider.GetPromotionProductVariantInfo(syncLogInfo.SyncLogKenticoItemID);

                var promotion = PromotionInfoProvider.GetPromotionInfo(promotionProduct.PromotionProductVariantPromotionFormID);

                var bizForm = BizFormInfoProvider.GetBizFormInfo(promotion.PromotionBizFormID);
                
                var dci = DataClassInfoProvider.GetDataClassInfo(bizForm.FormClassID);

                // Loads the form's data
                var entry = BizFormItemProvider.GetItem(promotionProduct.PromotionProductVariantPromotionFormEntryID, dci.ClassName);

                var entryConnector = SyncedObjectInfoProvider.GetSyncedObjects()
                    .WhereEquals("SyncedObjectKenticoClass", dci.ClassID).FirstObject;

                var crmIdFormField = entryConnector.SyncedObjectKenticoReturnFieldName;

                var sfId = entry.GetStringValue(crmIdFormField, string.Empty);

                if (sfId.Length > 0)
                {
                    using (var connector = new SalesforceConnector(_settings))
                    {
                        var request = JsonConvert.DeserializeObject<SfObject>(syncLogInfo.SyncLogRequest);

                        request.SetValue("Promotion_Information__c", sfId);

                        request.Obj.type = syncedObjectInfo.SyncedObjectCRMClass;
                        request.Obj.Id = syncLogInfo.SyncLogCrmItemID;

                        var result = connector.Upsert("ID", request.Obj);

                        syncLogInfo = SyncLogHelper.SyncLogSuccess(syncLogInfo, result.success, JsonConvert.SerializeObject(request), XmlSerialization.Serialize(result), result.id);
                    }
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }

        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}
