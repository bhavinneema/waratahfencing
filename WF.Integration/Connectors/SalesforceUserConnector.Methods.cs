﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Globalization;
using CMS.Membership;
using CMS.SiteProvider;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.PartnerServices;

namespace WF.Integration.Connectors
{
    public partial class SalesforceUserConnector
    {
        private static SfObject GenerateLeadRequest(SiteInfo site, Lead lead, ref UserInfo user)
        {
            lead.Obj.type = "Lead";

            // Create/Update editable fields
            lead.SetValue("FirstName", user.FirstName);
            lead.SetValue("LastName", user.LastName);
            lead.SetValue("Email", user.Email);
            //lead.Site_Core_Username__c = user.UserID.ToString(); // TODO Update Service Reference Field
            lead.SetValue("Company", user.GetStringValue("UserBusinessName", "[Not Provided]"));
            lead.SetValue("Category__c", user.GetStringValue("UserCategory", string.Empty));
            lead.SetValue("Email_Opt_In__c", user.GetBooleanValue("UserNewsletterOptIn", false));
            lead.SetValue("City", user.GetStringValue("UserCity", string.Empty));

            // Set record type
            var country = CountryInfoProvider.GetCountryInfo(user.GetIntegerValue("UserCountry", 0));
            if (site.SiteID == 2)
            {
                lead.SetValue("RecordTypeId", "0127F000000UeoQQAS");
                lead.SetValue("OwnerId", "00G7F000000mn2Y");
            }
            else
            {
                if (country != null)
                {
                    switch (country.CountryID)
                    {
                        case 423: //New Zealand
                            lead.SetValue("RecordTypeId", "0127F000000UeoTQAS");
                            lead.SetValue("OwnerId", "00G7F000000mn2d");
                            break;
                        case 271: //USA
                        case 284: //Australia
                        default:
                            lead.SetValue("RecordTypeId", "0127F000000UeoRQAS");
                            lead.SetValue("OwnerId", "00G7F000000mn2b");
                            break;
                    }
                }
            }

            // Set address (new lead only)
            if (lead.Obj.Id == null && country != null)
            {
                lead.SetValue("PostalCode", user.GetStringValue("UserPostCode", string.Empty));
                lead.SetValue("Country", country.CountryDisplayName);
            }
            else
            {
                //lead.SetValue("Address", null);
            }

            return lead;
        }

        private static SfObject GenerateContactRequest(Contact contact, ref UserInfo user)
        {
            contact.Obj.type = "Contact";

            contact.SetValue("FirstName", user.FirstName);
            contact.SetValue("LastName", user.LastName);
            contact.SetValue("Email", user.Email);
            //contact.Site_Core_Username__c = user.UserID.ToString(); // TODO Update Service Reference Field
            contact.SetValue("Password__c", "hidden"); //user.GetStringValue("UserPassword", string.Empty);
            contact.SetValue("HasOptedOutOfEmail", !user.GetBooleanValue("UserNewsletterOptIn", false));

            var country = CountryInfoProvider.GetCountryInfo(user.GetIntegerValue("UserCountry", 0));
            var type = user.GetStringValue("UserCategory", "Other");

            contact.SetValue("OtherAddress", null);
            contact.SetValue("MailingAddress", null);

            return contact;
        }
    }
}
