﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using Newtonsoft.Json;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Connectors;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.PartnerServices;
using WDM.SalesforceConnector.Settings;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Connectors;
using WDM.SyncedObjects.ConnectorSettings;
using WDM.SyncedObjects.Helpers;

namespace WF.Integration.Connectors
{
    public partial class SalesforceUserConnector : ICrmConnector
    {
        private ISalesForceConnectorSettings _settings;
        private string _sfUsername;
        private string _sfPassword;
        private string _sfEndpointUrl;

        public SalesforceUserConnector(){}

        public List<string> GetRequiredSettingKeys()
        {
            return new List<string>(new string[] {
                "SalesForceEndPointName",
                "WDMSalesForceURL",
                "SalesForceProductionUsername",
                "SalesForceProductionPassword"
            });
        }

        public void SetSettings(ConnectorSettings settings)
        {
            if (settings["SalesForceEndPointName"] == "Production")
            {
                _settings = new SalesforceConnectorSettings(settings["WDMSalesForceURL"], settings["SalesForceProductionUsername"], settings["SalesForceProductionPassword"]);
            }
            else
            {
                _settings = new TestSalesforceConnectorSettings();
            }
        }

        public string BuildRequestBody(List<RequestField> requestFields, int itemId)
        {
            var request = new SfObject(new sObject());

            foreach (var field in requestFields)
            {
                if (field.Value == null || field.CrmFieldName.Length == 0)
                {
                    continue;
                }
                switch (field.DataType.ToLower())
                {
                    case "datetime":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetDateTime(field.Value, DateTime.MinValue, "en-AU"));
                        break;

                    case "boolean":
                        request.SetValue(field.CrmFieldName, ValidationHelper.GetBoolean(field.Value, false));
                        break;

                    case "string":
                        request.SetValue(field.CrmFieldName, field.Value as string);
                        break;

                    default: // object
                        request.SetValue(field.CrmFieldName, field.Value);
                        break;
                }
            }

            string json = JsonConvert.SerializeObject(request);
            return json;
        }


        public SyncLogInfo Sync(SyncedObjectInfo syncedObjectInfo, SyncLogInfo syncLogInfo)
        {
            try
            {
                using (var connector = new SalesforceConnector(_settings))
                {
                    var request = JsonConvert.DeserializeObject<SfObject>(syncLogInfo.SyncLogRequest);

                    // Get existing contact ID if possible
                    var user = UserInfoProvider.GetUserInfo(syncLogInfo.SyncLogKenticoItemID);

                    var contacts = connector.GetContactsByContactID(user.GetStringValue("UserSalesforceContactID", string.Empty));

                    if (contacts.Count <= 0)
                    {
                        contacts = connector.GetContactsByEmail(user.UserName);
                    }

                    if (contacts.Count <= 0)
                    {
                        var leads = connector.GetLeadsByLeadID(user.GetStringValue("UserSalesForceLeadID", string.Empty));

                        if (leads.Count <= 0)
                        {
                            leads = connector.GetLeadsByEmail(user.UserName);
                        }

                        if (leads.Count <= 0)
                        {
                            leads.Add(new Lead());
                        }

                        // Create lead based on user
                        foreach (var lead in leads)
                        {
                            if (lead.GetBoolean("IsConverted", false))
                            {
                                contacts = connector.GetContactsByContactID(lead.GetString("ConvertedContactId"));

                                foreach (var contact in contacts)
                                {
                                    request = GenerateContactRequest(contact, ref user);
                                }
                            }
                            else
                            {
                                var sites = UserSiteInfoProvider.GetUserSites()
                                    .WhereEquals("UserID", user.UserID)
                                    .Select(x => SiteInfoProvider.GetSiteInfo(x.SiteID))
                                    .Where(x => x != null);
                                foreach (var site in sites)
                                {
                                    request = GenerateLeadRequest(site, lead, ref user);
                                }
                            }
                        }
                    }
                    else
                    {
                        // Update Contact
                        foreach (var contact in contacts)
                        {
                            request = GenerateContactRequest(contact, ref user);
                        }
                    }

                    var result = connector.Upsert("ID", request.Obj);
                    

                    var leadId = result.id;

                    if (leadId != null)
                    {
                        user.SetValue("UserSalesForceLeadID", leadId);
                        UserInfoProvider.SetUserInfo(user);
                    }

                    syncLogInfo = SyncLogHelper.SyncLogSuccess(syncLogInfo, result.success, JsonConvert.SerializeObject(request), XmlSerialization.Serialize(result), result.id);
                }

                return syncLogInfo;
            }
            catch (Exception ex)
            {
                return SyncLogHelper.SyncLogFailed(syncLogInfo, ex.Message);
            }
        }

        public string GetFields(string crmModule)
        {
            throw new NotImplementedException();
        }

        public string GetEnum(string crmModule, string crmId)
        {
            throw new NotImplementedException();
        }
    }
}