﻿using System;
using CMS.Helpers;

namespace WF.Integration.Helpers
{
    public static class DateTimeConverter
    {
        public static DateTime FromString(object value)
        {
            var result = ValidationHelper.GetDateTime(value, DateTime.MinValue, "en-AU");

            if (result == DateTime.MinValue)
            {
                result = ValidationHelper.GetDateTime(value, DateTime.MinValue, "en-US");
            }

            return result;
        }
    }
}
