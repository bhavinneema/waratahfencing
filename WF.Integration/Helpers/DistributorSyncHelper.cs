﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Helpers;
using WDM.SalesforceConnector.Classes;
using WF.Distributors.Classes;
using WF.Domain.Classes;

namespace WF.Integration.Helpers
{
    public static class DistributorSyncHelper
    {
        public static Dictionary<string, object> ImportDistributors(List<Account> accounts)
        {
            var result = new Dictionary<string, object>();
            var duplicate = 0;
            var updated = 0;
            var created = 0;
            var skipped = 0;

            foreach (var account in accounts)
            {
                var checkDuplicate = DistributorInfoProvider.GetDistributors()
                    .WhereEquals("DistributorName", account.GetString("Name"))
                    .WhereEquals("DistributorAddress1", account.GetString("ShippingStreet"))
                    .WhereNotEquals("DistributorSalesforceID", account.Obj.Id)
                    .FirstObject;

                if (checkDuplicate != null)
                {
                    duplicate++;
                    continue;
                }

                var distributor = DistributorInfoProvider.GetDistributors()
                    .WhereEquals("DistributorSalesforceID", account.Obj.Id)
                    .FirstObject;

                if (distributor == null)
                {
                    created++;
                    distributor = new DistributorInfo();
                }
                else
                {
                    updated++;
                }

                distributor.DistributorSalesforceID = account.Obj.Id;
                distributor.DistributorName = account.GetString("Name");
                distributor.DistributorPhone = account.GetString("Phone");
                distributor.DistributorAddress1 = account.GetString("ShippingStreet");
                distributor.DistributorCity = account.GetString("ShippingCity") ?? account.GetString("BillingCity");
                if (account.GetString("ShippingCountry") == null && account.GetString("BillingCountry") == null)
                {
                    if (account.GetString("RecordTypeId") == "0127F000000UenzQAC") // TODO New Zealand Distributor Account Record Type ID
                        distributor.DistributorCountry = "New Zealand";
                    if (account.GetString("RecordTypeId") == "0127F000000UenyQAC") // TODO Australian Distributor Account Record Type ID
                        distributor.DistributorCountry = "Australia";
                }
                else
                {
                    distributor.DistributorCountry = account.GetString("ShippingCountry") ?? account.GetString("BillingCountry");
                }
                distributor.DistributorPostcode = account.GetString("ShippingPostalCode") ?? account.GetString("BillingPostalCode");
                distributor.DistributorType = account.GetString("Type");
                distributor.DistributorStatus = account.GetString("Waratah_Status__c");
                distributor.DistributorStarPartner = account.GetBoolean("Star_Partner__c", false);
                distributor.DistributorStarPartnerRating = ValidationHelper.GetInteger(account.GetInteger("Star_Rating__c"), 0);
                var lat = account.GetDouble("ShippingLatitude");
                if (lat != 0)
                    distributor.DistributorLat = lat;
                var lng = account.GetDouble("ShippingLongitude");
                if (lng != 0)
                    distributor.DistributorLng = lng;
                DistributorInfoProvider.SetDistributorInfo(distributor);
            }

            // Disable all not found in query
            var distributorsToDisable = DistributorInfoProvider.GetDistributors()
                .WhereNotIn("DistributorSalesforceID", accounts.Select(x => x.Obj.Id).ToList());
            foreach (var distributor in distributorsToDisable)
            {
                distributor.DistributorStatus = "Inactive";
                DistributorInfoProvider.SetDistributorInfo(distributor);
            }

            result.Add("skipped", skipped);
            result.Add("duplicate", duplicate);
            result.Add("updated", updated);
            result.Add("created", created);
            result.Add("total", accounts.Count);
            result.Add("disabled", distributorsToDisable.Count);

            return result;
        }
    }
}
