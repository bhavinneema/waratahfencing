﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.OnlineForms;
using WDM.SalesforceConnector.Classes;
using WF.Promotions.Classes;

namespace WF.Integration.Helpers
{
    public static class PromotionStatusUpdateRequestHelper
    {
        public static string UpdatePromotionStatuses(List<Promotion> promotions)
        {
            var skipped = 0;
            var updated = 0;
            var statusUpdateRequests = PromotionStatusUpdateRequestInfoProvider.GetPromotionStatusUpdateRequests();

            foreach (var statusUpdateRequest in statusUpdateRequests)
            {
                var promotion = promotions.FirstOrDefault(x => x.Obj.Id == statusUpdateRequest.PromotionStatusUpdateRequestSalesforceID);

                if (promotion == null)
                {
                    skipped++;
                    continue;
                }

                var promotionEntry = BizFormItemProvider.GetItem(
                    statusUpdateRequest.PromotionStatusUpdateRequestFormItemID,
                    statusUpdateRequest.PromotionStatusUpdateRequestFormClass);

                if (promotionEntry == null)
                {
                    skipped++;
                    continue;
                }

                var crmValue = promotion.GetString(statusUpdateRequest.PromotionStatusUpdateRequestCRMStatusField, string.Empty);
                var kenticoValue = promotionEntry.GetStringValue(statusUpdateRequest.PromotionStatusUpdateRequestKenticoStatusField, string.Empty);

                if (crmValue != kenticoValue)
                {
                    promotionEntry.SetValue(statusUpdateRequest.PromotionStatusUpdateRequestKenticoStatusField, crmValue);
                    BizFormItemProvider.SetItem(promotionEntry);
                    updated++;
                    continue;
                }
                skipped++;
            }

            return $"Updated {updated} promotion statuses and skipped {skipped}.";
        }
    }
}