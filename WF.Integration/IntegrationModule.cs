﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS;
using CMS.DataEngine;
using WF.Integration;

[assembly: RegisterModule(typeof(IntegrationModule))]
namespace WF.Integration
{
    public class IntegrationModule : Module
    {
        public IntegrationModule() : base("WF.Integration"){}
    }
}
