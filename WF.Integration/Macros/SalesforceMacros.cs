﻿using CMS;
using CMS.MacroEngine;
using System;
using CMS.Helpers;
using WDM.SalesforceConnector.Classes;
using WDM.SalesforceConnector.Helpers;
using WDM.SalesforceConnector.Macros;
using WDM.SyncedObjects.Helpers;
using CMS.Ecommerce;
using WF;
using WF.Promotions.Classes;
using WF.Promotions.Helpers;

[assembly: RegisterExtension(typeof(SalesforceMacros), typeof(string))]
[assembly: RegisterExtension(typeof(SalesforceMacros), typeof(StringNamespace))]

namespace WDM.SalesforceConnector.Macros
{
    public class SalesforceMacros : MacroMethodContainer
    {
        [MacroMethod(typeof(string), "Describe object in salesforce", 1)]
        [MacroMethodParam(0, "salesforceClassName", typeof(string), "object to map")]
        public static object DescribeSalesforceObject(EvaluationContext context, params object[] parameters)
        {
            if ((parameters.Length != 1) || (!(parameters[0] is string)))
            {
                throw new ArgumentException("Invalid arguments to macro");
            }
            //var connector = Connectors.SalesforceConnector.Instance;
            //return connector.DescribeObject(parameters[0] as string);
            return "";
        }


        [MacroMethod(typeof(string), "Gets salesforce object", 2)]
        [MacroMethodParam(0, "salesforceID", typeof(string), "Salesforce ID")]
        [MacroMethodParam(1, "salesforceClass", typeof(string), "Salesforce Class")]
        public static object GetSalesforceObject(EvaluationContext context, params object[] parameters)
        {
            if (parameters.Length != 2)
            {
                throw new NotSupportedException("Wrong number of arguments");
            }

            //var connector = Connectors.SalesforceConnector.Instance;

            //SfObject sfObject = connector.GetObjectByID(parameters[0] as string, parameters[1] as string);
            //return XmlSerialization.Serialize(sfObject.Obj);
            return "";
        }

        [MacroMethod(typeof(string), "Describe object in salesforce", 1)]
        [MacroMethodParam(0, "salesforceClassName", typeof(string), "object to map")]
        public static object GetVariantSalesforceID(EvaluationContext context, params object[] parameters)
        {
            if ((parameters.Length != 1))
            {
                throw new ArgumentException("Invalid arguments to macro");
            }

            var skuid = ValidationHelper.GetInteger(parameters[0], 0);
            var sfId = PromotionHelper.GetSKUSalesforceID(skuid);

            return sfId;
        }

        [MacroMethod(typeof(string), "Describe object in salesforce", 1)]
        [MacroMethodParam(0, "salesforceClassName", typeof(string), "object to map")]
        public static object GetVariantProduct(EvaluationContext context, params object[] parameters)
        {
            if ((parameters.Length != 1))
            {
                throw new ArgumentException("Invalid arguments to macro");
            }

            var productSkuid = ValidationHelper.GetInteger(parameters[0], 0);
            var sku = SKUInfoProvider.GetSKUInfo(productSkuid);

            return sku.SKUName;
        }

        [MacroMethod(typeof(string), "Describe object in salesforce", 1)]
        [MacroMethodParam(0, "salesforceClassName", typeof(string), "object to map")]
        public static object GetVariantGroup(EvaluationContext context, params object[] parameters)
        {
            if ((parameters.Length != 1))
            {
                throw new ArgumentException("Invalid arguments to macro");
            }

            var groupId = ValidationHelper.GetInteger(parameters[0], 0);
            var group = PromotionRewardGroupInfoProvider.GetPromotionRewardGroupInfo(groupId);

            return group.PromotionRewardGroupDisplayName;
        }
    }
}
