﻿using System.Linq;

using CMS.Automation;
using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.OnlineForms;

using WDM.SyncedObjects.Classes;
using WDM.SalesforceConnector.Connectors;
using WF.Promotions.Classes;

namespace WF.Integration.MarketingAutomation
{
    public class GetStatusFromSalesforce : AutomationAction
    {
        public override void Execute()
        {
            var formClass = GetResolvedParameter("FormClass", string.Empty);
            var salesforceField = GetResolvedParameter("SalesforceStatusField", string.Empty);
            var kenticoField = GetResolvedParameter("KenticoStatusField", string.Empty);
            var formItemId = ValidationHelper.GetInteger(StateObject.StateCustomData["TriggerDataActivityItemDetailId"], 0);
            var dci = DataClassInfoProvider.GetDataClassInfo(formClass);

            var syncedObject = SyncedObjectInfoProvider.GetSyncedObjects().WhereEquals("SyncedObjectKenticoClass", dci.ClassID).FirstObject;

            var bfi = BizFormItemProvider.GetItem(formItemId, formClass);

            if (syncedObject == null || bfi == null || string.IsNullOrEmpty(bfi.GetStringValue(syncedObject.SyncedObjectKenticoReturnFieldName, string.Empty)))
            {
                return;
            }

            var request = PromotionStatusUpdateRequestInfoProvider.GetPromotionStatusUpdateRequests()
                .WhereEquals("PromotionStatusUpdateRequestFormClass", formClass)
                .WhereEquals("PromotionStatusUpdateRequestFormItemID", formItemId)
                .FirstObject;

            if (request != null) return;

            request = new PromotionStatusUpdateRequestInfo
            {
                PromotionStatusUpdateRequestFormClass = formClass,
                PromotionStatusUpdateRequestFormItemID = formItemId,
                PromotionStatusUpdateRequestFormItemSalesforceIDField = syncedObject.SyncedObjectKenticoReturnFieldName,
                PromotionStatusUpdateRequestSalesforceID = bfi.GetStringValue(syncedObject.SyncedObjectKenticoReturnFieldName, string.Empty),
                PromotionStatusUpdateRequestCRMStatusField = salesforceField,
                PromotionStatusUpdateRequestKenticoStatusField = kenticoField
            };

            PromotionStatusUpdateRequestInfoProvider.SetPromotionStatusUpdateRequestInfo(request);
        }
    }
}
