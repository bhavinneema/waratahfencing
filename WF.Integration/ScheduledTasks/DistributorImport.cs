﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using CMS.DataEngine;
using CMS.Scheduler;
using CMS.SiteProvider;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Helpers;

namespace WF.Integration.ScheduledTasks
{
    public class DistributorImport : ITask
    {
        public string Execute(TaskInfo task)
        {
            var result = new Dictionary<string, object>();

            var syncedObject = SyncedObjectInfoProvider.GetSyncedObjectInfo("WaratahDistributorImport");

            SyncHelper.GenerateGenericQueueTask(syncedObject);

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            return serializer.Serialize(result);
        }
    }
}
