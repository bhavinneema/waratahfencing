﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using WF.Domain.Classes;
using CMS.DataEngine;
using CMS.Ecommerce;
using WF.Promotions.Classes;
using WF.Promotions.DataClasses;
using WF.SystemModule.Cashback;
using WF.SystemModule.DataClasses;

namespace WF.Promotions.Cashback
{
    public class CashbackDetails
    {
        [JsonProperty("calculationTimeStamp")]
        public DateTime CalculationTimeStamp { get; set; }

        [JsonProperty("rewardGroups")]
        public List<CashbackDetailsRewardGroup> RewardGroups { get; set; }

        [JsonProperty("totalClaimAmount")]
        public double TotalClaimAmount { get; set; }

        [JsonProperty("extraDiscountPercent")]
        public double ExtraDiscountPercent { get; set; }

        [JsonProperty("extraDiscountAmount")]
        public double ExtraDiscountAmount { get; set; }

        [JsonProperty("totalCashback")]
        public double TotalCashback { get; set; }

        [JsonProperty("totalItemRewards")]
        public List<ItemReward> TotalItemRewards { get; set; }

        [JsonProperty("")]


        private PurchaseInfo _purchaseInfo;
        private PromotionInfo _promotion;
        private double _lowestAmount;
        private double _totalCashbackBeforeExtraDiscount;
        private InfoDataSet<PromotionRewardGroupInfo> _allRewardGroups;
        private Dictionary<int, int> _totalItemRewards;


        private InfoDataSet<PromotionRewardGroupInfo> AllRewardGroups
        {
            get
            {
                return _allRewardGroups ?? (_allRewardGroups = PromotionRewardGroupInfoProvider.GetPromotionRewardGroups()
                .WhereEquals("PromotionRewardGroupPromotionID", _purchaseInfo.PromotionID)
                .TypedResult);
            }
        }


        /// <summary>
        /// The cashback details are calculated when the class is constructed.
        /// </summary>
        public CashbackDetails(PurchaseInfo purchaseInfo)
        {
            if (purchaseInfo == null)
            {
                return;
            }

            _purchaseInfo = purchaseInfo;
            _promotion = PromotionInfoProvider.GetPromotionInfo(purchaseInfo.PromotionID);

            if (_promotion == null)
            {
                return;
            }

            CalculationTimeStamp = DateTime.Now;
            TotalClaimAmount = GetClaimTotal();

            // We need to do the calculation for each reward group in two passes.
            // The first pass determines if the reward group is eligible, and gets the quantity/amount being claimed.
            // Once we have all the amounts, we can find the lowest - which may be used in the second pass.
            RewardGroups = GetRewardGroupsFirstPass();

            _lowestAmount = (RewardGroups.Count == 0)
                ? 0
                : RewardGroups.Min(rg => (rg.Rewards.Count == 0)
                    ? 0
                    : rg.Rewards.Min(r => r.Amount));

            RewardGroupsSecondPass();

            _totalCashbackBeforeExtraDiscount = RewardGroups.Select(r => r.Reward.Cashback).Sum();
            _totalItemRewards = new Dictionary<int, int>();
            foreach (var rewardGroup in RewardGroups.Where(r => r.Reward.ItemRewards.Count > 0))
            {
                foreach (var itemReward in rewardGroup.Reward.ItemRewards)
                {
                    if (_totalItemRewards.ContainsKey(itemReward.Key))
                    {
                        _totalItemRewards[itemReward.Key] += itemReward.Value;
                    }
                    else
                    {
                        _totalItemRewards.Add(itemReward.Key, itemReward.Value);
                    }
                }
            }
            ExtraDiscountPercent = _promotion.PromotionExtraDiscountPercent;
            ExtraDiscountAmount = ApplyExtraCashback();

            TotalCashback = (!_promotion.PromotionMustQualifyForAllGroups || AllRewardGroupsQualify())
                ? _totalCashbackBeforeExtraDiscount + ExtraDiscountAmount
                : 0;

            TotalItemRewards = (!_promotion.PromotionMustQualifyForAllGroups || AllRewardGroupsQualify())
                ? _totalItemRewards.Where(x => PromotionGiveawayItemInfoProvider.GetPromotionGiveawayItemInfo(x.Key) != null).Select(x => new ItemReward
                {
                    ItemRewardName = $"{PromotionGiveawayItemInfoProvider.GetPromotionGiveawayItemInfo(x.Key).PromotionGiveawayItemDisplayName}",
                    ItemRewardQuantity = x.Value
                }).ToList()
                : new List<ItemReward>();
        }


        /// <summary>
        /// Gets a list of reward groups for this claim that are eligible for cashback.
        /// </summary>
        private List<CashbackDetailsRewardGroup> GetRewardGroupsFirstPass()
        {
            if (_purchaseInfo.Groups == null)
            {
                return new List<CashbackDetailsRewardGroup>();
            }

            return _purchaseInfo.Groups.Select(g => new CashbackDetailsRewardGroup
            {
                GroupID = g.GroupID,
                Name = g.Name,
                Rewards = GetRewardsForGroupFirstPass(g),
                Reward = new GroupReward
                {
                    Cashback = 0,
                    ItemRewards = new Dictionary<int, int>()
                } // Will be calculated in the second pass
            }).ToList();
        }


        /// <summary>
        /// Gets the amount for the the claimed group (the group contains products/vartiants with qty's).
        /// </summary>
        private List<CashbackDetailsReward> GetRewardsForGroupFirstPass(PurchaseInfoGroup group)
        {
            return PromotionRewardInfoProvider.GetPromotionRewards()
                .WhereEquals("PromotionRewardGroupID", group.GroupID)
                .OrderBy("PromotionRewardOrder")
                .TypedResult
                .Select(ri => new CashbackDetailsReward
                {
                    RewardID = ri.PromotionRewardID,
                    Name = ri.PromotionRewardDisplayName,
                    Amount = GetAmountForReward(group, ri),
                    Cashback = 0, // Will be calculated in the second pass
                    ItemRewardSkuid = 0, // Will be calculated in the second pass
                    ItemRewardQty = 0 // Will be calculated in the second pass
                })
                .ToList();
        }


        /// <summary>
        /// Gets the qty or value for this reward.
        /// </summary>
        private double GetAmountForReward(PurchaseInfoGroup group, PromotionRewardInfo rewardInfo)
        {
            var applicableProductSkuids = PromotionRewardProductInfoProvider.GetPromotionRewardProducts()
                .Column("PromotionRewardProductSKUID")
                .WhereEquals("PromotionRewardProductPromotionRewardID", rewardInfo.PromotionRewardID)
                .GetListResult<int>();

            var applicableProductsInGroup = group.Products.Where(p => applicableProductSkuids.Contains(p.SKUID));

            double amount = rewardInfo.PromotionRewardIsValueBased
                ? applicableProductsInGroup.Select(p => p.Variants.Select(v => GetSubTotal(group.GroupID, v)).Sum()).Sum()
                : applicableProductsInGroup.Select(p => p.Variants.Select(v => v.Qty).Sum()).Sum();
            return amount;
        }


        /// <summary>
        /// Gets a list of reward groups for this claim that are eligible for cashback.
        /// </summary>
        private void RewardGroupsSecondPass()
        {
            foreach (CashbackDetailsRewardGroup rewardGroup in RewardGroups)
            {
                rewardGroup.Reward = GetRewardForGroup(rewardGroup);
            }

            // Remove reward groups with no reward.
            RewardGroups = RewardGroups.FindAll(rg => rg.Reward.Cashback > 0 || rg.Reward.ItemRewards.Sum(x => x.Value) > 0).ToList();
        }


        /// <summary>
        /// Gets the cashback for the the claimed group (the group contains products/vartiants with qty's).
        /// </summary>
        private GroupReward GetRewardForGroup(CashbackDetailsRewardGroup rewardGroup)
        {
            var gr = new GroupReward
            {
                Cashback = 0,
                ItemRewards = new Dictionary<int, int>()
            };
            foreach (CashbackDetailsReward reward in rewardGroup.Rewards)
            {
                var rewards = GetRewardForReward(reward);
                gr.Cashback += rewards.Cashback;
                if (gr.ItemRewards.ContainsKey(rewards.GiveawayItemID))
                {
                    gr.ItemRewards[rewards.GiveawayItemID] += rewards.Quantity;
                }
                else
                {
                    gr.ItemRewards.Add(rewards.GiveawayItemID, rewards.Quantity);
                }
            }
            return gr;
        }

        /// <summary>
        /// Gets the amount for the the claimed group (the group contains products/vartiants with qty's).
        /// </summary>
        private double GetAmountForGroup(PurchaseInfoGroup group)
        {
            double amount = 0;
            var rewards = PromotionRewardInfoProvider.GetPromotionRewards()
                .WhereEquals("PromotionRewardGroupID", group.GroupID)
                .OrderBy("PromotionRewardOrder")
                .TypedResult;

            foreach (PromotionRewardInfo rewardInfo in rewards)
            {
                amount += GetAmountForReward(group, rewardInfo);
            }
            return amount;
        }


        /// <summary>
        /// Gets any cashback for the given reward.
        /// </summary>
        private Reward GetRewardForReward(CashbackDetailsReward reward)
        {
            var result = new Reward();
            result.Cashback = 0;
            result.GiveawayItemID = 0;
            result.Quantity = 0;

            double amountToUse = (_promotion.PromotionUseLowestAmount)
                ? _lowestAmount
                : reward.Amount;

            var ri = PromotionRewardInfoProvider.GetPromotionRewardInfo(reward.RewardID);
            if (ri == null)
            {
                return result;
            }

            if (amountToUse >= ri.PromotionRewardMinRequired)
            {
                if (ri.PromotionRewardType == 1 || ri.PromotionRewardType == 3)
                {
                    result.Cashback = ri.PromotionRewardCashback;
                }

                if (ri.PromotionRewardType == 2 || ri.PromotionRewardType == 3)
                {
                    result.GiveawayItemID = ri.PromotionRewardGiveawayItemID;
                    result.Quantity = ri.PromotionRewardItemQty;
                }

                var additionalConditions = PromotionProductInfoProvider.GetPromotionProducts()
             .WhereEquals("PromotionProductPromotionRewardID", ri.PromotionRewardID)
             .OrderBy("PromotionProductMinRequired")
             .TypedResult;

                foreach (PromotionProductInfo additionalCondition in additionalConditions)
                {
                    var additionalReward = GetCashbackForAdditionalCondition(amountToUse, additionalCondition);
                    result.Cashback += additionalReward.Cashback;
                    result.Quantity += additionalReward.Quantity;
                }
            }

            return result;
        }


        /// <summary>
        /// Gets any cashback for the given additional condition.
        ///
        /// Note:   The naming here is very strange...
        ///          * PromotionProduct        = "Additional condition"
        ///          * PromotionProductValue   = "Every"
        /// </summary>
        private static AdditionalReward GetCashbackForAdditionalCondition(double amount, PromotionProductInfo condition)
        {
            var result = new AdditionalReward
            {
                Cashback = 0,
                Quantity = 0
            };
            int minRequired = condition.PromotionProductMinRequired;

            // We use <= instead of <, so that this condition only applys when the amount is MORE than the minimumRequired.
            if (amount <= minRequired)
            {
                return result;
            }

            int upperLimit = condition.PromotionProductUpperLimit;
            int every = condition.PromotionProductValue;

            double range = (upperLimit == 0)
                ? amount - minRequired
                : Math.Min(amount, upperLimit) - minRequired;

            result.Cashback = Math.Floor(range / every) * condition.PromotionProductCashback;
            result.Quantity = (int)(range / every) * condition.PromotionProductItemQty;
            return result;
        }


        /// <summary>
        /// Get a sum of all claimed products/variants.
        /// </summary>
        private double GetClaimTotal()
        {
            double claimTotal = _purchaseInfo.Groups.Select(
                g => g.Products.Select(
                    p => p.Variants.Select(
                        v => GetSubTotal(g.GroupID, v)
                    ).Sum()
                ).Sum()
            ).Sum();

            return claimTotal;
        }


        /// <summary>
        /// Returns the sub-total for the given variant, depending on the reward group's 'include tax' setting.
        /// </summary>
        private double GetSubTotal(int groupID, PurchaseInfoVariant variant)
        {
            // Get the specified reward group.
            var rewardGroups = AllRewardGroups.Where(rg => rg.PromotionRewardGroupID == groupID);
            if (rewardGroups.Count() != 1)
            {
                return 0;
            }

            bool includeTax = rewardGroups.First().PromotionRewardGroupIncludeTax;
            double tax = (includeTax) ? variant.Tax : 0;

            return variant.Qty * (variant.UnitPrice + tax);
        }


        /// <summary>
        /// Do ALL reward groups meet the minimum requirements to receive cashback?
        /// </summary>
        private bool AllRewardGroupsQualify()
        {
            // Get a list of all reward groups IDs in this promotion.
            var all = AllRewardGroups.Select(rg => rg.PromotionRewardGroupID).ToList();

            // Get a list of reward groups that qualify for the promotion.
            var claimed = RewardGroups
                .Where(g => g.Reward.Cashback > 0 && g.Reward.ItemRewards.Sum(x => x.Value) > 0)
                .Select(g => g.GroupID)
                .ToList();

            // Does the 'claimed' list contain all the IDs that are in the 'all' list?
            return !all.Except(claimed).Any();
        }


        /// <summary>
        /// Calculates the extra cashback applied to the total claim value.
        /// Only applies if cashback is given for all reward groups in the promotion.
        /// </summary>
        private double ApplyExtraCashback()
        {
            if (!AllRewardGroupsQualify())
            {
                return 0;
            }

            return _totalCashbackBeforeExtraDiscount * ExtraDiscountPercent / 100;
        }
    }
}