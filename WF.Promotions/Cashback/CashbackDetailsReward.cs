﻿using Newtonsoft.Json;

namespace WF.SystemModule.Cashback
{
    public class CashbackDetailsReward
    {
        [JsonProperty("rewardID")]
        public int RewardID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// This is the amount (either quantity or value) that's calculated on the first pass of the calculation.
        /// </summary>
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("cashback")]
        public double Cashback { get; set; }

        [JsonProperty("itemrewardskuid")]
        public int ItemRewardSkuid { get; set; }

        [JsonProperty("itemrewardqty")]
        public int ItemRewardQty { get; set; }
    }
}