﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace WF.SystemModule.Cashback
{
    public class CashbackDetailsRewardGroup
    {
        [JsonProperty("groupID")]
        public int GroupID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("rewards")]
        public List<CashbackDetailsReward> Rewards { get; set; }

        [JsonProperty("reward")]
        public GroupReward Reward { get; set; }
    }
}