﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF.SystemModule.Cashback
{
    public class GroupReward
    {
        public double Cashback { get; set; }
        public Dictionary<int, int> ItemRewards { get; set; }
    }
}
