﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WF.SystemModule.Cashback
{
    public class ItemReward
    {
        [JsonProperty("name")]
        public string ItemRewardName { get; set; }
        [JsonProperty("quantity")]
        public int ItemRewardQuantity { get; set; }
    }
}