﻿namespace WF.SystemModule.Cashback
{
    public class Reward
    {
        public double Cashback { get; set; }
        public int GiveawayItemID { get; set; }
        public int Quantity { get; set; }
    }

    public class AdditionalReward
    {
        public double Cashback { get; set; }
        public int Quantity { get; set; }
    }
}