using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionEntryInfo management.
    /// </summary>
    public partial class PromotionEntryInfoProvider : AbstractInfoProvider<PromotionEntryInfo, PromotionEntryInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionEntryInfoProvider()
            : base(PromotionEntryInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionEntryInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionEntryInfo> GetPromotionEntries()
        {
            return ProviderObject.GetPromotionEntriesInternal();
        }


        /// <summary>
        /// Returns PromotionEntryInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntryInfo ID</param>
        public static PromotionEntryInfo GetPromotionEntryInfo(int id)
        {
            return ProviderObject.GetPromotionEntryInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionEntryInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionEntryInfo GUID</param>                
        public static PromotionEntryInfo GetPromotionEntryInfo(Guid guid)
        {
            return ProviderObject.GetPromotionEntryInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionEntryInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntryInfo to be set</param>
        public static void SetPromotionEntryInfo(PromotionEntryInfo infoObj)
        {
            ProviderObject.SetPromotionEntryInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionEntryInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntryInfo to be deleted</param>
        public static void DeletePromotionEntryInfo(PromotionEntryInfo infoObj)
        {
            ProviderObject.DeletePromotionEntryInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionEntryInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntryInfo ID</param>
        public static void DeletePromotionEntryInfo(int id)
        {
            PromotionEntryInfo infoObj = GetPromotionEntryInfo(id);
            DeletePromotionEntryInfo(infoObj);
        }

        #endregion


        #region "Public methods - Advanced"


        /// <summary>
        /// Returns a query for all the PromotionEntryInfo objects of a specified site.
        /// </summary>
        /// <param name="siteId">Site ID</param>
        public static ObjectQuery<PromotionEntryInfo> GetPromotionEntries(int siteId)
        {
            return ProviderObject.GetPromotionEntriesInternal(siteId);
        }
        
        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionEntryInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionEntryInfo> GetPromotionEntriesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionEntryInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntryInfo ID</param>        
        protected virtual PromotionEntryInfo GetPromotionEntryInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionEntryInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionEntryInfo GUID</param>
        protected virtual PromotionEntryInfo GetPromotionEntryInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionEntryInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntryInfo to be set</param>        
        protected virtual void SetPromotionEntryInfoInternal(PromotionEntryInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionEntryInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntryInfo to be deleted</param>        
        protected virtual void DeletePromotionEntryInfoInternal(PromotionEntryInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion

        #region "Internal methods - Advanced"


        /// <summary>
        /// Returns a query for all the PromotionEntryInfo objects of a specified site.
        /// </summary>
        /// <param name="siteId">Site ID</param>
        protected virtual ObjectQuery<PromotionEntryInfo> GetPromotionEntriesInternal(int siteId)
        {
            return GetObjectQuery().OnSite(siteId);
        }    
        
        #endregion		
    }
}