using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionEntrySourceInfo), PromotionEntrySourceInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionEntrySourceInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionEntrySourceInfo : AbstractInfo<PromotionEntrySourceInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionentrysource";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionEntrySourceInfoProvider), OBJECT_TYPE, "WF.PromotionEntrySource", "PromotionEntrySourceID", "PromotionEntrySourceLastModified", "PromotionEntrySourceGuid", null, "PromotionEntrySourceDisplayName", null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            OrderColumn = "PromotionEntrySourceOrder"
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion entry source ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntrySourceID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntrySourceID"), 0);
            }
            set
            {
                SetValue("PromotionEntrySourceID", value);
            }
        }


        /// <summary>
        /// Promotion entry source display name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionEntrySourceDisplayName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionEntrySourceDisplayName"), String.Empty);
            }
            set
            {
                SetValue("PromotionEntrySourceDisplayName", value);
            }
        }


        /// <summary>
        /// Promotion entry source order
        /// </summary>
        [DatabaseField]
        public virtual int PromotionEntrySourceOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionEntrySourceOrder"), 0);
            }
            set
            {
                SetValue("PromotionEntrySourceOrder", value, 0);
            }
        }


        /// <summary>
        /// Promotion entry source guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionEntrySourceGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionEntrySourceGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionEntrySourceGuid", value);
            }
        }


        /// <summary>
        /// Promotion entry source last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionEntrySourceLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionEntrySourceLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionEntrySourceLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionEntrySourceInfoProvider.DeletePromotionEntrySourceInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionEntrySourceInfoProvider.SetPromotionEntrySourceInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionEntrySourceInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionEntrySourceInfo object.
        /// </summary>
        public PromotionEntrySourceInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionEntrySourceInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionEntrySourceInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}