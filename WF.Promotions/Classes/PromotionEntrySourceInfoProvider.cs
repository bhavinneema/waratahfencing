using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionEntrySourceInfo management.
    /// </summary>
    public partial class PromotionEntrySourceInfoProvider : AbstractInfoProvider<PromotionEntrySourceInfo, PromotionEntrySourceInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionEntrySourceInfoProvider()
            : base(PromotionEntrySourceInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionEntrySourceInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionEntrySourceInfo> GetPromotionEntrySources()
        {
            return ProviderObject.GetPromotionEntrySourcesInternal();
        }


        /// <summary>
        /// Returns PromotionEntrySourceInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntrySourceInfo ID</param>
        public static PromotionEntrySourceInfo GetPromotionEntrySourceInfo(int id)
        {
            return ProviderObject.GetPromotionEntrySourceInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionEntrySourceInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionEntrySourceInfo GUID</param>                
        public static PromotionEntrySourceInfo GetPromotionEntrySourceInfo(Guid guid)
        {
            return ProviderObject.GetPromotionEntrySourceInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionEntrySourceInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntrySourceInfo to be set</param>
        public static void SetPromotionEntrySourceInfo(PromotionEntrySourceInfo infoObj)
        {
            ProviderObject.SetPromotionEntrySourceInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionEntrySourceInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntrySourceInfo to be deleted</param>
        public static void DeletePromotionEntrySourceInfo(PromotionEntrySourceInfo infoObj)
        {
            ProviderObject.DeletePromotionEntrySourceInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionEntrySourceInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntrySourceInfo ID</param>
        public static void DeletePromotionEntrySourceInfo(int id)
        {
            PromotionEntrySourceInfo infoObj = GetPromotionEntrySourceInfo(id);
            DeletePromotionEntrySourceInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionEntrySourceInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionEntrySourceInfo> GetPromotionEntrySourcesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionEntrySourceInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionEntrySourceInfo ID</param>        
        protected virtual PromotionEntrySourceInfo GetPromotionEntrySourceInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionEntrySourceInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionEntrySourceInfo GUID</param>
        protected virtual PromotionEntrySourceInfo GetPromotionEntrySourceInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionEntrySourceInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntrySourceInfo to be set</param>        
        protected virtual void SetPromotionEntrySourceInfoInternal(PromotionEntrySourceInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionEntrySourceInfo.
        /// </summary>
        /// <param name="infoObj">PromotionEntrySourceInfo to be deleted</param>        
        protected virtual void DeletePromotionEntrySourceInfoInternal(PromotionEntrySourceInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}