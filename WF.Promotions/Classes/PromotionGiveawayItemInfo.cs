using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionGiveawayItemInfo), PromotionGiveawayItemInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionGiveawayItemInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionGiveawayItemInfo : AbstractInfo<PromotionGiveawayItemInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotiongiveawayitem";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionGiveawayItemInfoProvider), OBJECT_TYPE, "WF.PromotionGiveawayItem", "PromotionGiveawayItemID", "PromotionGiveawayItemLastModified", "PromotionGiveawayItemGuid", null, "PromotionGiveawayItemDisplayName", null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            ContinuousIntegrationSettings =
            {
                Enabled = true
            }
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion giveaway item ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionGiveawayItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionGiveawayItemID"), 0);
            }
            set
            {
                SetValue("PromotionGiveawayItemID", value);
            }
        }


        /// <summary>
        /// Promotion giveaway item display name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionGiveawayItemDisplayName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionGiveawayItemDisplayName"), String.Empty);
            }
            set
            {
                SetValue("PromotionGiveawayItemDisplayName", value);
            }
        }


        /// <summary>
        /// Promotion giveaway item guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionGiveawayItemGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionGiveawayItemGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionGiveawayItemGuid", value);
            }
        }


        /// <summary>
        /// Promotion giveaway item last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionGiveawayItemLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionGiveawayItemLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionGiveawayItemLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionGiveawayItemInfoProvider.DeletePromotionGiveawayItemInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionGiveawayItemInfoProvider.SetPromotionGiveawayItemInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionGiveawayItemInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionGiveawayItemInfo object.
        /// </summary>
        public PromotionGiveawayItemInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionGiveawayItemInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionGiveawayItemInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}