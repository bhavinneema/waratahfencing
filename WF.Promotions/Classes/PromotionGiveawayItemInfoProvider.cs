using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionGiveawayItemInfo management.
    /// </summary>
    public partial class PromotionGiveawayItemInfoProvider : AbstractInfoProvider<PromotionGiveawayItemInfo, PromotionGiveawayItemInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionGiveawayItemInfoProvider()
            : base(PromotionGiveawayItemInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionGiveawayItemInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionGiveawayItemInfo> GetPromotionGiveawayItems()
        {
            return ProviderObject.GetPromotionGiveawayItemsInternal();
        }


        /// <summary>
        /// Returns PromotionGiveawayItemInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionGiveawayItemInfo ID</param>
        public static PromotionGiveawayItemInfo GetPromotionGiveawayItemInfo(int id)
        {
            return ProviderObject.GetPromotionGiveawayItemInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionGiveawayItemInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionGiveawayItemInfo GUID</param>                
        public static PromotionGiveawayItemInfo GetPromotionGiveawayItemInfo(Guid guid)
        {
            return ProviderObject.GetPromotionGiveawayItemInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionGiveawayItemInfo.
        /// </summary>
        /// <param name="infoObj">PromotionGiveawayItemInfo to be set</param>
        public static void SetPromotionGiveawayItemInfo(PromotionGiveawayItemInfo infoObj)
        {
            ProviderObject.SetPromotionGiveawayItemInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionGiveawayItemInfo.
        /// </summary>
        /// <param name="infoObj">PromotionGiveawayItemInfo to be deleted</param>
        public static void DeletePromotionGiveawayItemInfo(PromotionGiveawayItemInfo infoObj)
        {
            ProviderObject.DeletePromotionGiveawayItemInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionGiveawayItemInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionGiveawayItemInfo ID</param>
        public static void DeletePromotionGiveawayItemInfo(int id)
        {
            PromotionGiveawayItemInfo infoObj = GetPromotionGiveawayItemInfo(id);
            DeletePromotionGiveawayItemInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionGiveawayItemInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionGiveawayItemInfo> GetPromotionGiveawayItemsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionGiveawayItemInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionGiveawayItemInfo ID</param>        
        protected virtual PromotionGiveawayItemInfo GetPromotionGiveawayItemInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionGiveawayItemInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionGiveawayItemInfo GUID</param>
        protected virtual PromotionGiveawayItemInfo GetPromotionGiveawayItemInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionGiveawayItemInfo.
        /// </summary>
        /// <param name="infoObj">PromotionGiveawayItemInfo to be set</param>        
        protected virtual void SetPromotionGiveawayItemInfoInternal(PromotionGiveawayItemInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionGiveawayItemInfo.
        /// </summary>
        /// <param name="infoObj">PromotionGiveawayItemInfo to be deleted</param>        
        protected virtual void DeletePromotionGiveawayItemInfoInternal(PromotionGiveawayItemInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}