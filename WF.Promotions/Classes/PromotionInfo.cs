using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF.Domain.Classes;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionInfo), PromotionInfo.OBJECT_TYPE)]
    
namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionInfo : AbstractInfo<PromotionInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotion";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionInfoProvider), OBJECT_TYPE, "WF.Promotion", "PromotionID", "PromotionLastModified", "PromotionGuid", null, "PromotionName", null, "PromotionSiteID", null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionBizFormID", "cms.form", ObjectDependencyEnum.Binding), 
			    new ObjectDependency("PromotionSiteID", "cms.site", ObjectDependencyEnum.Required), 
            },
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion biz form ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionBizFormID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionBizFormID"), 0);
            }
            set
            {
                SetValue("PromotionBizFormID", value, 0);
            }
        }


        /// <summary>
        /// Promotion active
        /// </summary>
        [DatabaseField]
        public virtual bool PromotionActive
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("PromotionActive"), true);
            }
            set
            {
                SetValue("PromotionActive", value);
            }
        }


        /// <summary>
        /// Promotion date from
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionDateFrom
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionDateFrom"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionDateFrom", value, DateTimeHelper.ZERO_TIME);
            }
        }


        /// <summary>
        /// Promotion date to
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionDateTo
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionDateTo"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionDateTo", value, DateTimeHelper.ZERO_TIME);
            }
        }


        /// <summary>
        /// Promotion type
        /// </summary>
        [DatabaseField]
        public virtual string PromotionType
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionType"), "Cashback");
            }
            set
            {
                SetValue("PromotionType", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionName"), String.Empty);
            }
            set
            {
                SetValue("PromotionName", value);
            }
        }


        /// <summary>
        /// Promotion description
        /// </summary>
        [DatabaseField]
        public virtual string PromotionDescription
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionDescription"), String.Empty);
            }
            set
            {
                SetValue("PromotionDescription", value, String.Empty);
            }
        }


        /// <summary>
        /// ...to receive any cashback
        /// </summary>
        [DatabaseField]
        public virtual bool PromotionMustQualifyForAllGroups
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("PromotionMustQualifyForAllGroups"), false);
            }
            set
            {
                SetValue("PromotionMustQualifyForAllGroups", value);
            }
        }


        /// <summary>
        /// Promotion use lowest amount
        /// </summary>
        [DatabaseField]
        public virtual bool PromotionUseLowestAmount
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("PromotionUseLowestAmount"), false);
            }
            set
            {
                SetValue("PromotionUseLowestAmount", value);
            }
        }


        /// <summary>
        /// given if all reward groups qualify for cashback
        /// </summary>
        [DatabaseField]
        public virtual double PromotionExtraDiscountPercent
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("PromotionExtraDiscountPercent"), 0);
            }
            set
            {
                SetValue("PromotionExtraDiscountPercent", value, 0);
            }
        }


        /// <summary>
        /// Products
        /// </summary>
        [DatabaseField]
        public virtual string Products
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Products"), String.Empty);
            }
            set
            {
                SetValue("Products", value, String.Empty);
            }
        }


        /// <summary>
        /// Operator
        /// </summary>
        [DatabaseField]
        public virtual string Operator
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Operator"), "And");
            }
            set
            {
                SetValue("Operator", value, String.Empty);
            }
        }


        /// <summary>
        /// Unit of measure
        /// </summary>
        [DatabaseField]
        public virtual string UnitOfMeasure
        {
            get
            {
                return ValidationHelper.GetString(GetValue("UnitOfMeasure"), "Quantity");
            }
            set
            {
                SetValue("UnitOfMeasure", value, String.Empty);
            }
        }


        /// <summary>
        /// State
        /// </summary>
        [DatabaseField]
        public virtual string State
        {
            get
            {
                return ValidationHelper.GetString(GetValue("State"), String.Empty);
            }
            set
            {
                SetValue("State", value, String.Empty);
            }
        }


        /// <summary>
        /// Every additional unit
        /// </summary>
        [DatabaseField]
        public virtual int EveryAdditionalUnit
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("EveryAdditionalUnit"), 0);
            }
            set
            {
                SetValue("EveryAdditionalUnit", value, 0);
            }
        }


        /// <summary>
        /// Bonus amount
        /// </summary>
        [DatabaseField]
        public virtual decimal BonusAmount
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("BonusAmount"), 0);
            }
            set
            {
                SetValue("BonusAmount", value, 0);
            }
        }


        /// <summary>
        /// Promotion landing page title
        /// </summary>
        [DatabaseField]
        public virtual string PromotionLandingPageTitle
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionLandingPageTitle"), String.Empty);
            }
            set
            {
                SetValue("PromotionLandingPageTitle", value);
            }
        }


        /// <summary>
        /// Promotion landing page URL alias
        /// </summary>
        [DatabaseField]
        public virtual string PromotionLandingPageURLAlias
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionLandingPageURLAlias"), String.Empty);
            }
            set
            {
                SetValue("PromotionLandingPageURLAlias", value);
            }
        }


        /// <summary>
        /// Promotion landing page content
        /// </summary>
        [DatabaseField]
        public virtual string PromotionLandingPageContent
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionLandingPageContent"), String.Empty);
            }
            set
            {
                SetValue("PromotionLandingPageContent", value);
            }
        }


        /// <summary>
        /// Promotion form content top
        /// </summary>
        [DatabaseField]
        public virtual string PromotionFormContentTop
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionFormContentTop"), String.Empty);
            }
            set
            {
                SetValue("PromotionFormContentTop", value);
            }
        }


        /// <summary>
        /// Promotion form content bottom
        /// </summary>
        [DatabaseField]
        public virtual string PromotionFormContentBottom
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionFormContentBottom"), String.Empty);
            }
            set
            {
                SetValue("PromotionFormContentBottom", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion form terms and conditions
        /// </summary>
        [DatabaseField]
        public virtual string PromotionFormTermsAndConditions
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionFormTermsAndConditions"), String.Empty);
            }
            set
            {
                SetValue("PromotionFormTermsAndConditions", value);
            }
        }


        /// <summary>
        /// Promotion expired content
        /// </summary>
        [DatabaseField]
        public virtual string PromotionExpiredContent
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionExpiredContent"), String.Empty);
            }
            set
            {
                SetValue("PromotionExpiredContent", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionID"), 0);
            }
            set
            {
                SetValue("PromotionID", value);
            }
        }


        /// <summary>
        /// Promotion site ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionSiteID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionSiteID"), 0);
            }
            set
            {
                SetValue("PromotionSiteID", value);
            }
        }


        /// <summary>
        /// Promotion guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionGuid", value);
            }
        }


        /// <summary>
        /// Promotion last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionInfoProvider.DeletePromotionInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionInfoProvider.SetPromotionInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionInfo object.
        /// </summary>
        public PromotionInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}