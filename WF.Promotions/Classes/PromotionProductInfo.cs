using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionProductInfo), PromotionProductInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionProductInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionProductInfo : AbstractInfo<PromotionProductInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionproduct";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionProductInfoProvider), OBJECT_TYPE, "WF.PromotionProduct", "PromotionProductID", "PromotionProductLastModified", "PromotionProductGuid", null, "PromotionProductID", null, null, "PromotionProductPromotionRewardID", PromotionRewardInfo.OBJECT_TYPE)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionProductPromotionRewardID", "wf.promotionreward", ObjectDependencyEnum.Required), 
            },
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion product ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductID"), 0);
            }
            set
            {
                SetValue("PromotionProductID", value);
            }
        }


        /// <summary>
        /// Promotion product promotion reward ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductPromotionRewardID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductPromotionRewardID"), 0);
            }
            set
            {
                SetValue("PromotionProductPromotionRewardID", value);
            }
        }


        /// <summary>
        /// Promotion product min required
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductMinRequired
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductMinRequired"), 0);
            }
            set
            {
                SetValue("PromotionProductMinRequired", value);
            }
        }


        /// <summary>
        /// Promotion product upper limit
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductUpperLimit
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductUpperLimit"), 0);
            }
            set
            {
                SetValue("PromotionProductUpperLimit", value);
            }
        }


        /// <summary>
        /// Promotion product value
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductValue
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductValue"), 0);
            }
            set
            {
                SetValue("PromotionProductValue", value);
            }
        }


        /// <summary>
        /// Promotion product cashback
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductCashback
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductCashback"), 0);
            }
            set
            {
                SetValue("PromotionProductCashback", value, 0);
            }
        }


        /// <summary>
        /// Promotion product item
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductItem
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductItem"), 0);
            }
            set
            {
                SetValue("PromotionProductItem", value, 0);
            }
        }


        /// <summary>
        /// Promotion product item qty
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductItemQty
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductItemQty"), 0);
            }
            set
            {
                SetValue("PromotionProductItemQty", value, 0);
            }
        }


        /// <summary>
        /// Promotion product guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionProductGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionProductGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionProductGuid", value);
            }
        }


        /// <summary>
        /// Promotion product last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionProductLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionProductLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionProductLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionProductInfoProvider.DeletePromotionProductInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionProductInfoProvider.SetPromotionProductInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionProductInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionProductInfo object.
        /// </summary>
        public PromotionProductInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionProductInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionProductInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}