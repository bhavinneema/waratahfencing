﻿using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionProductVariantInfo), PromotionProductVariantInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionProductVariantInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionProductVariantInfo : AbstractInfo<PromotionProductVariantInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionproductvariant";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionProductVariantInfoProvider), OBJECT_TYPE, "WF.PromotionProductVariant", "PromotionProductVariantID", "PromotionProductVariantLastModified", "PromotionProductVariantGuid", null, "PromotionProductVariantName", null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion product variant ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantID", value);
            }
        }


        /// <summary>
        /// Promotion product variant promotion form ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantPromotionFormID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantPromotionFormID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantPromotionFormID", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant promotion form entry ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantPromotionFormEntryID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantPromotionFormEntryID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantPromotionFormEntryID", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant group ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantGroupID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantGroupID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantGroupID", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant product SKUID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantProductSKUID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantProductSKUID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantProductSKUID", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant SKUID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantSKUID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantSKUID"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantSKUID", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionProductVariantName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionProductVariantName"), String.Empty);
            }
            set
            {
                SetValue("PromotionProductVariantName", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion product variant unit price
        /// </summary>
        [DatabaseField]
        public virtual double PromotionProductVariantUnitPrice
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("PromotionProductVariantUnitPrice"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantUnitPrice", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant tax
        /// </summary>
        [DatabaseField]
        public virtual double PromotionProductVariantTax
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("PromotionProductVariantTax"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantTax", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant qty
        /// </summary>
        [DatabaseField]
        public virtual int PromotionProductVariantQty
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionProductVariantQty"), 0);
            }
            set
            {
                SetValue("PromotionProductVariantQty", value, 0);
            }
        }


        /// <summary>
        /// Promotion product variant salesforce ID
        /// </summary>
        [DatabaseField]
        public virtual string PromotionProductVariantSalesforceID
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionProductVariantSalesforceID"), String.Empty);
            }
            set
            {
                SetValue("PromotionProductVariantSalesforceID", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion product variant guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionProductVariantGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionProductVariantGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionProductVariantGuid", value);
            }
        }


        /// <summary>
        /// Promotion product variant last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionProductVariantLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionProductVariantLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionProductVariantLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionProductVariantInfoProvider.DeletePromotionProductVariantInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionProductVariantInfoProvider.SetPromotionProductVariantInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionProductVariantInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionProductVariantInfo object.
        /// </summary>
        public PromotionProductVariantInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionProductVariantInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionProductVariantInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}