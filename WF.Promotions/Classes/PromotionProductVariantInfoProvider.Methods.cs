﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;

namespace WF.Promotions.Classes
{
    public partial class PromotionProductVariantInfoProvider : AbstractInfoProvider<PromotionProductVariantInfo, PromotionProductVariantInfoProvider>
    {
        public static PromotionProductVariantInfo GetExistingPromotionProductVariantInfo(int promotionId, int entryId, int groupId, int productSkuid, int skuid)
        {
            return GetPromotionProductVariants()
                .WhereEquals("PromotionProductVariantPromotionFormID", promotionId)
                .WhereEquals("PromotionProductVariantPromotionFormEntryID", entryId)
                .WhereEquals("PromotionProductVariantGroupID", groupId)
                .WhereEquals("PromotionProductVariantProductSKUID", productSkuid)
                .WhereEquals("PromotionProductVariantSKUID", skuid)
                .FirstObject;
        }
    }
}
