﻿using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionProductVariantInfo management.
    /// </summary>
    public partial class PromotionProductVariantInfoProvider : AbstractInfoProvider<PromotionProductVariantInfo, PromotionProductVariantInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionProductVariantInfoProvider()
            : base(PromotionProductVariantInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionProductVariantInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionProductVariantInfo> GetPromotionProductVariants()
        {
            return ProviderObject.GetPromotionProductVariantsInternal();
        }


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductVariantInfo ID</param>
        public static PromotionProductVariantInfo GetPromotionProductVariantInfo(int id)
        {
            return ProviderObject.GetPromotionProductVariantInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified name.
        /// </summary>
        /// <param name="name">PromotionProductVariantInfo name</param>
        public static PromotionProductVariantInfo GetPromotionProductVariantInfo(string name)
        {
            return ProviderObject.GetPromotionProductVariantInfoInternal(name);
        }


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionProductVariantInfo GUID</param>                
        public static PromotionProductVariantInfo GetPromotionProductVariantInfo(Guid guid)
        {
            return ProviderObject.GetPromotionProductVariantInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionProductVariantInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductVariantInfo to be set</param>
        public static void SetPromotionProductVariantInfo(PromotionProductVariantInfo infoObj)
        {
            ProviderObject.SetPromotionProductVariantInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionProductVariantInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductVariantInfo to be deleted</param>
        public static void DeletePromotionProductVariantInfo(PromotionProductVariantInfo infoObj)
        {
            ProviderObject.DeletePromotionProductVariantInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionProductVariantInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductVariantInfo ID</param>
        public static void DeletePromotionProductVariantInfo(int id)
        {
            PromotionProductVariantInfo infoObj = GetPromotionProductVariantInfo(id);
            DeletePromotionProductVariantInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionProductVariantInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionProductVariantInfo> GetPromotionProductVariantsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionProductVariantInfo ID</param>        
        protected virtual PromotionProductVariantInfo GetPromotionProductVariantInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified name.
        /// </summary>
        /// <param name="name">PromotionProductVariantInfo name</param>        
        protected virtual PromotionProductVariantInfo GetPromotionProductVariantInfoInternal(string name)
        {
            return GetInfoByCodeName(name);
        } 


        /// <summary>
        /// Returns PromotionProductVariantInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionProductVariantInfo GUID</param>
        protected virtual PromotionProductVariantInfo GetPromotionProductVariantInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionProductVariantInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductVariantInfo to be set</param>        
        protected virtual void SetPromotionProductVariantInfoInternal(PromotionProductVariantInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionProductVariantInfo.
        /// </summary>
        /// <param name="infoObj">PromotionProductVariantInfo to be deleted</param>        
        protected virtual void DeletePromotionProductVariantInfoInternal(PromotionProductVariantInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}