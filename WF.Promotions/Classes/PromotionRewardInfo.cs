using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionRewardInfo), PromotionRewardInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionRewardInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionRewardInfo : AbstractInfo<PromotionRewardInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionreward";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionRewardInfoProvider), OBJECT_TYPE, "WF.PromotionReward", "PromotionRewardID", "PromotionRewardLastModified", "PromotionRewardGuid", null, "PromotionRewardID", null, null, "PromotionRewardGroupID", PromotionRewardGroupInfo.OBJECT_TYPE)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("PromotionRewardGroupID", "wf.promotionrewardgroup", ObjectDependencyEnum.Required),
			    new ObjectDependency("PromotionRewardGiveawayItemID", "wf.promotiongiveawayitem", ObjectDependencyEnum.Required),
            },
            OrderColumn = "PromotionRewardOrder"
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion reward ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardID"), 0);
            }
            set
            {
                SetValue("PromotionRewardID", value);
            }
        }


        /// <summary>
        /// Promotion reward name
        /// </summary>
        [DatabaseField]
        public virtual string PromotionRewardDisplayName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionRewardDisplayName"), string.Empty);
            }
            set
            {
                SetValue("PromotionRewardDisplayName", value);
            }
        }


        /// <summary>
        /// Promotion reward group ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardGroupID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardGroupID"), 0);
            }
            set
            {
                SetValue("PromotionRewardGroupID", value);
            }
        }


        /// <summary>
        /// Promotion reward is value based
        /// </summary>
        [DatabaseField]
        public virtual bool PromotionRewardIsValueBased
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("PromotionRewardIsValueBased"), false);
            }
            set
            {
                SetValue("PromotionRewardIsValueBased", value);
            }
        }


        /// <summary>
        /// Promotion reward min required
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardMinRequired
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardMinRequired"), 0);
            }
            set
            {
                SetValue("PromotionRewardMinRequired", value);
            }
        }


        /// <summary>
        /// Promotion reward type
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardType
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardType"), 1);
            }
            set
            {
                SetValue("PromotionRewardType", value);
            }
        }


        /// <summary>
        /// Promotion reward cashback
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardCashback
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardCashback"), 0);
            }
            set
            {
                SetValue("PromotionRewardCashback", value, 0);
            }
        }


        /// <summary>
        /// Promotion reward item
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardGiveawayItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardGiveawayItemID"), 0);
            }
            set
            {
                SetValue("PromotionRewardGiveawayItemID", value, String.Empty);
            }
        }


        /// <summary>
        /// Promotion reward item qty
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardItemQty
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardItemQty"), 0);
            }
            set
            {
                SetValue("PromotionRewardItemQty", value, 0);
            }
        }


        /// <summary>
        /// Promotion reward order
        /// </summary>
        [DatabaseField]
        public virtual int PromotionRewardOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionRewardOrder"), 0);
            }
            set
            {
                SetValue("PromotionRewardOrder", value, 0);
            }
        }


        /// <summary>
        /// Promotion reward guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionRewardGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionRewardGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionRewardGuid", value);
            }
        }


        /// <summary>
        /// Promotion reward last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionRewardLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionRewardLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionRewardLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionRewardInfoProvider.DeletePromotionRewardInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionRewardInfoProvider.SetPromotionRewardInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionRewardInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionRewardInfo object.
        /// </summary>
        public PromotionRewardInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionRewardInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionRewardInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}