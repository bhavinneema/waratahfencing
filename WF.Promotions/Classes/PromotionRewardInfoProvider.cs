using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionRewardInfo management.
    /// </summary>
    public partial class PromotionRewardInfoProvider : AbstractInfoProvider<PromotionRewardInfo, PromotionRewardInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionRewardInfoProvider()
            : base(PromotionRewardInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionRewardInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionRewardInfo> GetPromotionRewards()
        {
            return ProviderObject.GetPromotionRewardsInternal();
        }


        /// <summary>
        /// Returns PromotionRewardInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardInfo ID</param>
        public static PromotionRewardInfo GetPromotionRewardInfo(int id)
        {
            return ProviderObject.GetPromotionRewardInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionRewardInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardInfo GUID</param>                
        public static PromotionRewardInfo GetPromotionRewardInfo(Guid guid)
        {
            return ProviderObject.GetPromotionRewardInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardInfo to be set</param>
        public static void SetPromotionRewardInfo(PromotionRewardInfo infoObj)
        {
            ProviderObject.SetPromotionRewardInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardInfo to be deleted</param>
        public static void DeletePromotionRewardInfo(PromotionRewardInfo infoObj)
        {
            ProviderObject.DeletePromotionRewardInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionRewardInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardInfo ID</param>
        public static void DeletePromotionRewardInfo(int id)
        {
            PromotionRewardInfo infoObj = GetPromotionRewardInfo(id);
            DeletePromotionRewardInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionRewardInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionRewardInfo> GetPromotionRewardsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionRewardInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardInfo ID</param>        
        protected virtual PromotionRewardInfo GetPromotionRewardInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionRewardInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardInfo GUID</param>
        protected virtual PromotionRewardInfo GetPromotionRewardInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardInfo to be set</param>        
        protected virtual void SetPromotionRewardInfoInternal(PromotionRewardInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardInfo to be deleted</param>        
        protected virtual void DeletePromotionRewardInfoInternal(PromotionRewardInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}