using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionRewardProductInfo management.
    /// </summary>
    public partial class PromotionRewardProductInfoProvider : AbstractInfoProvider<PromotionRewardProductInfo, PromotionRewardProductInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionRewardProductInfoProvider()
            : base(PromotionRewardProductInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionRewardProductInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionRewardProductInfo> GetPromotionRewardProducts()
        {
            return ProviderObject.GetPromotionRewardProductsInternal();
        }


        /// <summary>
        /// Returns PromotionRewardProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardProductInfo ID</param>
        public static PromotionRewardProductInfo GetPromotionRewardProductInfo(int id)
        {
            return ProviderObject.GetPromotionRewardProductInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionRewardProductInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardProductInfo GUID</param>                
        public static PromotionRewardProductInfo GetPromotionRewardProductInfo(Guid guid)
        {
            return ProviderObject.GetPromotionRewardProductInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardProductInfo to be set</param>
        public static void SetPromotionRewardProductInfo(PromotionRewardProductInfo infoObj)
        {
            ProviderObject.SetPromotionRewardProductInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardProductInfo to be deleted</param>
        public static void DeletePromotionRewardProductInfo(PromotionRewardProductInfo infoObj)
        {
            ProviderObject.DeletePromotionRewardProductInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionRewardProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardProductInfo ID</param>
        public static void DeletePromotionRewardProductInfo(int id)
        {
            PromotionRewardProductInfo infoObj = GetPromotionRewardProductInfo(id);
            DeletePromotionRewardProductInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionRewardProductInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionRewardProductInfo> GetPromotionRewardProductsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionRewardProductInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionRewardProductInfo ID</param>        
        protected virtual PromotionRewardProductInfo GetPromotionRewardProductInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionRewardProductInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionRewardProductInfo GUID</param>
        protected virtual PromotionRewardProductInfo GetPromotionRewardProductInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionRewardProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardProductInfo to be set</param>        
        protected virtual void SetPromotionRewardProductInfoInternal(PromotionRewardProductInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionRewardProductInfo.
        /// </summary>
        /// <param name="infoObj">PromotionRewardProductInfo to be deleted</param>        
        protected virtual void DeletePromotionRewardProductInfoInternal(PromotionRewardProductInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}