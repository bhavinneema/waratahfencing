using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WF.Promotions.Classes;

[assembly: RegisterObjectType(typeof(PromotionStatusUpdateRequestInfo), PromotionStatusUpdateRequestInfo.OBJECT_TYPE)]

namespace WF.Promotions.Classes
{
    /// <summary>
    /// PromotionStatusUpdateRequestInfo data container class.
    /// </summary>
	[Serializable]
    public partial class PromotionStatusUpdateRequestInfo : AbstractInfo<PromotionStatusUpdateRequestInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "wf.promotionstatusupdaterequest";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PromotionStatusUpdateRequestInfoProvider), OBJECT_TYPE, "WF.PromotionStatusUpdateRequest", "PromotionStatusUpdateRequestID", "PromotionStatusUpdateRequestLastModified", "PromotionStatusUpdateRequestGuid", null, null, null, null, null, null)
        {
			ModuleName = "WF.Promotions",
			TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Promotion status update request ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionStatusUpdateRequestID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionStatusUpdateRequestID"), 0);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestID", value);
            }
        }


        /// <summary>
        /// Promotion status update request form class
        /// </summary>
        [DatabaseField]
        public virtual string PromotionStatusUpdateRequestFormClass
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionStatusUpdateRequestFormClass"), String.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestFormClass", value);
            }
        }


        /// <summary>
        /// Promotion status update request form item ID
        /// </summary>
        [DatabaseField]
        public virtual int PromotionStatusUpdateRequestFormItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PromotionStatusUpdateRequestFormItemID"), 0);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestFormItemID", value);
            }
        }


        /// <summary>
        /// Promotion status update request form item salesforce ID field
        /// </summary>
        [DatabaseField]
        public virtual string PromotionStatusUpdateRequestFormItemSalesforceIDField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionStatusUpdateRequestFormItemSalesforceIDField"), String.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestFormItemSalesforceIDField", value);
            }
        }


        /// <summary>
        /// Promotion status update request salesforce ID
        /// </summary>
        [DatabaseField]
        public virtual string PromotionStatusUpdateRequestSalesforceID
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionStatusUpdateRequestSalesforceID"), String.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestSalesforceID", value);
            }
        }


        /// <summary>
        /// Promotion status update request CRM status field
        /// </summary>
        [DatabaseField]
        public virtual string PromotionStatusUpdateRequestCRMStatusField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionStatusUpdateRequestCRMStatusField"), String.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestCRMStatusField", value);
            }
        }


        /// <summary>
        /// Promotion status update request kentico status field
        /// </summary>
        [DatabaseField]
        public virtual string PromotionStatusUpdateRequestKenticoStatusField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PromotionStatusUpdateRequestKenticoStatusField"), String.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestKenticoStatusField", value);
            }
        }


        /// <summary>
        /// Promotion status update request guid
        /// </summary>
        [DatabaseField]
        public virtual Guid PromotionStatusUpdateRequestGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("PromotionStatusUpdateRequestGuid"), Guid.Empty);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestGuid", value);
            }
        }


        /// <summary>
        /// Promotion status update request last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime PromotionStatusUpdateRequestLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("PromotionStatusUpdateRequestLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("PromotionStatusUpdateRequestLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PromotionStatusUpdateRequestInfoProvider.DeletePromotionStatusUpdateRequestInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PromotionStatusUpdateRequestInfoProvider.SetPromotionStatusUpdateRequestInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected PromotionStatusUpdateRequestInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PromotionStatusUpdateRequestInfo object.
        /// </summary>
        public PromotionStatusUpdateRequestInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PromotionStatusUpdateRequestInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PromotionStatusUpdateRequestInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}