using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WF.Promotions.Classes
{    
    /// <summary>
    /// Class providing PromotionStatusUpdateRequestInfo management.
    /// </summary>
    public partial class PromotionStatusUpdateRequestInfoProvider : AbstractInfoProvider<PromotionStatusUpdateRequestInfo, PromotionStatusUpdateRequestInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PromotionStatusUpdateRequestInfoProvider()
            : base(PromotionStatusUpdateRequestInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PromotionStatusUpdateRequestInfo objects.
        /// </summary>
        public static ObjectQuery<PromotionStatusUpdateRequestInfo> GetPromotionStatusUpdateRequests()
        {
            return ProviderObject.GetPromotionStatusUpdateRequestsInternal();
        }


        /// <summary>
        /// Returns PromotionStatusUpdateRequestInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionStatusUpdateRequestInfo ID</param>
        public static PromotionStatusUpdateRequestInfo GetPromotionStatusUpdateRequestInfo(int id)
        {
            return ProviderObject.GetPromotionStatusUpdateRequestInfoInternal(id);
        }


        /// <summary>
        /// Returns PromotionStatusUpdateRequestInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionStatusUpdateRequestInfo GUID</param>                
        public static PromotionStatusUpdateRequestInfo GetPromotionStatusUpdateRequestInfo(Guid guid)
        {
            return ProviderObject.GetPromotionStatusUpdateRequestInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionStatusUpdateRequestInfo.
        /// </summary>
        /// <param name="infoObj">PromotionStatusUpdateRequestInfo to be set</param>
        public static void SetPromotionStatusUpdateRequestInfo(PromotionStatusUpdateRequestInfo infoObj)
        {
            ProviderObject.SetPromotionStatusUpdateRequestInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionStatusUpdateRequestInfo.
        /// </summary>
        /// <param name="infoObj">PromotionStatusUpdateRequestInfo to be deleted</param>
        public static void DeletePromotionStatusUpdateRequestInfo(PromotionStatusUpdateRequestInfo infoObj)
        {
            ProviderObject.DeletePromotionStatusUpdateRequestInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PromotionStatusUpdateRequestInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionStatusUpdateRequestInfo ID</param>
        public static void DeletePromotionStatusUpdateRequestInfo(int id)
        {
            PromotionStatusUpdateRequestInfo infoObj = GetPromotionStatusUpdateRequestInfo(id);
            DeletePromotionStatusUpdateRequestInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PromotionStatusUpdateRequestInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PromotionStatusUpdateRequestInfo> GetPromotionStatusUpdateRequestsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PromotionStatusUpdateRequestInfo with specified ID.
        /// </summary>
        /// <param name="id">PromotionStatusUpdateRequestInfo ID</param>        
        protected virtual PromotionStatusUpdateRequestInfo GetPromotionStatusUpdateRequestInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns PromotionStatusUpdateRequestInfo with specified GUID.
        /// </summary>
        /// <param name="guid">PromotionStatusUpdateRequestInfo GUID</param>
        protected virtual PromotionStatusUpdateRequestInfo GetPromotionStatusUpdateRequestInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PromotionStatusUpdateRequestInfo.
        /// </summary>
        /// <param name="infoObj">PromotionStatusUpdateRequestInfo to be set</param>        
        protected virtual void SetPromotionStatusUpdateRequestInfoInternal(PromotionStatusUpdateRequestInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PromotionStatusUpdateRequestInfo.
        /// </summary>
        /// <param name="infoObj">PromotionStatusUpdateRequestInfo to be deleted</param>        
        protected virtual void DeletePromotionStatusUpdateRequestInfoInternal(PromotionStatusUpdateRequestInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}