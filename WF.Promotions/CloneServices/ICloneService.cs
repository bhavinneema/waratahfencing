﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;

namespace WF.Promotions.CloneServices
{
    public interface ICloneService
    {
        BaseInfo Clone(BaseInfo objectToClone,string newDisplayName, int newParent = 0);
    }
}
