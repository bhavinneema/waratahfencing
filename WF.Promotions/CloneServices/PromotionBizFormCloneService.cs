﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.OnlineForms;
using WF.Domain.Classes;
using WF.Promotions.Classes;

namespace WF.Promotions.CloneServices
{
    public class PromotionBizFormCloneService
    {
        private string PromoDisplayName { get; set; }
        private string PromoFormName { get; set; }
        private string PomoFormDisplayName { get; set; }

        public BaseInfo Clone(BaseInfo objectToClone, string newDisplayName)
        {
            PromoDisplayName = newDisplayName;
            PomoFormDisplayName = string.Format("Promotion Form {0}", newDisplayName);
            PromoFormName = string.Format("PromotionForm_{0}", ValidationHelper.GetCodeName(newDisplayName, string.Empty));
            var syncedObject = (BizFormInfo)objectToClone;
            var settings = new CloneSettings
            {
                KeepFieldsTranslated = true,
                CloneBase = syncedObject,
                CodeName = PromoFormName,
                DisplayName = PomoFormDisplayName,
                IncludeBindings = true,
                IncludeOtherBindings = true,
                IncludeChildren = true,
                IncludeMetafiles = true,
                IncludeSiteBindings = true,
                CloneToSiteID = syncedObject.Generalized.ObjectSiteID,
                BeforeCloneInsertCallback = BeforeInsert,
                CustomParameters = new Hashtable
                {
                    { "cms.class.tablename", PromoFormName },
                    { "cms.class.alternativeforms", true },
                    { "cms.class.data", false }
                }
            };

            var result = new CloneResult();
            var clone = syncedObject.Generalized.InsertAsClone(settings, result);
            return clone;
        }

        private void BeforeInsert(CloneSettings settings, BaseInfo clonetobeinserted)
        {
            if (clonetobeinserted is PromotionInfo promotion)
            {
                promotion.IsCopy = true;
                promotion.PromotionName = PromoDisplayName;
            }
        }
    }
}
