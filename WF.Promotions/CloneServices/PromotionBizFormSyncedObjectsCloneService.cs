﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.OnlineForms;
using WDM.SyncedObjects.Classes;
using WF.Domain.Classes;

namespace WF.Promotions.CloneServices
{
    public class PromotionBizFormSyncedObjectsCloneService : ICloneService
    {
        public BaseInfo Clone(BaseInfo objectToClone, string newDisplayName, int newParent = 0)
        {
            var syncedObject = (SyncedObjectInfo)objectToClone;
            var settings = new CloneSettings
            {
                KeepFieldsTranslated = true,
                CloneBase = syncedObject,
                CodeName = ValidationHelper.GetCodeName(newDisplayName),
                DisplayName = newDisplayName,
                IncludeBindings = true,
                IncludeOtherBindings = true,
                IncludeChildren = true,
                IncludeMetafiles = true,
                IncludeSiteBindings = true,
                CloneToSiteID = syncedObject.Generalized.ObjectSiteID,
                ExcludedChildTypes = { SyncLogInfo.OBJECT_TYPE }
            };

            if (newParent > 0)
            {
                settings.ParentID = newParent;
            }

            var result = new CloneResult();
            var clone = syncedObject.Generalized.InsertAsClone(settings, result);
            return clone;
        }
    }
}
