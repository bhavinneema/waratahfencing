﻿using System;
using CMS.DataEngine;
using CMS.Ecommerce;
using CMS.Helpers;
using CMS.OnlineForms;
using WDM.SyncedObjects.Classes;
using WF.Domain.Classes;
using WF.Promotions.Classes;

namespace WF.Promotions.CloneServices
{
    public class PromotionCloneService : ICloneService
    {
        public BaseInfo Clone(BaseInfo objectToClone, string newPromotionName, int newParent = 0)
        {
            var promotion = (PromotionInfo)objectToClone;

            var form = BizFormInfoProvider.GetBizFormInfo(promotion.PromotionBizFormID);

            var bizFormCloneService = new PromotionBizFormCloneService();
            var newForm = (BizFormInfo)bizFormCloneService.Clone(form, newPromotionName);

            var newPromotion = PromotionInfoProvider.GetPromotions()
                .WhereEquals("PromotionBizFormID", newForm.FormID)
                .FirstObject;

            CloneSyncedObjects(form, newForm, newPromotion.PromotionName);

            return newPromotion;
        }

        private void CloneSyncedObjects(BizFormInfo form, BizFormInfo newForm, string newPromotionName)
        {
            foreach (var syncedObject in SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereEquals("SyncedObjectKenticoClass", form.FormClassID))
            {
                var syncedObjectsCloneService = new PromotionBizFormSyncedObjectsCloneService();
                syncedObjectsCloneService.Clone(syncedObject, newPromotionName, newForm.FormClassID);
            }
        }
    }
}
