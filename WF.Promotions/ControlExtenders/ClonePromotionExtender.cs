﻿using CMS.Base.Web.UI;
using CMS.UIControls;

namespace WF.SystemModule.ControlExtenders
{
    public class ClonePromotionExtender : ControlExtender
    {
        /// <summary>
        /// Adds custom code that occurs during the initialization of the extended control.
        /// </summary>
        public override void OnInit()
        {
            var page = (CMSUIPage)Control.Page;
        }
    }
}