﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Base.Web.UI;
using CMS.UIControls;

namespace WF.SystemModule.ControlExtenders
{
    public class PromotionRewardProductBindingExtender : ControlExtender
    {
        public override void OnInit()
        {
            var grid = (CMSAbstractUIWebpart)Control;
            var editElem = (UniSelector)grid.FindControl("editElem");
            editElem.DialogGridName = "~/App_Data/CMSModules/WF.Promotions/UI/Grids/WF_PromotionRewardProduct/skudialog.xml";
            editElem.AllowLocalizedFilteringInSelectionDialog = false;
        }
    }

}
