﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Base;
using CMS.FormEngine;

namespace WF.SystemModule.DataClasses
{
    public class FormFieldProperty
    {
        public FormFieldPropertyEnum Property { get; set; }
        public string Value { get; set; }
        public bool IsMacro { get; set; }

        public FormFieldProperty(FormFieldPropertyEnum property, string value, bool isMacro)
        {
            Property = property;
            Value = value;
            IsMacro = isMacro;
        }
    }
}
