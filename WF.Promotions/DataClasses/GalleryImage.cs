﻿using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base;

namespace WF.SystemModule.DataClasses
{
    /// <summary>
    /// By implementing the IDataContainer interface on our Image class, our fields will be accessible to the macro engine.
    /// </summary>
    public class GalleryImage : IDataContainer
    {
        #region "Properties"

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        [JsonProperty("src")]
        public string Src { get; set; }

        /// <summary>
        /// Gets or sets the link URL.
        /// </summary>
        [JsonProperty("href")]
        public string Href { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        [JsonProperty("alt")]
        public string Alt { get; set; }

        #endregion


        #region "IDataContainer members"


        /// <summary>
        /// Gets a list of column names.
        /// </summary>
        [JsonIgnore]
        public List<string> ColumnNames
        {
            get
            {
                return new List<string>() { "Src", "Href", "Alt" };
            }
        }


        /// <summary>
        /// Returns true if the class contains the specified column.
        /// </summary>
        /// <param name="columnName"></param>
        public bool ContainsColumn(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "src":
                case "href":
                case "alt":
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Gets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object GetValue(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "src":
                    return Src;

                case "href":
                    return Href;

                case "alt":
                    return Alt;

                default:
                    return null;
            }
        }


        // <summary>
        /// Sets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">New value</param>
        public bool SetValue(string columnName, object value)
        {
            switch (columnName.ToLower())
            {
                case "src":
                    Src = (string)value;
                    return true;

                case "href":
                    Href = (string)value;
                    return true;

                case "alt":
                    Alt = (string)value;
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Returns a boolean value indicating whether the class contains the specified column.
        /// Passes on the specified column's value through the second parameter.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">Return value</param>
        public bool TryGetValue(string columnName, out object value)
        {
            switch (columnName.ToLower())
            {
                case "src":
                    value = Src;
                    return true;

                case "href":
                    value = Href;
                    return true;

                case "alt":
                    value = Alt;
                    return true;

                default:
                    value = null;
                    return false;
            }
        }


        /// <summary>
        /// Gets or sets the value of the column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object this[string columnName]
        {
            get
            {
                return GetValue(columnName);
            }

            set
            {
                SetValue(columnName, value);
            }
        }

        #endregion
    }
}