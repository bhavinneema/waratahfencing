﻿namespace WF.SystemModule.DataClasses
{
    public class PromotionsFormItem
    {
        public int PromotionsFormItemID { get; set; }
        public int Category { get; set; }
        public int Product { get; set; }
        public int Quantity { get; set; }
    }
}
