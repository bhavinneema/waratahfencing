﻿using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base;
using WF.SystemModule.DataClasses;

namespace WF.Promotions.DataClasses
{
    /// <summary>
    /// By implementing the IDataContainer interface on our PurchaseInfo class, our fields will be accessible to the macro engine.
    /// </summary>
    public class PurchaseInfo : IDataContainer
    {
        #region "Properties"

        [JsonProperty("promotionID")]
        public int PromotionID { get; set; }

        [JsonProperty("taxName")]
        public string TaxName { get; set; }

        [JsonProperty("taxPercentage")]
        public double TaxPercentage { get; set; }

        [JsonProperty("groups")]
        public List<PurchaseInfoGroup> Groups { get; set; }

        [JsonProperty("totalClaimAmount")]
        public double TotalClaimAmount { get; set; }

        [JsonProperty("extraDiscountPercent")]
        public double ExtraDiscountPercent { get; set; }

        [JsonProperty("extraDiscountAmount")]
        public double ExtraDiscountAmount { get; set; }

        [JsonProperty("totalCashback")]
        public double TotalCashback { get; set; }



        public bool Deserialized { get; set; }

        public string DeserializationError { get; set; }

        #endregion


        #region "IDataContainer members"

        /// <summary>
        /// Gets a list of column names.
        /// </summary>
        [JsonIgnore]
        public List<string> ColumnNames
        {
            get
            {
                return new List<string>() { "PromotionID", "TaxName", "TaxPercentage", "Groups", "TotalClaimAmount", "ExtraDiscountPercent", "ExtraDiscountAmount", "TotalCashback" };
            }
        }


        /// <summary>
        /// Returns true if the class contains the specified column.
        /// </summary>
        /// <param name="columnName"></param>
        public bool ContainsColumn(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "promotionid":
                case "taxname":
                case "taxpercentage":
                case "groups":
                case "totalclaimamount":
                case "extradiscountpercent":
                case "extradiscountamount":
                case "totalcashback":
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Gets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object GetValue(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "promotionid":
                    return PromotionID;

                case "taxname":
                    return TaxName;

                case "taxpercentage":
                    return TaxPercentage;

                case "groups":
                    return Groups;

                case "totalclaimamount":
                    return TotalClaimAmount;

                case "extradiscountpercent":
                    return ExtraDiscountPercent;

                case "extradiscountamount":
                    return ExtraDiscountAmount;

                case "totalcashback":
                    return TotalCashback;

                default:
                    return null;
            }
        }


        // <summary>
        /// Sets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">New value</param>
        public bool SetValue(string columnName, object value)
        {
            switch (columnName.ToLower())
            {
                case "promotionid":
                    PromotionID = (int)value;
                    return true;

                case "taxname":
                    TaxName = (string)value;
                    return true;

                case "taxpercentage":
                    TaxPercentage = (double)value;
                    return true;

                case "groups":
                    Groups = (List<PurchaseInfoGroup>)value;
                    return true;

                case "totalclaimamount":
                    TotalClaimAmount = (double)value;
                    return true;

                case "extradiscountpercent":
                    ExtraDiscountPercent = (double)value;
                    return true;

                case "extradiscountamount":
                    ExtraDiscountAmount = (double)value;
                    return true;

                case "totalcashback":
                    TotalCashback = (double)value;
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Returns a boolean value indicating whether the class contains the specified column.
        /// Passes on the specified column's value through the second parameter.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">Return value</param>
        public bool TryGetValue(string columnName, out object value)
        {
            switch (columnName.ToLower())
            {
                case "promotionid":
                    value = PromotionID;
                    return true;

                case "taxname":
                    value = TaxName;
                    return true;

                case "taxpercentage":
                    value = TaxPercentage;
                    return true;

                case "groups":
                    value = Groups;
                    return true;

                case "totalclaimamount":
                    value = TotalClaimAmount;
                    return true;

                case "extradiscountpercent":
                    value = ExtraDiscountPercent;
                    return true;

                case "extradiscountamount":
                    value = ExtraDiscountAmount;
                    return true;

                case "totalcashback":
                    value = TotalCashback;
                    return true;

                default:
                    value = null;
                    return false;
            }
        }


        /// <summary>
        /// Gets or sets the value of the column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object this[string columnName]
        {
            get
            {
                return GetValue(columnName);
            }

            set
            {
                SetValue(columnName, value);
            }
        }

        #endregion
    }
}