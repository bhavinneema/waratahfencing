﻿using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base;

namespace WF.SystemModule.DataClasses
{
    /// <summary>
    /// By implementing the IDataContainer interface on our PurchaseInfoGroup class, our fields will be accessible to the macro engine.
    /// </summary>
    public class PurchaseInfoGroup : IDataContainer
    {
        #region "Properties"

        [JsonProperty("groupID")]
        public int GroupID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("products")]
        public List<PurchaseInfoProduct> Products { get; set; }

        [JsonProperty("cashback")]
        public double Cashback { get; set; }

        #endregion


        #region "IDataContainer members"

        /// <summary>
        /// Gets a list of column names.
        /// </summary>
        [JsonIgnore]
        public List<string> ColumnNames
        {
            get
            {
                return new List<string>() { "GroupID", "Name", "Products", "Cashback" };
            }
        }


        /// <summary>
        /// Returns true if the class contains the specified column.
        /// </summary>
        /// <param name="columnName"></param>
        public bool ContainsColumn(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "groupid":
                case "name":
                case "products":
                case "cashback":
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Gets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object GetValue(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "groupid":
                    return GroupID;

                case "name":
                    return Name;

                case "products":
                    return Products;

                case "cashback":
                    return Cashback;

                default:
                    return null;
            }
        }


        // <summary>
        /// Sets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">New value</param>
        public bool SetValue(string columnName, object value)
        {
            switch (columnName.ToLower())
            {
                case "groupid":
                    GroupID = (int)value;
                    return true;

                case "name":
                    Name = (string)value;
                    return true;

                case "products":
                    Products = (List<PurchaseInfoProduct>)value;
                    return true;

                case "cashback":
                    Cashback = (double)value;
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Returns a boolean value indicating whether the class contains the specified column.
        /// Passes on the specified column's value through the second parameter.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">Return value</param>
        public bool TryGetValue(string columnName, out object value)
        {
            switch (columnName.ToLower())
            {
                case "groupid":
                    value = GroupID;
                    return true;

                case "name":
                    value = Name;
                    return true;

                case "products":
                    value = Products;
                    return true;

                case "cashback":
                    value = Cashback;
                    return true;

                default:
                    value = null;
                    return false;
            }
        }


        /// <summary>
        /// Gets or sets the value of the column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object this[string columnName]
        {
            get
            {
                return GetValue(columnName);
            }

            set
            {
                SetValue(columnName, value);
            }
        }

        #endregion
    }
}