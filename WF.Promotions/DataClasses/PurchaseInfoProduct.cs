﻿using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base;

namespace WF.SystemModule.DataClasses
{
    /// <summary>
    /// By implementing the IDataContainer interface on our PurchaseInfoProduct class, our fields will be accessible to the macro engine.
    /// </summary>
    public class PurchaseInfoProduct : IDataContainer
    {
        #region "Properties"

        [JsonProperty("skuid")]
        public int SKUID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("variants")]
        public List<PurchaseInfoVariant> Variants { get; set; }

        #endregion


        #region "IDataContainer members"

        /// <summary>
        /// Gets a list of column names.
        /// </summary>
        [JsonIgnore]
        public List<string> ColumnNames
        {
            get
            {
                return new List<string>() { "SKUID", "Name", "Variants" };
            }
        }


        /// <summary>
        /// Returns true if the class contains the specified column.
        /// </summary>
        /// <param name="columnName"></param>
        public bool ContainsColumn(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                case "name":
                case "variants":
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Gets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object GetValue(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    return SKUID;

                case "name":
                    return Name;

                case "variants":
                    return Variants;

                default:
                    return null;
            }
        }


        // <summary>
        /// Sets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">New value</param>
        public bool SetValue(string columnName, object value)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    SKUID = (int)value;
                    return true;

                case "name":
                    Name = (string)value;
                    return true;

                case "variants":
                    Variants = (List<PurchaseInfoVariant>)value;
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Returns a boolean value indicating whether the class contains the specified column.
        /// Passes on the specified column's value through the second parameter.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">Return value</param>
        public bool TryGetValue(string columnName, out object value)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    value = SKUID;
                    return true;

                case "name":
                    value = Name;
                    return true;

                case "variants":
                    value = Variants;
                    return true;

                default:
                    value = null;
                    return false;
            }
        }


        /// <summary>
        /// Gets or sets the value of the column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object this[string columnName]
        {
            get
            {
                return GetValue(columnName);
            }

            set
            {
                SetValue(columnName, value);
            }
        }

        #endregion
    }
}