﻿using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.Base;

namespace WF.SystemModule.DataClasses
{
    /// <summary>
    /// By implementing the IDataContainer interface on our PurchaseInfoVariant class, our fields will be accessible to the macro engine.
    /// </summary>
    public class PurchaseInfoVariant : IDataContainer
    {
        #region "Properties"

        [JsonProperty("skuid")]
        public int SKUID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("unitPrice")]
        public double UnitPrice { get; set; }

        [JsonProperty("tax")]
        public double Tax { get; set; }

        [JsonProperty("qty")]
        public int Qty { get; set; }

        [JsonProperty("salesforceID")]
        public string SalesforceID { get; set; }

        #endregion


        #region "IDataContainer members"

        /// <summary>
        /// Gets a list of column names.
        /// </summary>
        [JsonIgnore]
        public List<string> ColumnNames
        {
            get
            {
                return new List<string>() { "SKUID", "Name", "UnitPrice", "Tax", "Qty","SalesforceID" };
            }
        }


        /// <summary>
        /// Returns true if the class contains the specified column.
        /// </summary>
        /// <param name="columnName"></param>
        public bool ContainsColumn(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                case "name":
                case "unitprice":
                case "tax":
                case "qty":
                case "salesforceid":
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Gets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object GetValue(string columnName)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    return SKUID;

                case "name":
                    return Name;

                case "unitprice":
                    return UnitPrice;

                case "tax":
                    return Tax;

                case "qty":
                    return Qty;

                case "salesforceid":
                    return SalesforceID;

                default:
                    return null;
            }
        }


        // <summary>
        /// Sets the value of the specified column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">New value</param>
        public bool SetValue(string columnName, object value)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    SKUID = (int)value;
                    return true;

                case "name":
                    Name = (string)value;
                    return true;

                case "unitprice":
                    UnitPrice = (double)value;
                    return true;

                case "tax":
                    Tax = (double)value;
                    return true;

                case "qty":
                    Qty = (int)value;
                    return true;

                case "salesforceid":
                    SalesforceID = (string)value;
                    return true;

                default:
                    return false;
            }
        }


        /// <summary>
        /// Returns a boolean value indicating whether the class contains the specified column.
        /// Passes on the specified column's value through the second parameter.
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="value">Return value</param>
        public bool TryGetValue(string columnName, out object value)
        {
            switch (columnName.ToLower())
            {
                case "skuid":
                    value = SKUID;
                    return true;

                case "name":
                    value = Name;
                    return true;

                case "unitprice":
                    value = UnitPrice;
                    return true;

                case "tax":
                    value = Tax;
                    return true;

                case "qty":
                    value = Qty;
                    return true;

                case "salesforceid":
                    value = SalesforceID;
                    return true;

                default:
                    value = null;
                    return false;
            }
        }


        /// <summary>
        /// Gets or sets the value of the column.
        /// </summary>
        /// <param name="columnName">Column name</param>
        public object this[string columnName]
        {
            get
            {
                return GetValue(columnName);
            }

            set
            {
                SetValue(columnName, value);
            }
        }

        #endregion
    }
}