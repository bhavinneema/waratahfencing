﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CMS;
using CMS.Base;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.OnlineForms;
using CMS.SiteProvider;
using WF.Domain.Classes;
using WF.Promotions.Classes;
using WF.Promotions.Helpers;
using WF.SystemModule.DataClasses;
using WF.SystemModule.Extensions;


namespace WF.SystemModule.Events
{
    public static class PromotionEvents
    {

        #region "Event Methods"

        public static void Promotion_InsertAfter(object sender, ObjectEventArgs e)
        {
            var promotion = e.Object as PromotionInfo;
            if (promotion != null && !promotion.IsCopy && promotion.PromotionBizFormID == 0)
            {
                var formDisplayName = string.Format("Promotion Form {0}", promotion.PromotionName);
                var formName = string.Format("PromotionForm_{0}", ValidationHelper.GetCodeName(promotion.PromotionName));

                var promotionForm = new BizFormInfo
                {
                    FormDisplayName = formDisplayName,
                    FormName = formName,
                    FormSiteID = promotion.PromotionSiteID,
                    FormEmailAttachUploadedDocs = true,
                    FormItems = 0,
                    FormClearAfterSave = false,
                    FormLogActivity = true
                };

                // Ensure the code name
                promotionForm.Generalized.EnsureCodeName();

                // Table name is combined from prefix ('BizForm_<sitename>_') and custom table name
                string safeFormName = ValidationHelper.GetIdentifier(promotionForm.FormName);
                promotionForm.FormName = safeFormName;

                string className = "PromotionForm." + safeFormName;

                // Generate the table name
                if (String.IsNullOrEmpty(formName) || (formName == InfoHelper.CODENAME_AUTOMATIC))
                {
                    formName = safeFormName;
                }

                TableManager tm = new TableManager(null);

                // TableName wont be longer than 60 letters and will be unique
                if (formName.Length > 60)
                {
                    string tmpTableName = formName.Substring(0, 59);
                    int x = 1;
                    do
                    {
                        formName = tmpTableName + x;
                        x++;
                    } while (tm.TableExists(formName));
                }

                // TableName should be unique
                if (tm.TableExists(formName))
                {
                    //ShowError(string.Format(GetString("bizform_edit.errortableexists"), tableName)); 
                    return;
                }

                // If first letter of safeFormName is digit, add "PK" to beginning
                string primaryKey = BizFormInfoProvider.GenerateFormPrimaryKeyName(promotionForm.FormName);
                try
                {
                    // Create new table in DB
                    tm.CreateTable(formName, primaryKey);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("BIZFORM_NEW", EventType.ERROR, ex);
                    //ShowError(string.Format(GetString("bizform_edit.createtableerror"), tableName));
                    return;
                }

                // Change table owner
                try
                {
                    string owner = SqlHelper.GetDBSchema(SiteContext.CurrentSiteName);
                    if (!String.IsNullOrEmpty(owner) && (owner.ToLowerCSafe() != "dbo"))
                    {
                        tm.ChangeDBObjectOwner(formName, owner);
                        formName = owner + "." + formName;
                    }
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("BIZFORM_NEW", EventType.ERROR, ex);
                }

                // Create the BizForm class
                var dci = BizFormInfoProvider.CreateBizFormDataClass(className, formDisplayName, formName, primaryKey);

                try
                {
                    DataClassInfoProvider.SetDataClassInfo(dci);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("BIZFORM_NEW", EventType.ERROR, ex);
                    //ShowError(ex.Message);

                    CleanUpOnError(formName, tm, dci);
                    return;
                }

                // Create new bizform
                promotionForm.FormClassID = dci.ClassID;
                try
                {
                    promotionForm.SetValue("FormIsPromotionForm", true);
                    BizFormInfoProvider.SetBizFormInfo(promotionForm);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("BIZFORM_NEW", EventType.ERROR, ex);
                    //ShowError(ex.Message);

                    CleanUpOnError(formName, tm, dci, promotionForm);
                    return;
                }

                promotion.PromotionBizFormID = promotionForm.FormID;

                PromotionInfoProvider.SetPromotionInfo(promotion);

                var fieldHtmlTemplate =
                    "<div class=\"{1}\"><div class=\"cashback-form-label\">$$label:{0}$$</div><div class=\"cashback-form-control\">$$input:{0}$$</div><div class=\"cashback-form-validation\">$$validation:{0}$$</div></div>";
                var categoryHeadingHtmlTemplate = "<h4>{0}</h4>";
                var fieldHtmlList = new List<string>();

                var publicFacingAlternativeForm = new Dictionary<string, List<string>>
                {
                    {"Store Details", new List<string> {"StoreState", "StoreTownSuburb", "StoreName"}},
                    {"Invoice Details", new List<string> {"InvoiceDate", "InvoiceNumber", "InvoiceUpload"}},
                    {"Purchase Info", new List<string> {"PurchaseInfoJson"}},
                    {"Payee Details", new List<string> {"PayeeFirstName", "PayeeLastName", "PayeeStreetAddress", "PayeeSuburb", "PayeeState", "PayeePostcode", "PayeeEmail", "PayeePhone", "Business_Company_Name", "PayeeCategory", "Gender", "Source", "AdviseOnFutureOffers", "AgreedToTerms" }},

                };

                PromotionBizFormAlternativeFormHelper.CreatePromotionAlternativeForm(dci, "Public Facing", publicFacingAlternativeForm, fieldHtmlTemplate, categoryHeadingHtmlTemplate);
            }

            PromotionInfoExtensions.RegenerateViews();
        }

        public static void BizForm_InsertAfter(object sender, ObjectEventArgs e)
        {
            var promotionForm = e.Object as BizFormInfo;

            if (promotionForm != null)
            {
                DataClassInfo dci = DataClassInfoProvider.GetDataClassInfo(promotionForm.FormClassID);
                if (dci != null && promotionForm.GetBooleanValue("FormIsPromotionForm", false))
                {

                    if (promotionForm.Form != null)
                    {
                        var tm = new TableManager(null);

                        PromotionBizFormHelper.AddFieldsToForm(ref dci, ref promotionForm, ref tm);

                        dci.ClassXmlSchema = tm.GetXmlSchema(dci.ClassTableName);
                        dci.ClassFormDefinition = promotionForm.Form.GetXmlDefinition();

                        // Update DataClassInfo object
                        DataClassInfoProvider.SetDataClassInfo(dci);

                        // Update inherited classes with new field
                        FormHelper.UpdateInheritedClasses(dci);

                        // Generate default queries
                        //CMS.DataEngine.SqlGenerator.GenerateDefaultQueries(dci, true, true);
                    }
                }

            }
        }

        public static void BizFormItem_InsertBefore(object sender, BizFormItemEventArgs e)
        {
            var entry = e.Item;

            if (entry == null || !entry.BizFormInfo.GetBooleanValue("FormIsPromotionForm", false))
                return;
        }

        public static void BizFormItem_UpdateAfter(object sender, BizFormItemEventArgs e)
        {
            var entry = e.Item;

            if (entry == null || !entry.BizFormInfo.GetBooleanValue("FormIsPromotionForm", false) ||
                !entry.GetBooleanValue("FormComplete", false) || entry.GetBooleanValue("InitialSent", false))
                return;

            var promotion = PromotionInfoProvider.GetPromotions()
                .WhereEquals("PromotionBizFormID", entry.BizFormInfo.FormID)
                .FirstObject;

            if (promotion != null)
            {
                // Notify administrator
                promotion.SendEmail(entry, "Promotion.NotifyAdministrator",
                    SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CMSAdminEmailAddress"));
                // Notify user
                promotion.SendEmail(entry, "Promotion.NotifyUser", entry.GetStringValue("EmailAddress", string.Empty));
                // Notify staff
                if (entry.GetStringValue("StaffEmail", string.Empty).Length > 0)
                    promotion.SendEmail(entry, "Promotion.NotifyStaff",
                        entry.GetStringValue("StaffEmail", string.Empty));

                entry.SetValue("InitialSent", true);
                BizFormItemProvider.SetItem(entry);
            }

        }

        #endregion

        #region "Private Methods"

        private static void CleanUpOnError(string tableName, TableManager tm, DataClassInfo dci = null, BizFormInfo bizForm = null)
        {
            if (tm.TableExists(tableName))
            {
                tm.DropTable(tableName);
            }
            if ((bizForm != null) && (bizForm.FormID > 0))
            {
                BizFormInfoProvider.DeleteBizFormInfo(bizForm);
            }
            if ((dci != null) && (dci.ClassID > 0))
            {
                DataClassInfoProvider.DeleteDataClassInfo(dci);
            }
        }

        #endregion

    }
}