﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.FormEngine;
using CMS.OnlineForms;
using WF.Promotions.Helpers;
using WF.SystemModule.DataClasses;

namespace WF.Promotions.Extensions
{
    public static class BizFormInfoExtensions
    {
        public static void AddCategory(this BizFormInfo promotionForm, string categoryName)
        {
           PromotionBizFormHelper.AddCategory(ref promotionForm, categoryName);
        }

        public static void AddField(this BizFormInfo promotionForm, ref DataClassInfo dci, ref TableManager tm, FormFieldInfo ffi, params FormFieldProperty[] properties)
        {
            PromotionBizFormHelper.AddField(ref promotionForm, ref dci, ref tm, ffi, properties);
        }
    }
}
