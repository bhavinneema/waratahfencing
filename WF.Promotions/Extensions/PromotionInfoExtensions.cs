﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.MacroEngine;
using CMS.OnlineForms;
using CMS.SiteProvider;

using WF;
using WF.Domain.Classes;
using WF.Promotions.Classes;
using EmailMessage = CMS.EmailEngine.EmailMessage;

namespace WF.SystemModule.Extensions
{
    public static class PromotionInfoExtensions
    {
        public static void SendEmail(this PromotionInfo promotion, BizFormItem promotionEntry, string templateName, string recipient)
        {
            if (promotionEntry.GetStringValue("UserCulture", string.Empty).Length > 0)
                templateName += string.Format(".{0}", promotionEntry.GetStringValue("UserCulture", string.Empty));
            SendEmailUsingTemplate(templateName, recipient, ResolveMacros(promotion, promotionEntry));
        }

        private static void SendEmailUsingTemplate(string templateName, string recipient, MacroResolver resolver)
        {
            // Get the email template
            var template = EmailTemplateProvider.GetEmailTemplate(templateName, SiteContext.CurrentSiteName);

            if (template != null)
            {
                // Email message
                var emailMessage = new EmailMessage
                {
                    EmailFormat = EmailFormatEnum.Default,
                    Recipients = recipient,
                    From = EmailHelper.GetSender(template, SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CMSNoreplyEmailAddress")),
                    CcRecipients = template.TemplateCc,
                    BccRecipients = template.TemplateBcc,
                    Subject = resolver.ResolveMacros(template.TemplateSubject),
                    PlainTextBody = resolver.ResolveMacros(template.TemplatePlainText)
                };

                emailMessage.Body = resolver.ResolveMacros(template.TemplateText, new MacroSettings { EncodeResolvedValues = true });

                try
                {
                    // Send the e-mail immediately
                    EmailSender.SendEmail(SiteContext.CurrentSiteName, emailMessage, true);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("PromotionHelper", "SendEmailUsingTemplate", ex);

                    throw;
                }
            }
        }
        private static MacroResolver ResolveMacros(PromotionInfo promotion, BizFormItem promotionEntry)
        {
            // create a new instnace of the resolver
            var resolver = MacroResolver.GetInstance();
            // set all the available values within the template
            resolver.SetNamedSourceData("PromotionName", promotion.GetStringValue("PromotionName", string.Empty));
            resolver.SetNamedSourceData("PromotionType", promotion.GetStringValue("PromotionType", string.Empty));

            resolver.SetNamedSourceData("FormInserted", promotionEntry.FormInserted);
            resolver.SetNamedSourceData("FormUpdated", promotionEntry.FormUpdated);
            resolver.SetNamedSourceData("StoreState", promotionEntry.GetStringValue("StoreState", string.Empty));
            resolver.SetNamedSourceData("StoreSuburb", promotionEntry.GetStringValue("StoreSuburb", string.Empty));
            resolver.SetNamedSourceData("StoreName", promotionEntry.GetStringValue("StoreName", string.Empty));
            resolver.SetNamedSourceData("InvoiceDate", promotionEntry.GetStringValue("InvoiceDate", string.Empty));
            resolver.SetNamedSourceData("InvoiceNumber", promotionEntry.GetStringValue("InvoiceNumber", string.Empty));
            resolver.SetNamedSourceData("PromotionEntryReceipt", promotionEntry.GetStringValue("PromotionEntryReceipt", string.Empty));
            resolver.SetNamedSourceData("CustomData", promotionEntry.GetStringValue("CustomData", string.Empty));
            resolver.SetNamedSourceData("Title", promotionEntry.GetStringValue("Title", string.Empty));
            resolver.SetNamedSourceData("FirstName", promotionEntry.GetStringValue("FirstName", string.Empty));
            resolver.SetNamedSourceData("LastName", promotionEntry.GetStringValue("LastName", string.Empty));
            resolver.SetNamedSourceData("BusinessField", promotionEntry.GetStringValue("BusinessField", string.Empty));
            resolver.SetNamedSourceData("PayeeName", promotionEntry.GetStringValue("PayeeName", string.Empty));
            resolver.SetNamedSourceData("Address", promotionEntry.GetStringValue("Address", string.Empty));
            resolver.SetNamedSourceData("Suburb", promotionEntry.GetStringValue("Suburb", string.Empty));
            resolver.SetNamedSourceData("State", promotionEntry.GetStringValue("State", string.Empty));
            resolver.SetNamedSourceData("Postcode", promotionEntry.GetStringValue("Postcode", string.Empty));
            resolver.SetNamedSourceData("EmailAddress", promotionEntry.GetStringValue("EmailAddress", string.Empty));
            resolver.SetNamedSourceData("HTMLEmail", promotionEntry.GetStringValue("HTMLEmail", string.Empty));
            resolver.SetNamedSourceData("ContactPhoneNumber", promotionEntry.GetStringValue("ContactPhoneNumber", string.Empty));
            resolver.SetNamedSourceData("Category", promotionEntry.GetStringValue("Category", string.Empty));
            resolver.SetNamedSourceData("JobFunction", promotionEntry.GetStringValue("JobFunction", string.Empty));
            resolver.SetNamedSourceData("Age", promotionEntry.GetStringValue("Age", string.Empty));
            resolver.SetNamedSourceData("Gender", promotionEntry.GetStringValue("Gender", string.Empty));
            resolver.SetNamedSourceData("AdviseOnFutureOffers", promotionEntry.GetStringValue("AdviseOnFutureOffers", string.Empty));
            resolver.SetNamedSourceData("AgreedToTerms", promotionEntry.GetStringValue("AgreedToTerms", string.Empty));
            resolver.SetNamedSourceData("PromotionEntryQualified", promotionEntry.GetStringValue("PromotionEntryQualified", string.Empty));
            resolver.SetNamedSourceData("PromotionEntryFinalised", promotionEntry.GetStringValue("PromotionEntryFinalised", string.Empty));
            resolver.SetNamedSourceData("UserID", promotionEntry.GetStringValue("UserID", string.Empty));

            var promotionEntryInfo = PromotionEntryInfoProvider.GetPromotionEntries()
                .WhereEquals("PromotionEntryFormID", promotion.PromotionBizFormID)
                .WhereEquals("PromotionEntryFormItemID", promotionEntry.ItemID)
                .FirstObject;
            if (promotionEntryInfo != null)
                resolver.SetNamedSourceData("ClaimID", promotionEntryInfo.GetStringValue("PromotionEntryClaimID", string.Empty));

            return resolver;
        }

        public static void RegenerateViews()
        {
            const string qryFormat = "SELECT\r\nPE.*,\r\nPF.*\r\nFROM\r\nWF_PromotionEntry PE\r\nJOIN CMS_Form F ON F.FormID = PE.PromotionEntryFormID\r\nJOIN CMS_Class C ON C.ClassID = F.FormClassID\r\nCROSS APPLY (\r\n{0}\r\n) PF";
            const string pfQryFormat = "SELECT FormID, {1}ID ItemID, FirstName, LastName FROM {1} CROSS APPLY (SELECT * FROM CMS_Form WHERE FormName = '{1}') F WHERE {0} = PE.PromotionEntryFormItemID AND FormID = PE.PromotionEntryFormID";

            var promotions = PromotionInfoProvider.GetPromotions();
            var pfQueries = new List<string>();

            foreach (var promotion in promotions)
            {
                var bfi = BizFormInfoProvider.GetBizFormInfo(promotion.PromotionBizFormID);
                if (bfi == null)
                    continue;
                var dci = DataClassInfoProvider.GetDataClassInfo(bfi.FormClassID);
                if (dci == null)
                    continue;
                var tableName = dci.ClassTableName ?? string.Empty;
                var pkName = bfi.Form.GetColumnNames().FirstOrDefault(x => x.StartsWith("PromotionForm_") && x.EndsWith("ID")) ?? string.Empty;
                var valid = bfi.Form.GetColumnNames().Contains("FirstName") ||
                            bfi.Form.GetColumnNames().Contains("LastName");
                if (tableName == string.Empty || pkName == string.Empty || !valid)
                    continue;
                pfQueries.Add(string.Format(pfQryFormat, pkName, tableName));
            }
            var resultQry = string.Format(qryFormat, string.Join("\r\nUNION\r\n", pfQueries));

            var alterViewQry = string.Format("ALTER VIEW View_Custom_PromotionEntries_Joined\r\nAS\r\n{0}", resultQry);
            var result = new DataQuery() { CustomQueryText = alterViewQry }.Execute();
        }
    }
}
