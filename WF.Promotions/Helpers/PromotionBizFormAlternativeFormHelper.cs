﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.FormEngine;
using CMS.Helpers;

namespace WF.Promotions.Helpers
{
    public class PromotionBizFormAlternativeFormHelper
    {
        public static void CreatePromotionAlternativeForm(DataClassInfo dci, string alternativeFormName,
               Dictionary<string, List<string>> showFieldsList, string fieldHtmlTemplate,
               string categoryHeadingHtmlTemplate)
        {
            var afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(dci.ClassName + "." + alternativeFormName) ??
                      new AlternativeFormInfo
                      {
                          FormClassID = dci.ClassID,
                          FormName = ValidationHelper.GetCodeName(alternativeFormName),
                          FormDisplayName = alternativeFormName,
                          FormIsCustom = false,
                          FormLayoutType = LayoutTypeEnum.Html,
                          FormHideNewParentFields = true,
                      };

            var newDef = FormHelper.MergeFormDefinitions(dci.ClassFormDefinition, afi.FormDefinition);
            var form = new FormInfo(newDef);

            // Build layout
            var formLayout = new List<string>();
            foreach (var kvp in showFieldsList)
            {
                formLayout.Add(string.Format(categoryHeadingHtmlTemplate, kvp.Key));
                foreach (var field in form.GetFields(true, true))
                {
                    var cssClass = "form-table-group form-control-group";
                    switch (ValidationHelper.GetString(field.Settings["controlname"], string.Empty).ToLower())
                    {
                        case "uploadcontrol":
                            cssClass = "form-table-group form-control-group invoice-upload";
                            break;
                        case "checkboxcontrol":
                        case "multiplechoicecontrol":
                            cssClass = "form-control-group__checkbox form-control-group";
                            break;
                    }

                    var updatedField = (FormFieldInfo)field.Clone();
                    var belongsToGroup = kvp.Value.Contains(field.Name);
                    var showField = showFieldsList.SelectMany(x => x.Value).ToList().Contains(field.Name);
                    updatedField.Visible = showField;
                    updatedField.PublicField = showField;
                    updatedField.Enabled = showField;
                    updatedField.AllowEmpty = !showField;
                    updatedField.ReferenceType = showField ? ObjectDependencyEnum.Required : ObjectDependencyEnum.NotRequired;
                    form.UpdateFormField(field.Name, updatedField);
                    if (belongsToGroup)
                        formLayout.Add(string.Format(fieldHtmlTemplate, field.Name, cssClass));
                }
            }

            afi.FormLayout = string.Format("<div class=\"cashback-form-wrapper\">{0}</div>", formLayout.Join(""));
            afi.FormDefinition =
                FormHelper.GetFormDefinitionDifference(dci.ClassFormDefinition, form.GetXmlDefinition(), true);
            AlternativeFormInfoProvider.SetAlternativeFormInfo(afi);
        }
    }
}
