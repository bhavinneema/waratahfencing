﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataEngine;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.OnlineForms;
using CMS.SiteProvider;
using WF.Promotions.Extensions;
using WF.SystemModule.DataClasses;
using WF.SystemModule.Extensions;

namespace WF.Promotions.Helpers
{
    public class PromotionBizFormHelper
    {
        public static void AddCategory(ref BizFormInfo promotionForm, string categoryName)
        {
            if (categoryName != null && !promotionForm.Form.GetCategoryNames().Contains(categoryName))
            {
                promotionForm.Form.AddFormCategory(new FormCategoryInfo
                {
                    CategoryName = categoryName
                });
            }
        }

        public static void AddField(ref BizFormInfo promotionForm, ref DataClassInfo dci, ref TableManager tm,
            FormFieldInfo ffi, params FormFieldProperty[] properties)
        {
            var fieldName = ffi.Name;
            var sqlColumnType = GetSqlColumnType(ffi);
            var allowEmpty = ffi.AllowEmpty;
            var defaultValue = ffi.DefaultValue;
            if (!promotionForm.Form.FieldExists(fieldName))
            {
                foreach (var property in properties)
                {
                    ffi.SetPropertyValue(property.Property, property.Value, property.IsMacro);
                }

                // Define field and add to form
                promotionForm.Form.AddFormItem(ffi);
                tm.AddTableColumn(dci.ClassTableName, fieldName, sqlColumnType, allowEmpty, defaultValue);
            }
        }

        private static string GetSqlColumnType(FormFieldInfo ffi)
        {
            switch (ffi.DataType)
            {
                case "text":
                    if (ffi.Size > 0)
                    {
                        return $"nvarchar({ffi.Size})";
                    }
                    else
                    {
                        return $"nvarchar(max)";
                    }
                case "longtext":
                    return $"nvarchar(max)";
                case "date":
                    return $"datetime2(0)";
                case "datetime":
                    return $"datetime2(7)";
                case "integer":
                    return $"int";
                case "double":
                    return $"float";
                case "boolean":
                    return $"bit";
            }

            return null;
        }

        public static void AddFieldsToForm(ref DataClassInfo dci, ref BizFormInfo promotionForm, ref TableManager tm)
        {
            var x = SettingsKeyInfoProvider.GetValue("DefaultPromotionFormDefinition", SiteContext.CurrentSiteID);

            var form = new FormInfo(x);

            foreach (var field in form.GetFields(true, true, true, false, true))
            {
                if (field.PrimaryKey)
                {
                    continue;
                }

                promotionForm.AddCategory(form.GetClosestCategory(field)?.CategoryName);

                promotionForm.AddField(ref dci, ref tm, field);
                //y.Add($"{field.Name}:{field.FieldType.ToStringRepresentation()}");
            }

            return;
        }
    }
}
