﻿using CMS.Ecommerce;
using Newtonsoft.Json;
using System.Collections.Generic;
using WF.Promotions.Classes;
using WF.Promotions.DataClasses;
using WF.SystemModule.DataClasses;

namespace WF.Promotions.Helpers
{
    public static class PromotionHelper
    {
        public static PurchaseInfo GetPurchaseInfo(string json)
        {
            var purchaseInfo = JsonConvert.DeserializeObject<PurchaseInfo>(json);
            return purchaseInfo;
        }

        public static List<PurchaseInfoGroup> GetPurchaseInfoGroups(PurchaseInfo purchaseInfo)
        {
            return purchaseInfo.Groups;
        }

        public static List<PurchaseInfoProduct> GetPurchaseInfoGroupProducts(PurchaseInfoGroup purchaseInfoGroup)
        {
            return purchaseInfoGroup.Products;
        }

        public static List<PurchaseInfoVariant> GetPurchaseInfoGroupProductVariants(PurchaseInfoProduct purchaseInfoGroupProduct)
        {
            return purchaseInfoGroupProduct.Variants;
        }

        public static void BuildRequestForProducts(string json, int entryId)
        {
            var purchaseInfo = PromotionHelper.GetPurchaseInfo(json);

            foreach (var group in purchaseInfo.Groups)
            {
                foreach (var product in group.Products)
                {
                    foreach (var variant in product.Variants)
                    {
                        var vi = PromotionProductVariantInfoProvider.GetExistingPromotionProductVariantInfo(purchaseInfo.PromotionID, entryId, group.GroupID, product.SKUID, variant.SKUID);

                        if (vi == null)
                        {
                            vi = new PromotionProductVariantInfo();
                        }

                        vi.PromotionProductVariantPromotionFormID = purchaseInfo.PromotionID;
                        vi.PromotionProductVariantPromotionFormEntryID = entryId;
                        vi.PromotionProductVariantGroupID = group.GroupID;
                        vi.PromotionProductVariantProductSKUID = product.SKUID;
                        vi.PromotionProductVariantSKUID = variant.SKUID;
                        vi.PromotionProductVariantName = variant.Name;
                        vi.PromotionProductVariantUnitPrice = variant.UnitPrice;
                        vi.PromotionProductVariantTax = variant.Tax;
                        vi.PromotionProductVariantQty = variant.Qty;

                        PromotionProductVariantInfoProvider.SetPromotionProductVariantInfo(vi);
                    }
                }
            }
        }

        public static string GetSKUSalesforceID(int skuId)
        {
            var defaultValue = string.Empty;
            var sku = SKUInfoProvider.GetSKUInfo(skuId);
            if (sku != null)
            {
                return sku.GetStringValue("SalesforceID", defaultValue);
            }

            return defaultValue;
        }
    }
}
