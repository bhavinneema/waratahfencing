﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using CMS.EventLog;
using CMS.Helpers;
using CMS.MacroEngine;

using WF.Domain.Classes;
using WF.SystemModule.DataClasses;
using WF.Distributors.Classes;
using WF.Promotions.DataClasses;

namespace WF.Promotions.Macros
{
    public class WTFCustomMacros : MacroMethodContainer
    {
        /// <summary>
        /// Outputs a list of gallery images (objects with a Src, Href, and Alt) from JSON a string.
        /// </summary>
        [MacroMethod(typeof(List<GalleryImage>), "Returns a List of gallery image objects.", 1)]
        [MacroMethodParam(0, "JSON", typeof(string), "JSON string representing the list of gallery image objects.")]
        public static object GalleryImagesFromJSON(EvaluationContext ctx, params object[] parameters)
        {
            if (parameters.Length != 1)
                throw new NotSupportedException();

            string json = ValidationHelper.GetString(parameters[0], string.Empty).Trim();
            List<GalleryImage> dto;
            try {
                dto = JsonConvert.DeserializeObject<List<GalleryImage>>(json);
            }
            catch (JsonSerializationException ex)
            {
                EventLogProvider.LogException("WTF.SystemModule.Macros.WTFCustomMacros", "GalleryImagesFromJSON", ex);
                return null;
            }

            var macroOutput = new List<GalleryImage>();
            foreach (var image in dto)
            {
                macroOutput.Add(new GalleryImage()
                {
                    Src = image.Src,
                    Href = image.Href,
                    Alt = image.Alt
                });
            }

            return macroOutput;
        }


        /// <summary>
        /// Outputs a list of gallery images (objects with a Src, Href, and Alt) from JSON a string.
        /// </summary>
        [MacroMethod(typeof(PurchaseInfo), "Returns an object with information about the purchase/claim.", 1)]
        [MacroMethodParam(0, "JSON", typeof(string), "JSON string representing the purchase info.")]
        public static object PurchaseInfoFromJSON(EvaluationContext ctx, params object[] parameters)
        {
            if (parameters.Length != 1)
                throw new NotSupportedException();

            string json = ValidationHelper.GetString(parameters[0], string.Empty).Trim();
            PurchaseInfo purchaseInfo;
            try
            {
                purchaseInfo = JsonConvert.DeserializeObject<PurchaseInfo>(json);
                purchaseInfo.Deserialized = true;
            }
            catch (Exception ex)
            {
                purchaseInfo = new PurchaseInfo
                {
                    DeserializationError = ex.Message,
                    Deserialized = false
                };
            }

            return purchaseInfo;
        }

        [MacroMethod(typeof(string), "Returns Salesforce ID for a named distributor",1)]
        [MacroMethodParam(0, "DistributorName", typeof(string), "Distributor Name")]
        public static object GetDistributorSalesforceID(EvaluationContext ctx, params object[] parameters)
        {
            if (parameters.Length != 1)
                throw new NotSupportedException();
            var distributor = DistributorInfoProvider.GetDistributors().WhereEquals("DistributorName", ValidationHelper.GetString(parameters[0], string.Empty)).FirstObject;
            if(distributor==null)
            {
                return "";
            }
            return distributor.DistributorSalesforceID;
        }
       

        [MacroMethod(typeof(List<PurchaseInfoProduct>),"Gets all products for a PurchaseInfo",1)]
        [MacroMethodParam(0,"PurchaseInfo",typeof(PurchaseInfo),"Purchase Info")]
        public static object GetAllProducts(EvaluationContext ctx, params object[] parameters)
        {
            if((parameters.Length!=1)||(!(parameters[0] is PurchaseInfo)))
            {
                throw new NotSupportedException();
            }
            var allProducts = new List<PurchaseInfoProduct>();
            var purchaseInfo = parameters[0] as PurchaseInfo;
            foreach(var group in purchaseInfo.Groups)
            {
                foreach(var product in group.Products)
                {
                    allProducts.Add(product);
                }
            }
            return allProducts;
        }

        [MacroMethod(typeof(List<PurchaseInfoVariant>), "Gets all products for a PurchaseInfo", 1)]
        [MacroMethodParam(0, "PurchaseInfo", typeof(PurchaseInfo), "Purchase Info")]
        public static object GetAllVariants(EvaluationContext ctx, params object[] parameters)
        {
            if ((parameters.Length != 1) || (!(parameters[0] is PurchaseInfo)))
            {
                throw new NotSupportedException();
            }
            var allProducts = new List<PurchaseInfoVariant>();
            var purchaseInfo = parameters[0] as PurchaseInfo;
            foreach (var group in purchaseInfo.Groups)
            {
                foreach (var product in group.Products)
                {
                    allProducts.AddRange(product.Variants);
                }
            }
            return allProducts;
        }
    }
}