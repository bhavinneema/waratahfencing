﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS;
using CMS.DataEngine;
using WF.SystemModule;
using WF.SystemModule.UniGridTransformations;

[assembly: RegisterModule(typeof(PromotionsModule))]
namespace WF.SystemModule
{
    public class PromotionsModule : Module
    {
        public PromotionsModule() : base("WF.Promotions"){}

        protected override void OnInit()
        {
            base.OnInit();

            CustomUniGridTransformations.RegisterCustomUniGridTransformations();
        }
    }
}
