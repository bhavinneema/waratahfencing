﻿namespace WF.SystemModule.UniGridTransformations
{
    public static class CustomUniGridTransformations
    {
        public static void RegisterCustomUniGridTransformations()
        {
            // Registers the #collectandsendorderstatus UniGrid transformation
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#skunodealiaspath", PromotionProductSKUDialogTransformations.GetSKUNodeAliasPath);
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#skuisvariant", PromotionProductSKUDialogTransformations.GetSKUIsVariant);
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#skusalesforceid", PromotionProductSKUDialogTransformations.GetSKUSalesforceID);
        }
    }
}
