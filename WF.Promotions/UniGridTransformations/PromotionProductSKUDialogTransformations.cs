﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DocumentEngine;
using CMS.Ecommerce;

namespace WF.SystemModule.UniGridTransformations
{
    public static class PromotionProductSKUDialogTransformations
    {
        public static object GetSKUNodeAliasPath(object arg)
        {
            var skuid = (int)arg;
            var sku = SKUInfoProvider.GetSKUInfo(skuid);

            TreeNode skuDocument = null;

            if (sku != null)
            {
                skuDocument = DocumentHelper.GetDocuments("CMS.Product")
                                      .WhereEquals("NodeSKUID", sku.SKUID)
                                      .FirstObject ?? DocumentHelper.GetDocuments("CMS.Product")
                                      .WhereEquals("NodeSKUID", sku.SKUParentSKUID)
                                      .FirstObject;
            }

            return skuDocument?.NodeAliasPath ?? string.Empty;
        }

        public static object GetSKUIsVariant(object arg)
        {
            var skuid = (int)arg;

            var sku = SKUInfoProvider.GetSKUInfo(skuid);

            return sku?.SKUParentSKUID > 0;
        }

        internal static object GetSKUSalesforceID(object arg)
        {
            var skuid = (int)arg;

            var sku = SKUInfoProvider.GetSKUInfo(skuid);

            return sku?.GetStringValue("SalesforceID", string.Empty);
        }
    }
}
