﻿using System.Web.Http;
using WF.Promotions.Cashback;
using WF.Promotions.DataClasses;
using WF.SystemModule.Cashback;
using WF.SystemModule.DataClasses;

namespace WF.SiteAPI.Controllers
{
    [RoutePrefix("api/promotion")]
    public class PromotionController : ApiController
    {
        [Route("calculate-cashback")]
        [HttpPost]
        public IHttpActionResult CalculateCashback(PurchaseInfo request)
        {
            var response = new CashbackDetails(request);
            return Ok(response);
        }
    }
}