﻿using System.Web.Http;

using CMS;

using WF.SiteAPI;
using WF.SiteAPI.ApiConfig;

using Module = CMS.DataEngine.Module;

[assembly: RegisterModule(typeof(SiteApiModule))]
namespace WF.SiteAPI
{
    public class SiteApiModule : Module
    {
        public SiteApiModule() : base("WF.SiteAPI") { }

        protected override void OnInit()
        {
            /* Prevent loading of the module if the application
             * is loaded from another assembly or program, instead of
             * being loaded as part of the web application.
             * This ensures no 'pre-init' exceptions are thrown
             * by programs such as ContinuousIntegration.exe
             */

            var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
            if (entryAssembly == null)
            {
                SiteApiConfig.Register(GlobalConfiguration.Configuration);
                GlobalConfiguration.Configuration.EnsureInitialized();
            }
        }
    }
}