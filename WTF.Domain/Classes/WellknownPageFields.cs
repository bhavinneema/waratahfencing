﻿namespace WF.Domain.Classes
{
    public static class WellknownPageFields
    {
       
        public const string DocumentID = "DocumentID";
        public const string CategoryName = "CategoryName";
        public const string CategoryID = "CategoryID";
        public const string Author = "Author";
        public const string JobTitle = "JobTitle";
        public const string Phone = "Phone";
        public const string Title = "Title";
        public const string FeaturedImage = "FeaturedImage";
        public const string DocumentLastPublished = "DocumentLastPublished";
        public const string DocumentPublishFrom = " DocumentPublishFrom";        
        public const string DocumentModifiedWhen = "DocumentModifiedWhen";
        public const string NodeGuid = "NodeGuid";
        public const string Code = "Code";
        public const string ItemOrder = "ItemOrder";
        public const string NodeLevel = "NodeLevel";
        public const string NodeOrder = "NodeOrder";
        public const string NodeName = "NodeName";
      

    }
} 
