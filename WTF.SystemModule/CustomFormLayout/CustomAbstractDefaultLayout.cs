﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Base.Web.UI;
using CMS.FormEngine;
using CMS.FormEngine.Web.UI;

namespace WTF.SystemModule.CustomFormLayout
{
    public class CustomLayout : DivDefaultLayout
    {
        public CustomLayout(BasicForm basicForm, Panel categoryListPanel) : base(basicForm, categoryListPanel)
        {
        }

        protected override void CreateField(FieldCreationArgs args)
        {
            if (BasicForm.IsDesignMode)
            {
                base.CreateField(args);
                return;
            }

            if (BasicForm.FieldsToHide.Contains(args.FormFieldInfo.Name))
            {
                args.FormFieldInfo.Visible = false;
            }

            var labelCssClass = args.FormFieldInfo.GetPropertyValue(FormFieldPropertyEnum.CaptionCssClass);

            // Add form group
            var pnl = new Panel();
            AddControlToPanel(pnl, args.FormFieldInfo);

            // Add label
            var pnlLabel = new Panel { CssClass = "contact-form-label" };
            var fieldLabel = CreateFieldLabel(args.FormFieldInfo);
            fieldLabel.CssClass = labelCssClass;
            pnlLabel.Controls.Add(fieldLabel);

            var pnlControl = new Panel { CssClass = "contact-form-control" }; // Add form control
            pnlControl.Controls.Add(CreateEditingFormControl(args.FormFieldInfo));

            var pnlValidation = new Panel { CssClass = "contact-form-validation" };
            pnlValidation.Controls.Add(CreateErrorLabel(args.FormFieldInfo));

            var pnlClearfix = new Panel { CssClass = "clearfix" };

            pnl.Controls.Add(pnlLabel);
            pnl.Controls.Add(pnlControl);
            pnl.Controls.Add(pnlValidation);
            pnl.Controls.Add(pnlClearfix);
        }

        protected override LocalizedLabel CreateFieldLabel(FormFieldInfo ffi, bool addTooltip = true)
        {
            var lbl = base.CreateFieldLabel(ffi, addTooltip);
            if (!ffi.AllowEmpty)
                lbl.Text += "<span class=\"text-warning\">*</span>";
            return lbl;
        }

        protected override void Load()
        {
            base.Load();

            // Get the submit button control
            var submitButton = (Control)GetSubmitButton();

            // Create a new panel to contain the submit button
            var submitButtonPanel = new Panel { CssClass = "contact-form-submit" };

            // Add wrapper div and submit button to panel
            submitButtonPanel.Controls.Add(submitButton);

            // Add the panel to the form
            FormPanel.Controls.Add(submitButtonPanel);
        }
    }
}
