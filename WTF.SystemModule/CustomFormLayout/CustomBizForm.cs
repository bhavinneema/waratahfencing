﻿using CMS.OnlineForms.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTF.SystemModule.CustomFormLayout
{
    public class CustomBizForm : BizForm
    {
        protected override void LoadDefaultLayout()
        {
            // Create a new instance of the custom layout
            var layout = new CustomLayout(this, categoryListPanel);

            // Load the layout, and assign it as the layout of the form
            layout.LoadLayout();
            Layout = layout;
        }
    }
}
