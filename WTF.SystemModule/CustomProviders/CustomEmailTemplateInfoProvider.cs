﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CMS;
using CMS.EmailEngine;
using WTF.SystemModule.CustomProviders;
using EmailTemplateInfo = CMS.EmailEngine.EmailTemplateInfo;
using CMS.Localization;

[assembly: RegisterCustomProvider(typeof(CustomEmailTemplateProvider))]
namespace WTF.SystemModule.CustomProviders
{
    public class CustomEmailTemplateProvider : EmailTemplateProvider
    {
        protected override EmailTemplateInfo GetEmailTemplateInternal(string templateName, string siteName)
        {
            var regexPattern = @"\.\D\D-\D\D$";

            if (Regex.IsMatch(templateName, regexPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline))
            {
                var eti = base.GetEmailTemplateInternal(templateName, siteName);
                if (eti == null)
                {
                    var templateNameWithoutCulture = Regex.Replace(templateName, regexPattern, string.Empty);
                    return base.GetEmailTemplateInternal(templateNameWithoutCulture, siteName);
                }
            }

            return base.GetEmailTemplateInternal(templateName, siteName);
        }
    }
}
