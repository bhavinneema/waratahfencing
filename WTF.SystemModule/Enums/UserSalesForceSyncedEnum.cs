﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Helpers;

namespace WTF.SystemModule.Enums
{
    public enum UserSalesForceSyncedEnum
    {
        [EnumStringRepresentation("None")]
        None,
        [EnumStringRepresentation("LeadSynced")]
        LeadSynced,
        [EnumStringRepresentation("ContactSynced")]
        ContactSynced,
        [EnumStringRepresentation("EitherSynced")]
        EitherSynced,
    }
}
