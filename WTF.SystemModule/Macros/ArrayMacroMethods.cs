﻿using CMS;
using CMS.MacroEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTF.SystemModule.Macros;

[assembly: RegisterExtension(typeof(ArrayMacroMethods), typeof(Array))]
namespace WTF.SystemModule.Macros
{
    public class ArrayMacroMethods : MacroMethodContainer
    {
        [MacroMethod(typeof(int), "Returns the number of items in an array.", 1)]
        [MacroMethodParam(0, "countedarray", typeof(Array), "Array to count items in.")]
        public static object Count(EvaluationContext context, params object[] parameters)
        {
            if (parameters.Length != 1)
                throw new NotSupportedException();

            var array = parameters[0] as Array;
            if (array == null)
                throw new NotSupportedException();

            return array.Length;
        }
    }
}
