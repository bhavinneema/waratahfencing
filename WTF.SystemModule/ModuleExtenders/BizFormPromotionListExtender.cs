﻿using CMS;
using CMS.Base;
using CMS.Base.Web.UI;
using CMS.Base.Web.UI.ActionsConfig;
using CMS.FormEngine.Web.UI;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Modules;
using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CMS.OnlineForms;
using WF.Domain.Classes;
using WF.Promotions.Classes;

[assembly: RegisterCustomClass("BizFormPromotionListExtender", typeof(WTF.SystemModule.ModuleExtenders.BizFormPromotionListExtender))]
namespace WTF.SystemModule.ModuleExtenders
{

    public class BizFormPromotionListExtender : ControlExtender<UniGrid>
    {
        /// <summary>
        /// Adds custom code that occurs during the initialization of the extended control.
        /// </summary>
        public override void OnInit()
        {
            ScriptHelper.RegisterDialogScript(Control.Page);
            ScriptHelper.RegisterCloseDialogScript(Control.Page);
            ScriptHelper.RegisterWOpenerScript(Control.Page);

            // Assigns handlers to the UniGrid control's OnAction and OnExternalDatabound events
            Control.OnExternalDataBound += Control_OnExternalDataBound;
            Control.OnAction += Control_OnAction;
        }

        private object Control_OnExternalDataBound(object sender, string sourceName, object parameter)
        {
            if (sourceName == "clonepromotion")
            {
                var cloneButton = (CMSButton)sender;
                cloneButton.Enabled = false;
                var module = UIElementInfoProvider.GetUIElementInfo("WF.Promotions", "ClonePromotion");

                if (module != null)
                {
                    var gvr = (GridViewRow) parameter;
                    var drv = (DataRowView)gvr.DataItem;
                    var bizFormId = ValidationHelper.GetInteger(drv.Row["PromotionBizFormID"], 0);
                    if (bizFormId > 0)
                    {
                        var url = UIContextHelper.GetElementUrl(module, true, bizFormId);
                        var script = ScriptHelper.GetModalDialogScript(url, "Clone Promotion", null, null, true);
                        cloneButton.OnClientClick = script;
                        cloneButton.Enabled = true;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Handles the UniGrid control's OnAction event.
        /// </summary>
        /// <param name="actionName">The name of the UniGrid action that was used.</param>
        /// <param name="actionArgument">The value of the data source column in the UniGrid row for which the action was used.</param>
        private void Control_OnAction(string actionName, object actionArgument)
        {
            if (actionName == "editpromotionentryfields")
            {
                // Show module page
                var module = UIElementInfoProvider.GetUIElementInfo("WF.Promotions", "EditPromotionEntryForm");

                if (module != null)
                {
                    var bizFormId = ValidationHelper.GetString(actionArgument, "0");
                    var url = UIContextHelper.GetElementUrl(module);
                    url = URLHelper.AddParameterToUrl(url, "formid", bizFormId);
                    url = URLHelper.AddParameterToUrl(url, "objectid", bizFormId);
                    string script = "window.location='" + UrlResolver.ResolveUrl(url) + "';";
                    ScriptHelper.RegisterClientScriptBlock(Control, Control.GetType(), "RedirectScript", script, true);
                }
            }
            if (actionName == "viewpromotionentries")
            {
                // Show module page
                var module = UIElementInfoProvider.GetUIElementInfo("WF.Promotions", "ListPromotionEntries");

                if (module != null)
                {
                    var bizFormId = ValidationHelper.GetString(actionArgument, "0");
                    var url = UIContextHelper.GetElementUrl(module);
                    url = URLHelper.AddParameterToUrl(url, "formid", bizFormId);
                    url = URLHelper.AddParameterToUrl(url, "objectid", bizFormId);
                    string script = "window.location='" + UrlResolver.ResolveUrl(url) + "';";
                    ScriptHelper.RegisterClientScriptBlock(Control, Control.GetType(), "RedirectScript", script, true);
                }
            }
            if (actionName == "viewpromotionlink")
            {
                var bizFormId = ValidationHelper.GetInteger(actionArgument, 0);
                var promotion = PromotionInfoProvider.GetPromotions().WhereEquals("PromotionBizFormID", bizFormId).FirstObject;
                var script = "window.prompt('Promotion URL:', '/Promotions/{0}/Form')";
                script = promotion != null ? String.Format(script, promotion.PromotionLandingPageURLAlias) : String.Format(script, "error occurred");
                ScriptHelper.RegisterClientScriptBlock(Control, Control.GetType(), "FormLink", script, true);
            }
        }
    }
}
