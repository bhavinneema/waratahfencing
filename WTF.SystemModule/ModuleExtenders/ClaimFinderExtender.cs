﻿using CMS;
using CMS.Base;
using CMS.Base.Web.UI;
using CMS.Base.Web.UI.ActionsConfig;
using CMS.FormEngine.Web.UI;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Modules;
using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CMS.OnlineForms;
using WF.Domain.Classes;
using WF;
using WF.Promotions.Classes;

[assembly: RegisterCustomClass("ClaimFinderExtender", typeof(WTF.SystemModule.ModuleExtenders.ClaimFinderExtender))]
namespace WTF.SystemModule.ModuleExtenders
{

    public class ClaimFinderExtender : ControlExtender<UniGrid>
    {
        /// <summary>
        /// Adds custom code that occurs during the initialization of the extended control.
        /// </summary>
        public override void OnInit()
        {
            // Assigns handlers to the UniGrid control's OnAction and OnExternalDatabound events
            Control.OnAction += Control_OnAction;
        }

        /// <summary>
        /// Handles the UniGrid control's OnAction event.
        /// </summary>
        /// <param name="actionName">The name of the UniGrid action that was used.</param>
        /// <param name="actionArgument">The value of the data source column in the UniGrid row for which the action was used.</param>
        private void Control_OnAction(string actionName, object actionArgument)
        {
            if (actionName == "viewentry")
            {
                // Show module page
                var url = "~/CMSModules/BizForms/Tools/BizForm_Edit_EditRecord.aspx";

                var promotionEntryId = ValidationHelper.GetInteger(actionArgument, 0);
                var promotionEntry = PromotionEntryInfoProvider.GetPromotionEntryInfo(promotionEntryId);
                url = URLHelper.AddParameterToUrl(url, "formID", promotionEntry.PromotionEntryFormID.ToString());
                url = URLHelper.AddParameterToUrl(url, "formRecordID", promotionEntry.PromotionEntryFormItemID.ToString());
                string script = "window.location='" + UrlResolver.ResolveUrl(url) + "';";
                ScriptHelper.RegisterClientScriptBlock(Control, Control.GetType(), "RedirectScript", script, true);
            }
        }
    }
}
