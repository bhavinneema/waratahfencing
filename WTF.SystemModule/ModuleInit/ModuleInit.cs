﻿using CMS.DataEngine;
using CMS.Membership;
using CMS.OnlineForms;

using WF.Domain.Classes;
using CMS.Helpers;
using WF.Promotions.Classes;
using WF.SystemModule.Events;
using WTF.SystemModule.UniGridTransformations;

namespace WTF.SystemModule.ModuleInit
{
    public class ModuleInit : Module
    {
        public ModuleInit() : base("WTF.SystemModule", false)
        {
        }

        // Contains initialization code that is executed when the application starts
        protected override void OnInit()
        {
            base.OnInit();

            // Assigns a handler to the Insert.After event for the ForumGroupInfo object type
            // This event occurs after the creation of every new forum group
            PromotionInfo.TYPEINFO.Events.Insert.After += PromotionEvents.Promotion_InsertAfter;
            BizFormInfo.TYPEINFO.Events.Insert.After += PromotionEvents.BizForm_InsertAfter;
            BizFormItemEvents.Insert.Before += PromotionEvents.BizFormItem_InsertBefore;
            BizFormItemEvents.Update.After += PromotionEvents.BizFormItem_UpdateAfter;

            CustomUniGridTransformations.RegisterCustomUniGridTransformations();
        }
    }
}
