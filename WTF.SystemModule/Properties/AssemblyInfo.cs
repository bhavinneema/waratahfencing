﻿using System.Reflection;
using System.Runtime.InteropServices;

using CMS;
using CMS.MacroEngine;

[assembly: RegisterModule(typeof(WTF.SystemModule.ModuleInit.ModuleInit))]
[assembly: AssemblyDiscoverable]
[assembly: AssemblyTitle("WTF.SystemModule")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WTF.SystemModule")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("7de89d70-965f-4d27-bd52-4017d93064ea")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]