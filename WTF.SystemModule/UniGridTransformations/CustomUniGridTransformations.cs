﻿
namespace WTF.SystemModule.UniGridTransformations
{
    public static class CustomUniGridTransformations
    {
        public static void RegisterCustomUniGridTransformations()
        {
            // Registers the #collectandsendorderstatus UniGrid transformation
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#usersalesforceleadid", UserListUniGridTransformations.GetUserSalesForceLeadID);
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#usersalesforcecontactid", UserListUniGridTransformations.GetUserSalesForceContactID);
            CMS.UIControls.UniGridTransformations.Global.RegisterTransformation("#usersalesforceresync", UserListUniGridTransformations.GetUserSalesForceResyncButton);
        }
    }
}
