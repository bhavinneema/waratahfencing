﻿
using System;
using System.Linq;
using CMS.Base.Web.UI;
using CMS.DataEngine;
using CMS.FormEngine.Web.UI;
using CMS.Helpers;
using CMS.Membership;
using WDM.SyncedObjects.Classes;
using WDM.SyncedObjects.Helpers;
using CMSGridActionButton = CMS.Base.Web.UI.CMSGridActionButton;

namespace WTF.SystemModule.UniGridTransformations
{
    public static class UserListUniGridTransformations
    {
        internal static object GetUserSalesForceLeadID(object arg)
        {
            var defaultValue = string.Empty;
            var userId = ValidationHelper.GetInteger(arg, 0);

            if (userId == 0)
            {
                return defaultValue;
            }

            var user = UserInfoProvider.GetUserInfo(userId);

            if (user == null)
            {
                return defaultValue;
            }

            var salesForceLeadId = user.GetStringValue("UserSalesForceLeadID", string.Empty);

            if (salesForceLeadId.Length == 0)
            {
                return defaultValue;
            }

            return salesForceLeadId;
        }

        internal static object GetUserSalesForceContactID(object arg)
        {
            var defaultValue = string.Empty;
            var userId = ValidationHelper.GetInteger(arg, 0);

            if (userId == 0)
            {
                return defaultValue;
            }

            var user = UserInfoProvider.GetUserInfo(userId);

            if (user == null)
            {
                return defaultValue;
            }

            var salesForceContactId = user.GetStringValue("UserSalesForceContactID", string.Empty);

            if (salesForceContactId.Length == 0)
            {
                return defaultValue;
            }

            return salesForceContactId;
        }

        internal static object GetUserSalesForceResyncButton(object arg)
        {
            var defaultValue = string.Empty;
            var userId = ValidationHelper.GetInteger(arg, 0);

            if (userId == 0)
            {
                return defaultValue;
            }

            var user = UserInfoProvider.GetUserInfo(userId);

            if (user == null)
            {
                return defaultValue;
            }

            var button = new CMSGridActionButton {IconCssClass = "icon-rotate-double-right", CommandArgument = userId.ToString()};
            
            var className = UIContext.Current.Data.GetKeyValue("objecttype", true, true).ToString();
            var userDci = DataClassInfoProvider.GetDataClassInfo(className);
            var syncedObjectIds = SyncedObjectInfoProvider.GetSyncedObjects()
                .Column("SyncedObjectID")
                .WhereEquals("SyncedObjectKenticoClass", userDci.ClassID)
                .GetListResult<int>();
            button.Enabled = !SyncLogInfoProvider.GetSyncLogs()
                .WhereIn("SyncLogSyncedObjectID", syncedObjectIds)
                .WhereEquals("SyncLogKenticoItemID", userId)
                .WhereEqualsOrNull("SyncLogStatus", (int) SyncLogStatus.New)
                .HasResults();
            button.Click += Button_Click;

            return button;
        }

        private static void Button_Click(object sender, EventArgs e)
        {
            var button = ((CMSGridActionButton) sender);
            var userId = ValidationHelper.GetInteger(button.CommandArgument, 0);
            var user = UserInfoProvider.GetUserInfo(userId);
            if (user == null)
            { 
                return;
            }
            var userDci = DataClassInfoProvider.GetDataClassInfo("cms.user");
            var syncedObjects = SyncedObjectInfoProvider.GetSyncedObjects()
                .WhereEquals("SyncedObjectKenticoClass", userDci.ClassID)
                .ToList();
            var existing = SyncLogInfoProvider.GetSyncLogs()
                .WhereIn("SyncLogSyncedObjectID", syncedObjects.Select(x => x.SyncedObjectID).ToList())
                .WhereEquals("SyncLogKenticoItemID", userId)
                .WhereEqualsOrNull("SyncLogStatus", (int)SyncLogStatus.New)
                .HasResults();
            if (!existing)
            {
                foreach (var syncedObject in syncedObjects)
                {
                    SyncHelper.QueueItem(syncedObject, user);
                }
            }
            button.Enabled = false;
        }
    }
}
